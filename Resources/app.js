///////// Global App Settings /////////

var TiGlobals = {
		dsTouch: true,
	platformHeight : 0,
	platformWidth : 0,
	osname : Ti.Platform.osname,
	timer:600000,
	// Custom
	
   /* appURLTOML : 'https://www.remit2india.com/json/services',
	//appURLBCM : 'https://172.16.0.28/mobileApp/config.php',
	//appURLBCM : 'http://172.16.0.28/mobileApp/config.php',
	appURLBCM : 'https://m.remit2india.com/mobileApp/config.php',
	//appURLBCM : 'http://www.remit2india.com/mobileApp/config.php',
	//appUploadBCM : 'https://www.remit2india.com/',
    appUploadBCM : 'https://m.remit2india.com/',
	staticPagesURL : 'https://m.remit2india.com/mobileApp/',
	//staticPagesURL : 'https://www.remit2india.com/mobileApp/',
	pushURL : 'https://m.remit2india.com/mobileApp/push.php', */
	
	
	//mstage 
   //appURLTOML : 'http://testnew.remit2india.com/json/services',
	appURLTOML : 'https://test.timesofmoney.com/json/services',
   // appURLTOML :'http://115.111.79.204/json/services',   //changing on 27
	appURLBCM : 'https://mstage.remit2india.com/mobileApp/config.php',
	appUploadBCM :'https://mstage.remit2india.com/',
	staticPagesURL :'https://mstage.remit2india.com/mobileApp/',
	//staticPagesURL :'https://m.remit2india.com/mobileApp/',
	pushURL : 'https://mstage.remit2india.com/mobileApp/push.php',
	//pushURL :'https://mstage.remit2india.com/mobileApp/push_notifications.php',  
		 
	googleAPI : '',
	menuSel : '0',
	menuOpen : '0',
	indicatorCheck : 0, // Check for android back
	sessionCheck : 0, // Check for session dialog

	// App specific

	partnerId : 'TOML',
	pushChannelId : 1,
	channelId : 'MobileApp',
	//channelId : 'MobileSite',
	ipAddress : '127.0.0.1',
	//ipAddress : null,
	bcmConfig : 'CONFIG',
	bcmSource : 'MobileApp',

	// App variables
	minExchangeValue : 0,
	maxExchangeValue : 0,
	
	sendamt : 0, //pallavi
	myEvent : 0,
	isPaused : false,
	mytime : 0,
	
	addressUpdate : false,
	OwnIdForAddrUpdate : null,
	updateDone : 0,
	
	//For updateuserdetails
	fname:null,
	gender:null,
	knowAboutUs:null,
	nationality:null,
	
	//CMN 31 channel diffrentiation
	deviceversion:Ti.Platform.getVersion(),
	devicemodel:null,
	channel:null,
	pageName:null,
	responseCode:null, //cmn55
	usKycDtlFlag:false,
        issuFlag:false,
         drivIssueFlag:false,
        limitBreachSI:false,
	usLicsState:[],//for cmn70
	expiDLDate:null,
        paymode: null,
	ipCheck:false,
	exp1:false,
	recDOB:null,
	dobFlag:false,
	dobCheckF:false,
	isNationality:false,
	
	issuFlagAus:false, //CMN73
	issuFlagUK:false,
	drivIssueFlagUK:false,
	expiDLDateUK:null,
	issuFlagOthers:false,
	issuFlagUAE:false
	
	};

	//CMN70  ////updated by sanjivani on 07Sept17 for CMN70b
	TiGlobals.usLicsState = ['Alabama','Alaska', 'Arizona','California','Colorado','Connecticut','Delaware','Florida','Georgia','Idaho','Illinois','Iowa','Kentucky','Louisiana','Maine','Maryland','Michigan','Missouri','Minnesota','Montana','New Hampshire','New Jersey','New Mexico','North Carolina','North Dakota','Oregon','Oklahoma','Pennsylvania','Rhode Island','South Carolina','Texas','Tennessee','Utah','Vermont','Virginia','Washington','Wisconsin','Wyoming','Arkansas' ,'Hawaii' , 'Indiana' , 'Kansas', 'Mississippi', 'Nebraska' , 'South Dakota' , 'West Virginia' , 'Ohio'];
     
/*alert(TiGlobals.osname);
alert(TiGlobals.devicemodel);
alert(TiGlobals.deviceversion);*/
//reference link -http://stackoverflow.com/questions/12505414/whats-the-device-code-platform-string-for-iphone-5-5c-5s-ipod-touch-5
///https://support.hockeyapp.net/kb/client-integration-ios-mac-os-x-tvos/ios-device-types

if (Ti.Platform.model=="iPhone1,1"){    TiGlobals.devicemodel= "iPhone 1G";}
else if (Ti.Platform.model=="iPhone1,2"){    TiGlobals.devicemodel= "iPhone 3G";}
else if (Ti.Platform.model=="iPhone2,1"){    TiGlobals.devicemodel= "iPhone 3GS";}
else if (Ti.Platform.model=="iPhone3,1"){    TiGlobals.devicemodel= "iPhone 4";}
else if (Ti.Platform.model=="iPhone3,3"){    TiGlobals.devicemodel= "Verizon iPhone 4";}
else if (Ti.Platform.model=="iPhone4,1"){    TiGlobals.devicemodel= "iPhone 4S";}
else if (Ti.Platform.model=="iPhone5,1"){    TiGlobals.devicemodel= "iPhone 5";}
else if (Ti.Platform.model=="iPhone5,2"){    TiGlobals.devicemodel= "iPhone 5 ";}
else if (Ti.Platform.model=="iPhone5,3"){    TiGlobals.devicemodel= "iPhone 5c ";}
else if (Ti.Platform.model=="iPhone5,4"){    TiGlobals.devicemodel= "iPhone 5c";}
else if (Ti.Platform.model=="iPhone6,1"){    TiGlobals.devicemodel= "iPhone 5s ";}
else if (Ti.Platform.model=="iPhone6,2"){    TiGlobals.devicemodel= "iPhone 5s";}
else if (Ti.Platform.model=="iPhone7,1"){    TiGlobals.devicemodel= "iPhone 6 Plus";}
else if (Ti.Platform.model=="iPhone7,2"){    TiGlobals.devicemodel= "iPhone 6";}
else if (Ti.Platform.model=="iPhone8,1"){    TiGlobals.devicemodel= "iPhone 6s";}
else if (Ti.Platform.model=="iPhone8,2"){    TiGlobals.devicemodel= "iPhone 6s Plus";}
else if (Ti.Platform.model=="iPhone8,4"){    TiGlobals.devicemodel= "iPhone SE";}
else if (Ti.Platform.model=="iPhone9,1"){    TiGlobals.devicemodel= "iPhone 7";}
else if (Ti.Platform.model=="iPhone9,2"){    TiGlobals.devicemodel= "iPhone 7 Plus";}
else if (Ti.Platform.model=="iPhone9,3"){    TiGlobals.devicemodel= "iPhone 7";}
else if (Ti.Platform.model=="iPhone9,4"){    TiGlobals.devicemodel= "iPhone 7 Plus";}
else if (Ti.Platform.model=="iPhone10,1"){    TiGlobals.devicemodel= "iPhone 8";}  // updated on 8 Dec-17 from the above mentioned reference site
else if (Ti.Platform.model=="iPhone10,2"){    TiGlobals.devicemodel= "iPhone 8 Plus";}
else if (Ti.Platform.model=="iPhone10,3"){    TiGlobals.devicemodel= "iPhone X";}
else if (Ti.Platform.model=="iPhone10,4"){    TiGlobals.devicemodel= "iPhone 8";}
else if (Ti.Platform.model=="iPhone10,5"){    TiGlobals.devicemodel= "iPhone 8 Plus";}
else if (Ti.Platform.model=="iPhone10,6"){    TiGlobals.devicemodel= "iPhone X";}

else if (Ti.Platform.model=="iPod1,1"){    TiGlobals.devicemodel= "iPod Touch 1G";}
else if (Ti.Platform.model=="iPod2,1"){    TiGlobals.devicemodel= "iPod Touch 2G";}
else if (Ti.Platform.model=="iPod3,1"){    TiGlobals.devicemodel= "iPod Touch 3G";}
else if (Ti.Platform.model=="iPod4,1"){    TiGlobals.devicemodel= "iPod Touch 4G";}
else if (Ti.Platform.model=="iPod5,1"){    TiGlobals.devicemodel= "iPod Touch 5G";}
else if (Ti.Platform.model=="iPod7,1"){    TiGlobals.devicemodel= "iPod Touch 6G";}

else if (Ti.Platform.model=="iPad1,1"){    TiGlobals.devicemodel= "iPad";}
else if (Ti.Platform.model=="iPad1,2"){    TiGlobals.devicemodel= "iPad 3G";}
else if (Ti.Platform.model=="iPad2,1"){    TiGlobals.devicemodel= "iPad 2";}
else if (Ti.Platform.model=="iPad2,2"){    TiGlobals.devicemodel= "iPad 2";}
else if (Ti.Platform.model=="iPad2,3"){    TiGlobals.devicemodel= "iPad 2 (CDMA)";}
else if (Ti.Platform.model=="iPad2,4"){    TiGlobals.devicemodel= "iPad 2";}
else if (Ti.Platform.model=="iPad2,5"){    TiGlobals.devicemodel= "iPad Mini";}
else if (Ti.Platform.model=="iPad2,6"){    TiGlobals.devicemodel= "iPad Mini";}
else if (Ti.Platform.model=="iPad2,7"){    TiGlobals.devicemodel= "iPad Mini";}
else if (Ti.Platform.model=="iPad3,1"){    TiGlobals.devicemodel= "iPad 3";}
else if (Ti.Platform.model=="iPad3,2"){    TiGlobals.devicemodel= "iPad 3";}
else if (Ti.Platform.model=="iPad3,3"){    TiGlobals.devicemodel= "iPad 3";}
else if (Ti.Platform.model=="iPad3,4"){    TiGlobals.devicemodel= "iPad 4";}
else if (Ti.Platform.model=="iPad3,5"){    TiGlobals.devicemodel= "iPad 4";}
else if (Ti.Platform.model=="iPad3,6"){    TiGlobals.devicemodel= "iPad 4";}
else if (Ti.Platform.model=="iPad4,1"){    TiGlobals.devicemodel= "iPad Air";}
else if (Ti.Platform.model=="iPad4,2"){    TiGlobals.devicemodel= "iPad Air";}
else if (Ti.Platform.model=="iPad4,4"){    TiGlobals.devicemodel= "iPad Mini 2";}
else if (Ti.Platform.model=="iPad4,5"){    TiGlobals.devicemodel= "iPad Mini 2";}
else if (Ti.Platform.model=="iPad4,6"){    TiGlobals.devicemodel= "iPad Mini 2";}
else if (Ti.Platform.model=="iPad4,7"){    TiGlobals.devicemodel= "iPad Mini 3";}
else if (Ti.Platform.model=="iPad4,8"){    TiGlobals.devicemodel= "iPad Mini 3";}
else if (Ti.Platform.model=="iPad4,9"){    TiGlobals.devicemodel= "iPad Mini 3";} //ipad5,1 - 5,2 -
else if (Ti.Platform.model=="iPad5,1"){    TiGlobals.devicemodel= "iPad mini 4";}
else if (Ti.Platform.model=="iPad5,2"){    TiGlobals.devicemodel= "iPad mini 4";}
else if (Ti.Platform.model=="iPad5,3"){    TiGlobals.devicemodel= "iPad Air 2";}
else if (Ti.Platform.model=="iPad5,4"){    TiGlobals.devicemodel= "iPad Air 2";}
else if (Ti.Platform.model=="iPad6,3"){    TiGlobals.devicemodel= "iPad Pro";}
else if (Ti.Platform.model=="iPad6,4"){    TiGlobals.devicemodel= "iPad Pro";}
else if (Ti.Platform.model=="iPad6,7"){    TiGlobals.devicemodel= "iPad Pro";}
else if (Ti.Platform.model=="iPad6,8"){    TiGlobals.devicemodel= "iPad Pro";}
else if (Ti.Platform.model=="iPad6,11"){    TiGlobals.devicemodel= "iPad";}
else if (Ti.Platform.model=="iPad6,12"){    TiGlobals.devicemodel= "iPad";}
else if (Ti.Platform.model=="iPad7,3"){    TiGlobals.devicemodel= "iPad Pro";}
else if (Ti.Platform.model=="iPad7,4"){    TiGlobals.devicemodel= "iPad Pro";}

else if (Ti.Platform.model=="i386"){    TiGlobals.devicemodel= "Simulator";}
else if (Ti.Platform.model=="x86_64"){    TiGlobals.devicemodel= "Simulator";}

else{
	TiGlobals.devicemodel=Ti.Platform.getModel();
}

  if((Ti.Platform.osname === 'iphone')||(Ti.Platform.osname === 'ipad')){
  TiGlobals.osname = 'iOS';	
  }

 TiGlobals.channel = TiGlobals.channelId+'~'+TiGlobals.osname+'~'+TiGlobals.devicemodel+'~'+TiGlobals.deviceversion;

TiGlobals.platformHeight = TiGlobals.osname === 'android' ? ((Ti.Platform.displayCaps.platformHeight / Ti.Platform.displayCaps.logicalDensityFactor) - 25) : Ti.Platform.displayCaps.platformHeight;
TiGlobals.platformWidth = TiGlobals.osname === 'android' ? Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor : Ti.Platform.displayCaps.platformWidth;

var base64 = require('/lib/Crypt/Base64');
//mstage key
var key = base64.decode('irBvFMACV3956lHuu+y3jQ==');

//Production key
//var key = base64.decode('ynw1TalFGK+0FGaClrjHYA==');

// Global Container
var _scrollableView = null;

// Activity indicator assigned in main
var ActivityIndicator = null;
var activityIndicator = null;

var viewArr = [];

// Fonts
var TiFonts = require('/utils/Font');

require('/lib/config').loggedIn();

////////Status///////////////////

//Activity tracker

//if(TiGlobals.osname === 'iphone' || TiGlobals.osname === 'ipad')
if((Ti.Platform.osname === 'iphone')||(Ti.Platform.osname === 'ipad'))
{
//Titanium.App.addEventListener('touchstart', require('/utils/activity-tracker').didActivity);
/////For Session & Event  by Sanjivani on 22/11/16 and on 26/12/16/////////
Titanium.App.addEventListener('pause', function (e) {            
    Ti.API.info('app.js: Pause event on '+Ti.Platform.osname);
    
    if(Ti.App.Properties.getString('loginStatus') !== '0')
	{
      TiGlobals.myEvent =  1;
      Ti.API.info("Value of TiGlobals.myEvent in pause is",+TiGlobals.myEvent);
      setTimeout(function(){ 
    	  if(TiGlobals.myEvent == '2'){
    		  Ti.API.info("Now your session will not expire...");
    		  Ti.API.info("2..Value of TiGlobals.myEvent in if timeout pause is",+TiGlobals.myEvent);
    	  }
    	  
    	  else{
      Ti.API.info("1..Value of TiGlobals.myEvent in else timeout pause is",+TiGlobals.myEvent);
      Ti.App.Properties.setString('loginStatus','0');
      //Ti.App.fireEvent('loginData');
      if(Ti.App.Properties.getString('loginStatus') === '0' && TiGlobals.myEvent == '1'){
	  Ti.API.info("Session expired");
	  var alertDialog = require('/utils/AlertDialog').showAlert('', 'Your session has expired. Please log in again.', [L('btn_ok')]);
		
		alertDialog.show();

		alertDialog.addEventListener('click', function(e) {
			alertDialog.hide();
			setTimeout(function(){
			    Ti.App.fireEvent('loginData');
				if(Ti.App.Properties.getString('mPIN') === '1')
				{
					require('/js/Passcode').Passcode();
				}
				else
				{
					require('/js/LoginModal').LoginModal();	
				}
			 },200);
			
			//TiGlobals.sessionCheck = 0; // Reset to 0
			try{
			require('/utils/RemoveViews').removeAllScrollableViews(); // Move to dashboard
			}
			catch(e){
				Ti.API.info(e);
			}
			alertDialog=null;
		});
		
	  //alert("Your session has expired. Please log in again");
      }
      else{
    	  }
      }
	},
		
     600000);  //timeout for 10 min 
	//10000);  //timeout for 10 sec 
    }
    else{}
   
}); 
}

Titanium.App.addEventListener ('paused', function (e) {
    Ti.API.info('app.js: Paused event on '+Ti.Platform.osname);
   
  
});     
Titanium.App.addEventListener('resume', function (e) {
    Ti.API.info('app.js: Resume event on '+Ti.Platform.osname);
    TiGlobals.myEvent = 2;
    Ti.API.info("2..Value of TiGlobals.myEvent in Resume is",+TiGlobals.myEvent);
    
});
Titanium.App.addEventListener('resumed', function (e) {
    Ti.API.info('app.js: Resumed event on '+Ti.Platform.osname);
});

//////////// IP Address ////////////

function ipFinder() {

if(Ti.Platform.osname === 'Android'){	
//by new URL
var xmlhttp = Ti.Network.createHTTPClient();
	xmlhttp.onload = function()
	{
		//require('/utils/Console').info('freegeoip.net' + xmlhttp.responseText);
		require('/utils/Console').info('geoip.nekudo.com' + xmlhttp.responseText);
		var hostipInfo = JSON.parse(xmlhttp.responseText);
		
		TiGlobals.ipAddress = hostipInfo.ip;

		require('/utils/Console').info(TiGlobals.ipAddress);
		xmlhttp = null;
	};
	
	xmlhttp.onerror = function(e)
	{
		//TiGlobals.ipAddress = '127.0.0.1';
		if(TiGlobals.osname === 'android')
	{
		require('/utils/AlertDialog').toast('Error in genrating IP Address.');
	}
	else
	{
		require('/utils/AlertDialog').iOSToast('Error in genrating IP Address.');
	}
		
	};
	
	//xmlhttp.open('GET', 'http://freegeoip.net/json/',true);
	xmlhttp.open('GET', 'https://geoip.nekudo.com/api',true);
	xmlhttp.send();


}else{
var ipUrl = TiGlobals.staticPagesURL+'ipforapp.php';
//var url = '/ipforapp.php';
 var xmlhttp = Ti.Network.createHTTPClient({
    
     onload : function(e) {
         Ti.API.info("ipforapp: " + this.responseText);
         TiGlobals.ipAddress = this.responseText;
		require('/utils/Console').info(TiGlobals.ipAddress);
		xmlhttp = null;
     },
   
     onerror : function(e) {
         require('/utils/AlertDialog').iOSToast('Error in genrating IP Address.');
    
     },
     timeout : 5000  // in milliseconds
 });
 // Prepare the connection.
 xmlhttp.open("GET", ipUrl , true);
 // Send the request.
 xmlhttp.send();

}
}

ipFinder();

/*function ipFinder() {
	var xmlhttp = Ti.Network.createHTTPClient();
	xmlhttp.onload = function()
	{
		//require('/utils/Console').info('freegeoip.net' + xmlhttp.responseText);
		require('/utils/Console').info('geoip.nekudo.com' + xmlhttp.responseText);
		var hostipInfo = JSON.parse(xmlhttp.responseText);
		
		TiGlobals.ipAddress = hostipInfo.ip;

		require('/utils/Console').info(TiGlobals.ipAddress);
		xmlhttp = null;
	};
	
	xmlhttp.onerror = function(e)
	{
		//TiGlobals.ipAddress = '127.0.0.1';
		if(TiGlobals.osname === 'android')
	{
		require('/utils/AlertDialog').toast('Error in genrating IP Address.');
	}
	else
	{
		require('/utils/AlertDialog').iOSToast('Error in genrating IP Address.');
	}
		
	};
	
	//xmlhttp.open('GET', 'http://freegeoip.net/json/',true);
	xmlhttp.open('GET', 'https://geoip.nekudo.com/api',true);
	xmlhttp.send();
}


ipFinder();*/

// Main
var Main = require('/js/Main');
var winMain = new Main();


if (Ti.Platform.osname === 'iphone') {
	winMain.orientationModes = [Ti.UI.PORTRAIT];

	winMain.open({
		transition : Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
	});

	// App in background

	Ti.App.addEventListener('resumed', function(e) {
		require('/utils/Console').info('Resume');
	});

	Ti.App.addEventListener('paused', function(e) {
		require('/utils/Console').info('Pause');
		
	});

} else if (Ti.Platform.osname === 'ipad') {
	winMain.orientationModes = [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT];
	winMain.open({
		transition : Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
	});

} else {
	winMain.exitOnClose = true;
	winMain.orientationModes = [Ti.UI.PORTRAIT];
	winMain.open();

	var clickCheck = 0;
	winMain.addEventListener('androidback', function() {

		if (clickCheck === 1) {
			return;
		}

		clickCheck = 1;
		// Reset click
		setTimeout(function() {
			clickCheck = 0;
		}, 350);

		if (TiGlobals.indicatorCheck === 0) {
			popView();
		}
	});
}

/////////// Social Apps ///////////

var fb = require('facebook');

if (TiGlobals.osname === 'android') {
	winMain.fbProxy = fb.createActivityWorker({
		lifecycleContainer : winMain
	});
}

var social = require('/lib/social');

var linkedIn = social.create({
	site : 'linkedin',
	consumerKey : '75itpvupwsy8lc',
	consumerSecret : 'sB9Zsugu3DPeOAgd'
});

var twitter = social.create({
	site : 'twitter',
	consumerKey : 'd00C5MvGsA5vfr6Q3GKKxliZG',
	consumerSecret : '4fMYDDG7JckMEEOLNS6smrWzoZnwC946wead9SgOkWC53Ebw5v'
});

var GoogleAuth = require('lib/googleAuth');
var googleAuth = new GoogleAuth({
	clientId : '61470950714-h4frlbvm5bidncl76o7r5pnojmtn6ql2.apps.googleusercontent.com',
	clientSecret : 'gD08L_yrr6WlbDU-Lil-XBCx',
	propertyName : 'googleToken',
	scope : ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email']
});

// Rate App
require('/lib/rate').plus(1, true);

// Back handler

function pushView(view) {
	if (viewArr.indexOf(view) === -1) {
		viewArr.push(view);
	}

	require('/utils/Console').info('Stacked Pages ====> ' + viewArr.length);
};

function popView() {

	if (viewArr.length > 1) {

		var currentPg = viewArr[viewArr.length - 1];
		var previousPg = viewArr[viewArr.length - 2];

		if (TiGlobals.menuOpen === '1') {
			Ti.App.fireEvent('menuAnimate');
			return;
		} else {

			if (Titanium.Network.online) {
				// Current page destroy
				require('/utils/PageController').pageBackController(currentPg);
			} else {
				require('/utils/Network').Network();
			}
		}
	} else {
		if (TiGlobals.menuOpen === '0') {
			var alertDialog = require('/utils/AlertDialog').showAlert('', L('exit_app'), [L('btn_yes'), L('btn_no')]);
			alertDialog.show();

			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if (e.index === 0 || e.index === "0") {
					alertDialog = null;
					winMain.close();
				}
			});
		} else if (TiGlobals.menuOpen === '1') {
			// Close Menu
			Ti.App.fireEvent('menuAnimate');
			return;
		} else {
			// do nothing
		}
	}
};

function emptyView() {
	viewArr = [];
};

/////////////// URL SCHEME /////////////////

var resSplit = [];
function redirectScheme(res, schemeType) {
	if (schemeType === 'custom') {
		require('/utils/Console').info('RES === ' + res.validation);
		if (res.validation.search('%3D') == -1) {
			var res1 = (Ti.Utils.base64decode(Ti.Utils.base64decode(res.validation + '=='))).toString();
		} else {
			var res1 = (Ti.Utils.base64decode(Ti.Utils.base64decode(res.validation.replace('%3D', '=')))).toString();
		}
		resSplit = res1.split('#$$#');
		require('/utils/Console').info('length === ' + resSplit.length + '====Original === ' + res1 + ' ======= Split === ' + resSplit[resSplit.length - 1]);
		if (resSplit[resSplit.length - 1] === 'mor') {
			require('/utils/Console').info('mor === ' + resSplit[resSplit.length - 1]);
			setTimeout(function() {
				//require('/js/Detail').Detail(resSplit[0]);
			}, 500);
		}
	} else {
		resSplit = res.split('#$$#');
		require('/utils/Console').info('length === ' + resSplit.length + '====Original === ' + res + ' ======= Split === ' + resSplit[resSplit.length - 1]);
		if (resSplit[resSplit.length - 1] === 'mor') {
			require('/utils/Console').info('mor === ' + resSplit[resSplit.length - 1]);
			setTimeout(function() {
				//require('/js/Detail').Detail(resSplit[0]);
			}, 500);
		}
	}
}

// We don't want our URL to do anything before our main window is open

var type = '';

function urlToObject(url) {
	//require('/utils/Console').info(url);
	if (url.indexOf('remit2india:/?') > -1) {
		type = 'custom';
		//require('/utils/Console').info(url);
		var returnObj = {};
		url = url.replace('remit2india:/?', '');
		var params = url.split('&');
		params.forEach(function(param) {
			var keyAndValue = param.split('=');
			//returnObj[keyAndValue[0]] = decodeURI(keyAndValue[1]);
			returnObj[keyAndValue[0]] = keyAndValue[1];
		});
		return returnObj;

	} else {
		type = 'http';
		var urlSplit = url.split('/');
		return urlSplit[4] + '#$$#' + urlSplit[3];
	}
}

// Handle the URL in case it resumed the app

Ti.App.addEventListener('resumed', function() {
	ipFinder();
	try {
		if (Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== 'undefined') {
			var res = urlToObject(Ti.App.getArguments().url);
			redirectScheme(res, type);
		}
	} catch(e) {
	}
});

winMain.addEventListener('open', function(e) {
	ipFinder();
	if (Ti.Platform.osname === 'iphone' || Ti.Platform.osname === 'ipad') {
		// Handle the URL in case it opened the app
		try {
			if (Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== 'undefined') {

				var res = urlToObject(Ti.App.getArguments().url);
				redirectScheme(res, type);
			}
		} catch(e) {
		}
	} else {

		var intentURL = Ti.Android.currentActivity.intent.data;
		//require('/utils/Console').info('intentURL ===== '+intentURL);
		// On Android, somehow the app always opens as new
		try {
			if (intentURL !== null || intentURL !== '' || intentURL !== 'undefined') {
				var res = urlToObject(intentURL);
				redirectScheme(res, type);
			}
		} catch(e) {
		}

		// Android resume and pause events

		winMain.activity.addEventListener('resume', function() {
			require('/utils/Console').info('Resumed');
			ipFinder();
		});
		
		winMain.activity.addEventListener('pause', function() {
			require('/utils/Console').info('Pause');
			ipFinder();
			//if(Ti.App.Properties.getString('mPIN') === '1')
			//{
				//require('/js/Passcode').Passcode();
			//}
		});
	}
});

////////// PUSH NOTIFICATIONS /////////////

function redirectPush(res) {
	try {
		if (Ti.Platform.osname === 'android') {
			Ti.API.info("IN REDIRECT PUSH!!!!");
			//var resSplit = res.pl.split('||');
			var title = res.android.title;
			var message = res.android.alert;
			var alertDialog = Ti.UI.createAlertDialog({
				title : title,
				buttonNames : ['Ok'],
				message : message
			});
			alertDialog.show();
			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if (e.index === 0 || e.index === "0") {
					alertDialog = null;
					//require('/js/Detail').Detail(resSplit[1]);
				}
			});

		} else {
			Ti.API.info("IN ELSE!!!!");
			var resSplit = res.data.pl.split('||');
			var title = res.data.title;
			var message = res.data.alert;
		}
		if (resSplit[0] === 'mor') {
			var alertDialog = Ti.UI.createAlertDialog({
				title : '',
				buttonNames : ['Yes', 'No'],
				message : title + ' Do you want to check it out?'
			});
			alertDialog.show();
			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if (e.index === 0 || e.index === "0") {
					alertDialog = null;
					//require('/js/Detail').Detail(resSplit[1]);
				}
			});

		} else {

		}
	} catch(e) {
		//require('/utils/Console').info('Invalid push data');
	}
}

if (Ti.Platform.osname === 'android') {
	var deviceToken;
	var Cloud = require('ti.cloud');
	Cloud.debug = true;
	var CloudPush = require('ti.cloudpush');
	CloudPush.debug = true;
	//CloudPush.enabled = true;
	CloudPush.showTrayNotificationsWhenFocused = false;
	CloudPush.focusAppOnPush = false;
	CloudPush.singleCallback = true;

	function checkDeviceTokenAndroid() {
		var xhr = require('/utils/XHR_BCM');
		xhr.call({
			url : TiGlobals.pushURL,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"CHECKPUSH",' + '"source":"mobile",' + '"platform":"' + TiGlobals.osname + '",' + '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' + '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"channelId":"' + TiGlobals.pushChannelId + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Success:' + e.result);
			if (e.result.status === 'F') {
				// Not found in database
				try {
					Cloud.Users.create({
						username : deviceToken.substr(0, 90),
						first_name : deviceToken.substr(0, 90),
						last_name : deviceToken.substr(0, 90),
						password : deviceToken.substr(0, 90),
						password_confirmation : deviceToken.substr(0, 90)
					}, function(e) {
						if (e.success) {
							require('/utils/Console').info('Success:' + JSON.stringify(e));
							var user = e.users[0];

							require('/utils/Console').info('push/push.php?deviceid=' + deviceToken + '&acsuser=' + deviceToken.substr(0, 90) + '&acspwd=' + deviceToken.substr(0, 90) + '&platform=' + TiGlobals.osname + '&acsid=' + user.id);

							var xmlHttpACS = require('/utils/XHR_BCM');
							xmlHttpACS.call({
								url : TiGlobals.pushURL,
								get : '',
								post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"PUSH",' + '"source":"mobile",' + '"platform":"' + TiGlobals.osname + '",' + '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' + '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"acsuser":"' + deviceToken.substr(0, 90) + '",' + '"acspwd":"' + deviceToken.substr(0, 90) + '",' + '"acsid":"' + user.id + '",' + '"channelId":"' + TiGlobals.pushChannelId + '"' + '}',
								success : xmlHttpACSSuccess,
								error : xmlHttpACSError,
								contentType : 'application/json',
								timeout : 10000
							});

							function xmlHttpACSSuccess(eACS) {
								require('/utils/Console').info('Success:' + eACS.result);
								if (eACS.result.status === 'S') {
									loginAndroidCloudUser();
								}
								xmlHttpACS = null;
							}

							function xmlHttpACSError(eACS) {
								require('/utils/Network').Network();
								xmlHttpACS = null;
							}

						} else {
							//require('/utils/Console').info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
						}
					});
				} catch(e) {
				}
			} else {
				loginAndroidCloudUser();
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	// Fetch device token

	CloudPush.retrieveDeviceToken({
		success : function deviceTokenSuccess(e) {
			deviceToken = e.deviceToken;
			Ti.App.Properties.setString('deviceToken', deviceToken);
			require('/utils/Console').info('Device Token: ' + deviceToken);
			checkDeviceTokenAndroid();
		},
		error : function deviceTokenError(e) {
			//require('/utils/Console').info('Failed to register for push! ' + e.error);
		}
	});

	function loginAndroidCloudUser(e) {
		//Create a Default User in Cloud Console, and login with same credential
		Cloud.Users.login({
			login : deviceToken.substr(0, 90),
			password : deviceToken.substr(0, 90)
		}, function(e) {
			if (e.success) {
				require('/utils/Console').info("Android Login success");
				androidCloudUserSubscribe();
			} else {
				require('/utils/Console').info('Android Login error: ' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}

	// Subscribe for push notification on cloud server

	function androidCloudUserSubscribe() {
		Cloud.PushNotifications.subscribe({
			channel : 'Remit2India',
			device_token : deviceToken,
			type : 'gcm'
		}, function(e) {
			if (e.success) {
				require('/utils/Console').info('Subscribed for Push Notification!');
			} else {
				require('/utils/Console').info('Subscribe error:' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}
	function receivePushAndroid(evt) {
		try {
			require('/utils/Console').info(evt.payload);
			var result = JSON.parse(evt.payload);
			setTimeout(function() {
				redirectPush(result);
			}, 1000);
		} catch(e) {
			require('/utils/Console').info('Error in push');
		}
	}


	CloudPush.addEventListener('callback', function(evt) {
		receivePushAndroid(evt);
	});

	CloudPush.addEventListener('trayClickLaunchedApp', function(evt) {
		//require('/utils/Console').info('@@## Tray Click Launched App (app was not running)');
		//receivePush(evt);
	});

	CloudPush.addEventListener('trayClickFocusedApp', function(evt) {
		//require('/utils/Console').info('@@## Tray Click Focused App (app was already running)');
		//receivePush(evt);
	});
} 
/*
if (Ti.Platform.osname === 'android') {


// Require the module
var CloudPush = require('ti.cloudpush');
var deviceToken = null;
  
// Initialize the module
CloudPush.retrieveDeviceToken({
    success: deviceTokenSuccess,
    error: deviceTokenError
});
 
// Enable push notifications for this device
// Save the device token for subsequent API calls
function deviceTokenSuccess(e) {
    deviceToken = e.deviceToken;
    alert('Success ' );
    subscribeToToken(deviceToken);
}
function deviceTokenError(e) {
    alert('Failed to register for push notifications! ' + e.error);
}
  
// Process incoming push notifications
CloudPush.addEventListener('callback', function (evt) {
	Ti.API.info("IN CALLBACK---",evt.payload);
	Ti.API.info(JSON.stringify(evt.payload));
	var result = JSON.stringify(evt.payload);
Ti.API.info("INSIDE result::::::::",result);
//"{\"android\":{\"title\":\"Remit2India\",\"sound\":\"none\",\"alert\":\"App is under maintenance sorry for inconvenience.\",\"vibrate\":false,\"badge\":1}}"

var alertSplit1 = evt.payload.split(":");
Ti.API.info("INSIDE result::::::::",alertSplit1);//"{\"android\":{\"title\":\"Remit2India\",\"sound\":\"none\",\"alert\":\"App is under maintenance sorry for inconvenience.\",\"vibrate\":false,\"badge\":1}}"

var alertSplit4 = alertSplit1[6];
Ti.API.info("INSIDE alertSplit4::::::::",alertSplit4);
 Ti.App.Properties.setString("badgeNo",alertSplit4);

var alertSplit2 = alertSplit1[4].split(",");
Ti.API.info("INSIDE result::::::::",alertSplit2);//{"android",{"title","Remit2India","sound","none","alert","App is under maintenance sorry for inconvenience.","vibrate",false,"badge",1}}

var alertSplit3 = alertSplit2[0];
//Ti.API.info("INSIDE result::::::::",alertSplit3);//"App is under maintenance sorry for inconvenience.","vibrate"
//alert(alertSplit3);

var alertDialog = Ti.UI.createAlertDialog({

title : 'Test',

buttonNames : ['OK', 'Cancel'],

message : alertSplit3 

});

alertDialog.show();

alertDialog.addEventListener('click', function(e) {

alertDialog.hide();


});
    
   // function receivePush(e) {
	//alert("Received push: " + JSON.stringify(e.data));
	// reset badge
	//alert(JSON.stringify(evt.payload.android.alert));  //ok
	//alert("EVT");
	//alert(evt);
   setTimeout(function(){
	Cloud.PushNotifications.resetBadge({
		
		//device_token : Ti.App.Properties.getString('deviceToken')
	}, function(e) {
		Ti.API.info("INSIDE resetBadge::::::::");
		//alert("INSIDE resetBadge::::::::");
		try {
			if (e.success) {
				require('/utils/Console').info('Badge Reset!');
				//alert('Badge Reset!');
				//Ti.UI.iPhone.setAppBadge(null);
				
			} else {
				require('/utils/Console').info(e);
				//alert(e);
			}
		} catch(e) {
		}
	});
	},1000);*

 //logger.info("Received push: " + JSON.stringify(e.data));

 //Alloy.Globals.isPNClicked = true;

//}
});*


function subscribeToToken(deviceToken){

 var Cloud = require("ti.cloud");

 try {

  Cloud.PushNotifications.subscribeToken({

  device_token: deviceToken,

  channel: "Remit2India",

  type: 'gcm'

}, function(e) {

if (e.success) {

 alert('Subscribed');

}

else {

 alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));

}

});

} catch (e) {


 }

}


}
*/


else {

	//for iOS

	require('/utils/Console').info('Cloud Push Enter');
	var Cloud = require('ti.cloud');
	var deviceToken;

	function checkDeviceTokenIOS() {
		Ti.API.info("INSIDE CHECKDEVICETOKENIOS::::::::");
		var xhr = require('/utils/XHR_BCM');
		xhr.call({
			url : TiGlobals.pushURL,
			get : '',
			post : '{' + 
			'"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' +
			 '"requestName":"CHECKPUSH",' + 
			 '"source":"mobile",' +
			 '"platform":"' + TiGlobals.osname + '",' +
			 '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' +
			  '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + 
			  '"channelId":"' + TiGlobals.pushChannelId + '"' +
            '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Success:' + e.result);
			if (e.result.status === 'F') {
				// Not found in database
				try {
					Cloud.Users.create({
						username : deviceToken.substr(0, 90),
						first_name : deviceToken.substr(0, 90),
						last_name : deviceToken.substr(0, 90),
						password : deviceToken.substr(0, 90),
						password_confirmation : deviceToken.substr(0, 90)
					}, function(e) {
						if (e.success) {
							require('/utils/Console').info('Success:' + JSON.stringify(e));
							var user = e.users[0];

							var xmlHttpACS = require('/utils/XHR_BCM');
							xmlHttpACS.call({
								url : TiGlobals.pushURL,
								get : '',
								post : '{' +
								 '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) +  '",' + 
								'"requestName":"PUSH",' +
								  '"source":"mobile",' +
								  '"platform":"' + TiGlobals.osname + '",' + 
								  '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' +
								   '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + 
								   '"acsuser":"' + deviceToken.substr(0, 90) + '",' +
								    '"acspwd":"' + deviceToken.substr(0, 90) + '",' +
								     '"acsid":"' + user.id + '",' + 
								     '"channelId":"' + TiGlobals.pushChannelId + '"' +
								      '}',
								success : xmlHttpACSSuccess,
								error : xmlHttpACSError,
								contentType : 'application/json',
								timeout : 10000
							});

							function xmlHttpACSSuccess(eACS) {
								if (eACS.result.status === 'S') {
									loginIOSCloudUser();
								}
								xmlHttpACS = null;
							}

							function xmlHttpACSError(eACS) {
								require('/utils/Network').Network();
								xmlHttpACS = null;
							}

						} else {
							require('/utils/Console').info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
						}
					});
				} catch(e) {
				}
			} else {
				loginIOSCloudUser();
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	//user login on cloud using default credential

	function loginIOSCloudUser() {
		Ti.API.info("INSIDE LOGINIOSCLOUDUSER::::::::");
		Cloud.Users.login({
			login : deviceToken.substr(0, 90),
			password : deviceToken.substr(0, 90)
		}, function(e) {
			if (e.success) {
				var user = e.users[0];
				require('/utils/Console').info("Loggin successfully");
				iosCloudUserSubscribe();
			} else {
				require('/utils/Console').info("Error :" + e.message);
			}
		});
	}

	// Process incoming push notifications
	function receivePushiOS(evt) {
		Ti.API.info("INSIDE RECEIVEPUSHIOS::::::::");
		require('/utils/Console').info('Received push: ' + JSON.stringify(evt));
		var result = JSON.parse(JSON.stringify(evt));

		setTimeout(function() {
			redirectPush(result);
		}, 1000);
	}

	// getting device token
	if (true && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
//alert("Hello");
 Ti.App.iOS.addEventListener("usernotificationsettings", 
  function registerForPush() {
 Ti.App.iOS.removeEventListener("usernotificationsettings", 
  registerForPush);
 Ti.Network.registerForPushNotifications({
  success: deviceTokenSuccess,
  error: deviceTokenError,
  callback: receivePush
 });
});
 Ti.App.iOS.registerUserNotificationSettings({
  types: [ Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, 
   Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, 
   Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE ]
 });
} else Ti.Network.registerForPushNotifications({
 types: [ Ti.Network.NOTIFICATION_TYPE_BADGE, 
   Ti.Network.NOTIFICATION_TYPE_ALERT,   
   Ti.Network.NOTIFICATION_TYPE_SOUND ],
 success: deviceTokenSuccess,
 error: deviceTokenError,
 callback: receivePush
});

function deviceTokenSuccess(e) {
	//alert(e.deviceToken);
	Ti.App.Properties.setString('deviceToken',e.deviceToken);
 subscribeToToken(e.deviceToken,"ios");
}

function deviceTokenError() {}
function subscribeToToken(deviceToken,deviceType){

 var Cloud = require("ti.cloud");

 try {

  Cloud.PushNotifications.subscribeToken({

  device_token: deviceToken,

  //channel: "announcement_alert",
  channel:'Remit2India',

  type: deviceType

}, function(e) {

if (e.success) {

 //alert('Subscribed');

}

else {

 //alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));

}

});
} catch (e) {


 }

}
function receivePush(e) {
	//alert("Received push: " + JSON.stringify(e.data));
	// reset badge
	alert(JSON.stringify(e.data.alert));  //ok
	//var alertDialog = require('/utils/AlertDialog').showAlert('', JSON.stringify(e.data.alert), [L('btn_yes'), L('btn_no')]);//yes /no
	//var alertDialog = require('/utils/AlertDialog').showAlert('', JSON.stringify(e.data.alert), [L('btn_ok')]); //ok
	/*var alertDialog = require('/utils/AlertDialog').showAlert('', JSON.stringify(e.data.alert));  //ok
			alertDialog.show();

			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if (e.index === 0 || e.index === "0") {
					alertDialog = null;
					winMain.close();
				}
			});*/
    setTimeout(function(){
	Cloud.PushNotifications.resetBadge({
		
		device_token : Ti.App.Properties.getString('deviceToken')
	}, function(e) {
		Ti.API.info("INSIDE resetBadge::::::::");
		//alert("INSIDE resetBadge::::::::");
		try {
			if (e.success) {
				require('/utils/Console').info('Badge Reset!');
				//alert('Badge Reset!');
				Ti.UI.iPhone.setAppBadge(null);
			} else {
				require('/utils/Console').info(e);
				//alert(e);
			}
		} catch(e) {
		}
	});
	},1000);

 //logger.info("Received push: " + JSON.stringify(e.data));

 //Alloy.Globals.isPNClicked = true;

}
} //iOS push ends here
