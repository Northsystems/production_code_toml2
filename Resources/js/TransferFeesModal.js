exports.TransferFeesModal = function()
{
	require('/lib/analytics').GATrackScreen('Transfer Fees');
	
	var _obj = {
		style : require('/styles/TransferFees').TransferFees,
		winTransferFees : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		webView : null,
		currCode : null,
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winTransferFees = Ti.UI.createWindow(_obj.style.winTransferFees);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winTransferFees);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Transfer Fees';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'fees_app.php?corridor='+_obj.currCode[0];
	//_obj.webView.url = 'https://mstage.remit2india.com/exchange-calculator/index.php?corridor='+_obj.currCode[0];
	//staticPagesURL :'https://mstage.remit2india.com/mobileApp/',
	//_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/UnitedKingdom/exchange-rate.jsp';
	
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	_obj.winTransferFees.add(_obj.globalView);
	_obj.winTransferFees.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_transfer();
	});
	
	_obj.winTransferFees.addEventListener('androidback', function(){
		destroy_transfer();
	});
	
	function destroy_transfer()
	{
		try{
			
			require('/utils/Console').info('############## Remove transfer start ##############');
			
			_obj.winTransferFees.close();
			require('/utils/RemoveViews').removeViews(_obj.winTransferFees);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_transfer',destroy_transfer);
			require('/utils/Console').info('############## Remove transfer end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_transfer', destroy_transfer);
}; // TransferFeesModal()