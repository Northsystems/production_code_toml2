function Terms(_self) {
	Ti.App.Properties.setString('pg', 'terms');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen('Terms & Conditions');

	var _obj = {
		style : require('/styles/StaticPages').StaticPages, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		webView : null,
		currCode : null
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	// CreateUI
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Terms & Conditions';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	//alert("Out of terms back event");
	
	_obj.imgBack.addEventListener('click',function(){
		//alert("In my terms back event");
		//popView();
		destroy_terms(); //changed on 21 feb 17
	});
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.enableZoomControls = true;
	_obj.webView.scalesPageToFit = false;
	
  /* if(_obj.currCode[0] == 'CAN'){
		// _obj.webView.url =  'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-can.jsp';
	  _obj.webView.url =  'https://testnew.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry=CAN&orgCurrency=CAD&orgCountryName=Canada ';

	}*/
	
	//_obj.webView.url='https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry='+countryCode[0]+'&orgCurrency='+countryCode[1]+'&orgCountryName='+countryName[0];

 
	if(_obj.currCode[0] == 'US'){
		//_obj.webView.url='https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-us.jsp?orgCountry=US&orgCurrency=USD&orgCountryName=United+States';
	_obj.webView.url='https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_US.jsp?origCountryCode=US&origCurrencyCode=USD&destCountryCode=IND&senderCountry=United%20States';
	
		}
	else if(_obj.currCode[0] == 'AUS'){
		//_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-aus.jsp?orgCountry=AUS&orgCurrency=AUD&orgCountryName=Australia';
	_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_AUS.jsp?origCountryCode=AUS&origCurrencyCode=AUD&destCountryCode=IND&senderCountry=Australia';
	
	}
	else if(_obj.currCode[0] == 'UK'){
		//_obj.webView.url ='https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-uk.jsp?orgCountry=UK&orgCurrency=GBP&orgCountryName=United%20Kingdom';
	_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_UK.jsp?origCountryCode=UK&origCurrencyCode=GBP&destCountryCode=IND&senderCountry=United%20Kingdom';
	
	}
      if(_obj.currCode[0] == 'CAN'){  //CMN84a
		  // _obj.webView.url =  'http://testnew.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry=CAN&orgCurrency=CAD&orgCountryName=Canada ';
         _obj.webView.url ='https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_CAN.jsp?origCountryCode=CAN&origCurrencyCode=CAD&destCountryCode=IND&senderCountry=Canada';
	      
	}
	else{
		_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry='+countryCode[0]+'&orgCurrency='+countryCode[1]+'&orgCountryName='+countryName[0];
	}

  if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	
	function destroy_terms() {
		try {
			require('/utils/Console').info('############## Remove Termsr2i start ##############');

			require('/utils/RemoveViews').removeViews(_obj.globalView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_terms', destroy_terms);
			require('/utils/Console').info('############## Remove Termsr2i end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_terms', destroy_terms);

	return _obj.globalView;

};// Terms()

module.exports = Terms;
