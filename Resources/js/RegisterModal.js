exports.RegisterModal = function()
{
	require('/lib/analytics').GATrackScreen('Registration');
	
	var _obj = {
		style : require('/styles/Register').Register,
		
		varForTest : null,
		
		winRegister : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		progressView : null,
		progressMainView : null,
		lblPercent : null,
		lblComplete : null,
		imgProgress : null,
		scrollableView : null,
		
		//Step 1
		stepView1 : null,
		txtFirstName : null,
		borderViewS11 : null,
		txtLastName : null,
		borderViewS12 : null,
		txtEmail : null,
		lblEmailMsg:null,
		borderViewS13 : null,
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS14 : null,
		dobView : null,
		lblDOB : null,
		imgDOB : null,
		borderViewS15 : null,
		doneDay : null,
		txtDayNo : null,
		borderViewS16 : null,
		genderView : null,
		lblGender : null,
		lblMale : null,
		lblFemale : null,
		citizenshipView : null,
		lblCitizenship : null,
		imgCitizenship : null,
		borderViewS18 : null,
		btnStep1 : null,
		
		//Step 2
		stepView2 : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		occupationView : null,
		lblOccupation : null,
		imgOccupation : null,
		borderViewS210 : null,
		btnStep2 : null,
		
		// Step 3
		stepView3 : null,
		lblPasswordSetup : null,
		lblUsername : null,
		txtPassword : null,
		borderViewS31 : null,
		txtConfirmPassword : null,
		borderViewS32 : null,
		lblPasswordValidate : null,
		btnStep3 : null,
		
		//Step 4
		stepView4 : null,
		lblHow : null,
		HDYView : null,
		lblHDY : null,
		imgHDY : null,
		borderViewS41 : null,
		txtHDY : null,
		borderViewS42 : null,
		assuranceView : null,
		imgAssurance : null,
		lblAssurance : null,
		termsView : null,
		lblTerms : null,
		lblAgreement : null,
		lblAnd : null,
		lblPrivacy : null,
		btnRegister : null,
	
		gender : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		assurance : null,
		dob : null,
		
		//social view
		socialView:null,
		imgFB:null,
	    imgLI:null,
	    imgGP:null,
	    borderRegistrationView:null,
	    lblOR:null,
	    lblOR1 : null,
	     promoValue:null
	    
	   
	};
	
	try{
	//TiGlobals.addressUpdate = false;
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winRegister = Titanium.UI.createWindow(_obj.style.winRegister);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winRegister);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Register';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	//_obj.progressView = Ti.UI.createView(_obj.style.progressView);
	_obj.progressMainView = Ti.UI.createView(_obj.style.progressMainView);
	_obj.lblPercent = Ti.UI.createLabel(_obj.style.lblPercent);
	_obj.lblPercent.text = '0%';
	_obj.lblComplete = Ti.UI.createLabel(_obj.style.lblComplete);
	_obj.lblComplete.text = 'Profile Completed';
	_obj.imgProgress = Ti.UI.createImageView(_obj.style.imgProgress);
	_obj.imgProgress.image = '/images/progress_0.png';
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
		//Main registration view
		_obj.stepView1 = Ti.UI.createScrollView(_obj.style.stepView1);
		
	//social view
	_obj.lblOR = Ti.UI.createLabel(_obj.style.lblOR);
	_obj.lblOR.text = 'Register With';
	
	_obj.socialView = Ti.UI.createView(_obj.style.socialView);
	
	_obj.progressView = Ti.UI.createView(_obj.style.socialMainView);
	
	_obj.imgFB = Ti.UI.createImageView(_obj.style.imgFB);
	
	_obj.imgLI = Ti.UI.createImageView(_obj.style.imgLI);
	
	_obj.imgGP = Ti.UI.createImageView(_obj.style.imgGP);
	
	_obj.lblOR1 = Ti.UI.createLabel(_obj.style.lblOR1);
	_obj.lblOR1.text = 'OR';
	
	_obj.borderRegistrationView = Ti.UI.createView(_obj.style.borderRegistrationView);
	////
	
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	

	_obj.progressView.add(_obj.lblOR);
	_obj.socialView.add(_obj.imgFB);
	_obj.socialView.add(_obj.imgLI);
	_obj.socialView.add(_obj.imgGP);
	
	_obj.progressView.add(_obj.socialView);
	//_obj.progressView.add(_obj.borderRegistrationView );
	_obj.progressView.add(_obj.lblOR1);
	//_obj.progressView.add(_obj.progressMainView);
	
	//_obj.stepView1.add(_obj.progressView);   //Commenting on 12-June-17 to remove linked in and temp remove gmail and facebook
	
	
	//social view event
		_obj.socialView.addEventListener('click',function(e){
		if(e.source.type === 'fb')
		{
			require('/lib/facebook').facebookAuthentication(_obj.globalView,'function','destroy_login');
		}
		else if(e.source.type === 'li')
		{
			if(linkedIn.isAuthorized())
			{
				linkedIn.getProfileLinkedin({
					success: function(e) {
						var result = JSON.parse(e);
			            var params = {
				    		social:'LinkedIn',
				    		fname:result.firstName,
				    		lname:result.lastName,
				    		email:result.emailAddress
				    	};
				    	
				    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
			        },
			        error: function(error) {
			        	
			        }
			    });
			}
			else
			{
				linkedIn.authorize(function(e){
					linkedIn.getProfileLinkedin({
						success: function(e) {
				            var result = JSON.parse(e);
				            var params = {
					    		social:'LinkedIn',
					    		fname:result.firstName,
					    		lname:result.lastName,
					    		email:result.emailAddress
					    	};
					    	
					    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
				        },
				        error: function(error) {
				        	
				        }
				    });
				});
			}
		}
		else{
			googleAuth.isAuthorized(function(e) {
				require('/utils/Console').info('Access Token: ' + googleAuth.getAccessToken());
				
				var xhr = Ti.Network.createHTTPClient({
					// function called when the response data is available
					onload : function(e) {
						var json = JSON.parse(this.responseText);
		                require('/utils/Console').info(json.given_name + ' --- ' + json.family_name + ' --- ' + json.email);
						
						var params = {
				    		social:'Google+',
				    		fname:json.given_name,
				    		lname:json.family_name,
				    		email:json.email
				    	};
				    	
				    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
						
					},
					onerror : function(e) {
						Ti.API.error('HTTP: '+JSON.stringify(e));
					}
				});
				
				xhr.open("GET", 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+googleAuth.getAccessToken()+'&alt=json&v=2');
				xhr.send();	
				
			}, function() {
				//authorize first
				googleAuth.authorize(function(){
					require('/utils/Console').info('Access Token: ' + googleAuth.getAccessToken());
					
					var xhr = Ti.Network.createHTTPClient({
						// function called when the response data is available
						onload : function(e) {
							var json = JSON.parse(this.responseText);
			                require('/utils/Console').info(json.given_name + ' --- ' + json.family_name + ' --- ' + json.email);
							
							var params = {
					    		social:'Google+',
					    		fname:json.given_name,
					    		lname:json.family_name,
					    		email:json.email
					    	};
					    	
					    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
							
						},
						onerror : function(e) {
							Ti.API.error('HTTP: '+JSON.stringify(e));
						}
					});
					
					xhr.open("GET", 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+googleAuth.getAccessToken()+'&alt=json&v=2');
					xhr.send();	
					
				});
			});
		}
	});
	function progressBar(val){
		_obj.lblPercent.text = val+'%';
		_obj.imgProgress.image = '/images/progress_'+val+'.png';
	}
	
	
	

	/////////////////// Step 1 ///////////////////
	//First Name
	
	_obj.txtFirstName = Ti.UI.createTextField(_obj.style.txtFirstName);
	
	_obj.txtFirstName.hintText = '*First Name';
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);


_obj.txtFirstName.addEventListener('focus',function(){
_obj.borderViewS11.backgroundColor= '#007fff'; //azure
//_obj.citizenshipView.height=0;
});


_obj.txtFirstName.addEventListener('blur',function(){
	 try{
	var fName = require('/lib/toml_validations').trim(_obj.txtFirstName.value);
	 }catch(e){
		
	}
	 try{
	////////////////////// First Name ////////////////////
	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
	{
		 try{
		_obj.borderViewS11.backgroundColor= '#ff0000';
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Please Enter First Name');
		
		}
		else
		{
			var size = 320;
			require('/utils/AlertDialog').iOSToast1('Please Enter First Name',size);
		}
		return;
		 }catch(e){
				
			}
	}
	else if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
	{  
		_obj.borderViewS11.backgroundColor= '#ff0000';
		
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Please enter only alphabetical characters');
		
		}
		else
		{
			var size = 320;
			require('/utils/AlertDialog').iOSToast1('Please enter only alphabetical characters',size);
		}
    	
	}
	else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFirstName.value) === false)
	{
		
		_obj.borderViewS11.backgroundColor= '#ff0000';
	
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Space not allowed at the beginning of First Name');
		
		}
		else
		{
			require('/utils/AlertDialog').iOSToast('Space not allowed at the beginning of First Name');
		}
	
	}

	
	else
	{
		_obj.borderViewS11.backgroundColor= '#228b22';//dark lime green
	}
	
}catch(e){
				
			}
	//function didSngAlphaActivity () {
	 
	//setTimeout(function(){
         try{
	   	 if(fName.length == 1 && fName.length > 0)
	   	{	
	   		_obj.borderViewS11.backgroundColor= '#ff0000';
	   		
	   		
	   		if(TiGlobals.osname === 'android')
	   		{
	   			require('/utils/AlertDialog').toast('First Name cannot be a single alphabet. Please enter your complete First Name');
	   		
	   		}
	   		else
	   		{
	   			require('/utils/AlertDialog').iOSToast('First Name cannot be a single alphabet. Please enter your complete First Name');
	   		}
	   		return;
	   	}
	   	}catch(e){}	
	   // }, 3000);
	//};
    //timeoutID = setTimeout(userIsInactive,  10 * 60 * 1000);
	
});

//added on 11feb17
_obj.txtFirstName.addEventListener('change',function(){
	
	if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
	{  
	 _obj.borderViewS11.backgroundColor= '#ff0000';
	}
	else if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
	{
		_obj.borderViewS11.backgroundColor= '#ff0000';
		}
	
	else{
		_obj.borderViewS11.backgroundColor= '#228b22';//dark lime green
	}
});

_obj.txtFirstName.addEventListener('change',function(){
	 
	var fName = require('/lib/toml_validations').trim(_obj.txtFirstName.value);
	
	////////////////////// First Name ////////////////////
	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
	{
		_obj.borderViewS11.backgroundColor= '#ff0000';
		
	}
	else if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
	{  
		_obj.borderViewS11.backgroundColor= '#ff0000';
		
		
	}
	else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFirstName.value) === false)
	{
		
		_obj.borderViewS11.backgroundColor= '#ff0000';
	
		
	}

	
	else
	{
		_obj.borderViewS11.backgroundColor= '#228b22';//dark lime green
	}
	
});


	//Last NAme
	_obj.txtLastName = Ti.UI.createTextField(_obj.style.txtLastName);
	_obj.txtLastName.hintText = '*Last Name';
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	
	
	_obj.txtLastName.addEventListener('focus',function(){
		try{
		_obj.borderViewS12.backgroundColor= '#007fff';
		}
		catch(e){
			
		}
	});
   
	
	_obj.txtLastName.addEventListener('blur',function(){
		 
	try{	
//////////////////////Last Name ////////////////////
try{
var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
}catch(e){}

if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
{
	
	setTimeout(function(){
          _obj.borderViewS12.backgroundColor= '#ff0000';
         if(TiGlobals.osname === 'android')
          {
              require('/utils/AlertDialog').toast('Please Enter Last Name');

         }
      else
        {
	    var size = 275;
        require('/utils/AlertDialog').iOSToast1('Please Enter Last Name',size);
//_obj.txtLastName.focus();
       }
        },2000);

}
else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
{
_obj.borderViewS12.backgroundColor= '#ff0000';
       if(TiGlobals.osname === 'android')
           {
             require('/utils/AlertDialog').toast('Please enter only alphabetical characters');

           }
      else
         {
            var size = 275;
            require('/utils/AlertDialog').iOSToast1('Please enter only alphabetical characters',size);
         }

}
else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
{
_obj.borderViewS12.backgroundColor= '#ff0000';
     if(TiGlobals.osname === 'android')
     {
        require('/utils/AlertDialog').toast('Space not allowed at the beginning of Last Name');

     }
     else
     {
       require('/utils/AlertDialog').iOSToast('Space not allowed at the beginning of Last Name');
     }		

}
else if(lName.length == 1 && lName.length > 0)
{	
	_obj.borderViewS12.backgroundColor= '#ff0000';
	
	if(TiGlobals.osname === 'android')
	{
		require('/utils/AlertDialog').toast('Last Name cannot be a single alphabet. Please enter your complete Last Name');
	
	}
	else
	{
		require('/utils/AlertDialog').iOSToast('Last Name cannot be a single alphabet. Please enter your complete Last Name');
	}
	
}	

else
{
_obj.borderViewS12.backgroundColor= '#228b22';//dark lime green;
}

	
}catch(e){Ti.API.info(e);}
});

	/*_obj.txtLastName.addEventListener('blur',function(){
		try{
	if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
		{
			_obj.borderViewS12.backgroundColor= '#ff0000';
			}
		
		else{
			_obj.borderViewS12.backgroundColor= '#228b22';//dark lime green
		}
		}
		catch(e){
			Ti.API.info(e);
		}
	});*/
	
	
	_obj.txtLastName.addEventListener('change',function(){
		 
	
//////////////////////Last Name ////////////////////
	var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
try{
	//setTimeout(function(){
	if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
	{
		_obj.borderViewS12.backgroundColor= '#ff0000';
		
		
	}
	//},3000);
	
	else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
	 {
		_obj.borderViewS12.backgroundColor= '#ff0000';
		
	}
	else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
	{
		_obj.borderViewS12.backgroundColor= '#ff0000';
		
	}


	else
	{
		_obj.borderViewS12.backgroundColor= '#228b22';//dark lime green;
	}
	 
}
catch(e){
	Ti.API.info(e);
}

	   
	});
	
	//this will be your Username for Remit2India--email
	//Day time Number should be in following format 0


	//Email
	_obj.txtEmail = Ti.UI.createTextField(_obj.style.txtEmail);
	//_obj.txtEmail.hintText = '*Your Email ID (Username)';
	_obj.txtEmail.hintText = '*Your Email ID (will be your login id)';
	_obj.txtEmail.keyboardType = Ti.UI.KEYBOARD_TYPE_EMAIL;
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	_obj.lblEmailMsg = Ti.UI.createLabel(_obj.style.lblEmailMsg);
	
	
//////////////////////Email ////////////////////
	_obj.txtEmail.addEventListener('focus',function(){
		_obj.borderViewS13.backgroundColor= '#007fff'; //azure
		
	});


	_obj.txtEmail.addEventListener('blur',function(){
		try{
		if(_obj.txtEmail.value === '' || _obj.txtEmail.value === 'Email')
		{
	   		
			//require('/utils/AlertDialog').showAlert('','Please enter Email Address',[L('btn_ok')]).show();
			
			if(TiGlobals.osname === 'android')
	   		{
	   			require('/utils/AlertDialog').toast('The email address is required');
	   		
	   		}
	   		else
	   		{
	   			_obj.borderViewS13.backgroundColor= '#ff0000';
	   		    _obj.lblEmailMsg.height=30;
	   		    _obj.lblEmailMsg.text = 'The email address is required';
	   			//require('/utils/AlertDialog').iOSToast('The email address is required');
	   		}
		
		}
		else{
	
		//check user availability
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		try{
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CHECKUSERAVAILABILITY",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+_obj.txtEmail.value+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
		}catch(e){}

		function xhrSuccess(e1) {
			activityIndicator.hideIndicator();
			if(e1.result.responseFlag === "F")
			{  try{
		   		
	
		   			if(TiGlobals.osname === 'android')
			   		{
                                                _obj.borderViewS13.backgroundColor= '#ff0000';
			   			require('/utils/AlertDialog').toast('Please enter a valid Email Address');
			   			_obj.txtEmail.value = '';
			   		
			   		}
			   		else
			   		{
			   			_obj.borderViewS13.backgroundColor= '#ff0000';
		   		        _obj.lblEmailMsg.height = 30;
		   		        _obj.lblEmailMsg.text = 'Please enter a valid Email Address';
			   		}
		   		
				
			}catch(e1){}
			}
			else{
				_obj.borderViewS13.backgroundColor= '#228b22';//dark lime green;
				if(TiGlobals.osname === 'android')
			   		{
			   			require('/utils/AlertDialog').toast('This Email Address is available for use');
			   		
			   		}
			   		else
			   		{
			   			//var size = 230;
			   			//require('/utils/AlertDialog').iOSToast('This Email Address is available for use');
			   			_obj.lblEmailMsg.color= '#228b22';//dark lime green; 
			   		    _obj.lblEmailMsg.text = 'This Email Address is available for use';
			   		    _obj.lblEmailMsg.height = 30;
			   		}
			   		
         
			}
				
			xhrCheck = null;
		}
		function xhrError(e1) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		}
		}catch(e){}
	});
	
	//Password
	_obj.txtPassword = Ti.UI.createTextField(_obj.style.txtPassword);
	_obj.txtPassword.hintText = '*Password';
	//_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderViewS31);
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	_obj.txtPassword.maxLength = 20;
	
	///////////////Password validations/////////////////
	_obj.txtPassword.addEventListener('focus',function(){
		_obj.borderViewS31.backgroundColor= '#007fff'; //azure
		/*setTimeout(function(){
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Password should be of minimum 8 and maximum 20 characters');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Password should be of minimum 8 and maximum 20 characters');
			}
		},1000);*/
		
	});
	
	var count1 = '0';
	_obj.txtPassword.addEventListener('change',function(){
		try{
		/*if(count1 == '0'){
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Password should be of minimum 8 and maximum 20 characters');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Password should be of minimum 8 and maximum 20 characters');
			}
			count1 = '1';
		}*/
		if(_obj.txtPassword.value ==='')
		{
              if (TiGlobals.osname === 'android') {
			//_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
               }else{
                   _obj.borderViewS31.backgroundColor=  '#ff0000'; //red
             }
			
		}
		
		else if(_obj.txtPassword.value.length != 0 ){
			/*if(!((/\d/.test(_obj.txtPassword.value)) && (/[a-z]/i.test(_obj.txtPassword.value))))  //commented for \\ accepted.
			{
				//require('/utils/AlertDialog').showAlert('','Password should be alpha-numeric',[L('btn_ok')]).show();
				_obj.borderViewS31.backgroundColor = '#ff0000'; //red
				
	    		
			}
			else */if(_obj.txtPassword.value.length < 8 || _obj.txtPassword.value.length > 20){
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red

			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassword.value) == false)
			 {
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
	           
			   }
			else{
			
				   _obj.borderViewS31.backgroundColor= '#228b22';//dark lime green;
				   _obj.txtConfirmPassword.editable = true;
               }
			}
		else
	     {
		  //_obj.borderViewS31.backgroundColor= '#228b22';//dark lime green;
		  // _obj.txtConfirmPassword.editable = true;

	     }
		
		 }
		 catch(e){
		 	Ti.API.info(e);
		 }
			
		
			//},5000);

		     
		  
		     
	
	});
	
	_obj.txtPassword.addEventListener('blur',function(){
		try{
		if(_obj.txtPassword.value.length < 8 || _obj.txtPassword.value.length > 20)
		{
			if(_obj.txtPassword.value ==='')
			{
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
			}
				else{
			//require('/utils/AlertDialog').showAlert('','Password should be of minimum 8 and maximum 20 characters',[L('btn_ok')]).show();
			_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Password should be Alphanumeric Character and minimum 8 and maximum 20 characters long');

			}
			else
			{
				var size = 200;
				require('/utils/AlertDialog').iOSToast1('Password should be Alphanumeric Character and minimum 8 and maximum 20 characters long',size);
			}
				}
			//_obj.txtPassword.focus();
    		//_obj.txtConfirmPassword.value = '';
			//return;
		}
		
		/* if(_obj.txtPassword.value.length != 0){
			if(!((/\d/.test(_obj.txtPassword.value)) && (/[a-z]/i.test(_obj.txtPassword.value))))
			{
				//require('/utils/AlertDialog').showAlert('','Password should be alpha-numeric',[L('btn_ok')]).show();
				_obj.borderViewS31.backgroundColor = '#ff0000'; //red
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Password should be Alphanumeric Character and minimum 8 and maximum 20 characters long');

				}
				else
				{
					var size = 200;
					require('/utils/AlertDialog').iOSToast1('Password should be Alphanumeric Character and minimum 8 and maximum 20 characters long',size);
				}
	    		
			}
	   }*/
		
		   else if(_obj.txtPassword.value ==='')
			{
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
				//require('/utils/AlertDialog').showAlert('','Please enter password',[L('btn_ok')]).show();
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('The password is required and cannot be empty');

				}
				else
				{
					var size = 185;
					require('/utils/AlertDialog').iOSToast1('The password is required and cannot be empty',size);
				}
	    		//_obj.txtPassword.focus();
				//return;
			}
			
		
			//},5000);

		     else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassword.value) == false)
			 {
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
	            //require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of password',[L('btn_ok')]).show();
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('The password is required and cannot be empty');
       
				}
				else
				{
					require('/utils/AlertDialog').iOSToast('The password is required and cannot be empty');

				}
	    		//_obj.txtPassword.focus();
				//return;
			   }
		  
		     else
		     {
			   _obj.borderViewS31.backgroundColor= '#228b22';//dark lime green;
			   //_obj.txtConfirmPassword.editable = true;

		     }
		    }catch(e){}
	
	});

	
	/*_obj.txtPassword.addEventListener('blur',function(){
		try{
		 if(_obj.txtPassword.value === '')
			{
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
				
			}
		
		   else{
			   _obj.borderViewS31.backgroundColor= '#228b22';//dark lime green;

		   }
		}
		catch(e){
			Ti.API.info(e);
		}
	});
	var count = 0;
	_obj.txtPassword.addEventListener('change',function(){
		
		if(count == 0) {
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Password should be of minimum 8 and maximum 20 characters');

		}
		else
		{
			require('/utils/AlertDialog').iOSToast('Password should be of minimum 8 and maximum 20 characters');
		}
		count = count + 1;
		}
		else if(_obj.txtPassword.value.length != 0){
			if(!((/\d/.test(_obj.txtPassword.value)) && (/[a-z]/i.test(_obj.txtPassword.value))))
			{
				//require('/utils/AlertDialog').showAlert('','Password should be alpha-numeric',[L('btn_ok')]).show();
				_obj.borderViewS31.backgroundColor = '#ff0000'; //red
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Password should be alpha-numeric');

				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Password should be alpha-numeric');
				}
	    		
			}
	   }
		
		else if(_obj.txtPassword.value ==='')
			{
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
				//require('/utils/AlertDialog').showAlert('','Please enter password',[L('btn_ok')]).show();
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Please enter password');

				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Please enter password');
				}
	    		//_obj.txtPassword.focus();
				//return;
			}
			
		else if(_obj.txtPassword.value.length < 8 || _obj.txtPassword.value.length > 20)
			{
				//require('/utils/AlertDialog').showAlert('','Password should be of minimum 8 and maximum 20 characters',[L('btn_ok')]).show();
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Password should be of minimum 8 and maximum 20 characters');

				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Password should be of minimum 8 and maximum 20 characters');
				}
				//_obj.txtPassword.focus();
	    		//_obj.txtConfirmPassword.value = '';
				//return;
			}
			//},5000);

		     else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassword.value) == false)
			 {
				_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
	            //require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of password',[L('btn_ok')]).show();
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Space not allowed at the beginning of password');
					require('/utils/AlertDialog').toast('Space not allowed at the beginning of password');
       
				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Space not allowed at the beginning of password');

					require('/utils/AlertDialog').iOSToast('Space not allowed at the beginning of password');
				}
	    		//_obj.txtPassword.focus();
				//return;
			   }
		  
		     else
		     {
			   _obj.borderViewS31.backgroundColor= '#228b22';//dark lime green;

		     }
	
	});*/

	
	_obj.txtConfirmPassword = Ti.UI.createTextField(_obj.style.txtConfirmPassword);
	_obj.txtConfirmPassword.maxLength = 20;
	_obj.txtConfirmPassword.hintText = '*Re-type Password';
	_obj.txtConfirmPassword.editable = false;
	//_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderViewS32);
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
	_obj.lblPasswordValidate = Ti.UI.createLabel(_obj.style.lblPasswordValidate);
	_obj.lblPasswordValidate.text = 'Password must be a minimum of 8 characters with at least one letter and one number';
	
	//confirm password validation
	_obj.txtConfirmPassword.addEventListener('focus',function(){
		try{
		_obj.borderViewS32.backgroundColor= '#007fff'; //azure
		if(_obj.txtPassword.value === '')
		{
			//_obj.borderViewS31.backgroundColor=  '#ff0000'; //red
			_obj.borderViewS32.backgroundColor=  '#ff0000'; //red

			//require('/utils/AlertDialog').showAlert('','Please enter password',[L('btn_ok')]).show();
			/*if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Please enter password');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Please enter password');
			}*/
			_obj.txtConfirmPassword.value = '';
		}
		}catch(e){}

	});
	_obj.txtConfirmPassword.addEventListener('blur',function(){
		try{
		if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtConfirmPassword.value) == false)
		{
			//require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Re-type password',[L('btn_ok')]).show();
			_obj.borderViewS32.backgroundColor=  '#ff0000'; //red
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Space not allowed at the beginning of Re-type password');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Space not allowed at the beginning of Re-type password');
			}			
			
		}
		else if(_obj.txtConfirmPassword.value === '')
		{
			_obj.borderViewS32.backgroundColor=  '#ff0000'; //red
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('The confirm password is required and cannot be empty');

			}
			else
			{
				var size = 160;
				require('/utils/AlertDialog').iOSToast1('The confirm password is required and cannot be empty',size);
			}			
			
		}
		else if(_obj.txtPassword.value != _obj.txtConfirmPassword.value)
		{
		   _obj.borderViewS32.backgroundColor=  '#ff0000'; //red
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('The password and its confirmation did not match');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('The password and its confirmation did not match');
			}		
			_obj.txtConfirmPassword.value = '';
		}
		else if(_obj.txtPassword.value !== '' &&_obj.txtPassword.value == _obj.txtConfirmPassword.value ){
			_obj.borderViewS32.backgroundColor= '#228b22';//dark lime green;
		} 
		else{
			
		}
		}catch(e){
			Ti.API.info(e);
		}
		});
	

	_obj.txtConfirmPassword.addEventListener('change',function(){
	try{
	if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtConfirmPassword.value) == false)
	{
		_obj.borderViewS32.backgroundColor=  '#ff0000'; //red
				
		
	}
	else if(_obj.txtConfirmPassword.value === '')
	{
         if (TiGlobals.osname === 'android') {
			//_obj.borderViewS32.backgroundColor=  '#ff0000'; //red
               }else{
                   _obj.borderViewS32.backgroundColor=  '#ff0000'; //red
             }
		
	}
	else if(_obj.txtPassword.value != _obj.txtConfirmPassword.value)
	{
	   _obj.borderViewS32.backgroundColor=  '#ff0000'; //red
		
	}
	else{
		_obj.borderViewS32.backgroundColor= '#228b22';//dark lime green;
	}   
	}catch(e){}
	});
	
	
	//DOB
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = '*Date of Birth';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS14 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.dobView.addEventListener('click',function(e){
		_obj.borderViewS14.backgroundColor= '#007fff'; //azure

		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('You need to be at least 18 years old to use our services');

		}
		else
		{
			require('/utils/AlertDialog').iOSToast('You need to be at least 18 years old to use our services');
		}
		setTimeout(function(){
			require('/utils/DatePicker').DatePicker1(_obj.lblDOB,'dob18',_obj.borderViewS14);
             if (TiGlobals.osname === 'android') {
				_obj.borderViewS14.backgroundColor= '#228b22';//dark lime green;
               }else{
                   
             }
		

		},800);
	//require('/utils/DatePicker').DatePicker(_obj.lblDOB,'dob18');
	});
	
	/*_obj.dobView.addEventListener('click',function(e){
	require('/utils/DatePicker').DatePicker(_obj.lblDOB,'dob18',_obj.borderViewS14);
	//_obj.borderViewS14.backgroundColor= '#228b22';//dark lime green;

});
	
	_obj.imgDOB.addEventListener('click',function(e){
		//_obj.borderViewS14.backgroundColor= '#228b22';//dark lime green;

	require('/utils/DatePicker').DatePicker(_obj.lblDOB,'dob18',_obj.borderViewS14);

	});*/
	
	_obj.dobView.addEventListener('touchend',function(){
		try{
	if(_obj.lblDOB.text === '' || _obj.lblDOB.text === '*Date of Birth')
	{
		//require('/utils/AlertDialog').showAlert('','Please Enter your Date of Birth',[L('btn_ok')]).show();
		_obj.borderViewS14.backgroundColor=  '#ff0000'; //red
		_obj.lblDOB.text = 'Date of Birth*';
		
	}
	else
	{
		_obj.borderViewS14.backgroundColor= '#228b22';//dark lime green;
	}
	}catch(e){}
	});

	////////////////////// DOB ////////////////////
		
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		try{
		_obj.txtDayNo.blur();
		}
		catch(e){
			Ti.API.info(e);
		}
	});
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	
	_obj.doneMobile.addEventListener('click',function(){
		try{
		_obj.txtDayNo.blur();
		}
		catch(e){
			Ti.API.info(e);
		}
	});
	
	//Mobile
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtMobile);  //Will be treated as Day time no  //from 29June17 will be treated as mobile no as confirmed by Kiran Chile(TOML) on 28June17
	
	//_obj.txtDayNo.hintText = '*Mobile Number';
	_obj.txtDayNo.hintText = '*Phone Number';
	//_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
    _obj.txtDayNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
   
	_obj.txtDayNo.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS15 = Ti.UI.createView(_obj.style.borderView);
	
	//added by Sanjivani on 15-03-17 
	if((countryName[0] === 'Germany'))
		{
			_obj.txtDayNo.maxLength = 14;
		}
	 else if(((countryName[0] === 'Malta')||(countryName[0] === 'Belgium')||(countryName[0] === 'Cyprus')))
		{ 
			_obj.txtDayNo.maxLength = 08;    							
		}
	else if(((countryName[0] === 'Australia')||(countryName[0] === 'New Zealand')||(countryName[0] === 'Portugal')))
	    {   
	   	_obj.txtDayNo.maxLength = 09;     
        }
    else if(!((countryName[0] === 'Australia')||(countryName[0] ===  'New Zealand')||(countryName[0] === 'Portugal')||(countryName[0] === 'Belgium')||(countryName[0] === 'Malta')||(countryName[0] === 'Germany')||(countryName[0] === 'Cyprus')))
		{ 
			_obj.txtDayNo.maxLength = 10;
	  }
	
else{
	
}
	
	//Mobile no validations
//////////////////////Mobile No ////////////////////
	_obj.txtDayNo.addEventListener('focus',function(){
		_obj.borderViewS15.backgroundColor= '#007fff'; //azure
	
	});
_obj.txtDayNo.addEventListener('change',function(){
try{
	if(_obj.txtDayNo.value === '' || _obj.txtDayNo.value === '*Phone Number')
	{
		_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
		
	}
		
	else if((countryName[0] === 'Germany') && (_obj.txtDayNo.value.length != 14))
		{
			_obj.borderViewS15.backgroundColor=  '#ff0000'; //red							
	       
		}
	 else if(((countryName[0] === 'Malta')||(countryName[0] === 'Belgium')||(countryName[0] === 'Cyprus')) &&( _obj.txtDayNo.value.length != 8))
		{
				_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
									
		}
	else if(((countryName[0] === 'Australia')||(countryName[0] === 'New Zealand')||(countryName[0] === 'Portugal')) && ( _obj.txtDayNo.value.length != 9))
	   {
	   		_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
	   
        }
    else if(!((countryName[0] === 'Australia')||(countryName[0] ===  'New Zealand')||(countryName[0] === 'Portugal')||(countryName[0] === 'Belgium')||(countryName[0] === 'Malta')||(countryName[0] === 'Germany')||(countryName[0] === 'Cyprus'))&&( _obj.txtDayNo.value.length != 10))
		{
		    _obj.borderViewS15.backgroundColor=  '#ff0000'; //red
			
		  }
  
   else
	{
		_obj.borderViewS15.backgroundColor= '#228b22';//dark lime green;
	}
	}catch(e){}
	});	  
	_obj.txtDayNo.addEventListener('blur',function(){
		try{
		setTimeout(function(){
			
	try{	
	if(_obj.txtDayNo.value === '' || _obj.txtDayNo.value === '*Phone Number')
	{
		_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
		//require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('The phone number is required');

		}
		else
		{
			var size = 250;
			require('/utils/AlertDialog').iOSToast1('The phone number is required',size);
		}
		

	}
	}catch(e){}
		},2000);
		
	//Adding mobile no validations same  as desktop as requirement from TOML on 17 Feb 17  -- added by Sanjivani
			
		if((countryName[0] === 'Germany') && (_obj.txtDayNo.value.length != 14))
		{
				_obj.borderViewS15.backgroundColor=  '#ff0000'; //red							
	        if(TiGlobals.osname === 'android')
		          {
			        require('/utils/AlertDialog').toast('Length should be 14 digits');

		          }
		    else
		          {
		          	var size = 250;
			require('/utils/AlertDialog').iOSToast1('Length should be 14 digits',size);
		         }
		}
	 else if(((countryName[0] === 'Malta')||(countryName[0] === 'Belgium')||(countryName[0] === 'Cyprus')) &&( _obj.txtDayNo.value.length != 8))
		{
				_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
			if(TiGlobals.osname === 'android')
		          {
			        require('/utils/AlertDialog').toast('Length should be 08 digits');

		          }
		    else
		          {
		          	var size = 250;
			require('/utils/AlertDialog').iOSToast1('Length should be 08 digits',size);
		         }
		         							
		}
	else if(((countryName[0] === 'Australia')||(countryName[0] === 'New Zealand')||(countryName[0] === 'Portugal')) && ( _obj.txtDayNo.value.length != 9))
	   {
	   		_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
	   	  if(TiGlobals.osname === 'android')
		          {
			        require('/utils/AlertDialog').toast('Length should be 09 digits');

		          }
		    else
		          {
		          	var size = 250;
			require('/utils/AlertDialog').iOSToast1('Length should be 09 digits',size);
		         }
		         
        }
    else if(!((countryName[0] === 'Australia')||(countryName[0] ===  'New Zealand')||(countryName[0] === 'Portugal')||(countryName[0] === 'Belgium')||(countryName[0] === 'Malta')||(countryName[0] === 'Germany')||(countryName[0] === 'Cyprus'))&&( _obj.txtDayNo.value.length != 10))
		{
		    _obj.borderViewS15.backgroundColor=  '#ff0000'; //red
			 if(TiGlobals.osname === 'android')
		          {
			        require('/utils/AlertDialog').toast('Length should be 10 digits');

		          }
		    else
		          {
		          	var size = 250;
			require('/utils/AlertDialog').iOSToast1('Length should be 10 digits',size);
		         }
		         
		  }
  else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDayNo.value) === false)
	{
		_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
		//require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Space not allowed at the beginning of phone number');

		}
		else
		{
			var size = 250;
			require('/utils/AlertDialog').iOSToast1('Space not allowed at the beginning of phone number',size);
		}
		

	}
	else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDayNo.value) === false)
	{
		_obj.borderViewS15.backgroundColor=  '#ff0000'; //red
		//require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Space not allowed at the end of phone number');

		}
		else
		{
			var size = 250;
			require('/utils/AlertDialog').iOSToast1('Space not allowed at the end of phone number',size);
		}
		

	}	  
		else
	{
		_obj.borderViewS15.backgroundColor= '#228b22';//dark lime green;
		// Call LEADREQ
						
				var xhrLead = require('/utils/XHR');

				xhrLead.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"LEADREQ",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"name":"'+_obj.txtFirstName.value + ' ' + _obj.txtLastName.value+'",'+ 
						'"remitEmailId":"'+_obj.txtEmail.value+'",'+
						'"phoneNumber":"'+_obj.txtDayNo.value+'",'+
						'"country":"'+countryName[0]+'",'+
						'}',
					success : xhrLeadSuccess,
					error : xhrLeadError,
					contentType : 'application/json',
					timeout : 10000
				});
		
				function xhrLeadSuccess(e) {
			
					xhrLead = null;
				}
		
				function xhrLeadError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhrLead = null;
				}
	}
	}catch(e){}
	});	  

	
	
	
	/*//Day time no.
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtMobile);
	//_obj.txtDayNo.hintText = 'Day Time Number*';
	//_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDayNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
	
	_obj.txtDayNo.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS16= Ti.UI.createView(_obj.style.borderView);*/
	
	//Gender
	_obj.genderView = Ti.UI.createView(_obj.style.genderView);
	_obj.lblGender = Ti.UI.createLabel(_obj.style.lblGender);
	_obj.lblGender.text = '*Gender';
	_obj.lblMale = Ti.UI.createLabel(_obj.style.lblMale);
	_obj.lblFemale = Ti.UI.createLabel(_obj.style.lblFemale);
	_obj.borderViewSG= Ti.UI.createView(_obj.style.borderView);
	_obj.lblMale.addEventListener('click',function(e){
		e.source.backgroundImage = '/images/male_sel.png';
	    _obj.lblFemale.backgroundImage = '/images/female_unsel.png';
	    _obj.gender = 'M';
		_obj.borderViewSG.backgroundColor= '#228b22';//dark lime green;

	});
	
	_obj.lblFemale.addEventListener('click',function(e){
		e.source.backgroundImage = '/images/female_sel.png';
	    _obj.lblMale.backgroundImage = '/images/male_unsel.png';
	    _obj.gender = 'F';
		_obj.borderViewSG.backgroundColor= '#228b22';//dark lime green;

	});
	
  //////////////////////Gender validations////////////////////
    if (TiGlobals.osname === 'android') {
	_obj.genderView.addEventListener('blur',function(){
		
		if(_obj.gender === null)
		{
			_obj.borderViewSG.backgroundColor=  '#ff0000'; //red
			//require('/utils/AlertDialog').showAlert('','Please select Gender',[L('btn_ok')]).show();
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Please select Gender');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Please select Gender');
			}
			
		}
		else{
			_obj.borderViewSG.backgroundColor= '#228b22';//dark lime green;
		}
		
	});
}else{

   _obj.genderView.addEventListener('touchend',function(){
		
		if(_obj.gender === null)
		{
			_obj.borderViewSG.backgroundColor=  '#ff0000'; //red
			//require('/utils/AlertDialog').showAlert('','Please select Gender',[L('btn_ok')]).show();
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Please select Gender');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Please select Gender');
			}
			
		}
		else{
			_obj.borderViewSG.backgroundColor= '#228b22';//dark lime green;
		}
		
	});




}
	


	//CitizenShip
	_obj.citizenshipView = Ti.UI.createView(_obj.style.citizenshipView);
	_obj.lblCitizenship = Ti.UI.createLabel(_obj.style.lblCitizenship);
	_obj.lblCitizenship.text = '*Country of Citizenship';
	_obj.imgCitizenship = Ti.UI.createImageView(_obj.style.imgCitizenship);
	_obj.borderViewS18 = Ti.UI.createView(_obj.style.borderView);
	
	//_obj.btnStep1 = Ti.UI.createButton(_obj.style.btnStep1);
	//_obj.btnStep1.title = 'CONTINUE';
	
try{
	_obj.txtPromoCode = Ti.UI.createTextField(_obj.style.txtFirstName);
	//_obj.txtPromoCode .hintText = 'Do you have promo code?';//Commenting on 04-July-17 for CMN67
	_obj.txtPromoCode .hintText = 'Do you have a promo code?'; 
	_obj.borderViewS33= Ti.UI.createView(_obj.style.borderView);
}catch(e){}
	////////////////////// Promo code validations////////////////////
	_obj.txtPromoCode.addEventListener('focus',function(){
		try{
		_obj.borderViewS33.backgroundColor= '#007fff'; //azure
		}catch(e){}
		});
		
		try{
		_obj.txtPromoCode.addEventListener('blur',function(){
			try{
		_obj.borderViewS33.backgroundColor='#000000'; //black
		}catch(e){}
	});
	}catch(e){}

	/*_obj.txtPromoCode.addEventListener('blur',function(){
		try{
		if(_obj.txtPromoCode.value === '')
		{
			_obj.borderViewS33.backgroundColor= '#ff0000';
			
		}
		else{
			_obj.borderViewS33.backgroundColor= '#228b22';//dark lime green;
			}
		}catch(e){}
	});

	_obj.txtPromoCode.addEventListener('change',function(){
		try{
			if(_obj.txtPromoCode.value === '')
			{
				_obj.borderViewS33.backgroundColor= '#ff0000';
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Please enter your Promo Code');
				
				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Please enter your Promo Code');
				}
				
			}
			else{
				_obj.borderViewS33.backgroundColor= '#228b22';//dark lime green;
				}
		}catch(e){}
});*/
	//Adding components in step 1
	//First Name

	_obj.stepView1.add(_obj.txtFirstName);
	_obj.stepView1.add(_obj.borderViewS11);
	
	//Last Name
	_obj.stepView1.add(_obj.txtLastName);
	_obj.stepView1.add(_obj.borderViewS12);
	//Email ID
	_obj.stepView1.add(_obj.txtEmail);
	
	_obj.stepView1.add(_obj.borderViewS13);
	_obj.stepView1.add(_obj.lblEmailMsg); //newly added by sanjivani on 06-06-17
	//Mobile no
	_obj.mobileView.add(_obj.lblDay);
	_obj.mobileView.add(_obj.txtDayNo);
	_obj.stepView1.add(_obj.mobileView);
	_obj.stepView1.add(_obj.borderViewS15);
	
	//Password
	//_obj.stepView1.add(_obj.lblUsername);
	//_obj.stepView1.add(_obj.txtPassword);
	_obj.stepView1.add(_obj.txtPassword);
	_obj.stepView1.add(_obj.borderViewS31);
	_obj.stepView1.add(_obj.txtConfirmPassword);
	_obj.stepView1.add(_obj.borderViewS32);
	//_obj.stepView1.add(_obj.lblPasswordValidate);
	
	//Country of citizenship
	_obj.citizenshipView.add(_obj.lblCitizenship);
	_obj.citizenshipView.add(_obj.imgCitizenship);
	_obj.stepView1.add(_obj.citizenshipView);
	_obj.stepView1.add(_obj.borderViewS18);
	
	
	/*Day time no.
	 * _obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDayNo);
	_obj.stepView1.add(_obj.dayView);
	_obj.stepView1.add(_obj.borderViewS16);*/
	
	//Gender
	_obj.genderView.add(_obj.lblGender);
	_obj.genderView.add(_obj.lblMale);
	_obj.genderView.add(_obj.lblFemale);
	_obj.stepView1.add(_obj.genderView);
	_obj.stepView1.add(_obj.borderViewSG);
	
	//DOB
	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.stepView1.add(_obj.dobView);
	_obj.stepView1.add(_obj.borderViewS14);
	
	//Promo code --new field
	//_obj.stepView1.add(_obj.txtPromoCode);  //FOR CMN 69 on 26th July
	//_obj.stepView1.add(_obj.borderViewS33);
	
	//How did you hear about us
    
	_obj.HDYView = Ti.UI.createView(_obj.style.HDYView);
	_obj.lblHDY = Ti.UI.createLabel(_obj.style.lblHDY);
	_obj.lblHDY.text = 'How did you hear about us?';
	_obj.imgHDY = Ti.UI.createImageView(_obj.style.imgHDY);
	_obj.borderViewS41 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtHDY = Ti.UI.createTextField(_obj.style.txtHDY);
	_obj.txtHDY.hintText = '*Enter Source Name';
	//_obj.txtHDY.editable = false;
	_obj.borderViewS42 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtHDY.addEventListener('focus',function(){
		/*if(_obj.lblHDY.text == 'How did you hear about us?' ||_obj.lblHDY.text == 'Select Source*'){
			_obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY
			}
			else{*/
				_obj.borderViewS42.backgroundColor= '#007fff'; //azure
			//}

	});
	//necessary code but commenting as TOML requirement to make it as desktop on 17 feb 17
	
	/*_obj.txtHDY.addEventListener('change',function(){
		if(_obj.lblHDY.text == 'How did you hear about us?' ||_obj.lblHDY.text == 'Select Source*'){
		_obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY
		}
		else{
			_obj.borderViewS42.backgroundColor='#228b22';//dark lime green;

		}
     });
*/    
_obj.txtHDY.addEventListener('change',function(){
		 if(_obj.txtHDY.value.length < 1){
            if (TiGlobals.osname === 'android') {
		      //_obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY
             }else{
                   _obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY
              }
		}
		else{
			_obj.borderViewS42.backgroundColor='#228b22';//dark lime green;

		}
     });

	_obj.txtHDY.addEventListener('blur',function(){
		/*if(_obj.lblHDY.text == 'How did you hear about us?' ||_obj.lblHDY.text == 'Select Source*'){
			_obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY
			}*/
			try{
		 if(_obj.txtHDY.value == ''){
			_obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY
			if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Please enter the source');
				
				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Please enter the source');
				}
				
           }
			else{
				_obj.borderViewS42.backgroundColor='#228b22';//dark lime green;
			}
}catch(e){}
	});
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblTerms.text = 'By clicking on "I Agree" you are accepting Remit2India'+'s ';
	
	_obj.lblAgreement = Ti.UI.createLabel(_obj.style.lblAgreement);
	_obj.lblAgreement.text = 'Terms and Conditions ';
	
	_obj.lblAnd = Ti.UI.createLabel(_obj.style.lblAnd);
	_obj.lblAnd.text = '& ';
	
	_obj.lblPrivacy = Ti.UI.createLabel(_obj.style.lblPrivacy);
	_obj.lblPrivacy.text = 'Privacy Policy';
	
	_obj.btnRegister = Ti.UI.createButton(_obj.style.btnRegister);
	_obj.btnRegister.title = 'I Agree, Create My Account';
	
	_obj.stepView1.add(_obj.lblHow);
	_obj.HDYView.add(_obj.lblHDY);
	_obj.HDYView.add(_obj.imgHDY);
	_obj.stepView1.add(_obj.HDYView);
	_obj.stepView1.add(_obj.borderViewS41);
	_obj.stepView1.add(_obj.txtHDY);
	_obj.stepView1.add(_obj.borderViewS42);
	
	_obj.termsView.add(_obj.lblTerms);
	_obj.termsView.add(_obj.lblAgreement);
	_obj.termsView.add(_obj.lblAnd);
	_obj.termsView.add(_obj.lblPrivacy);
	_obj.stepView1.add(_obj.termsView);
	_obj.stepView1.add(_obj.btnRegister);
	
	_obj.lblAgreement.addEventListener('click',function(e){
		require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
	});
	
	_obj.lblPrivacy.addEventListener('click',function(e){
		//require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
		require('/js/StaticPagesModal').StaticPagesModal('Privacy Policy','about','privacy-policy');
	
	});
	
	
	
	//_obj.stepView1.add(_obj.btnStep1);
	
	_obj.btnRegister.addEventListener('click',function(e){
		
		var fName = require('/lib/toml_validations').trim(_obj.txtFirstName.value);
		var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
		
		////////////////////// First Name ////////////////////
    	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
			return;
		}
		else if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in First Name field',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(fName.length <= 1 && fName.length > 0)
		{	
			require('/utils/AlertDialog').showAlert('','First Name cannot be a single alphabet. Please enter your complete First Name',[L('btn_ok')]).show();
    		_obj.txtFirstName.focus();
    		return;
		}
		else
		{
			
		}
		////////////////////// Last Name ////////////////////
		var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
		if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
    		return;
		}
		else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Last Name field',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
			return;
		}
		else if(lName.length <= 1 && lName.length > 0) // Last NAME SHOULD BE GREATER THEN 1
		{	
			require('/utils/AlertDialog').showAlert('','Last Name cannot be a single alphabet. Please enter your complete Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
			return;
		}
		else
		{
			
		}
		//Pallavi code:defect no.116
		/*if(_obj.txtLastName.value === _obj.txtFirstName.value)
		{
			require('/utils/AlertDialog').showAlert('','First Name and Last Name cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDayNo.focus();
			return;
		}*/
		
		////////////////////// Email ////////////////////
		if(_obj.txtEmail.value === '' || _obj.txtEmail.value === 'Email')
		{
			try{
			require('/utils/AlertDialog').showAlert('','Please enter Email Address',[L('btn_ok')]).show();
    		_obj.txtEmail.focus();
			return;
			}
			catch(e){
				
			}
		}
		else if((require('/lib/toml_validations').isEmail(_obj.txtEmail.value)) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Email Address',[L('btn_ok')]).show();
    		_obj.txtEmail.focus();
	
			return;
		}
		else
		{
			
		}
		
		////////////////////// Day Time  No ////////////////////
		if(_obj.txtDayNo.value === '' || _obj.txtDayNo.value === 'Mobile Number*')
	{
		require('/utils/AlertDialog').showAlert('','The phone number is required',[L('btn_ok')]).show();
        return;
	}
		
	//Adding mobile no validations same  as desktop as requirement from TOML on 17 Feb 17  -- added by Sanjivani
			//return for phone number is added on 22-june-17 by sanjivani
		else if((countryName[0] === 'Germany') && (_obj.txtDayNo.value.length != 14))
		{
			require('/utils/AlertDialog').showAlert('','Length should be 14 digits',[L('btn_ok')]).show();
			return;				
	      
		}
	 else if(((countryName[0] === 'Malta')||(countryName[0] === 'Belgium')||(countryName[0] === 'Cyprus')) &&( _obj.txtDayNo.value.length != 8))
		{
			require('/utils/AlertDialog').showAlert('','Length should be 08 digits',[L('btn_ok')]).show();
            return;	
									
		}
	else if(((countryName[0] === 'Australia')||(countryName[0] === 'New Zealand')||(countryName[0] === 'Portugal')) && ( _obj.txtDayNo.value.length != 9))
	   {
	   	require('/utils/AlertDialog').showAlert('','Length should be 09 digits',[L('btn_ok')]).show();
           return;	
		         
        }
    else if(!((countryName[0] === 'Australia')||(countryName[0] ===  'New Zealand')||(countryName[0] === 'Portugal')||(countryName[0] === 'Belgium')||(countryName[0] === 'Malta')||(countryName[0] === 'Germany')||(countryName[0] === 'Cyprus'))&&( _obj.txtDayNo.value.length != 10))
		{
		require('/utils/AlertDialog').showAlert('','Length should be 10 digits',[L('btn_ok')]).show();
            return;	
		    
		  }
  else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDayNo.value) === false)
	{
	require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of phone number',[L('btn_ok')]).show();
		 return;	

	}
	else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDayNo.value) === false)
	{
	  require('/utils/AlertDialog').showAlert('','Space not allowed at the end of phone number',[L('btn_ok')]).show();
         return;	
	}	  
		else
	{
	}
		
		//Password and confirm password validation
		
		if(_obj.txtPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter password',[L('btn_ok')]).show();
    		_obj.txtPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassword.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of password',[L('btn_ok')]).show();
    		_obj.txtPassword.focus();
			return;
		}
		else if(_obj.txtConfirmPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Re-type password',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtConfirmPassword.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Re-type password',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else if(_obj.txtPassword.value.length != 0 && _obj.txtConfirmPassword.value.length != 0)
		{
			/*if(!((/\d/.test(_obj.txtPassword.value)) && (/[a-z]/i.test(_obj.txtPassword.value))))
			{
				require('/utils/AlertDialog').showAlert('','Password should be alpha-numeric',[L('btn_ok')]).show();
	    		_obj.txtPassword.focus();
	    		_obj.txtConfirmPassword.value = '';
				return;
			}
			else */if(_obj.txtPassword.value.length < 8 || _obj.txtPassword.value.length > 20)
			{
				require('/utils/AlertDialog').showAlert('','Password should be of minimum 8 and maximum 20 characters',[L('btn_ok')]).show();
	    		_obj.txtPassword.focus();
	    		_obj.txtConfirmPassword.value = '';
				return;
			}
			else if(_obj.txtPassword.value != _obj.txtConfirmPassword.value)
			{
				require('/utils/AlertDialog').showAlert('','Password mismatched please retype your password',[L('btn_ok')]).show();
	    		_obj.txtConfirmPassword.focus();
				return;
			}
			else
			{
				
			}
		}
		
		
		//Citizenship Validation Start Here
		if(_obj.lblCitizenship.text === '' || _obj.lblCitizenship.text === 'Citizenship*'||_obj.lblCitizenship.text === '*Country of Citizenship')
		{
			//_obj.lblCitizenship.text = '*Country of Citizenship';
			require('/utils/AlertDialog').showAlert('','Please select Citizenship',[L('btn_ok')]).show();
    		_obj.lblCitizenship.text = 'Citizenship*';
			return;	
		}
		else
		{
			// Do nothing
		}
		
		////////////////////// Gender ////////////////////
		if(_obj.gender === null)
		{
			require('/utils/AlertDialog').showAlert('','Please select Gender',[L('btn_ok')]).show();
    		return;
		}
		
		////////////////////// DOB ////////////////////
		if(_obj.lblDOB.text === '' || _obj.lblDOB.text === '*Date of Birth')
		{
			require('/utils/AlertDialog').showAlert('','Please Enter your Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = '*Date of Birth';
			return;
		}
		else
		{
			
		}
		
		//Promo field validation
		
		if(_obj.txtPromoCode.value === '')
			{
			_obj.promoValue = 'F';
           
        }
        else{
        	_obj.promoValue = _obj.txtPromoCode.value;
        }
		/*	if(_obj.txtPromoCode.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Promo Code',[L('btn_ok')]).show();
               return;
           }*/
           
           //How did you hear about us validation
	if(_obj.lblHDY.text == 'How did you hear about us?' ||_obj.lblHDY.text == 'Select Source*'){
		require('/utils/AlertDialog').showAlert('','Please enter the source',[L('btn_ok')]).show();
          return;
			}
			
	 //Khow About US source validation

			try{
		 if(_obj.txtHDY.value == ''){
		
					require('/utils/AlertDialog').showAlert('','Please enter the source',[L('btn_ok')]).show();
              return;
              }
		
}catch(e){}
	
	///Call remit registration api///
		
		activityIndicator.showIndicator();
		var dobSplit = _obj.lblDOB.text.split('/');
		var dob = dobSplit[0] + '-' + dobSplit[1] + '-' + dobSplit[2];
			
		/*///Thratmetrix  Implementation ///
  			
////////////IP Address ////////////
 var IPADDRESS;
	
			
			
			var url ='https://mstage.remit2india.com/mobileApp/tm-registration.php';
			//?ipAddress='+TiGlobals.ipAddress+'&loginId='+_obj.txtUserName.value+'&password='+Ti.Utils.md5HexDigest(_obj.txtPassword.value)+'&noofAttempts=1';

			
			
			var xhr = Ti.Network.createHTTPClient({timeout:50000});
			xhr.onload = function(e) { 
			    var json = this.responseText;
			     Ti.API.info("Response is:------>",json);
			   var json2 = json.split("*~*");
			    Ti.API.info("In Json2 is:------>",json2[1]);
			     var response = JSON.parse(json2[1]);
			     Ti.API.info("Received text16: " + response.responseFlag);
			    
			  if(response.responseFlag === "S")
				{					
		

				    Ti.App.Properties.setString('ownerId',response.ownerId);
					Ti.App.Properties.setString('loginId',response.loginId);
					Ti.App.Properties.setString('sessionId',response.sessionId);
					
					
					TiGlobals.OwnIdForAddrUpdate = Ti.App.Properties.getString('ownerId');
				activityIndicator.hideIndicator();
			
			var name = _obj.txtFirstName.value;
					
               ///Getuserdetails
		        // TiGlobals.gender = response.gender;
			     TiGlobals.knowAboutUs = response.knowAboutUs;
			     TiGlobals.nationality = response.nationality;///////
			//require('/js/RegisterModalSuccess').RegisterSuccess(gender,knowAboutUs,nationality);
				require('/js/RegisterModalSuccess').RegisterSuccess(name);
				destroy_register();
				/*
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(L('register_success'));
				}
				else
				{
			
					require('/utils/AlertDialog').iOSToast(L('register_success'));
				}  //comment end
				
			}
			else
			{
				if(response.message === L('invalid_session') || response.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',response.message,[L('btn_ok')]).show();
				}
			}

			    //var response = JSON.parse(json2[2]);
			  //  Ti.API.info("Received text1: " + json2[1]);
			      Ti.App.Properties.setString('ipAddress', response.ipAddress);
			     Ti.App.Properties.setString('firstName', response.firstName);
			    Ti.App.Properties.setString('middleName', response.middleName);
			    Ti.App.Properties.setString('lastName', response.lastName);
			   Ti.App.Properties.setString('country', response.country);
			   Ti.App.Properties.setString('dayTimePhone', response.dayTimePhone);
			    Ti.App.Properties.setString('mobileNumber', response.mobileNumber);
			    Ti.App.Properties.setString('remitEmailId', response.remitEmailId);
			    Ti.App.Properties.setString('dob', response.dob);
			  Ti.App.Properties.setString('gender',response.gender);
			     Ti.App.Properties.setString('knowAboutUs', response.knowAboutUs);
			      //Ti.API.info("Received text13 " + response.address);
			       Ti.App.Properties.setString('address', response.address);
			        Ti.App.Properties.setString('state', response.state);
			        Ti.App.Properties.setString('city', response.city);
			         Ti.App.Properties.setString('zip', response.zip);
			          Ti.App.Properties.setString('password', response.password);
			           Ti.App.Properties.setString('nationality', response.nationality);
			            Ti.App.Properties.setString('occupation', response.occupation);
			             Ti.App.Properties.setString('assureTransfer', response.assureTransfer);
			      		
		
			xhr = null;
		};
  			
			xhr.onerror = function(e) {
			    Ti.API.info('Error >>>> ' + JSON.stringify(e));
			    //Ti.API.info("S Response2 is:" +JSON.parse(this.responseText));
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			};    

			xhr.open("POST",url);
			  var params = {

				    ipAddress:TiGlobals.ipAddress,

				    firstName:_obj.txtFirstName.value,

				    middleName:'',

				    lastName:_obj.txtLastName.value,

				    country:countryName[0],

				    dayTimePhone:_obj.lblDay.text+_obj.txtDayNo.value,

				    mobileNumber:'F',

                    offPhone:'F',
				    remitEmailId:_obj.txtEmail.value,

				    dob:dob,

				    gender:_obj.gender,

				    knowAboutUs:_obj.lblHDY.text+'-'+_obj.txtHDY.value,
                    state:'F',

				    city:'F',

				    zip:'F',

				    password:require('/lib/Crypt/Base64').encode(_obj.txtPassword.value),

				    nationality:_obj.lblCitizenship.text,

				    occupation:'',

				    assureTransfer:'N',


				    flat:'F',

				    building:'F',

				    street1:'F',

				    street2:'F',

				    Locality:'F',

				    subLocality:'F',
                    tncFlag :'Y',
                    
                    promoCode: _obj.promoValue
				    };  

				 
				  Ti.API.info('PARAMS >>>> ',JSON.stringify(params));
                  xhr.send(params);
		});*/
		/*var occupation = _obj.lblOccupation.text;
		
		// if Arizona US then update user details
		if(countryName[0] === 'United States' && _obj.state === 'Arizona')
		{
			if(occupation === 'Others')
			{
				occupation = _obj.txtOccupation.value;
			}
		}
		else
		{
			occupation = '';
		}*/
	
	   var xhr = require('/utils/XHR');
		xhr.call({
			
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"REMITREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				//'"channelId":"'+TiGlobals.channelId+'",'+
				'"channelId":"'+TiGlobals.channel+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"firstName":"'+_obj.txtFirstName.value+'",'+
				'"middleName":"",'+
				'"lastName":"'+_obj.txtLastName.value+'",'+
				'"country":"'+countryName[0]+'",'+
				'"dayTimePhone":"'+_obj.lblDay.text+_obj.txtDayNo.value+' ",'+
				'"mobileNumber":"F",'+
				'"offPhone":"F",'+
				'"remitEmailId":"'+_obj.txtEmail.value+'",'+
				'"dob":"'+dob+'",'+
				'"gender":"'+_obj.gender+'",'+
				'"knowAboutUs":"'+_obj.lblHDY.text+'-'+_obj.txtHDY.value+'",'+
				'"address":{"flat":"F","building":"F","street1":"F","street2":"F","Locality":"F","subLocality":"F"},'+
				'"state":"F",'+
				'"city":"F" ,'+
				'"zip":"F",'+
				'"password":"'+require('/lib/Crypt/Base64').encode(_obj.txtPassword.value)+'",'+
				'"nationality":"'+_obj.lblCitizenship.text+'",'+
				'"occupation":"",'+
				'"assureTransfer":"N",'+
				
				'"tncFlag" :"Y",'+
				'"promoCode": "'+_obj.promoValue+'"'+
				'}',
				    				
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

function xhrSuccess(e) {
			if(e.result.responseFlag === "S")
			{

				    Ti.App.Properties.setString('ownerId',e.result.ownerId);
					Ti.App.Properties.setString('loginId',e.result.loginId);
					Ti.App.Properties.setString('sessionId',e.result.sessionId);
					
					
					TiGlobals.OwnIdForAddrUpdate = Ti.App.Properties.getString('ownerId');
				activityIndicator.hideIndicator();
			
			var name = _obj.txtFirstName.value;
					Ti.App.Properties.setString('gender',_obj.gender);
					var knowaboutus = _obj.lblHDY.text+'-'+_obj.txtHDY.value;
					Ti.App.Properties.setString('knowAboutUs',knowaboutus);
					Ti.App.Properties.setString('nationality',_obj.lblCitizenship.text);
               ///Getuserdetails
		         /*TiGlobals.gender = e.result.gender;
			     TiGlobals.knowAboutUs = e.result.knowAboutUs;
			     TiGlobals.nationality = e.result.nationality;///////*/
			//require('/js/RegisterModalSuccess').RegisterSuccess(gender,knowAboutUs,nationality);
				require('/js/RegisterModalSuccess').RegisterSuccess(name);
				destroy_register();
				/*//for comment
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(L('register_success'));
				}
				else
				{
			
					require('/utils/AlertDialog').iOSToast(L('register_success'));
				} //for comment */
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	
	});
	
	
	/////////////////// Step 2 ///////////////////
	//All code moved in updateuserdetails.js
	
	
	/////////////////// Step 3 ///////////////////
	
	
	/////////////////// Step 4 ///////////////////
	
	
	_obj.HDYView.addEventListener('click',function(e){
		_obj.borderViewS41.backgroundColor= '#007fff'; //azure
		activityIndicator.showIndicator();
		var howOptions = [];
		var howOptionsTxt = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"howdidyouhereaboutus",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].howdidyouhereaboutus.length;i++)
				{
					howOptions.push(e.result.response[0].howdidyouhereaboutus[i].title);
					howOptionsTxt.push(e.result.response[0].howdidyouhereaboutus[i].asso_text);
				}
				
				var optionDialogHow = require('/utils/OptionDialog').showOptionDialog('How did you hear about us?',howOptions,-1);
				optionDialogHow.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogHow.addEventListener('click',function(evt){
					if(optionDialogHow.options[evt.index] !== L('btn_cancel'))
					{   //_obj.txtHDY.editable = true;
						_obj.borderViewS41.backgroundColor= '#228b22';//dark lime green;
						_obj.lblHDY.text = optionDialogHow.options[evt.index];
						_obj.txtHDY.hintText = howOptionsTxt[evt.index];
						optionDialogHow = null;
						
						// MOR validate
						
						_obj.doneHDY = Ti.UI.createButton(_obj.style.done);
						_obj.doneHDY.addEventListener('click',function(){
							_obj.txtHDY.blur();
						});
						
						if(_obj.lblHDY.text === 'Money on Referral (MOR)')
						{
							_obj.txtHDY.value = '';
							_obj.txtHDY.maxLength = 7;
							_obj.txtHDY.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
							_obj.txtHDY.keyboardToolbar = [_obj.doneHDY];
						}
						else
						{
							_obj.txtHDY.value = '';
							_obj.txtHDY.maxLength = 500;
							_obj.txtHDY.keyboardType = Ti.UI.KEYBOARD_DEFAULT;
							_obj.txtHDY.keyboardToolbar = [];
						}
						optionDialogHow = null;
					}
					else
					{
						_obj.borderViewS41.backgroundColor= '#ff0000'; //red  //lblHDY
						//_obj.borderViewS42.backgroundColor= '#ff0000'; //red  //_obj.txtHDY

						optionDialogHow = null;
						_obj.lblHDY.text = 'Select Source*';
						//_obj.txtHDY.editable = false;
						_obj.txtHDY.hintText = 'Enter Source Name*';
						
					}
				});	
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
			}
			xhr = null;
			}catch(e){
				alert("Request timeout.Please try again later.");
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	
	
	
	function citizenship()
	{
		activityIndicator.showIndicator();
		var citizenshipOptions = [];
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+Ti.App.Properties.getString('destinationCountryCurName')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
				
				for(var i=0;i<splitArr.length;i++)
				{
					citizenshipOptions.push(splitArr[i]);
				}
				
				var optionDialogCitizenship = require('/utils/OptionDialog').showOptionDialog('Citizenship',citizenshipOptions,-1);
				optionDialogCitizenship.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogCitizenship.addEventListener('click',function(evt){
					if(optionDialogCitizenship.options[evt.index] !== L('btn_cancel'))
					{
						_obj.borderViewS18.backgroundColor= '#228b22';//dark lime green;

						_obj.lblCitizenship.text = optionDialogCitizenship.options[evt.index];
						optionDialogCitizenship = null;
					}
					else
					{
						_obj.borderViewS18.backgroundColor= '#ff0000'; //red
						optionDialogCitizenship = null;
						_obj.lblCitizenship.text = 'Citizenship*';
						if(TiGlobals.osname === 'android')
						{
							require('/utils/AlertDialog').toast('Please select Citizenship');

						}
						else
						{
							require('/utils/AlertDialog').iOSToast('Please select Citizenship');
						}
						
					}
				});	
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			_obj.borderViewS18.backgroundColor= '#ff0000'; //red
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').toast('Please select Citizenship');

			}
			else
			{
				require('/utils/AlertDialog').iOSToast('Please select Citizenship');
			}
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.citizenshipView.addEventListener('click',function(){
		_obj.borderViewS18.backgroundColor= '#007fff'; //azure
      
		citizenship();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_register();
	});
	
	//_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2,_obj.stepView3,_obj.stepView4];
	_obj.scrollableView.views = [_obj.stepView1];
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winRegister.add(_obj.globalView);
	_obj.winRegister.open();
	
	_obj.winRegister.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_register();
				alertDialog = null;
			}
		});
	});
	
	function destroy_register()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winRegister.close();
			require('/utils/RemoveViews').removeViews(_obj.winRegister);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_register',destroy_register);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_register', destroy_register);
	}
	catch(e){
		Ti.API.info(e);
	}
}; // RegisterModal()
