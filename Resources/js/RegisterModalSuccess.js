exports.RegisterSuccess = function(name)
{

	require('/lib/analytics').GATrackScreen('Register Success');
	
	var _obj = {
		style : require('/styles/RegisterSuccess').RegiSuccess,
		winConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		imgInfo : null,
		lblInfo : null,
		infoTextView : null,
		lblAddressHeader : null,
		lblAddress : null,
		infoMail : null
		
	};
	//TiGlobals.nameForAddrUpdate = loginId;
	_obj.RegiSuccess = Ti.UI.createWindow(_obj.style.winRegiSuccess);
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Registration';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
		_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView.add(_obj.lblHeader);
	//_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);

	_obj.globalView.add(_obj.mainView);
	_obj.RegiSuccess.add(_obj.globalView);
	_obj.RegiSuccess.open();
	
                _obj.infoMail = Ti.UI.createLabel(_obj.style.infoMail);
                _obj.infoMail.text = Ti.App.Properties.getString('sourceCountryInfoEmail');
                _obj.infoMail.color = 'red';
                //var infoMail = _obj.infoMail.text;
                
                _obj.lblAddressHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
				_obj.lblAddressHeader.text = 'Congratulations '+name+'!';
				
				_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
				_obj.lblAddress.text = 'Your registration is almost complete with Remit2India.A couple of details more that we need.'+'\n\n'+'A confirmation mail will be sent to your registered email address. In case the mail does not reflect in your inbox please check your Spam/Junk folder for the same. To avoid further inconvenience in receiving important notifications, we recommend you add '+_obj.infoMail.text+' to your safe sender list.';
				
				_obj.mainView.add(_obj.lblAddressHeader);
				_obj.mainView.add(_obj.lblAddress);
				
				Ti.API.info("IN REGISTRATION:---NAME",name);
				
				 setTimeout(function(){ 
					       if(TiGlobals.OwnIdForAddrUpdate == Ti.App.Properties.getString('ownerId') && TiGlobals.addressUpdate == false){
				      // if( TiGlobals.addressUpdate == false){
  
					       
							//require('/js/updateuserdetails').addressUpdate(logId,sesId,ownId);
							require('/js/updateuserdetails').addressUpdate(name);
							//require('/js/updateuserdetails').addressUpdate();
							destroy_regiSuccess();
					       }
					       else{
					    	   destroy_regiSuccess();   
					       }
				    },
						
				    4000); 
				
	//Destroy Register success page		
destroy_regiSuccess = function ()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration success start ##############');
			
			_obj.RegiSuccess.close();
			require('/utils/RemoveViews').removeViews(_obj.RegiSuccess);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_regiSuccess',destroy_regiSuccess);
			require('/utils/Console').info('############## Remove registration success end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	};
	
	Ti.App.addEventListener('destroy_regiSuccess',destroy_regiSuccess);
}; // RegisterModalSuccess()
	
	
			
				