function Menu(_self)
{
	////////// Include Style //////////
	var style = require('/styles/Menu').Menu;
	
	var _menuView = null;
	var _mainView = null;
	var _menuScrollableView = null;
	var _view1 = null;
	var _view2 = null;
	var _view3 = null;
	var _view4 = null;
	var _exchangeView = null;
	var _transferView = null;
	var _transferLockView = null;
	var _sendingOptionsView = null;
	var _trackTransactionView = null;
	var _transactionLockView = null;
	var _myAccountView = null;
	var _accountLockView = null;
	var _earnView = null;
	var _faqView = null;
	var _aboutView = null;
	var _imgExchangeView = null;
	var _imgTransferView = null;
	var _imgSendingOptionsView = null;
	var _imgTrackTransactionView = null;
	var _imgMyAccountView = null;
	var _imgEarnView = null;
	var _imgFAQView = null;
	var _imgAboutView = null;
	
	var _topView = null;
	var _imgCall = null;
	var _imgMail = null;
	var _imgLocation = null;
	var _locationView = null;
	var _lblLocation = null;
	var _lblLogout = null;
	
	var _data = [];
	
	//////// Sending Options SubView ////////
	var _subSOView = null;
	var _subSOImage = null;
	var _subSOMainView = null;
	var _subSOBackImage = null;
	var _subSOTxt = null;
	
	//////// About SubView ////////
	var _subAboutView = null;
	var _subAboutImage = null;
	var _subAboutTxt1 = null;
	var _subAboutTxt2 = null;
	var _subAboutTxt3 = null;
	var _subAboutTxt4 = null;
	var _subAboutTxt5 = null;
	var _subAboutTxt6 = null;
	//var _subAboutTxt7 = null;
	var _subAboutMainView = null;
	var _subAboutBackImage = null;
	
	//////////////////// Menu ////////////////////
	
	_menuView = Ti.UI.createView(style.menuView);
	_mainView = Ti.UI.createScrollView(style.mainView);
	_menuScrollableView = Ti.UI.createScrollableView(style.scrollableView);
	
	_topView = Ti.UI.createView(style.topView);
	_imgCall = Ti.UI.createImageView(style.imgCall);
	_imgMail = Ti.UI.createImageView(style.imgMail);
	_imgLocation = Ti.UI.createImageView(style.imgLocation);
	_locationView = Ti.UI.createView(style.locationView);
	_lblLocation = Ti.UI.createLabel(style.lblLocation);
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	_lblLocation.text = countryName[0];
	
	function changeLocation(e){
		var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
		_lblLocation.text = countryName[0];
	}
	
	Ti.App.addEventListener('changeLocation',changeLocation);
	
	_imgCall.addEventListener('click',function(e){
		Ti.Platform.openURL('tel:'+Ti.App.Properties.getString('sourceCountryTollFree'));
	});
	
	_imgMail.addEventListener('click',function(e){
		var emailDialog = Titanium.UI.createEmailDialog();
		emailDialog.toRecipients = [Ti.App.Properties.getString('sourceCountryInfoEmail')];
		emailDialog.open();
	});
	
	_locationView.addEventListener('click',function(e){
		require('/js/LocationModal').LocationModal();
	});
	
	_imgLocation.addEventListener('click',function(e){
		require('/js/LocationModal').LocationModal();
	});
	
	var _view1 = Ti.UI.createView(style.view1);
	var _view2 = Ti.UI.createView(style.view2);
	var _view3 = Ti.UI.createView(style.view3);
	var _view4 = Ti.UI.createView(style.view4);
	
	_exchangeView = Ti.UI.createView(style.exchangeView);
	_imgExchangeView = Ti.UI.createImageView(style.imgExchangeView);
	_exchangeView.sel = 'exchange';
	_imgExchangeView.sel = 'exchange';
	
	_transferView = Ti.UI.createView(style.transferView);
	_imgTransferView = Ti.UI.createImageView(style.imgTransferView);
	_transferLockView = Ti.UI.createImageView(style.imgLock);
	_transferView.sel = 'transfer';
	_imgTransferView.sel = 'transfer';
	
	_exchangeView.add(_imgExchangeView);
	_view1.add(_exchangeView);
	_transferView.add(_imgTransferView);
	_transferView.add(_transferLockView);
	_view1.add(_transferView);
	
	_sendingOptionsView = Ti.UI.createView(style.sendingOptionsView);
	_imgSendingOptionsView = Ti.UI.createImageView(style.imgSendingOptionsView);
	_sendingOptionsView.sel = 'sending';
	_imgSendingOptionsView.sel = 'sending';
	
	_trackTransactionView = Ti.UI.createView(style.trackTransactionView);
	_imgTrackTransactionView = Ti.UI.createImageView(style.imgTrackTransactionView);
	_transactionLockView = Ti.UI.createImageView(style.imgLock);
	_trackTransactionView.sel = 'track';
	_imgTrackTransactionView.sel = 'track';
	
	_sendingOptionsView.add(_imgSendingOptionsView);
	_view2.add(_sendingOptionsView);
	_trackTransactionView.add(_imgTrackTransactionView);
	_trackTransactionView.add(_transactionLockView);
	_view2.add(_trackTransactionView);

	_myAccountView = Ti.UI.createView(style.myAccountView);
	_imgMyAccountView = Ti.UI.createImageView(style.imgMyAccountView);
	_accountLockView = Ti.UI.createImageView(style.imgLock);
	_myAccountView.sel = 'account';
	_imgMyAccountView.sel = 'account';
	
	_earnView = Ti.UI.createView(style.earnView);
	_imgEarnView = Ti.UI.createImageView(style.imgEarnView);
	_earnView.sel = 'earn';
	_imgEarnView.sel = 'earn';
	
	_myAccountView.add(_imgMyAccountView);
	_myAccountView.add(_accountLockView);
	_view3.add(_myAccountView);
	_earnView.add(_imgEarnView);
	_view3.add(_earnView);

	_faqView = Ti.UI.createView(style.faqView);
	_imgFAQView = Ti.UI.createImageView(style.imgFAQView);
	_faqView.sel = 'faq';
	_imgFAQView.sel = 'faq';
	
	_aboutView = Ti.UI.createView(style.aboutView);
	_imgAboutView = Ti.UI.createImageView(style.imgAboutView);
	_aboutView.sel = 'about';
	_imgAboutView.sel = 'about';
	
	_faqView.add(_imgFAQView);
	_view4.add(_faqView);
	_aboutView.add(_imgAboutView);
	_view4.add(_aboutView);
	
	_lblLogout = Ti.UI.createLabel(style.lblLogout);
	_lblLogout.text = 'LOGOUT';
	
	_lblLogout.addEventListener('click',function(e){
		var alertDialog = require('/utils/AlertDialog').showAlert('', L('logout'), [L('btn_yes'),L('btn_no')]);
		alertDialog.show();
	
		alertDialog.addEventListener('click', function(e) {
			alertDialog.hide();
			if(e.index === 0 || e.index === "0") 
			{
				animateBottom();
				Ti.App.Properties.setString('loginStatus','0');
				
				//Ti.App.Properties.setString('ownerId','0');
				//Ti.App.Properties.setString('loginId','0');
				//Ti.App.Properties.setString('sessionId','0');
				
				Ti.App.fireEvent('loginData');
				
				require('/utils/RemoveViews').removeAllScrollableViews();
				
				Ti.App.fireEvent('menuAnimate');
				
				_imgEarnView.image = '/images/earn_free_money.png';
				_menuScrollableView.bottom = 59;
				_lblLogout.hide();
			}
			alertDialog=null;
		});
	});
	
	function statusCheck()
	{
		if(Ti.App.Properties.getString('loginStatus') !== '0')
		{
			_imgEarnView.image = '/images/perks.png';
			_menuScrollableView.bottom = 100;
			_lblLogout.show();
			_transactionLockView.hide();
			_transferLockView.hide();
			_accountLockView.hide();
		}
		else
		{
			_imgEarnView.image = '/images/earn_free_money.png';
			_menuScrollableView.bottom = 59;
			_lblLogout.hide();
			_transactionLockView.show();
			_transferLockView.show();
			_accountLockView.show();
		}
	}
	
	function logout()
	{
		statusCheck();
	}
	
	Ti.App.addEventListener('logout',logout);
	
	_topView.add(_imgCall);
	_topView.add(_imgMail);
	_topView.add(_imgLocation);
	_locationView.add(_lblLocation);
	_topView.add(_locationView);
	_menuView.add(_topView);
	_mainView.add(_view1);
	_mainView.add(_view2);
	_mainView.add(_view3);
	_mainView.add(_view4);
	_menuView.add(_menuScrollableView);
	_menuView.add(_lblLogout);
	
	_menuScrollableView.views = [_mainView];
	
	_mainView.addEventListener('click',function(e){
		switch(e.source.sel)
		{
			case 'exchange':
				if(Titanium.Network.online)
				{
					Ti.App.fireEvent('menuAnimate');
					require('/utils/RemoveViews').removeAllScrollableViews();
					require('/utils/PageController').pageController('exchange');	
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 'transfer':
				if(Titanium.Network.online)
				{
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						Ti.App.fireEvent('menuAnimate');
						require('/utils/RemoveViews').removeAllScrollableViews();
						require('/utils/PageController').pageController('transfer');
					}
					else
					{
						if(Ti.App.Properties.getString('mPIN') === '1')
						{
							require('/js/Passcode').Passcode();
						}
						else
						{
							require('/js/LoginModal').LoginModal();
						}
					}	
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 'sending':
				if(Titanium.Network.online)
				{
					_menuScrollableView.views = [_mainView,_subSOView];
					_menuScrollableView.scrollToView(1);	
				} 
				else 
				{
					require('/utils/Network').Network();
				}	
			break;
			
			case 'track':
				if(Titanium.Network.online)
				{
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						require('/js/my_account/TransactionTrackerModal').TransactionTrackerModal();
						Ti.App.fireEvent('menuAnimate');
					}
					else
					{
						if(Ti.App.Properties.getString('mPIN') === '1')
						{
							require('/js/Passcode').Passcode();
						}
						else
						{
							require('/js/LoginModal').LoginModal();
						}
					}	
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 'account':
				if(Titanium.Network.online)
				{
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						require('/utils/RemoveViews').removeAllScrollableViews();
						require('/utils/PageController').pageController('myaccount');
						Ti.App.fireEvent('menuAnimate');
					}
					else
					{
						if(Ti.App.Properties.getString('mPIN') === '1')
						{
							require('/js/Passcode').Passcode();
						}
						else
						{
							require('/js/LoginModal').LoginModal();
						}
					}	
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 'earn':
				if(Titanium.Network.online)
				{
					Ti.App.fireEvent('menuAnimate');
					
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						require('/utils/RemoveViews').removeAllScrollableViews();
						require('/utils/PageController').pageController('money_on_referral');
					}
					else
					{
						require('/utils/RemoveViews').removeAllScrollableViews();
						require('/utils/PageController').pageController('refer_earn');
					}
				} 
				else 
				{
					require('/utils/Network').Network();
				}	
			break;
			
			case 'faq':
				if(Titanium.Network.online)
				{
					require('/utils/RemoveViews').removeAllScrollableViews();
					require('/utils/PageController').pageController('faq');
					Ti.App.fireEvent('menuAnimate');
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 'about':
				if(Titanium.Network.online)
				{
					activityIndicator.showIndicator();
					
					var xhr = require('/utils/XHR_BCM');
					var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
				
					xhr.call({
						url : TiGlobals.appURLBCM,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"PAGETYPES",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"source":"'+TiGlobals.bcmSource+'",'+
							'"platform":"'+TiGlobals.osname+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"corridor":"'+countryCode[0]+'"'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
					
					function xhrSuccess(e) {
						if(e.result.status === "S")
						{
							var j=0;
							_subAboutView.add(_subAboutImage);
							_subAboutMainView.add(_subAboutTxt1);
							
							if(e.result.response.about != 0)
							{
								for(i=0; i<e.result.response.about.length; i++)
								{
									j++;
									
									switch(e.result.response.about[i].page_title)
									{
										case 'Privacy Policy' : 
											_subAboutTxt2.top = (25+(25*(i+1))+(10*(i+1)));
											_subAboutMainView.add(_subAboutTxt2);
										break;
										
										case 'Terms & Conditions' : 
											_subAboutTxt3.top = (25+(25*(i+1))+(10*(i+1)));
											_subAboutMainView.add(_subAboutTxt3);
										break;
										
										/*case 'Fraud Awareness' : //cmn 61
											_subAboutTxt7.top = (25+(25*(i+1))+(10*(i+1)));
											_subAboutMainView.add(_subAboutTxt7);
										break;*/
										
										case 'How Remit2India Works' :
											_subAboutTxt4.top = (25+(25*(i+1))+(10*(i+1))); 
											_subAboutMainView.add(_subAboutTxt4);
										break;
										
										case 'Remit2India Power 30' : 
											_subAboutTxt5.top = (25+(25*(i+1))+(10*(i+1)));
											_subAboutMainView.add(_subAboutTxt5);
										break;
									}
									
								}	
							}
							
							_subAboutTxt6.top = (25+(25*(j+1))+(10*(j+1)));
							_subAboutMainView.add(_subAboutTxt6);
							
							_subAboutView.add(_subAboutMainView);
							_subAboutView.add(_subAboutBackImage);
							
							activityIndicator.hideIndicator();
							
							_menuScrollableView.views = [_mainView,_subAboutView];
							_menuScrollableView.scrollToView(1);
						}
						else
						{
							
						}
						xhr = null;
					}
				
					function xhrError(e) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
		}
	});
	
	//////// Sending Options SubView ////////
	
	_subSOView = Ti.UI.createView(style.subView);
	_subSOImage = Ti.UI.createImageView(style.subImage);
	_subSOImage.image = '/images/sending_options.png';
	_subSOMainView = Ti.UI.createView(style.subMainView);
	
	_subSOTxt1 = Ti.UI.createLabel(style.subTxt);
	_subSOTxt1.top = 50;
	_subSOTxt1.text = 'Sending Options';
	_subSOTxt1.sel = 'sending_options';
	
	_subSOTxt2 = Ti.UI.createLabel(style.subTxt);
	_subSOTxt2.top = 90;
	_subSOTxt2.text = 'Receiving Options';
	_subSOTxt2.sel = 'receiving_options';
	
	_subSOTxt3 = Ti.UI.createLabel(style.subTxt);
	_subSOTxt3.top = 130;
	_subSOTxt3.text = 'Delivery Time';
	_subSOTxt3.sel = 'delivery_time';
	
	_subSOTxt4 = Ti.UI.createLabel(style.subTxt);
	_subSOTxt4.top = 170;
	_subSOTxt4.text = 'Locations';
	_subSOTxt4.sel = 'delivery_locations';
	
	_subSOBackImage = Ti.UI.createImageView(style.subBackImage);
	_subSOBackImage.sel = 'back';
	
	
	_subSOView.add(_subSOImage);
	_subSOMainView.add(_subSOTxt1);
	_subSOMainView.add(_subSOTxt2);
	_subSOMainView.add(_subSOTxt3);
	_subSOMainView.add(_subSOTxt4);
	_subSOView.add(_subSOMainView);
	_subSOView.add(_subSOBackImage);
	
	_subSOView.addEventListener('click',function(e){
		switch(e.source.sel)
		{
			case 'sending_options':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('sending_options');
				},200);
			break;
			
			case 'receiving_options':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('receiving_options');
				},200);
			break;
			
			case 'delivery_time':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('delivery_time');
				},200);
			break;
			
			case 'delivery_locations':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('delivery_locations');
				},200);
			break;
			
			case 'back':
				_menuScrollableView.scrollToView(0);
			break;
			
			setTimeout(function(){
				_menuScrollableView.scrollToView(0);
			},300);
		}
	});
	
	//////// About SubView ////////
	
	_subAboutView = Ti.UI.createView(style.subView);
	_subAboutImage = Ti.UI.createImageView(style.subImage);
	_subAboutImage.image = '/images/aboutr2i.png';
	_subAboutMainView = Ti.UI.createView(style.subMainView);
	_subAboutTxt1 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt1.top = 25;
	_subAboutTxt1.text = 'About Remit2India';
	_subAboutTxt1.sel = 'aboutr2i';
	_subAboutTxt2 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt2.text = 'Privacy Policy';
	_subAboutTxt2.sel = 'privacyr2i';
	_subAboutTxt3 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt3.text = 'Terms & Conditions';
	_subAboutTxt3.sel = 'termsr2i';
	/*
	_subAboutTxt7 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt7.text = 'Fraud Awareness';  //CMN 61
	_subAboutTxt7.sel = 'fraudr2i';*/
	
	_subAboutTxt4 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt4.text = 'How Remit2India Works';
	_subAboutTxt4.sel = 'howr2i';
	_subAboutTxt5 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt5.text = 'Remit2India Power 30';
	_subAboutTxt5.sel = 'powerr2i';
	_subAboutTxt6 = Ti.UI.createLabel(style.subTxt);
	_subAboutTxt6.text = 'Contact Us';
	_subAboutTxt6.sel = 'contactr2i';
	
	_subAboutBackImage = Ti.UI.createImageView(style.subBackImage);
	_subAboutBackImage.sel = 'back';
	
	_subAboutView.addEventListener('click',function(e){
		
		switch(e.source.sel)
		{
			case 'aboutr2i':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('about');
				},200);
			break;
			
			case 'privacyr2i':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('privacy');
				},200);
			break;
			
			case 'termsr2i':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('terms');
				},200);
			break;
			
			/*case 'fraudr2i':    //CMN 61
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('terms');
				},200);
			break;*/
			
			
			case 'howr2i':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('how');
				},200);
			break;
			
			case 'powerr2i':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('power');
				},200);
			break;
			
			case 'contactr2i':
				Ti.App.fireEvent('menuAnimate');
				setTimeout(function(){
					require('/utils/PageController').pageController('contact');
				},200);
			break;
			
			case 'back':
				_menuScrollableView.scrollToView(0);
			break;
		}
		
		setTimeout(function(){
			_menuScrollableView.scrollToView(0);
		},300);
	});
	
	///////////////////////////////// Animate View ///////////////////////////////

	// Added when menu is opened
	var animateViewTop = Ti.UI.createAnimation({
	    top:TiGlobals.osname === 'android' ? 0 : 20,
	    duration:200
	});
	
	var animateViewBottom = Ti.UI.createAnimation({
	    top:TiGlobals.osname === 'android' ? TiGlobals.platformHeight : parseInt(TiGlobals.platformHeight-20),
	    duration:200
	});
	
	function animateBottom()
	{
		if(TiGlobals.menuOpen === '1')
		{
			// Localize
			
			_menuView.animate(animateViewBottom);
			statusCheck();
			
			// Avoid menu open and close simultaneously
			setTimeout(function(){
				TiGlobals.menuOpen = '0';
			},150);
		}
	}
	
	Ti.App.addEventListener('animateBottom',animateBottom);
	
	function animateTop()
	{
		if(TiGlobals.menuOpen === '0')
		{
			_menuView.animate(animateViewTop);
			statusCheck();
			
			// Avoid menu open and close simultaneously
			setTimeout(function(){
				TiGlobals.menuOpen = '1';
			},150);
		}
	}
	
	Ti.App.addEventListener('animateTop',animateTop);
	
	///////////////////////////////// Animate View End ///////////////////////////////
	
	return _menuView;
	
}; // Menu()

module.exports = Menu;
