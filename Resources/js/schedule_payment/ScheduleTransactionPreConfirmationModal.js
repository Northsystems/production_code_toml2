exports.ScheduleTransactionPreConfirmationModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Schedule Payments Pre-Confirmation');
	
	var _obj = {
		style : require('/styles/schedule_payment/ScheduleTransactionPreConfirmation').ScheduleTransactionPreConfirmation,
		winScheduleTransactionPreConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tblSchedulePayment : null,
		btnConfirm : null,
	};
	
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winScheduleTransactionPreConfirmation = Ti.UI.createWindow(_obj.style.winScheduleTransactionPreConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winScheduleTransactionPreConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Easy EMI Payments';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.tblSchedulePayment = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblSchedulePayment.height = 0;
	
	_obj.btnConfirm = Ti.UI.createButton(_obj.style.btnConfirm);
	_obj.btnConfirm.title = 'CONFIRM';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.tblSchedulePayment);
	_obj.mainView.add(_obj.btnConfirm);
	_obj.globalView.add(_obj.mainView);
	_obj.winScheduleTransactionPreConfirmation.add(_obj.globalView);
	_obj.winScheduleTransactionPreConfirmation.open();
	
	_obj.btnConfirm.addEventListener('click',function(e){
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BOOKSI",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+params.paymodeCode+'",'+
				'"receiverNickName":"'+params.receiverNickName+'",'+
				'"amountToBeDebited":"'+params.amountToBeDebited+'",'+
				'"accountId":"'+params.accountId+'",'+
				'"accountNo":"'+params.accountNo+'",'+
				'"startDate":"'+params.startDate+'",'+
				'"sipFrequency":"'+params.sipFrequency+'",'+
				'"totalNoOfPayments":"'+params.totalNoOfPayments+'",'+
				'"minAmount":"'+params.minAmount+'",'+
				'"maxAmount":"'+params.maxAmount+'",'+
				'"siType":"'+params.siType+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				destroy_scheduletxnpreconf();
				require('/js/schedule_payment/ScheduleTransactionConfirmationModal').ScheduleTransactionConfirmationModal();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_scheduletxnpreconf();
				}
				else
				{
					if(e.result.message =='ERROR: could not insert record.'){
					    require('/utils/AlertDialog').showAlert('Transaction Error','Your ACH Bank Account has not been verified by you. Kindly verify your account by clicking on the link in the Bank Accounts section.', [L('btn_ok')]).show();
	
					}
					else{
						
					}
					//require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					
					setTimeout(function(){
						destroy_scheduletxnpreconf();
						require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
					},300);
				}
			}
			xhr = null;
			activityIndicator.hideIndicator();
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	// Check confirmation
	
	function confirmation()
	{
		for(var i=0; i<8; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : 60,
				backgroundColor : 'transparent',
				className : 'schedule_payment'
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var lblKey = Ti.UI.createLabel({
				top : 10,
				left : 20,
				height : 15,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal12'),
				color : TiFonts.FontStyle('greyFont')
			});
			
			var lblValue = Ti.UI.createLabel({
				top : 30,
				height : 20,
				left : 20,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal14'),
				color : TiFonts.FontStyle('blackFont')
			});
			
			switch(i)
			{
				case 0:
					lblKey.text = 'Country';
					lblValue.text = countryName[0];
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 1:
					lblKey.text = 'Currency';
					lblValue.text = origSplit[1];
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 2:
					lblKey.text = 'Start Date';
					lblValue.text = params.startDate;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 3:
					lblKey.text = 'Receiver Name';
					lblValue.text = params.receiverNickName;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 4:
					lblKey.text = 'Bank Account';
					lblValue.text = params.accountNo;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 5:
					lblKey.text = 'Amount to be debited regularly';
					lblValue.text = origSplit[1]+' '+params.amountToBeDebited;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 6:
					lblKey.text = 'Periodicity';
					lblValue.text = params.sipFrequency + ' days';
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
				
				case 7:
					lblKey.text = 'Total No. of Payments';
					lblValue.text = params.totalNoOfPayments;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblSchedulePayment.appendRow(row);
					_obj.tblSchedulePayment.height = parseInt(_obj.tblSchedulePayment.height + 60);
				break;
			}
		}
	}
	
	confirmation();
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_scheduletxnpreconf();
				require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
				alertDialog = null;
			}
		});
	});
	
	_obj.winScheduleTransactionPreConfirmation.addEventListener('androidback',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_scheduletxnpreconf();
				require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
				alertDialog = null;
			}
		});
	});
	
	function destroy_scheduletxnpreconf()
	{
		try{
			require('/utils/Console').info('############## Remove ScheduleTransactionPreConfirmation start ##############');
			
			_obj.winScheduleTransactionPreConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winScheduleTransactionPreConfirmation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_scheduletxnpreconf',destroy_scheduletxnpreconf);
			require('/utils/Console').info('############## Remove ScheduleTransactionPreConfirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_scheduletxnpreconf', destroy_scheduletxnpreconf);
}; // ScheduleTransactionPreConfirmationModal()
