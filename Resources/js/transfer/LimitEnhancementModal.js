exports.LimitEnhancementModal = function(conf)
{
	require('/lib/analytics').GATrackScreen('Limit Enhancement');
	
	var _obj = {
		style : require('/styles/transfer/LimitEnhancement').LimitEnhancement,
		winLimitEnhancement : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		scrollableView : null,
		
		// Instruction View
		
		instructionView : null,
		lblInstructions : null,
		lblInstructions : null,
		lblInstruction1 : null,
		lblInstruction2 : null,
		lblInstruction3 : null,
		noteView : null,
		lblNote : null,
		lblPt1 : null,
		btnStart : null,
		senderName : null,
		//Step 1
		stepView1 : null,
		
		lblSenderName:null,
	    lblSenderNameValue:null,
		lblMarital : null,
		imgMarital : null,
		maritalView : null,
		borderViewS13 : null,
	
		btnStep1 : null,
		
		//Step 2
		stepView2 : null,
		txtOrganization : null,
		borderViewS21 : null,		
		txtDesignation : null,
		borderViewS22 : null,
		txtSource : null,
		borderViewS23 : null,
		lblStay : null,
		imgStay : null,
		stayView : null,
		borderViewS24 : null,
		lblIncome : null,
		imgIncome : null,
		incomeView : null,
		borderViewS25 : null,
		lblAmount : null,
		imgAmount : null,
		amountView : null,
		borderViewS26 : null,
		btnStep2 : null,
		
		// Step 3
		stepView3 : null,
		lblReceiverFirstNameHead:null,
		lblReceiverFirstName:null,
		lblReceiverLastNameHead:null,
		lblReceiverLastName:null,
		lblReceiverNameHead : null,
		lblReceiver : null,
		lblPaymodeHead : null,
		lblPaymode : null,
		
		lblPurposeHead : null,
		lblPurpose : null,
		
		imgPurpose : null,
		purposeView : null,
		borderViewS31 : null,
		txtRelation : null,
		borderViewS32 : null,
		lblSendingAmountHead : null,
		lblSendingAmount : null,
		btnStep3 : null,
		
		//Step 4
		stepView4 : null,
		lblKYCTxt1 : null,
		lblDOB : null,
		imgDOB : null,
		dobView : null,
		borderViewS41 : null,
		lblTime1 : null,
		imgTime1 : null,
		lblTimeOR : null,
		timeView1 : null,
		lblTime2 : null,
		imgTime2 : null,
		timeView2 : null,
		ddTimeView : null,
		borderViewS42 : null,
		borderViewS43 : null,
		ddBorderTimeView : null,
		lblNoteTime : null,
		lblNote1 : null,
		lblNote2 : null,
		noteView : null,
		btnStep4 : null,
		btnView : null,
		
		purposeOptions : [],
		purposeCodeOptions : [],
		purposeCode : null,
	
		selStay : null,
		selIncome : null,
		selAmount : null,
		timeOptions : [],
		purposeOptions : []
		
	};
	try{
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.dob = require('/utils/Date').today('/','dmy').split('/');
	}catch(e){}
	
	_obj.winLimitEnhancement = Titanium.UI.createWindow(_obj.style.winLimitEnhancement);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winLimitEnhancement);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	// Instruction View
	
	function instructions() 
	{
		_obj.instructionView = Ti.UI.createScrollView(_obj.style.instructionView);
	
		_obj.lblInstructions = Ti.UI.createLabel(_obj.style.lblInstructions);
		_obj.lblInstructions.text = 'Instructions';
		
		_obj.lblInstruction1 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblInstruction1.text = '\u00B7 Fill out all details in the Know Your Customer(KYC) form displayed below. After filling up this form, you will be allowed to book a single transaction for the total amount you have requested for.';
		
		_obj.lblInstruction2 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblInstruction2.text = '\u00B7 Go ahead and book one single transaction for your requested amount - immediately. Remember, your limits will be reset after this transaction so do not enter an amount lesser than you have requested.';
		
		_obj.lblInstruction3 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblInstruction3.text = '\u00B7 A Remit2india Compliance Officer will get in touch with you either by phone or email to verify the details and approve your transaction. Please respond to this call or email at the earliest.';
		
		_obj.noteView = Ti.UI.createView(_obj.style.noteView);
		
		_obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
		_obj.lblNote.text = 'Note';
		
		_obj.lblPt1 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblPt1.text = '\u00B7 Unlike before, you do not need to wait for our representative to inform you of your limit increase - you can transact for your requested amount immediately. This is convenient for you and helps process your transaction faster. Moreover, you do not need any assistance from a Remit2India representative to book this transaction.';
		_obj.lblPt1.bottom = 20;
		
		_obj.btnStart = Ti.UI.createButton(_obj.style.btnStep);
		_obj.btnStart.title = 'START NOW';
		
		_obj.instructionView.add(_obj.lblInstructions);
		_obj.instructionView.add(_obj.lblInstruction1);
		_obj.instructionView.add(_obj.lblInstruction2);
		_obj.instructionView.add(_obj.lblInstruction3);
		_obj.noteView.add(_obj.lblNote);
		_obj.noteView.add(_obj.lblPt1);
		_obj.instructionView.add(_obj.noteView);
		_obj.instructionView.add(_obj.btnStart);
		_obj.mainView.add(_obj.instructionView);	
		
		_obj.btnStart.addEventListener('click',function(){
			_obj.mainView.remove(_obj.instructionView);
			_obj.instructionView.remove(_obj.lblInstructions);
			_obj.instructionView.remove(_obj.lblInstruction1);
			_obj.instructionView.remove(_obj.lblInstruction2);
			_obj.instructionView.remove(_obj.lblInstruction3);
			_obj.noteView.remove(_obj.lblNote);
			_obj.noteView.remove(_obj.lblPt1);
			_obj.instructionView.remove(_obj.noteView);
			_obj.instructionView.remove(_obj.btnStart);
			
			limitEnhancement();
		});
	}
	
	instructions();

	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	_obj.globalView.add(_obj.mainView);
	_obj.winLimitEnhancement.add(_obj.globalView);
	_obj.winLimitEnhancement.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_limitenhancement();
	});
	

	function limitEnhancement()
	{

		
		//_obj.lblSectionHead1 = Ti.UI.createLabel(_obj.style.lblSectionHead);
		_obj.lblSectionHead1 = Ti.UI.createLabel(_obj.style.lblHeader1);
		_obj.lblSectionHead1.text = 'Personal Details';
		//_obj.sectionHeaderBorder1 = Ti.UI.createView(_obj.style.sectionHeaderBorder);
		
		_obj.lblSectionHead2 = Ti.UI.createLabel(_obj.style.lblHeader1);
		_obj.lblSectionHead2.top = 30;
		_obj.lblSectionHead2.text = 'Professional Details';
		_obj.sectionHeaderBorder2 = Ti.UI.createView(_obj.style.sectionHeaderBorder); 
		
		_obj.lblSectionHead3 = Ti.UI.createLabel(_obj.style.lblHeader1);
		_obj.lblSectionHead3.top = 30;
		_obj.lblSectionHead3.text = 'Receiver Details';
		_obj.sectionHeaderBorder3 = Ti.UI.createView(_obj.style.sectionHeaderBorder);  
		
		_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
		
		/////////////////// Step 1 ///////////////////
		
	
		
		/////////////////// Step 2 ///////////////////
		
		_obj.stepView2 = Ti.UI.createScrollView(_obj.style.stepView2);
		
		
		//declaring & Craeting componenets
		
		//_obj.lblSectionHead.text = 'Professional Details';
		//Sender Name 
		/*_obj.lblSenderName = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblSenderName.text = 'Sender Name';
		_obj.lblSenderNameValue = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblSenderNameValue.text = _obj.senderName;*/
		
		_obj.maritalView = Ti.UI.createView(_obj.style.maritalView);
		_obj.lblMarital = Ti.UI.createLabel(_obj.style.lblMarital);
		_obj.lblMarital.text = 'Marital Status';
		_obj.imgMarital = Ti.UI.createImageView(_obj.style.imgMarital);
		_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.maritalView.addEventListener('click',function(e){
			var optionDialogMarital = require('/utils/OptionDialog').showOptionDialog('Marital Status',['Single','Married'],-1);
			optionDialogMarital.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogMarital.addEventListener('click',function(evt){
				if(optionDialogMarital.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblMarital.text = optionDialogMarital.options[evt.index];
					optionDialogMarital = null;
				}
				else
				{
					optionDialogMarital = null;
					_obj.lblMarital.text = 'Marital Status';
				}
			});	
		});
		

		
		_obj.txtOrganization = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtOrganization.hintText = "Name of the Organization";
		_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtDesignation = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtDesignation.hintText = "Designation";
		_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtSource = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtSource.hintText = "Other source of income";
		//_obj.txtSource.hintText = "Income from Profession";
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.stayView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblStay = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblStay.text = 'Stay';
		_obj.imgStay = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS24 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.stayView.addEventListener('click',function(e){
			var optionDialogStay = require('/utils/OptionDialog').showOptionDialog('Stay',['Less than a year','1 - 3 years','3 - 5 years','More than 5 years'],-1);
			optionDialogStay.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogStay.addEventListener('click',function(evt){
				if(optionDialogStay.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblStay.text = optionDialogStay.options[evt.index];
					
					switch(evt.index)
					{
						case 0:
							_obj.selStay = 'less than 1 year';
						break;
						
						case 1:
							_obj.selStay = '1-3 years';
						break;
						
						case 2:
							_obj.selStay = '3-5 years';
						break;
						
						case 3:
							_obj.selStay = 'more than 5 years';
						break;
					}
					
					optionDialogStay = null;
				}
				else
				{
					optionDialogStay = null;
					_obj.lblStay.text = 'Stay';
					_obj.selStay = null;
				}
			});	
		});
		
		_obj.incomeView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblIncome = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblIncome.text = 'Household Income';
		//_obj.lblIncome.text = 'Other Income';
		_obj.imgIncome = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS25 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.incomeView.addEventListener('click',function(e){
			var optionDialogIncome = require('/utils/OptionDialog').showOptionDialog('Other Income',['Less than 50,000','50,001 - 1,00,000','1,00,001 - 2,00,000','2,00,001 - 3,00,000','3,00,001 - 4,00,000','4,00,001 - 5,00,000','5,00,001 and above'],-1);
			optionDialogIncome.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogIncome.addEventListener('click',function(evt){
				if(optionDialogIncome.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblIncome.text = optionDialogIncome.options[evt.index];
					
					switch(evt.index)
					{
						case 0:
							_obj.selIncome = 'Less than 50000';
						break;
						
						case 1:
							_obj.selIncome = '50001 - 100000';
						break;
						
						case 2:
							_obj.selIncome = '100001 - 200000';
						break;
						
						case 3:
							_obj.selIncome = '200001 - 300000';
						break;
						
						case 4:
							_obj.selIncome = '300001 - 400000';
						break;
						
						case 5:
							_obj.selIncome = '400001 - 500000';
						break;
						
						case 6:
							_obj.selIncome = '500001 and above';
						break;
					}
					
					optionDialogIncome = null;
				}
				else
				{
					optionDialogIncome = null;
					_obj.lblIncome.text = 'Other Income';
					_obj.selIncome = null;
				}
			});
		});		
		
		_obj.amountView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblAmount = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblAmount.text = 'Amount of Other Income';
		_obj.imgAmount = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS26 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.amountView.addEventListener('click',function(e){
			var optionDialogAmount = require('/utils/OptionDialog').showOptionDialog('Amount of Other Income',['Less than 10,000','10,000 - 30,000','30,000 - 50,000','More than 50,000'],-1);
			optionDialogAmount.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogAmount.addEventListener('click',function(evt){
				if(optionDialogAmount.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblAmount.text = optionDialogAmount.options[evt.index];
					
					switch(evt.index)
					{
						case 0:
							_obj.selAmount = 'Less than 10000';
						break;
						
						case 1:
							_obj.selAmount = '10000-30000';
						break;
						
						case 2:
							_obj.selAmount = '30000-50000';
						break;
						
						case 3:
							_obj.selAmount = 'More than 50000';
						break;
					}
					
					optionDialogAmount = null;
				}
				else
				{
					optionDialogAmount = null;
					_obj.lblAmount.text = 'Amount of Other Income';
					_obj.selAmount = null;
				}
			});
		});	

		
		
	
	    _obj.btnView = Ti.UI.createView(_obj.style.ButtonView);
		
		_obj.btnStep2 = Ti.UI.createButton(_obj.style.SubBtn);
		_obj.btnStep2.title = 'SUBMIT';
		
		//added by sanjivani on 17-march-17
		_obj.btnCancel = Ti.UI.createButton(_obj.style.CancelBtn);
		//_obj.btnCancel.left = 150;
		//_obj.btnCancel.right = 20;
		_obj.btnCancel.title = 'CANCEL';
		
		
		//from step 3
		/*//Receiver fname & lname  //for CMN 41
		_obj.lblReceiverFullNameHead = Ti.UI.createLabel(_obj.style.lblStateHead);
	    _obj.lblReceiverFullNameHead.text = 'Receiver Name';
		_obj.lblReceiverFLName = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblReceiverFLName.text = conf['receiverFName']+' '+conf['receiverLName'];*/
		
		//Separating 'Receiver Name' to 'Receiver first Name' & 'Receiver last Name' //added by sanjivani on 21-march-17//CMN 41 
		////Receiver's firs' name
		_obj.lblReceiverFirstNameHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblReceiverFirstNameHead.text = "Receiver's First Name";
		_obj.lblReceiverFirstName = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblReceiverFirstName.text = conf['receiverFName'];
		
		//Receiver's Last Name
		_obj.lblReceiverLastNameHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblReceiverLastNameHead.text = "Receiver's Last Name";
		_obj.lblReceiverLastName = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblReceiverLastName.text = conf['receiverLName'];
		
	    //Receiver nick name
		_obj.lblReceiverNameHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		//_obj.lblReceiverNameHead.text = 'Receiver';
		_obj.lblReceiverNameHead.text = 'Receiver Nick Name';
		_obj.lblReceiver = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblReceiver.text = conf['receiverNickName'];
		
		_obj.lblPaymodeHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		//_obj.lblPaymodeHead.text = 'Pay Mode';   //for CMN 41
		_obj.lblPaymodeHead.text = 'Sending Option';
		_obj.lblPaymode = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblPaymode.text = conf['paymodeLabel'];
		//_obj.lblPaymode.text = conf['paymode'];   //again changing(reverting) as per TOML requirement on 08-march-17 for CMN 41
		//_obj.lblPaymode.text = conf['paymodeLabel']; //changing as per TOML requirement
		
		_obj.lblPurposeHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblPurposeHead.text = 'Purpose of Remittance';
		_obj.lblPurpose = Ti.UI.createLabel(_obj.style.lblState);
		//_obj.lblPurpose.text = conf['purposeShort'];
		_obj.lblPurpose.text = conf['purpose1'];
		
		_obj.txtRelation = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtRelation.hintText = "Relation with Receiver";
		_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.lblSendingAmountHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblSendingAmountHead.text = 'Your sending amount';
		_obj.lblSendingAmount = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblSendingAmount.text = origSplit[1] +' '+ conf['amount'];
		
		//end step3 creation here
		
		//Step 4 declaration start
		_obj.noteView = Ti.UI.createView(_obj.style.noteView);
		
		_obj.lblNoteTime = Ti.UI.createLabel(_obj.style.lblNote);
		_obj.lblNoteTime.text = 'Note';
		
		_obj.lblNote1 = Ti.UI.createLabel(_obj.style.lblNoteTxt);
		_obj.lblNote1.text = '\u00B7 Your approval for the limits enhancement will be applicable for a single transaction only. Now go ahead and send the amount entered here immediately. Your transaction shall not be processed till verification is completed.';
		_obj.lblNote1.bottom = null; 
		
		_obj.lblNote2 = Ti.UI.createLabel(_obj.style.lblNoteTxt);
		_obj.lblNote2.text = '\u00B7 Post filling of the KYC form, your limit reset will be done on successful Compliance verification. Your enhanced limit will expire on 7th day from the day of reset of your remittance amount/transaction limit.';
		//Step 4 declaration ends here
		
		//Adding components
		
		//from step 1
		/*_obj.mainView.add(_obj.lblSectionHead1);
		_obj.mainView.add(_obj.sectionHeaderBorder1);*/
		
		_obj.stepView2.add(_obj.lblSectionHead1);
		//_obj.stepView2.add(_obj.sectionHeaderBorder1);
		
		/*_obj.stepView2.add(_obj.lblSenderName);
		_obj.stepView2.add(_obj.lblSenderNameValue);*/
		
		_obj.maritalView.add(_obj.lblMarital);
		_obj.maritalView.add(_obj.imgMarital);
		_obj.stepView2.add(_obj.maritalView);
		_obj.stepView2.add(_obj.borderViewS13);
		
		//step 2
		_obj.stepView2.add(_obj.lblSectionHead2);
		_obj.stepView2.add(_obj.txtOrganization);
		_obj.stepView2.add(_obj.borderViewS21);		
		_obj.stepView2.add(_obj.txtDesignation);
		_obj.stepView2.add(_obj.borderViewS22);
		_obj.stepView2.add(_obj.txtSource);
		_obj.stepView2.add(_obj.borderViewS23);
		_obj.stayView.add(_obj.lblStay);
		_obj.stayView.add(_obj.imgStay);
		_obj.stepView2.add(_obj.stayView);
		_obj.stepView2.add(_obj.borderViewS24);
		_obj.incomeView.add(_obj.lblIncome);
		_obj.incomeView.add(_obj.imgIncome);
		_obj.stepView2.add(_obj.incomeView);
		_obj.stepView2.add(_obj.borderViewS25);
		_obj.amountView.add(_obj.lblAmount);
		_obj.amountView.add(_obj.imgAmount);
		_obj.stepView2.add(_obj.amountView);
		_obj.stepView2.add(_obj.borderViewS26);

		
		//Step 3
		_obj.stepView2.add(_obj.lblSectionHead3);
		/*_obj.stepView2.add(_obj.lblReceiverFullNameHead); //for CMN 41
		_obj.stepView2.add(_obj.lblReceiverFLName);*/
		_obj.stepView2.add(_obj.lblReceiverFirstNameHead);
		_obj.stepView2.add(_obj.lblReceiverFirstName);
		_obj.stepView2.add(_obj.lblReceiverLastNameHead);
		_obj.stepView2.add(_obj.lblReceiverLastName);
	
		_obj.stepView2.add(_obj.lblReceiverNameHead);
		_obj.stepView2.add(_obj.lblReceiver);
		
		_obj.stepView2.add(_obj.lblPurposeHead);
		_obj.stepView2.add(_obj.lblPurpose);
		
		_obj.stepView2.add(_obj.txtRelation);
		_obj.stepView2.add(_obj.borderViewS32);
		_obj.stepView2.add(_obj.lblSendingAmountHead);
		_obj.stepView2.add(_obj.lblSendingAmount);
		
		//step 4
		_obj.noteView.add(_obj.lblNoteTime);
		_obj.noteView.add(_obj.lblNote1);
		_obj.noteView.add(_obj.lblNote2);
		_obj.stepView2.add(_obj.noteView);
		
		//submit button
		/*_obj.stepView2.add(_obj.btnStep2);
		_obj.stepView2.add(_obj.btnCancel);*/
		_obj.btnView.add(_obj.btnStep2);
		_obj.btnView.add(_obj.btnCancel);
		_obj.stepView2.add(_obj.btnView);
	
	_obj.btnCancel.addEventListener('click',function(e){
		destroy_limitenhancement();
	});
		//Button Event
		_obj.btnStep2.addEventListener('click',function(e){
			
			//Step 1 validation - Marital status
			if(_obj.lblMarital.text === 'Marital Status')
			{
				require('/utils/AlertDialog').showAlert('','Please select a marital status',[L('btn_ok')]).show();
	    		return;
			}
			else {
				
			}
			
			//step 2 validation
			if(_obj.txtOrganization.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Organisation Name',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtOrganization.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Organisation Name',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtOrganization.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Organisation Name',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtOrganization.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtOrganization.value.charAt(i)+' entered for '+_obj.txtOrganization.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;
			}
			
			if(_obj.txtDesignation.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Designation',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDesignation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Designation',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDesignation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Designation',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtDesignation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtDesignation.value.charAt(i)+' entered for '+_obj.txtDesignation.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;
			}
			
			if(_obj.txtSource.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter the Income from Profession',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtSource.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Income from Profession',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtSource.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Income from Profession',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtSource.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtSource.value.charAt(i)+' entered for '+_obj.txtSource.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;
			}
			
			if(_obj.lblStay.text === 'Stay')
			{
				require('/utils/AlertDialog').showAlert('','Please enter the Years in current job/profession',[L('btn_ok')]).show();
	    		return;		
			}
			
			if(_obj.lblIncome.text === 'Other Income')
			{
				require('/utils/AlertDialog').showAlert('','Please select Other Income',[L('btn_ok')]).show();
	    		return;		
			}
			
			if(_obj.lblAmount.text === 'Amount of Other Income')
			{
				require('/utils/AlertDialog').showAlert('','Please select Amount of Other Income',[L('btn_ok')]).show();
	    		return;		
			}
			
			//Step 3 validation 
				if(_obj.lblPurpose.text === 'Purpose of Sending Money')
			{
				require('/utils/AlertDialog').showAlert('','Please select a purpose of sending money',[L('btn_ok')]).show();
	    		return;		
			}
			else if(_obj.txtRelation.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter your relationship with the Receiver',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtRelation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Relationship field',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtRelation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Relationship field',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtRelation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtRelation.value.charAt(i)+' entered for '+_obj.txtRelation.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;
			}
			
			//Step 4 - API call
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"TXNLIMITENHANCEMENT",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'",'+
					'"sendingCurrency":"'+origSplit[1]+'",'+
					//'"state":"'+_obj.lblState.text+'",'+
					'"state":"F",'+
					'"paymodeCode":"'+conf['paymode']+'",'+
					'"txnNoIncrease":"1",'+
					//'"txnAmtIncrease":"1000",'+
					//'"txnAmtIncrease":"'+_obj.txtAmount+'",'+
					'"txnAmtIncrease":"'+TiGlobals.sendamt+'",'+
					'"leadFlag":"I",'+
					'"supervisorName":"F",'+
					'"responseDate":"SYSDATE",'+
					'"purpose":"F",'+
					'"prevEmployer":"F",'+
					'"prevDesignation":"F",'+
					'"education":"F",'+
					'"stayOverseas":"F",'+
					'"firstName":"'+conf['receiverFName']+'",'+ 
					'"lastName":"'+conf['receiverLName']+'",'+ 
					//'"email":"'+conf['receiverLoginid']+'",'+ 
					'"email":"F",'+
					'"bene1Name":"'+conf['receiverNickName']+'",'+ 
					//'"passportNo":"'+_obj.txtPassport.value+'",'+
					'"passportNo":"F",'+
					'"maritalStatus":"'+_obj.lblMarital.text+'",'+
					//'"offPhone":"'+_obj.txtCC.value+_obj.txtAC.value+_obj.txtNo.value+'",'+
					'"offPhone":"F",'+
					//'"resPhone":"'+_obj.txtDayCC.value+_obj.txtDayAC.value+_obj.txtDayNo.value+'",'+
					'"resPhone":"F",'+
					//'"mobileSite":"'+_obj.txtMobileCC.value+_obj.txtMobileNo.value+'",'+
					'"mobileSite":"F",'+
					//'"ssn":"'+ssn+'",'+
					'"ssn":"NaN",'+
					'"organisation":"'+_obj.txtOrganization.value+'",'+
					'"designation":"'+_obj.txtDesignation.value+'",'+
					'"otherIncomeSrc":"'+_obj.txtSource.value+'",'+
					'"experience":"'+_obj.selStay+'",'+
					'"annualIncome":"'+_obj.selIncome+'",'+
					'"otherIncome":"'+_obj.selAmount+'",'+
					'"remitPurpose":"'+_obj.lblPurpose.text+'",'+
					'"bene1Relation":"'+_obj.txtRelation.value+'",'+
					'"contactDate":"01-Jan-1920",'+
					'"timeFrom":"00:00",'+
					'"timeTo":"00:00",'+	
					'"timeFrom1":"00:00",'+
					'"timeTo1":"00:00"'+
					'}',
									
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					destroy_limitenhancement();
					setTimeout(function(){
						require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
					},200);
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_limitenhancement();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
			
		});
		
		/////////////////// Step 3 ///////////////////
		
		//_obj.stepView3 = Ti.UI.createScrollView(_obj.style.stepView3);
		
		
		
		
		/////////////////// Step 4 ///////////////////
		
		//_obj.stepView4 = Ti.UI.createScrollView(_obj.style.stepView4);
	
		
		
		
		//_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2,_obj.stepView3,_obj.stepView4];
		_obj.scrollableView.views = [_obj.stepView2];
		_obj.mainView.add(_obj.scrollableView);
		
	
		
		function getTransactionInfo()
		{
			var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETTRANSACTIONINFO",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"payModeCode":"",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"originatingCurrency":"'+origSplit[1]+'",'+
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				require('/utils/Console').info('Result ======== ' + e.result);
				
				if(e.result.responseFlag === "S")
				{
					try{
					_obj.purposeOptions = [];
				
					for(var i=0;i<e.result.purposeData.length;i++)
					{
						_obj.purposeOptions.push(e.result.purposeData[i].purposeName);
					}
				}catch(e){}
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_limitenhancement();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		
		
		getTransactionInfo();
	}
	
	_obj.winLimitEnhancement.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_limitenhancement();
				alertDialog = null;
			}
		});
	});
	
	function destroy_limitenhancement()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove limit enhancement start ##############');
			
			_obj.winLimitEnhancement.close();
			require('/utils/RemoveViews').removeViews(_obj.winLimitEnhancement);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_limitenhancement',destroy_limitenhancement);
			require('/utils/Console').info('############## Remove limit enhancement end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_limitenhancement', destroy_limitenhancement);
}; // LimitEnhancementModal()
