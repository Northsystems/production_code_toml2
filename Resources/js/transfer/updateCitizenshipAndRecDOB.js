//exports.updateCitizenshipAndRecDOB = function(conf) {
	function updateCitizenshipAndRecDOB(_self){
	require('/lib/analytics').GATrackScreen('update Citizenship And RecDOB Form');
	
	Ti.App.Properties.setString('pg', 'updateCDOB');
	
	pushView(Ti.App.Properties.getString('pg'));
	
	var _obj = {
		style : require('/styles/transfer/updateCitizenshipAndRecDOB').updateCDOB,
		winCDOB : null,
		lblUpdate : null,
		imgClose : null,
		headerBorder : null,
		updateCDOBView:null,
		
		lblDear:null,
		lblNote:null,
		lblDtl:null,
		DtlBorder:null,
		lblCitizenShip:null,
		lblCountry:null,
		imgCountry:null,
		borderCountry:null,
		
		lblRecDtl:null,
		recDtlBorder:null,
		lblRecNote:null,
		
		lblRNickNm:null,
		valRNickNm:null,
		lblRFNm:null,
		valRFNm:null,
		
		recNick:null,
		recFName:null,
		recLName:null,
		recDOB:null,
		split:null,
		
		recDOBview:null,
        lblRecDOB:null,
        imgDOB:null,
        recDOBborderView:null,
        
        btnSubmit:null,
        btnCancel:null,
        
        beneDataArray:null,
        split:null,
        dobFlag:false,
        
        recDOBDay:null,
        recDOBMon:null,
        recDOBYr:null
        
    	
	};
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	

	_obj.winCDOB = Ti.UI.createView(_obj.style.globalView);

	_obj.headerView = Ti.UI.createView(_obj.style.headerView);

	_obj.lblUpdate = Ti.UI.createLabel(_obj.style.lblHeader);
	
	_obj.lblUpdate.text = 'Update your Information';
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblDear =  Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblDear.text = 'Dear '+Ti.App.Properties.getString('firstName')+',';
  
    _obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
    _obj.lblNote.height = Ti.UI.SIZE;
    _obj.lblNote.text = 'You are requested to provide us with the following additional details about yourself and your receiver’s added.';

     _obj.lblDtl = Ti.UI.createLabel(_obj.style.lblHeader);
	
	_obj.lblDtl.text = 'Details about yourself';
	
	_obj.DtlBorder = Ti.UI.createView(_obj.style.headerBorder);

	_obj.lblCitizenShip = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblCitizenShip.font = TiFonts.FontStyle('lblSwissBold14');
	_obj.lblCitizenShip.text = 'Your Citizenship is of :';
	
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblCountry.left = 0;
	_obj.lblCountry.text = 'Country';
   
    _obj.updateCDOBView = Ti.UI.createScrollView(_obj.style.CDOBView);
	
	 if((Ti.Platform.osname === 'iphone')||(Ti.Platform.osname === 'ipad')){
       _obj.updateCDOBView.top= 40;
    }else{
  	_obj.updateCDOBView.top= 0;
    }
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	

	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderCountry = Ti.UI.createView(_obj.style.borderView);

    _obj.lblRecDtl = Ti.UI.createLabel(_obj.style.lblHeader);
	
	
	_obj.lblRecDtl.text = 'Details about your receiver';
	
	_obj.lblRecNote = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblRecNote.height = Ti.UI.SIZE;
	_obj.lblRecNote.font = TiFonts.FontStyle('lblSwissBold14');
	_obj.lblRecNote.text = 'Provide us your receiver’s Date of Birth';
	
	_obj.recDtlBorder = Ti.UI.createView(_obj.style.headerBorder);

    _obj.recNick = Ti.App.Properties.getString('receiverNickName');
    _obj.recFName= Ti.App.Properties.getString('receiverFName');
    _obj.recLName = Ti.App.Properties.getString('receiverLName');
	_obj.recDOB = Ti.App.Properties.getString('receiverDOB');
		
	   
    _obj.lblRNickNm = Ti.UI.createLabel(_obj.style.lblNote);
    _obj.lblRNickNm.font = TiFonts.FontStyle('lblSwissBold14'),
	_obj.lblRNickNm.text = 'Nick Name';
	
	_obj.valRNickNm = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.valRNickNm.height = 15;
	_obj.valRNickNm.text = _obj.recNick;
	
	_obj.lblRFNm = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblRFNm.font = TiFonts.FontStyle('lblSwissBold14'),
	_obj.lblRFNm.top = 10;
	_obj.lblRFNm.text = 'Name';
	
	_obj.valRFNm = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.valRFNm.height = 15;
	_obj.valRFNm.text = _obj.recFName +' '+ _obj.recLName;


    //DOB view
    
    _obj.recDOBview = Ti.UI.createView(_obj.style.dobView);
	_obj.lblRecDOB = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblRecDOB.left = 0;
	
		
	//if(_obj.recDOB != null || _obj.recDOB!== ''){
       _obj.split = _obj.recDOB.split('-');
       var mon1 = _obj.split[1];
   // getMonthFromString(mon1);
       _obj.mon =  new Date(Date.parse(mon1 +" 1, 2012")).getMonth()+1;
       _obj.lblRecDOB.text = _obj.split[0]+'/'+_obj.mon+'/'+_obj.split[2];
       _obj.beneDataArray =[_obj.recNick,_obj.recFName,_obj.recLName,_obj.split[0],_obj.mon,_obj.split[2]];
	   // }
	   // else{
	   // _obj.lblRecDOB.text = 'Date of Birth*'; 
	   // }
	//_obj.lblRecDOB.text = Ti.App.Properties.getString('receiverDOB');
	
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.recDOBborderView = Ti.UI.createView(_obj.style.borderView);

    _obj.recDOBview.addEventListener('click', function(e) {
		
	    require('/utils/DatePicker').DatePicker(_obj.lblRecDOB, 'dob18');
	    TiGlobals.dobCheckF = true;
	
	});
   
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.suBbutton);
	_obj.btnSubmit.title = 'SUBMIT';

	_obj.btnCancel = Ti.UI.createButton(_obj.style.btnCancel);
	_obj.btnCancel.title = 'CANCEL';

	_obj.updateCDOBView.add(_obj.lblUpdate);
	_obj.updateCDOBView.add(_obj.headerBorder);
	_obj.updateCDOBView.add(_obj.lblDear);
	_obj.updateCDOBView.add(_obj.lblNote);
	_obj.updateCDOBView.add(_obj.lblDtl);
	_obj.updateCDOBView.add(_obj.DtlBorder);
	_obj.updateCDOBView.add(_obj.lblCitizenShip);
	
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.updateCDOBView.add(_obj.countryView);
	_obj.updateCDOBView.add(_obj.borderCountry);
	
	_obj.updateCDOBView.add(_obj.lblRecDtl);
	_obj.updateCDOBView.add(_obj.lblRecNote);
	_obj.updateCDOBView.add(_obj.recDtlBorder);
	
	_obj.updateCDOBView.add(_obj.lblRNickNm);
	_obj.updateCDOBView.add(_obj.valRNickNm);
	_obj.updateCDOBView.add(_obj.lblRFNm);
	_obj.updateCDOBView.add(_obj.valRFNm);
    _obj.recDOBview.add(_obj.lblRecDOB);
	_obj.recDOBview.add(_obj.imgDOB);
	_obj.updateCDOBView.add(_obj.recDOBview);
	_obj.updateCDOBView.add(_obj.recDOBborderView); 
	
	_obj.updateCDOBView.add(_obj.btnSubmit);
	_obj.updateCDOBView.add(_obj.btnCancel);
 
	_obj.winCDOB.add(_obj.updateCDOBView);
	
	_obj.countryView.addEventListener('click', function() {
		country();
	});
	
	
		function country() {
		activityIndicator.showIndicator();
		var countryOptions = [];

		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"GETCOUNTRYRELATEDDETAILS",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"country":"' + countryName[0] + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if (e.result.responseFlag === "S") {
				var splitArr = e.result.citizenship.split('~');

				for (var i = 0; i < splitArr.length; i++) {
					countryOptions.push(splitArr[i]);
				}

				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country', countryOptions, -1);
				optionDialogCountry.show();

				optionDialogCountry.addEventListener('click', function(evt) {
					if (optionDialogCountry.options[evt.index] !== L('btn_cancel')) {
						_obj.lblCountry.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					} else {
						optionDialogCountry = null;
						_obj.lblCountry.text = 'Country';
					}
				});
			} else {
				if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
					require('/lib/session').session();
					destroy_updateCDOB();
				} else {
					require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
				}
			 }
			  xhr = null;
		    }

		     function xhrError(e) {
			  activityIndicator.hideIndicator();
			  require('/utils/Network').Network();
			  xhr = null;
		    }

	     }
	    //Submit event
	 _obj.btnSubmit.addEventListener('click', function(e){
	 	
	 	
	     
	   //Validations
	   // Passport Issued by
			if (_obj.lblCountry.text === 'Country') {
				require('/utils/AlertDialog').showAlert('', 'Please select the country', [L('btn_ok')]).show();
				return;
			}
			
			if(TiGlobals.dobFlag == true){
	 		//_obj.lblRecDOB.text = TiGlobals.recDOB;
	 		//var dobSlpit = TiGlobals.recDOB.split('/');
	 		
	 		if(TiGlobals.recDOB!== null){
	 		   var pickerVl = TiGlobals.recDOB ;
				var DOBsplit =_obj.lblRecDOB.text.split('-'); 
				var DOBsplit1 = DOBsplit[0].split('/');
	
				_obj.recDOBDay = DOBsplit1[0];
                _obj.recDOBYr = DOBsplit1[2];
				
				if(DOBsplit1[1] == 1){
					_obj.recDOBMon = 'Jan';
				}
				
				else if(DOBsplit1[1] == 2){
					_obj.recDOBMon = 'Feb';
				}
				
				else if(DOBsplit1[1] == 3){
					_obj.recDOBMon = 'Mar';
				}
				
				else if(DOBsplit1[1] == 4){
					_obj.recDOBMon = 'Apr';
				}
				
				else if(DOBsplit1[1] == 5){
					_obj.recDOBMon = 'May';
				}
				
				else if(DOBsplit1[1] == 6){
					_obj.recDOBMon = 'Jun';
				}
				
				else if(DOBsplit1[1] == 7){
					_obj.recDOBMon = 'Jul';
				}
				
				else if(DOBsplit1[1] == 8){
					_obj.recDOBMon = 'Aug';
				}
				
				else if(DOBsplit1[1] == 9){
					_obj.recDOBMon = 'Sep';
				}
				
				else if(DOBsplit1[1] == 10){
					_obj.recDOBMon = 'Oct';
				}
				
				else if(DOBsplit1[1] == 11){
					_obj.recDOBMon = 'Nov';
				}
				
				else if(DOBsplit1[1] == 12){
					_obj.recDOBMon = 'Dec';
				}
				else{}
			
       
			_obj.lblRecDOB.text = DOBsplit[0];
			//_obj.beneDataArray =[_obj.recNick,_obj.recFName,_obj.recLName,DOBsplit1[0],DOBsplit1[1],DOBsplit1[2]];
			//_obj.beneDataArray =[_obj.recNick,_obj.recFName,_obj.recLName,_obj.recDOBDay ,_obj.recDOBMon,_obj.recDOBYr];//_obj.recDOBDay = DOBsplit1[0];
               // _obj.recDOBYr = DOBsplit1[2];
	 		TiGlobals.dobFlag = false;
	 		}
	 		
	 	}
	 	
	    activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"UPDATENATIONALITYANDBENEDOB",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"senderNationality":"'+_obj.lblCountry.text+'",'+ 
				///'"beneDataArray":"'+_obj.beneDataArray+'"'+
			        // '"beneDataArray":["' + _obj.beneDataArray + '"]' +  //Updated on 03Nov17 - fix of UPDATENATIONALITYANDBENEDOB API issue 
		
                               '"beneDataArray": [{"beneNickname":"'+_obj.recNick+'","beneFirstName":"'+_obj.recFName+'","beneLastName":"'+_obj.recLName+'","birthDay":"'+_obj.recDOBDay+'","birthMonth":"'+_obj.recDOBMon+'","birthYear":"'+_obj.recDOBYr+'"}]'+
				 '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
				activityIndicator.hideIndicator();
			require('/utils/Console').info('Result ======== ' + e.result);
			
			
			if(e.result.responseFlag === "S")
			{
				TiGlobals.isNationality = 'Yes';
				if(TiGlobals.osname === 'android')
							{	
								require('/utils/AlertDialog').toast(e.result.message);
							        _obj = null;
							require('/utils/PageController').pageController('transfer');
                                                        }
							else
							{
								require('/utils/AlertDialog').iOSToast(e.result.message);
							_obj = null;
							 popView();
                       				        }
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	
	
		   
		   
		});
	
	    //Cancel event
		_obj.btnCancel.addEventListener('click', function(e){
			if(TiGlobals.osname === 'android')
							{	
								
							        _obj = null;
							require('/utils/PageController').pageController('transfer');
                                                        }
							else
							{
								
							_obj = null;
							 popView();
                       				        }
		
		});
	
		function destroy_updateCDOB() {
		try {
			require('/utils/Console').info('############## Remove updateCDOB start ##############');

			require('/utils/RemoveViews').removeViews(_obj.winCDOB);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_updateCDOB', destroy_updateCDOB);
			
			require('/utils/Console').info('############## Remove updateCDOB end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}

	Ti.App.addEventListener('destroy_updateCDOB', destroy_updateCDOB);

	return _obj.winCDOB;
};
module.exports = updateCitizenshipAndRecDOB;
