

function TransferMoney(_self) {
	
	Ti.App.Properties.setString('pg', 'transfer');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));

	var _obj = {
		style : require('/styles/transfer/TransferMoney').TransferMoney, // style
		txtOtherBank : null,
	    btnCancel:null,
		btnProceed: null,
		btnView: null,
	    otherBankMainView: null,
		otherBankView : null,
		transferView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		
		tabView : null,
		tabSelView : null,
		lblQuickPay : null,
		lblOneClickPay : null,
		lblSavedTransaction : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		// Quick Pay
		title : null,
		quickPayView : null,
		alertView : null,
		lblPaymentHeader : null,
		lblPaymode : null,
		imgPaymode : null,
		paymodeView : null,
		borderViewP1 : null,
		paymentDetailsView : null,
		lblTransactionHeader : null,
		lblReceiver : null,
		imgReceiver : null,
		receiverView : null,
		borderViewP2 : null,
		lblPurpose : null,
		imgPurpose : null,
		purposeView : null,
		borderViewP3 : null,
		txtAmount : null,
		borderViewP4 : null,
		lblExchangeRate : null,
		lblNote : null,
		lblPt1 : null,
		lblPt2 : null,
		lblPt3 : null,
		lblPt4 : null,
		lblPt5 : null,
		lblPt6 : null,
		pmc1:null,
		transactionDetailsView : null,
		lblBenefits : null,
		imgBenefits : null,
		benefitsView : null,
		promoView : null,
		//lblRecommended : null,
		//imgRecommended : null,
		//recommendedView : null,
		assuranceView : null,
		btnSendMoney : null,
		imgMORBanner : null,
		lblMORBannerHead : null,
		lblMORBannerTxt : null,
		MORTxtView : null,
		MORBannerView : null,
		lblPromo1 : null,
		borderViewPT1 : null,
		lblPromoNormalType1 : null,
		imgPromoCheck1 : null,
		promoNormalView1 : null,	
		borderViewPT2 : null,
		lblPromoNormalType2 : null,
		imgPromoCheck2 : null,
		promoNormalView2 : null,
		borderViewPT3 : null,
		txtPromoCode1 : null,
		borderViewPromoCode1 : null,
		borderViewPTB1 : null,
		lblPromo2 : null,
		borderViewPT4 : null,
		lblPromoNormalType3 : null,
		imgPromoCheck3 : null,
		promoNormalView3 : null,	
		borderViewPT5 : null,
		lblPromoNormalType4 : null,
		imgPromoCheck4 : null,
		promoNormalView4 : null,
		borderViewPT6 : null,
		txtPromoCode2 : null,
		borderViewPromoCode2 : null,
		borderViewPTB2 : null,
		
		// One Click Pay
		lblLastTransaction : null,
		tblOneClick : null,
		oneClickPayView : null,
		
		// Saved Transaction
		savedTransactionView : null,
		lblLastSavedTransaction : null,
		tblSavedTxn : null,
		
		//FW
		fwVoucherView : [],
		lblFW : [],
		imgFW : [],
		
		//FX
		lblFXHeader : null,
		
		// Variables
		tab : null,
		paymentOptions : [],
		paymodeCode : [],
		
		bankDetailsSel : null,
		paymodeResult : null,
		purposeOptions : [],
		purposeCodeOptions : [],
		purposeCode : null,
		receiverOptions : [],
		receiverDetails : [],
		receiverSel : null,
		transactionInfoResult : null,
		listOptions : [],
		listDisplayOptions : [],
		bankDetails : [],
		bankDetailsSel : null,
		pmc:null,
	    paymodeSelected : null,
		promoShow : 0,
		//recommendShow : 0,
		fxVoucherRedeem : 'no',
		voucherMaxQty : null,
		mobileAlerts : 'No',
		transactionInsurance : '',
		isKYCDetailsPresent : null,
		isFirstTransaction : null,
		isOccupationPresent : null,
		minExchange : null,
		maxExchange : null,
		
		fxfwVoucherSel : null,
		freewayMinAmt : null,
		voucherCount : '', 
		voucherCode : '',
		morRefferalAmnt : null,
		
		// First Txn
		
		contactDate : '',
		timeFrom : '',
		timeTo : '',
		timeFrom1 : '',
		timeTo1 : '',
		
		quickFlag : 0,
		oneClickFlag : 0,
		savedTxnFlag : 0,
		
		//CMN41
		fname:null,
		lname:null,
		state:null,
		paymodecheck:false,
        ipAddr:null,
        
       recData : [], //cmn84a
        params : null
		
	};
	
	function ipCheck(){   //IPADDRESS issue fix added on 06-Sept-17
	 if(Ti.App.Properties.getString('loginStatus') !== '0'){
	 Ti.App.Properties.setString('loginStatus','0');
	 var alertDialog = require('/utils/AlertDialog').showAlert('', 'Dear User, for security reason please login again and book the transaction. Sorry for the inconvenience caused.', [L('btn_ok')]);
     alertDialog.show();
     alertDialog.addEventListener('click', function(e) {
     	try{
			alertDialog.hide();
			  Ti.App.fireEvent('loginData');
			    try{
			    require('/utils/RemoveViews').removeAllScrollableViews(); // Move to dashboard
			    }
			    catch(e){
			    	Ti.API.info("in catch");
			    }
				alertDialog=null;
				 }catch(e){}
			});
      }
  }
  //
  
  if(TiGlobals.ipAddress == '127.0.0.1'){
				ipCheck();
  }
	         
   var xmlhttp = Ti.Network.createHTTPClient();
	              xmlhttp.onload = function()
	              {
		           var hostipInfo = JSON.parse(xmlhttp.responseText);
		           TiGlobals.ipAddress = hostipInfo.ip;
                   xmlhttp = null;
	              };
	
	             xmlhttp.onerror = function(e)
	             {
		            // require('/utils/AlertDialog').toast('Error in genrating IP Address.');
	             };
	             xmlhttp.open('GET', 'https://geoip.nekudo.com/api',true);
	             xmlhttp.send();
	             
    

	_obj.fxfwVoucherSel = '';
	_obj.tab = 'quick';
 
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	// CreateUI
    
	_obj.transferView = Ti.UI.createView(_obj.style.transferView);
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Send Money';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_ok'), L('btn_cancel')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				popView();
				alertDialog = null;
			}
		});
	});
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblQuickPay = Ti.UI.createLabel(_obj.style.lblQuickPay);
	_obj.lblQuickPay.text = 'Quick Pay';
	_obj.lblOneClickPay = Ti.UI.createLabel(_obj.style.lblOneClickPay);
	_obj.lblOneClickPay.text = 'One-Click Pay';
	_obj.lblSavedTransactions = Ti.UI.createLabel(_obj.style.lblSavedTransactions);
	_obj.lblSavedTransactions.text = 'Saved Transactions';
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.mainView.add(_obj.headerView);
	_obj.tabView.add(_obj.lblQuickPay);
	_obj.tabView.add(_obj.lblOneClickPay);
	_obj.tabView.add(_obj.lblSavedTransactions);
	_obj.mainView.add(_obj.tabView);
	_obj.mainView.add(_obj.tabSelView);
	_obj.mainView.add(_obj.tabSelIconView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.mainView.add(_obj.scrollableView);
	_obj.transferView.add(_obj.mainView);
	
	// Quick Pay View
	
	//Added on 12Sept17 for CMN70b
	_obj.lblPt4 = Ti.UI.createLabel(_obj.style.lblPt);
	//_obj.lblPt4.height = 0;
	_obj.lblPt5 = Ti.UI.createLabel(_obj.style.lblPt);
	//_obj.lblPt5.height = 0;
	_obj.lblPt5.hide();
	
	_obj.quickPayView = Ti.UI.createScrollView(_obj.style.quickPayView);
	
	function quickPay()
	{
		//CMN-43 by Pallavi
		    var currentdate1 = require('/utils/Date').today('-', 'ymd');
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestName":"ALERTMESSAGE",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"corridor":"'+origSplit[0]+'",'+
					'"source":"mobile",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"type":"WAP",'+
					'"currentdate":"'+currentdate1+'"'+
				'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.status === "S")
				{
				 // var count =JSON.stringify(e.result.response.length);
				 // Ti.API.info("**********",count);
				  for(var i=0;i<e.result.response.length;i++)
				    {
				    	if(e.result.response[i].message !== ''){
							var title2 = e.result.response[i].title;
							var title1 = title2.toUpperCase();
														
							var row = Ti.UI.createTableViewRow({
								hasChild:false,
                                height:80
                                 });
                                 
						    var innerView = Ti.UI.createView({
									   height :Ti.UI.SIZE,
									   backgroundColor:'#FCFFDD',
									   borderRadius:4,
									   borderWidth:1,
									 //  borderColor:'#f7ff8f'
									    borderColor:'#ffcf8f'
									 });
									 
						    var alert = Ti.UI.createImageView({
									 	image:'/images/r2I_alert_ico.png',
										    width:16,
										    height:16,
										    left:10
									 });
									  
                          _obj.title = Ti.UI.createLabel({
							           left:35,
		                               font : {
								          fontSize : 14,
								          fontWeight : 'bold'
								          },
							           top:10,
							           width: Ti.UI.FILL,
							           text:title1 +':'
							        });
							
							 var message = Ti.UI.createLabel({
						                   top:24,
			                               text:e.result.response[i].message +'\n',
							               left:35,
							               font:TiFonts.FontStyle('lblNormal14'),
							        });
                       } //if close
				    	 innerView.add(alert);
				    	 innerView.add(_obj.title);
	                     innerView.add(message);
	                     row.add(innerView);
	                     _obj.alertView.appendRow(row);
	               } //for end
				 } //if status end
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_kyc();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
			
			////CMN end here
		
	if(origSplit[0] === 'US')
		{
			_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
			_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
			_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
		}
		
		
		_obj.alertView = Ti.UI.createTableView(_obj.style.tableView1);
		
		_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
		_obj.lblPaymentHeader.text = 'Payment Details';
		
		_obj.paymodeView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblPaymode = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblPaymode.text = 'Select Your Payment Mode*';
		_obj.imgPaymode = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewP1 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.paymentDetailsView = Ti.UI.createView(_obj.style.paymentDetailsView);
		
		_obj.transactionDetailsView = Ti.UI.createView(_obj.style.transactionDetailsView);
		_obj.lblTransactionHeader = Ti.UI.createLabel(_obj.style.lblTransactionHeader); 
		_obj.lblTransactionHeader.text = 'Transaction Details';
		
		_obj.receiverView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblReceiver = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblReceiver.text = 'Select/Add Receiver*';
		_obj.imgReceiver = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewP2 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.purposeView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblPurpose = Ti.UI.createLabel(_obj.style.lblDD);
		/*if(_obj.lblPurpose.length > 26){
			//lblDD : _.defaults({top:0,left:0,right:20}, textField),
			_obj.lblPurpose = {
					height:70,
					left:TiGlobals.osname === 'android' ? 15 : 10,
					textAlign:'left',
					font:TiFonts.FontStyle('lblNormal14'),
					color:TiFonts.FontStyle('blackFont'),
					backgroundColor:'transparent',
					borderColor:'transparent',
					borderWidth:1,
					paddingLeft:10,
					paddingRight:20,
					autoComplete:false,
					autocorrect:false,
					autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
				};
		}
		else{
			_obj.lblPurpose = Ti.UI.createLabel(_obj.style.lblDD);
		}*/
		_obj.lblPurpose.text = 'Select Purpose of Remittance*';
		_obj.imgPurpose = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewP3 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.doneAmount = Ti.UI.createButton(_obj.style.done);
		_obj.doneAmount.addEventListener('click',function(){
			_obj.txtAmount.blur();
		});
		
		_obj.txtAmount = Ti.UI.createTextField(_obj.style.txtAmount);
		_obj.txtAmount.hintText = 'Enter Amount ('+origSplit[1]+')';
		
		//_obj.txtAmount.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtAmount.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
		Ti.API.info("Sending Amount++++--->",_obj.txtAmount.keyboardType);
		Ti.API.info("Sending Amount++++--->",JSON.stringify(_obj.txtAmount.keyboardType));
		_obj.txtAmount.keyboardToolbar = [_obj.doneAmount];
		sendamt = _obj.txtAmount;
		Ti.API.info("Sending Amount--->",sendamt);
		_obj.borderViewP4 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtAmount.addEventListener('change',function(e){
			try{
			Ti.API.info('MIN = '+_obj.minExchange+', MAX = '+_obj.maxExchange);
			
			if(_obj.lblPaymode.text !== 'Select Your Payment Mode*')
			{
				if(_obj.txtAmount.value.charAt(0) == '0')
				{
					_obj.lblExchangeRate.text = 'Value cannot be less than ' + _obj.minExchange;  //bug.690
					_obj.txtAmount.value = _obj.minExchange;
		
				}
				
				if(_obj.txtAmount.value === '')
				{
					_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = --.-- ' + destSplit[1];
				}
				else if((parseInt(_obj.txtAmount.value) < parseInt(_obj.minExchange)))
				{
					//if(_obj.txtAmount.value.length > 1)
					if(_obj.txtAmount.value.length > 0)
					{
						_obj.lblExchangeRate.text = 'Value cannot be less than ' + _obj.minExchange;
					}
				}
				else if((parseInt(_obj.txtAmount.value) > _obj.maxExchange))
				{
					_obj.lblExchangeRate.text = 'Value cannot be greater than '+_obj.maxExchange;
					_obj.txtAmount.value = _obj.maxExchange;
				}
				else if((parseInt(_obj.txtAmount.value) > _obj.maxExchange))
				{
					_obj.lblExchangeRate.text = 'Value cannot be greater than '+_obj.maxExchange;
					_obj.txtAmount.value = _obj.maxExchange;
				}
				else
				{
					getExchangeRate(_obj.paymodeSelected,_obj.txtAmount.value);
				}
			}
		
			else
			{
				require('/utils/AlertDialog').showAlert('','Please select Your Payment Mode',[L('btn_ok')]).show();
				_obj.quickPayView.scrollTo(0,0);
				_obj.txtAmount.blur();
	    		return;
			}
			}catch(e){}
		});
		
		_obj.lblExchangeRate = Ti.UI.createLabel(_obj.style.lblTransactionHeader); 
		_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = --.-- ' + destSplit[1];
		
		_obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
		_obj.lblNote.text = 'Note';
		
		_obj.lblPt1 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblPt1.text = '\u00B7 The exchange rate offered for Guaranteed Rate Transfer is a guaranteed exchange rate. The same shall be displayed in the pre confirmation page.';
		
		_obj.lblPt2 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblPt2.text = '\u00B7 You can only use your existing verified bank account to transfer funds using Guaranteed Rate option.';
		
		_obj.lblPt3 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblPt3.bottom = 20;
		_obj.lblPt3.text = '\u00B7 In case of non-receipt of funds within 2 working days for any reason, TOML may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.';
		
						
		// Apply Benefits
		
		_obj.benefitsView = Ti.UI.createView(_obj.style.collapsibleView);
		_obj.lblBenefits = Ti.UI.createLabel(_obj.style.lblCollapsible);
		_obj.lblBenefits.text = 'Apply Your Benefits';
		_obj.imgBenefits = Ti.UI.createImageView(_obj.style.imgCollapsible);
		
		_obj.promoView = Ti.UI.createView(_obj.style.promoView);
		_obj.promoView.hide();
		_obj.promoView.height = 0;
		
		// Normal Promo 
		_obj.lblPromo1 = Ti.UI.createLabel(_obj.style.lblPromoHead);
		_obj.lblPromo1.text =  'Select Your Promo';
		_obj.promoNormalView1 = Ti.UI.createView(_obj.style.promoSelView);
		_obj.promoNormalView1.sel = 'T1';
		_obj.borderViewPT1 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.lblPromoNormalType1 = Ti.UI.createLabel(_obj.style.lblPromoType);
		_obj.lblPromoNormalType1.sel = 'T1';
		_obj.lblPromoNormalType1.text = 'Agent Code';
		_obj.imgPromoCheck1 = Ti.UI.createImageView(_obj.style.imgPromoCheck);
		_obj.imgPromoCheck1.sel = 'T1';
		_obj.borderViewPT2 = Ti.UI.createView(_obj.style.borderPromoView);
		
		_obj.txtPromoCode1 = Ti.UI.createTextField(_obj.style.txtPromoCode);
		_obj.borderViewPromoCode1 = Ti.UI.createView(_obj.style.borderView);
		_obj.txtPromoCode1.editable = false;
		_obj.txtPromoCode1.height = 0;
		_obj.borderViewPromoCode1.height = 0;
		_obj.borderViewPTB1 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.borderViewPTB1.height = 0;
		_obj.borderViewPTB1.top = 0;
		
		
		// Special Promo
		
		_obj.promoNormalView2 = Ti.UI.createView(_obj.style.promoSelView);
		_obj.promoNormalView2.sel = 'T2';
		_obj.lblPromoNormalType2 = Ti.UI.createLabel(_obj.style.lblPromoType);
		_obj.lblPromoNormalType2.sel = 'T2';
		_obj.lblPromoNormalType2.text = 'R2I Coupon';
		_obj.imgPromoCheck2 = Ti.UI.createImageView(_obj.style.imgPromoCheck);
		_obj.imgPromoCheck2.sel = 'T2';
		_obj.borderViewPT3 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.txtPromoCode2 = Ti.UI.createTextField(_obj.style.txtPromoCode);
		_obj.borderViewPromoCode2 = Ti.UI.createView(_obj.style.borderView);
		_obj.txtPromoCode2.editable = false;
		_obj.txtPromoCode2.height = 0;
		_obj.borderViewPromoCode2.height = 0;
		_obj.borderViewPTB2 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.borderViewPTB2.height = 0;
		_obj.borderViewPTB2.top = 0;
		
		_obj.promoView.addEventListener('click',function(e){
			switch(e.source.sel)
			{
				case 'T1':
					if(_obj.imgPromoCheck1.image === '/images/black_radio_unsel.png')
					{
						_obj.promoNormalView1.backgroundColor = '#f4343b';
						_obj.imgPromoCheck1.image = '/images/white_radio_sel.png';
						_obj.lblPromoNormalType1.color = '#fff';
						
						_obj.promoNormalView2.backgroundColor = '#fff';
						_obj.imgPromoCheck2.image = '/images/black_radio_unsel.png';
						_obj.lblPromoNormalType2.color = '#000';
						
						_obj.txtPromoCode1.editable = true;
						_obj.txtPromoCode1.hintText = 'Enter your promotional code';
						_obj.txtPromoCode1.height = 35;
						_obj.borderViewPromoCode1.height = 1;
						_obj.borderViewPTB1.height = 1;
						_obj.borderViewPTB1.top = 5;
						
						_obj.txtPromoCode2.editable = false;
						_obj.txtPromoCode2.hintText = 'Enter your R2I Coupons';
						_obj.txtPromoCode2.height = 0;
						_obj.borderViewPromoCode2.height = 0;
						_obj.borderViewPTB2.height = 0;
						_obj.borderViewPTB2.top = 0;
					}
					else {
						  if(_obj.imgPromoCheck1.image === '/images/white_radio_sel.png')
					        {
						_obj.promoNormalView1.backgroundColor = '#fff';
						_obj.imgPromoCheck1.image = '/images/black_radio_unsel.png';
						_obj.lblPromoNormalType1.color = '#000';
						_obj.lblPromoNormalType1.text = 'Agent Code';
						
						_obj.txtPromoCode1.editable = false;
						_obj.txtPromoCode1.hintText = 'Enter your promotional code';
						_obj.txtPromoCode1.height = 0;
						_obj.borderViewPromoCode1.height = 0;
						_obj.borderViewPTB1.height = 0;
						_obj.borderViewPTB1.top = 0;

						   }
					}
					
				break;
				
				case 'T2':
					if(_obj.imgPromoCheck2.image === '/images/black_radio_unsel.png')
					{
						_obj.promoNormalView1.backgroundColor = '#fff';
						_obj.imgPromoCheck1.image = '/images/black_radio_unsel.png';
						_obj.lblPromoNormalType1.color = '#000';
						
						_obj.promoNormalView2.backgroundColor = '#f4343b';
						_obj.imgPromoCheck2.image = '/images/white_radio_sel.png';
						_obj.lblPromoNormalType2.color = '#fff';
						
						_obj.txtPromoCode1.editable = true;
						_obj.txtPromoCode1.hintText = 'Enter your promotional code';
						_obj.txtPromoCode1.height = 0;
						_obj.borderViewPromoCode1.height = 0;
						_obj.borderViewPTB1.height = 0;
						_obj.borderViewPTB1.top = 0;
						
						_obj.txtPromoCode2.editable = true;
						_obj.txtPromoCode2.hintText = 'Enter your R2I Coupons';
						_obj.txtPromoCode2.height = 35;
						_obj.borderViewPromoCode2.height = 1;
						_obj.borderViewPTB2.height = 1;
						_obj.borderViewPTB2.top = 5;
					}
					else {
						  if(_obj.imgPromoCheck2.image === '/images/white_radio_sel.png')
					        {
						_obj.promoNormalView2.backgroundColor = '#fff';
						_obj.imgPromoCheck2.image = '/images/black_radio_unsel.png';
						_obj.lblPromoNormalType2.color = '#000';
						_obj.lblPromoNormalType2.text = 'R2I Coupon';
						
						_obj.txtPromoCode2.editable = false;
						_obj.txtPromoCode2.hintText = 'Enter your R2I Coupons';
						_obj.txtPromoCode2.height = 0;
						_obj.borderViewPromoCode2.height = 0;
						_obj.borderViewPTB2.height = 0;
						_obj.borderViewPTB2.top = 0;

						   }
					}
				break;
			}
		});
		
		var rowBody = []; 
		var lblB1 = []; 
		var lblB2 = []; 
		var lblB3 = []; 
		var lblB4 = []; 
		var viewB5 = []; 
		var txtB5 = [];
		var done = [];
		var borderB = [];
		
		_obj.benefitsView.addEventListener('click',function(e){
			try{
			if(_obj.promoShow%2 === 0)
			{
				if(_obj.transactionInfoResult !== null)
				{
					_obj.imgBenefits.image = '/images/transfer_hide.jpg';
					
					if(_obj.transactionInfoResult.txnParameter[0].promoCode === 'Yes' || _obj.transactionInfoResult.txnParameter[0].specialPromoCode === 'Yes')
					{
						if(_obj.promoShow === 0) // Add only first time
						{
							_obj.promoView.add(_obj.lblPromo1);
							_obj.promoView.add(_obj.borderViewPT1);
						}
					}
					
					if(_obj.transactionInfoResult.txnParameter[0].promoCode === 'Yes')
					{
						if(_obj.promoShow === 0) // Add only first time
						{
							_obj.promoNormalView1.add(_obj.lblPromoNormalType1);
							_obj.promoNormalView1.add(_obj.imgPromoCheck1);
							_obj.promoView.add(_obj.promoNormalView1);	
							_obj.promoView.add(_obj.borderViewPT2);
							_obj.promoView.add(_obj.txtPromoCode1);
							_obj.promoView.add(_obj.borderViewPromoCode1);
							_obj.promoView.add(_obj.borderViewPTB1);
						}
						
						_obj.promoView.show();
						_obj.promoView.height = Ti.UI.SIZE;
					}
					
					if(_obj.transactionInfoResult.txnParameter[0].specialPromoCode === 'Yes')
					{
						if(_obj.promoShow === 0) // Add only first time
						{
							_obj.promoNormalView2.add(_obj.lblPromoNormalType2);
							_obj.promoNormalView2.add(_obj.imgPromoCheck2);
							_obj.promoView.add(_obj.promoNormalView2);
							_obj.promoView.add(_obj.borderViewPT3);
							_obj.promoView.add(_obj.txtPromoCode2);
							_obj.promoView.add(_obj.borderViewPromoCode2);
							_obj.promoView.add(_obj.borderViewPTB2);
						}
						
						_obj.promoView.show();
						_obj.promoView.height = Ti.UI.SIZE;
					}
					
					if(_obj.transactionInfoResult.txnParameter[0].promoCode === 'No' && _obj.transactionInfoResult.txnParameter[0].specialPromoCode === 'No')
					{
						_obj.quickPayView.remove(_obj.benefitsView);
					}
					
					// Get Voucher CONFIG
					
					activityIndicator.showIndicator();
					var xhr = require('/utils/XHR_BCM');
			
					xhr.call({
						url : TiGlobals.appURLBCM,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"'+TiGlobals.bcmConfig+'",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"source":"'+TiGlobals.bcmSource+'",'+
							'"type":"voucherSettingsExtra",'+
							'"platform":"'+TiGlobals.osname+'",'+
							'"corridor":"'+origSplit[0]+'"'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
			
					function xhrSuccess(evt) {
						//try{
						if(evt.result.status === "S")
						{
							if(evt.result.response[0].voucherSettings !== '' && evt.result.response[0].voucherSettings !== null)
							{
								_obj.voucherMaxQty = evt.result.response[0].voucherSettings.maxqty;
								_obj.voucherType = evt.result.response[0].voucherSettings.voucherType;
								
								if(_obj.voucherType !== null)
								{
									var xhr1 = require('/utils/XHR');
							
									xhr1.call({
										url : TiGlobals.appURLTOML,
										get : '',
										post : '{' +
											'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
											'"requestName":"GETACTIVEVOUCHERDETAILS",'+
											'"partnerId":"'+TiGlobals.partnerId+'",'+
											'"channelId":"'+TiGlobals.channelId+'",'+
											'"ipAddress":"'+TiGlobals.ipAddress+'",'+
											'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
											'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
											'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
											'"originatingCountry":"'+origSplit[0]+'",'+ 
											'"originatingCurrency":"'+origSplit[1]+'",'+
											'"destinationCountry":"'+destSplit[0]+'",'+
											'"destinationCurrency":"'+destSplit[1]+'"'+
											'}',
										success : xhr1Success,
										error : xhr1Error,
										contentType : 'application/json',
										timeout : TiGlobals.timer
									});
							
									function xhr1Success(evt1) {
										try{
										require('/utils/Console').info('Result ======== ' + evt1.result);
										activityIndicator.hideIndicator();
										
										if(evt1.result.responseFlag === "S")
										{
											if(_obj.voucherType === 'FW')
											{
												_obj.freewayMinAmt = evt.result.response[0].voucherSettings.freewayMinAmt;
												
												if(_obj.fwVoucherView.length === 0)
												{
													var sel=0;
													
													for(var i=0;i<evt1.result.voucherArray.length;i++)
													{
														// FW Voucher View
														
														_obj.fwVoucherView[i] = Ti.UI.createView(_obj.style.fwVoucherView);
														_obj.fwVoucherView[i].sel = evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit;
														_obj.fwVoucherView[i].vid = i;
														_obj.lblFW[i] = Ti.UI.createLabel(_obj.style.lblFW);
														_obj.lblFW[i].sel = evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit;
														_obj.lblFW[i].vid = i;
														_obj.lblFW[i].text = 'Subscribe to Remit2India\'s ' + evt1.result.voucherArray[i].voucherDesc + ' for only ' + origSplit[0] + ' ' + evt1.result.voucherArray[i].voucherFee + ' and pay zero transfer fees on all transactions upto ' + evt.result.response[0].voucherSettings.lastDate + '.';
														_obj.imgFW[i] = Ti.UI.createImageView(_obj.style.imgFW);
														_obj.imgFW[i].sel = evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit;
														_obj.imgFW[i].vid = i;
														_obj.imgFW[i].voucherCode = evt1.result.voucherArray[i].voucherCode;
														
														if(origSplit[0]=='UK'){                   //changes added by Sanjivani on 17Nov 16 for bug.719
														//_obj.fwVoucherView[i].add(_obj.lblFW[i]);
														//_obj.fwVoucherView[i].add(_obj.imgFW[i]);
														//_obj.promoView.add(_obj.fwVoucherView[i]);
														}
														else{
															_obj.fwVoucherView[i].add(_obj.lblFW[i]);
															_obj.fwVoucherView[i].add(_obj.imgFW[i]);
															_obj.promoView.add(_obj.fwVoucherView[i]);
														}
														_obj.voucherCount = '';
														
														_obj.fwVoucherView[i].addEventListener('click',function(efw){
															
															if(_obj.imgFW[efw.source.vid].image === '/images/checkbox_unsel.png')
															{
																for(var i=0;i<evt1.result.voucherArray.length;i++)
																{
																	_obj.imgFW[i].image = '/images/checkbox_unsel.png';
																	_obj.fxfwVoucherSel = 0;
																	_obj.voucherCount = '';
																}
																
																_obj.fxfwVoucherSel = 1;
																_obj.imgFW[efw.source.vid].image = '/images/checkbox_sel.png';
																_obj.voucherCount = _obj.voucherCount + 1;
																_obj.voucherCode = _obj.imgFW[efw.source.vid].voucherCode;
															}
															else
															{
																if(_obj.voucherCount > 0)
																{
																	_obj.fxfwVoucherSel = 0;
																	_obj.imgFW[efw.source.vid].image = '/images/checkbox_unsel.png';
																	_obj.voucherCount = _obj.voucherCount - 1;
																}
																
																_obj.voucherCode = '';
															}
														});
													}
												}
											}
											
											if(_obj.voucherType === 'FX')
											{
												if(_obj.lblFXHeader === null)
												{	
													_obj.freewayMinAmt = evt.result.response[0].voucherSettings.freewayMinAmt;
													
													_obj.lblFXHeader = Ti.UI.createLabel(_obj.style.lblFXHeader);
													_obj.lblFXHeader.text = 'Select the FXvoucher of your choice and the quantity to get extra paisa on all your transactions.';
													_obj.borderTopFXView = Ti.UI.createView(_obj.style.borderPromoView);
													_obj.borderTopFXView.top = 10;
													_obj.voucherView = Ti.UI.createView(_obj.style.voucherView);
													_obj.voucherView.height = (35*(evt1.result.voucherArray.length+1)+(evt1.result.voucherArray.length+1));
													
													var rowHeader = Ti.UI.createView({
														top : 0,
														left : 0,
														right : 0,
														height : 35,
														backgroundColor : '#c9c9c9',
														horizontalWrap:false,
														layout:'horizontal'
													});
													
													var lblH1 = Ti.UI.createLabel({
														text:'FXvoucher',
														top : 0,
														left : 0,
														width : '20%',
														height : 35,
														textAlign:'center',
														font:TiFonts.FontStyle('lblNormal10'),
														color:TiFonts.FontStyle('redFont'),
														backgroundColor : '#FFF'
													});
													
													var lblH2 = Ti.UI.createLabel({
														text:'Amount Cap',
														top : 0,
														left:TiGlobals.osname === 'android' ? 1 : 2,
														width : '20%',
														height : 35,
														textAlign:'center',
														font:TiFonts.FontStyle('lblNormal10'),
														color:TiFonts.FontStyle('redFont'),
														backgroundColor : '#FFF'
													});
													
													var lblH3 = Ti.UI.createLabel({
														text:'Fees',
														top : 0,
														left:TiGlobals.osname === 'android' ? 1 : 2,
														width : '20%',
														height : 35,
														textAlign:'center',
														font:TiFonts.FontStyle('lblNormal10'),
														color:TiFonts.FontStyle('redFont'),
														backgroundColor : '#FFF'
													});
													
													var lblH4 = Ti.UI.createLabel({
														text:'Extra Paisa',
														top : 0,
														left:TiGlobals.osname === 'android' ? 1 : 2,
														width : '20%',
														height : 35,
														textAlign:'center',
														font:TiFonts.FontStyle('lblNormal10'),
														color:TiFonts.FontStyle('redFont'),
														backgroundColor : '#FFF'
													});
													
													var lblH5 = Ti.UI.createLabel({
														text:'Quantity',
														top : 0,
														left:TiGlobals.osname === 'android' ? 1 : 2,
														width : '20%',
														height : 35,
														textAlign:'center',
														font:TiFonts.FontStyle('lblNormal10'),
														color:TiFonts.FontStyle('redFont'),
														backgroundColor : '#FFF'
													});
													
													var borderH = Ti.UI.createView(_obj.style.borderPromoView);
													
													rowHeader.add(lblH1);
													rowHeader.add(lblH2);
													rowHeader.add(lblH3);
													rowHeader.add(lblH4);
													rowHeader.add(lblH5);
													_obj.voucherView.add(rowHeader);
													_obj.voucherView.add(borderH);
													
													for(var i=0; i<evt1.result.voucherArray.length; i++)
													{
														rowBody[i] = Ti.UI.createView({
															top : 0,
															left : 0,
															right : 0,
															height : 35,
															backgroundColor : '#c9c9c9',
															horizontalWrap:false,
															layout:'horizontal'
														});
														
														if(i%2 === 0)
														{
															var bgColor = '#f5f5f5';
														}
														else
														{
															var bgColor = '#e5e5e5';
														}
														
														lblB1[i] = Ti.UI.createLabel({
															text:evt1.result.voucherArray[i].voucherDesc,
															top : 0,
															left : 0,
															width : '20%',
															height : 35,
															textAlign:'center',
															font:TiFonts.FontStyle('lblNormal10'),
															color:TiFonts.FontStyle('blackFont'),
															backgroundColor : bgColor
														});
														
														lblB2[i] = Ti.UI.createLabel({
															text:origSplit[1]+' '+evt1.result.voucherArray[i].voucherValue,
															top : 0,
															left:TiGlobals.osname === 'android' ? 1 : 2,
															width : '20%',
															height : 35,
															textAlign:'center',
															font:TiFonts.FontStyle('lblNormal10'),
															color:TiFonts.FontStyle('blackFont'),
															backgroundColor : bgColor
														});
														
														lblB3[i] = Ti.UI.createLabel({
															text:origSplit[1]+' '+evt1.result.voucherArray[i].voucherFee,
															top : 0,
															left:TiGlobals.osname === 'android' ? 1 : 2,
															width : '20%',
															height : 35,
															textAlign:'center',
															font:TiFonts.FontStyle('lblNormal10'),
															color:TiFonts.FontStyle('blackFont'),
															backgroundColor : bgColor
														});
														
														lblB4[i] = Ti.UI.createLabel({
															text:evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit,
															top : 0,
															left:TiGlobals.osname === 'android' ? 1 : 2,
															width : '20%',
															height : 35,
															textAlign:'center',
															font:TiFonts.FontStyle('lblNormal10'),
															color:TiFonts.FontStyle('blackFont'),
															backgroundColor : bgColor
														});
														
														viewB5[i] = Ti.UI.createView({
															top : 0,
															left:TiGlobals.osname === 'android' ? 1 : 2,
															width : '20%',
															height : 35,
															backgroundColor : bgColor
														});
														
														done[i] = Ti.UI.createButton(_obj.style.done);
														done[i].addEventListener('click',function(){
															for(var i=0; i<evt1.result.voucherArray.length; i++)
															{
																txtB5[i].blur();
															}
														});
														
														txtB5[i] = Ti.UI.createTextField({
												value:'0',
												width : '50%',
												height : 35,
												font:TiFonts.FontStyle('lblNormal10'),
												color:TiFonts.FontStyle('blackFont'),
												textAlign:'center',
												backgroundColor : 'transparent',
												borderColor:'#000',
												borderWidth:1,
												paddingLeft:5,
												paddingRight:5,
												autoComplete:false,
												autocorrect:false,
												autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
												keyboardType:Ti.UI.KEYBOARD_TYPE_NUMBER_PAD,
												keyboardToolbar:[done[i]],
												voucherCode:evt1.result.voucherArray[i].voucherCode,
												fxSel : evt1.result.voucherArray[i].voucherCode+'~'+evt1.result.voucherArray[i].voucherValue+'~'+evt1.result.voucherArray[i].voucherFee+'~'+evt1.result.voucherArray[i].voucherId+'~'+evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit,
												sel:i,
												bubbleParent:false
											});
											
											txtB5[i].addEventListener('click',function(e){
												_obj.fxfwVoucherSel = 0;
												_obj.voucherCount = '';
												_obj.voucherCode = '';
												for(var j=0; j<evt1.result.voucherArray.length; j++)
												{
													txtB5[j].value = '0';
												}
												
												setTimeout(function(){
													txtB5[e.source.sel].value = '';
													txtB5[e.source.sel].focus();
												},20);
											});
														
														txtB5[i].addEventListener('change',function(e){
															if(parseInt(e.source.value) > 0)
															{
																_obj.fxfwVoucherSel = 1;
																_obj.voucherCount = e.source.value;
																_obj.voucherCode = e.source.voucherCode;
															}
														});
														
														borderB[i] = Ti.UI.createView(_obj.style.borderPromoView);
														
														rowBody[i].add(lblB1[i]);
														rowBody[i].add(lblB2[i]);
														rowBody[i].add(lblB3[i]);
														rowBody[i].add(lblB4[i]);
														viewB5[i].add(txtB5[i]);
														rowBody[i].add(viewB5[i]);
														_obj.voucherView.add(rowBody[i]);
														_obj.voucherView.add(borderB[i]);
													}
													
													_obj.promoView.add(_obj.lblFXHeader);
													_obj.promoView.add(_obj.borderTopFXView);
													_obj.promoView.add(_obj.voucherView);
												}
											}
											
											if(parseInt(_obj.transactionInfoResult.txnParameter[0].morRefferalAmnt) > 0 && _obj.transactionInfoResult.txnParameter[0].morRefferalAmnt !== '')
											{
												_obj.morView = Ti.UI.createView(_obj.style.collapsibleView);
												_obj.lblMor = Ti.UI.createLabel(_obj.style.lblCollapsible);
												_obj.lblMor.text = 'Money On Referral';
												
												_obj.morMainView = Ti.UI.createView(_obj.style.transactionDetailsView);
												_obj.morMainView.top = 0;
												_obj.morRateView = Ti.UI.createView(_obj.style.morRateView);
												_obj.lblMOR1 = Ti.UI.createLabel(_obj.style.lblMOR1);
												_obj.lblMOR1.text = 'You have ';
												_obj.lblMOR2 = Ti.UI.createLabel(_obj.style.lblMOR2);
												_obj.lblMOR2.text = 'Rs. ' + _obj.transactionInfoResult.txnParameter[0].morRefferalAmnt;
												_obj.lblMOR3 = Ti.UI.createLabel(_obj.style.lblMOR3);
												_obj.lblMOR3.text = ' balance in your account.';
												
												_obj.doneRedeem = Ti.UI.createButton(_obj.style.done);
												_obj.doneRedeem.addEventListener('click',function(){
													_obj.txtRedeemAmount.blur();
												});
												
												_obj.txtRedeemAmount = Ti.UI.createTextField(_obj.style.txtRedeemAmount);
												_obj.txtRedeemAmount.hintText = 'Enter amount to redeem';
												_obj.txtRedeemAmount.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
												_obj.txtRedeemAmount.keyboardToolbar = [_obj.doneRedeem];
												_obj.morBorderView = Ti.UI.createView(_obj.style.borderView);
												
												_obj.lblMORMax = Ti.UI.createLabel(_obj.style.lblMORMax);
												_obj.lblMORMax.text = 'Max. Rs. 3,500/Transaction';
												
												_obj.morView.add(_obj.lblMor);
												_obj.morMainView.add(_obj.lblMOR1);
												_obj.morMainView.add(_obj.lblMOR2);
												_obj.morMainView.add(_obj.lblMOR3);
												_obj.morMainView.add(_obj.txtRedeemAmount);
												_obj.morMainView.add(_obj.morBorderView);
												_obj.morMainView.add(_obj.lblMORMax);
												_obj.promoView.add(_obj.morView);
												_obj.promoView.add(_obj.morMainView);
											}
										}
										else
										{
											if(evt1.result.message === L('invalid_session') || evt1.result.message === 'Invalid Session')
											{
												require('/lib/session').session();
											}
										}
										
										xhr1 = null;
										}catch(e){}
									}
							
									function xhr1Error(evt1) {
										activityIndicator.hideIndicator();
										require('/utils/Network').Network();
										xhr1 = null;
									}
								}
							}
							else // evt.result.response[0].voucherSettings !== '' && evt.result.response[0].voucherSettings !== null
							{
								activityIndicator.hideIndicator();
							}
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
						}
						xhr = null;
					}
			
					function xhrError(evt) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
				}
			}
			else
			{
				_obj.imgBenefits.image = '/images/transfer_show.jpg';
				_obj.promoView.hide();
				_obj.promoView.height = 0;
			}
			
			_obj.promoShow = _obj.promoShow + 1;
			}catch(e){}
		});
		
		// Recommended View
		
		/*_obj.recommendedView = Ti.UI.createView(_obj.style.collapsibleView);
		_obj.recommendedView.top = 2;
		_obj.lblRecommended = Ti.UI.createLabel(_obj.style.lblCollapsible);
		_obj.lblRecommended.text = 'Recommended Just For You!';
		_obj.imgRecommended = Ti.UI.createImageView(_obj.style.imgCollapsible);*/
		
		_obj.assuranceView = Ti.UI.createView(_obj.style.assuranceView);
		_obj.assuranceView.hide();
		_obj.assuranceView.height = 0;
		 
		_obj.lblAssuranceHead = Ti.UI.createLabel(_obj.style.lblAssuranceHead);
		_obj.lblAssuranceHead.text =  'Remit2India Assurance';
		
		_obj.lblAssuranceTxt = Ti.UI.createLabel(_obj.style.lblAssuranceTxt);
		_obj.lblAssuranceTxt.text =  'Secure your money transfers with Remit2India Assurance';
		
		// Yearly
		
		_obj.borderViewA1 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.YAFView = Ti.UI.createView(_obj.style.promoSelView);
		_obj.YAFView.sel = 'A1';
		_obj.lblYAF = Ti.UI.createLabel(_obj.style.lblPromoType);
		_obj.lblYAF.sel = 'A1';
		_obj.imgYAF = Ti.UI.createImageView(_obj.style.imgPromoCheck);
		_obj.imgYAF.sel = 'A1';
		_obj.borderViewA2 = Ti.UI.createView(_obj.style.borderPromoView);
		
		// This
		
		_obj.borderViewA3 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.SAFView = Ti.UI.createView(_obj.style.promoSelView);
		_obj.SAFView.sel = 'A2';
		_obj.lblSAF = Ti.UI.createLabel(_obj.style.lblPromoType);
		_obj.lblSAF.sel = 'A2';
		_obj.imgSAF = Ti.UI.createImageView(_obj.style.imgPromoCheck);
		_obj.imgSAF.sel = 'A2';
		_obj.borderViewA4 = Ti.UI.createView(_obj.style.borderPromoView);
		
		_obj.assuranceInfoView = Ti.UI.createView(_obj.style.assuranceInfoView);
		_obj.assuranceInfoMainView = Ti.UI.createView(_obj.style.assuranceInfoMainView);
		_obj.lblAssuranceMore = Ti.UI.createLabel(_obj.style.lblAssuranceInfoTxt);
		_obj.lblAssuranceMore.text = 'More Details';
		_obj.lblAssuranceMore.sel = 'more';
		_obj.lblAssuranceDash = Ti.UI.createLabel(_obj.style.lblAssuranceInfoTxt);
		_obj.lblAssuranceDash.text = '-';
		_obj.lblAssuranceDash.left = 5;
		_obj.lblAssuranceTerms = Ti.UI.createLabel(_obj.style.lblAssuranceInfoTxt);
		_obj.lblAssuranceTerms.left = 5;
		_obj.lblAssuranceTerms.text = 'Terms & Conditions';
		_obj.lblAssuranceTerms.sel = 'terms';
		
		// Mobile Alerts
		
		_obj.borderViewMA1 = Ti.UI.createView(_obj.style.borderPromoView);
		_obj.lblMobileAlerts = Ti.UI.createLabel(_obj.style.lblMobileAlerts);
		_obj.lblMobileAlerts.text =  'Mobile Alerts';
		_obj.borderViewMA2 = Ti.UI.createView(_obj.style.borderPromoView);
		
		_obj.mobileAlertsView = Ti.UI.createView(_obj.style.mobileAlertsView);
		_obj.imgMobileAlerts = Ti.UI.createImageView(_obj.style.imgMobileAlerts);
		_obj.lblMobileAlertsTxt = Ti.UI.createLabel(_obj.style.lblMobileAlertsTxt);
		_obj.lblMobileAlertsTxt.text =  'I would like to receive Mobile Alerts';
		_obj.borderViewMA3 = Ti.UI.createView(_obj.style.borderPromoView);
		
		// Mobile Alerts Subscribe
		
		_obj.mobileSubscribeView = Ti.UI.createView(_obj.style.mobileAlertsView);
		_obj.lblMobileSubscribeTxt = Ti.UI.createLabel(_obj.style.lblMobileAlertsTxt);
		_obj.lblMobileSubscribeTxt.left = null;
		_obj.lblMobileSubscribeTxt.text =  'Register for Mobile Alerts';
		
		// Common More Details
		
		_obj.mobileAlertInfoView = Ti.UI.createView(_obj.style.assuranceInfoView);
		_obj.lblMobileAlertInfo = Ti.UI.createLabel(_obj.style.lblAssuranceInfoTxt);
		_obj.lblMobileAlertInfo.left = null;
		_obj.lblMobileAlertInfo.text = 'More Details';
		
		_obj.mobileSubscribeView.addEventListener('click',function(e){
			require('/js/transfer/RegisterMobileAlertsModal').RegisterMobileAlertsModal();
		});
		
		_obj.mobileAlertInfoView.addEventListener('click',function(e){
			try{
			
			if(_obj.transactionInfoResult.txnParameter[0].isMobileAlertsDisplay === 'Yes')
			{
				require('/utils/AlertDialog').showAlert('','Our Mobile Alerts would keep you and your receiver updated about the status of the money transfer while being on the move. Receive end-to-end update on the status of the Money Transfer for as little as ' + _obj.transactionInfoResult.txnParameter[0].mobileAlertsFee +' ' + origSplit[1],[L('btn_ok')]).show(); 	
			}
			else if(_obj.transactionInfoResult.txnParameter[0].isMobileAlertsDisplay === 'Subscribe')
			{
				require('/js/transfer/MobileAlertsMoreModal').MobileAlertsMoreModal();
			}
			else
			{
				// do nothing
			}
			}catch(e){}
		});
		
		_obj.mobileAlertsView.addEventListener('click',function(e){
			if(_obj.imgMobileAlerts.image === '/images/checkbox_unsel.png')
			{
				_obj.imgMobileAlerts.image = '/images/checkbox_sel.png';
				_obj.mobileAlerts = 'Yes';
			}
			else
			{
				_obj.imgMobileAlerts.image = '/images/checkbox_unsel.png';
				_obj.mobileAlerts = 'No';
			}
		});
		
		_obj.assuranceInfoMainView.addEventListener('click',function(e){
			if(e.source.sel === 'more')
			{
				require('/js/transfer/TransferAssuranceMoreModal').TransferAssuranceMoreModal();
			}
			
			if(e.source.sel === 'terms')
			{
				require('/js/transfer/TransferAssuranceTermsModal').TransferAssuranceTermsModal();
			}
		});
		
		_obj.assuranceView.addEventListener('click',function(e){
			switch(e.source.sel)
			{
				case 'A1':
					if(_obj.imgYAF.image === '/images/black_radio_unsel.png')
					{
						_obj.YAFView.backgroundColor = '#f4343b';
						_obj.imgYAF.image = '/images/white_radio_sel.png';
						_obj.lblYAF.color = '#fff';
						
						_obj.SAFView.backgroundColor = '#fff';
						_obj.imgSAF.image = '/images/black_radio_unsel.png';
						_obj.lblSAF.color = '#000';
						
						_obj.transactionInsurance = 'Yearly';
					}
				break;
				
				case 'A2':
					if(_obj.imgSAF.image === '/images/black_radio_unsel.png')
					{
						_obj.YAFView.backgroundColor = '#fff';
						_obj.imgYAF.image = '/images/black_radio_unsel.png';
						_obj.lblYAF.color = '#000';
						
						_obj.SAFView.backgroundColor = '#f4343b';
						_obj.imgSAF.image = '/images/white_radio_sel.png';
						_obj.lblSAF.color = '#fff';
						
						_obj.transactionInsurance = 'Single';
					}
				break;
			}
		});
		
		/*_obj.recommendedView.addEventListener('click',function(e){
			if(_obj.recommendShow%2 === 0)
			{
				if(_obj.transactionInfoResult !== null)
				{
					_obj.imgRecommended.image = '/images/transfer_hide.jpg';
					
					if(_obj.transactionInfoResult.txnParameter[0].transactionInsurance === 'Yes')
					{
						if(_obj.recommendShow === 0) // Add only first time
						{
							//_obj.assuranceView.add(_obj.lblAssuranceHead);
							//_obj.assuranceView.add(_obj.lblAssuranceTxt);
							
							if(_obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee !== '' || _obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee !== '0')
							{
								_obj.lblYAF.text = _obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee + ' ' + origSplit[1] + ' for a whole year';
								//_obj.assuranceView.add(_obj.borderViewA1);
								_obj.YAFView.add(_obj.lblYAF);
								_obj.YAFView.add(_obj.imgYAF);
								//_obj.assuranceView.add(_obj.YAFView);
								//_obj.assuranceView.add(_obj.borderViewA2);
							}
							
							if(_obj.transactionInfoResult.txnParameter[0].singleAssuranceFee !== '' || _obj.transactionInfoResult.txnParameter[0].singleAssuranceFee !== '0')
							{
								if(_obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee === '' || _obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee === '0')
								{
									//_obj.assuranceView.add(_obj.borderViewA3);
								}
								_obj.lblSAF.text = _obj.transactionInfoResult.txnParameter[0].singleAssuranceFee + ' ' + origSplit[1] + ' for this transaction';
								_obj.SAFView.add(_obj.lblSAF);
								_obj.SAFView.add(_obj.imgSAF);
								//_obj.assuranceView.add(_obj.SAFView);
								//_obj.assuranceView.add(_obj.borderViewA4);
							}
							
							if(_obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee !== '' || _obj.transactionInfoResult.txnParameter[0].yearlyAssuranceFee !== '0' || _obj.transactionInfoResult.txnParameter[0].singleAssuranceFee !== '' || _obj.transactionInfoResult.txnParameter[0].singleAssuranceFee !== '0')
							{
								_obj.assuranceInfoMainView.add(_obj.lblAssuranceMore);
								_obj.assuranceInfoMainView.add(_obj.lblAssuranceDash);
								_obj.assuranceInfoMainView.add(_obj.lblAssuranceTerms);
								_obj.assuranceInfoView.add(_obj.assuranceInfoMainView);
								//_obj.assuranceView.add(_obj.assuranceInfoView);
							}
							
							if(_obj.transactionInfoResult.txnParameter[0].isMobileAlertsDisplay === 'Yes')
							{
								_obj.assuranceView.add(_obj.borderViewMA1);
								_obj.assuranceView.add(_obj.lblMobileAlerts);
								_obj.assuranceView.add(_obj.borderViewMA2);
								_obj.mobileAlertsView.add(_obj.imgMobileAlerts);
								_obj.mobileAlertsView.add(_obj.lblMobileAlertsTxt);
								_obj.assuranceView.add(_obj.mobileAlertsView);
								_obj.assuranceView.add(_obj.borderViewMA3);
								_obj.mobileAlertInfoView.add(_obj.lblMobileAlertInfo);
								_obj.assuranceView.add(_obj.mobileAlertInfoView);
								
							}
							
							if(_obj.transactionInfoResult.txnParameter[0].isMobileAlertsDisplay === 'Subscribe')
							{
								_obj.assuranceView.add(_obj.borderViewMA1);
								_obj.assuranceView.add(_obj.lblMobileAlerts);
								_obj.assuranceView.add(_obj.borderViewMA2);
								_obj.mobileSubscribeView.add(_obj.lblMobileSubscribeTxt);
								_obj.assuranceView.add(_obj.mobileSubscribeView);
								_obj.assuranceView.add(_obj.borderViewMA3);
								_obj.mobileAlertInfoView.add(_obj.lblMobileAlertInfo);
								_obj.assuranceView.add(_obj.mobileAlertInfoView);
							}
						}
						_obj.assuranceView.show();
						_obj.assuranceView.height = Ti.UI.SIZE;
					}
					else
					{
						
					}
				}
			}
			else
			{
				_obj.imgRecommended.image = '/images/transfer_show.jpg';
				_obj.assuranceView.hide();
				_obj.assuranceView.height = 0;	
			}
			
			_obj.recommendShow = _obj.recommendShow + 1;
		});
		*/
		_obj.MORBannerView = Ti.UI.createView(_obj.style.MORBannerView);
		_obj.imgMORBanner = Ti.UI.createImageView(_obj.style.imgMORBanner);
		_obj.MORTxtView = Ti.UI.createView(_obj.style.MORTxtView);
		_obj.lblMORBannerHead = Ti.UI.createLabel(_obj.style.lblMORBannerHead);
		_obj.lblMORBannerHead.text = 'Money On Referal';
		_obj.lblMORBannerTxt = Ti.UI.createLabel(_obj.style.lblMORBannerTxt);
		
		_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
		_obj.btnSendMoney.title = 'SEND MONEY';
		
		function createUIElements(ui)
		{
			_obj.paymentDetailsView.removeAllChildren();
			try{
			
			for(var i=0; i<_obj.paymodeResult.paymodeDetails[0].htmlFormFields.length; i++)
			{
				if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementType === 'TextBox')
				{
					var rangeSplit = _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].minMaxLength.split('~');
					
					_obj.paymentDetailsView.add(
						Ti.UI.createTextField({
							top:15,
							hintText:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].isMandatoryField === 'Y' ? _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel+'*' : _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel,
							maxLength:rangeSplit[1],
							left:TiGlobals.osname === 'android' ? 15 : 10,
							right:20,
							height:35,
							textAlign:'left',
							font:TiFonts.FontStyle('lblNormal14'),
							color:TiFonts.FontStyle('blackFont'),
							backgroundColor:'transparent',
							borderColor:'transparent',
							borderWidth:1,
							paddingLeft:10,
							paddingRight:20,
							autoComplete:false,
							autocorrect:false,
							autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
							htmlElementLabel:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel,
							webServiceToCall:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].webServiceToCall,
							webServiceArray:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].webServiceArray,
							webServiceElementKey:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].webServiceElementKey,
							isMandatoryField:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].isMandatoryField,
							allowableCharacters:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].allowableCharacters
						})
					);
					
					_obj.paymentDetailsView.add(
						Ti.UI.createView({
							top:0,
							left:20,
							right:20,
							height:1,
							backgroundColor:'#000'
						})
					);
				}
				else if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementType === 'List')
				{
					_obj.paymentDetailsView.add(
						Ti.UI.createLabel({
							top:10,
							text: _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].isMandatoryField === 'Y' ? 'Select ' + _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel+'*' : 'Select ' + _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel,
							left:20,
							right:20,
							height:40,
							textAlign:'left',
							font:TiFonts.FontStyle('lblNormal14'),
							color:TiFonts.FontStyle('blackFont'),
							htmlElementLabel:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel,
							webServiceToCall:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].webServiceToCall,
							webServiceArray:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].webServiceArray,
							webServiceElementKey:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].webServiceElementKey,
							isMandatoryField:_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].isMandatoryField,
							backgroundImage:'/images/bg_dropdown.png',
							child:i
						})
					);
					
					_obj.paymentDetailsView.add(
						Ti.UI.createView({
							top:0,
							left:20,
							right:20,
							height:1,
							backgroundColor:'#000'
						})
					);
					
					_obj.paymentDetailsView.children[i*2].addEventListener('click',function(evt){
						if(TiGlobals.ipAddress == '127.0.0.1'){
				              ipCheck();
                         }
	         
						var webServiceArray = evt.source.webServiceArray;
						var webServiceElementKey = evt.source.webServiceElementKey;
						
						activityIndicator.showIndicator();
						var xhr = require('/utils/XHR');
						
						if(evt.source.webServiceToCall === 'GETTRANSACTIONINFO')
						{
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"'+evt.source.webServiceToCall+'",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"paymodeCode":"'+_obj.paymodeSelected+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'"'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
											
						}
						else if(evt.source.webServiceToCall === 'GETCIPTRACEBANKLIST')
						{
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"'+evt.source.webServiceToCall+'",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"payModeCode":"'+_obj.paymodeSelected+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'"'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
						}
						
						function xhrSuccess(response) {
							require('/utils/Console').info('Result ======== ' + response.result);
							
							if(response.result.responseFlag === "S")
							{
								try{
								// Convert JSON to Array
								if(evt.source.webServiceToCall === 'GETTRANSACTIONINFO')
								{
									try{
									var arr = [];
									_obj.listDisplayOptions = [];
									}catch(e){}
									var data = response.result;
									for (var prop in data) {
										try{
											if(data[prop][0].accHolder)
											{
												arr.push(data[prop]);
											}
										}catch(e){Ti.API.info(e);}
									}
									
									//alert(arr[0].length);
									// Populate OptionDialog
									
								if(arr.length>0)
									{
										for(var i=0; i<arr[0].length; i++)
										{
											try{
											//_obj.listDisplayOptions.push(arr[0][i].bankName + ' - ' + arr[0][i].accNo);
											_obj.listDisplayOptions.push(arr[0][i].bankName);//+ ' - ' + arr[0][i].accNo);
											try{
											if(_obj.pmc == 'ACH')
											{
									    		//_obj.bankDetails.push(arr[0][i].bankName+'~'+arr[0][i].accNo+'~'+arr[0][i].achId);
									    		//_obj.bankDetails.push(arr[0][i].bankName);//+'~'+arr[0][i].accNo+'~'+arr[0][i].achId);
												_obj.bankDetails.push(arr[0][i].bankName+'~'+arr[0][i].accNo+'~'+arr[0][i].achId);
											}
											
											else if(_obj.pmc == 'CIP' || _obj.pmc == 'POLI')
											{
												_obj.bankDetails.push('~~');
												_obj.bankDetails.push(arr[0][i].bankName+'~'+arr[0][i].accNo);
												
											}
											else if(_obj.pmc == 'DEB')
											{
												//_obj.bankDetails.push(arr[0][i].bankName+'~'+arr[0][i].accNo+'~'+arr[0][i].debId);
												//_obj.bankDetails.push(arr[0][i].bankName);//+'~'+arr[0][i].accNo+'~'+arr[0][i].debId);
												_obj.bankDetails.push(arr[0][i].bankName+'~'+arr[0][i].accNo+'~'+arr[0][i].debId);
											}
											}catch(e){}	
											}catch(e){}
										}
									}
									
									_obj.listDisplayOptions.push('ADD BANK');
								}
								
								
								
								if(evt.source.webServiceToCall === 'GETCIPTRACEBANKLIST')
								{
									try{
									_obj.listDisplayOptions = [];
									
									if(response.result.bankDtls.length>0)
									{
										for(var i=0; i<response.result.bankDtls.length; i++)
										{
											_obj.listDisplayOptions.push(response.result.bankDtls[i].bankName);
											_obj.bankDetails.push(response.result.bankDtls[i].bankName+'~~');
										}
									}
									if(origSplit[0] !== 'CAN'){ //added on 8 Dec 17
									_obj.listDisplayOptions.push('Other Bank');  //added other bank option in bank list
									}
									}catch(e){}
								}
								
								var optionDialogList = require('/utils/OptionDialog').showOptionDialog(evt.source.htmlElementLabel,_obj.listDisplayOptions,-1);
								optionDialogList.show();   //displays bank list with other bank option
								
								setTimeout(function(){
									activityIndicator.hideIndicator();
								},200);
								
								}catch(e){}
								//if you click on bank list  option 
								 optionDialogList.addEventListener('click',function(evtDialog){
									if(optionDialogList.options[evtDialog.index] !== L('btn_cancel'))
									{
										if(optionDialogList.options[evtDialog.index] === 'ADD BANK')
										{
											try{
											if(_obj.pmc == 'ACH')
											{
									    		require('/js/ach/AddBankModal').AddBankModal();	 
											}
											else
											{
												require('/js/deb/AddBankModal').AddBankModal();
											}
											}catch(e){}
										}
										else if(optionDialogList.options[evtDialog.index] === 'Other Bank')  // if you click on other bank option of list
										{
											/*var alertDialog = require('/utils/AlertDialog').showAlert('', 'Functionality is in progress', [L('btn_ok')]);

											alertDialog.show();*/
											
											// Add other bank as modal popup
											var alertDialog = require('/utils/AlertDialog').showAlert('', 'Ensure that you mention the remittance transaction reference number and your name in the description field while initiating the remittance amount from your domestic bank account to our bank account.', [L('btn_ok')]);
											Ti.API.info("Check 1");
											alertDialog.show();
									
											alertDialog.addEventListener('click', function(e) {
												alertDialog.hide();
												if (e.index === 0 || e.index === "0") {
												   
													
													_obj.otherBankView = Ti.UI.createScrollView(_obj.style.otherBankView);
													_obj.otherBankMainView = Ti.UI.createView(_obj.style.otherBankMainView);
													_obj.txtOtherBank = Ti.UI.createTextField(_obj.style.txtOtherBank);
													_obj.txtOtherBank.hintText = 'Other Bank';
												
													_obj.btnView = Titanium.UI.createView(_obj.style.btnView);
													_obj.btnCancel = Titanium.UI.createButton(_obj.style.btnCancel);
													_obj.btnCancel.title = L('btn_cancel');
													_obj.btnProceed = Ti.UI.createButton(_obj.style.btnProceed);
													_obj.btnProceed.title = L('btn_proceed');
													
													_obj.otherBankMainView.add(_obj.txtOtherBank);
													_obj.btnView.add(_obj.btnCancel);
													_obj.btnView.add(_obj.btnProceed);
													_obj.otherBankMainView.add(_obj.btnView);
													_obj.otherBankView.add(_obj.otherBankMainView);
													_obj.transferView.add(_obj.otherBankView);
													
													_obj.btnProceed.addEventListener('click',function(e){
														
														if(_obj.txtOtherBank.value === '')
														{

															require('/utils/AlertDialog').showAlert('','Please enter Bank Name',[L('btn_ok')]).show();
															_obj.txtOtherBank.value = '';
															_obj.txtOtherBank.focus();
												    		return;
														}
														else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtOtherBank.value) === false)
														{
															
															require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Bank Name',[L('btn_ok')]).show();
															_obj.txtOtherBank.value = '';
															_obj.txtOtherBank.focus();
															return;
														}
														else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtOtherBank.value) === false)
														{
															require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Bank Name',[L('btn_ok')]).show();
															_obj.txtOtherBank.value = '';
															_obj.txtOtherBank.focus();
															return;
														}
														else
														{   Ti.API.info("Check 2 in else");
														     
														    _obj.bankDetailsSel =  _obj.txtOtherBank.value;
															_obj.paymentDetailsView.children[evt.source.child].text = _obj.txtOtherBank.value;

															Ti.API.info("_obj.bankDetailsSel:",+_obj.bankDetailsSel);
															
															_obj.transferView.remove(_obj.otherBankView);
															_obj.otherBankMainView.remove(_obj.txtOtherBank);
															_obj.btnView.remove(_obj.btnCancel);
															_obj.btnView.remove(_obj.btnProceed);
															_obj.otherBankMainView.remove(_obj.btnView);
															_obj.otherBankView.remove(_obj.otherBankMainView);
															
															_obj.otherBankView = null;
															_obj.btnCancel = null;
															_obj.btnProceed = null;
															_obj.btnView = null;
															_obj.otherBankMainView = null;
														}
														
														
													});
													
													_obj.btnCancel.addEventListener('click',function(e){
														_obj.paymentDetailsView.children[evt.source.child].text = evt.source.isMandatoryField === 'Y' ? 'Select ' + evt.source.htmlElementLabel+'*' : 'Select ' + evt.source.htmlElementLabel;
														Ti.API.info("Am cancelled");
														_obj.transferView.remove(_obj.otherBankView);
														_obj.otherBankMainView.remove(_obj.txtOtherBank);
														_obj.btnView.remove(_obj.btnCancel);
														_obj.btnView.remove(_obj.btnProceed);
														_obj.otherBankMainView.remove(_obj.btnView);
														_obj.otherBankView.remove(_obj.otherBankMainView);
														
														_obj.otherBankView = null;
														_obj.btnCancel = null;
														_obj.btnProceed = null;
														_obj.btnView = null;
														_obj.otherBankMainView = null;
													});
													
												}
											});
										}
										else
										{
											_obj.paymentDetailsView.children[evt.source.child].text = optionDialogList.options[evtDialog.index];
											
											_obj.bankDetailsSel = _obj.bankDetails[evtDialog.index];
                                           
											Ti.API.info("_obj.bankDetailsSel111",+_obj.bankDetailsSel);
											
											if(origSplit[0] == 'CAN' && _obj.pmc == 'CIP')  //CMN84a
											{
												require('/utils/AlertDialog').showAlert('', 'Ensure that you mention the remittance transaction reference number and your name in the description field while initiating the remittance amount from your domestic bank account to our bank account.', [L('btn_ok')]).show();
											}
											
											optionDialogList = null;
										}
									}
									else
									{
										optionDialogList = null;
										_obj.paymentDetailsView.children[evt.source.child].text = evt.source.isMandatoryField === 'Y' ? 'Select ' + evt.source.htmlElementLabel+'*' : 'Select ' + evt.source.htmlElementLabel;
									}
								});
							}
							else
							{
								if(response.result.message === L('invalid_session') || response.result.message === 'Invalid Session')
								{
									require('/lib/session').session();
								}
								else
								{
									try{
									
									//require('/utils/AlertDialog').showAlert('',response.result.message,[L('btn_ok')]).show();
									_obj.listDisplayOptions = [];
									if(origSplit[0] !== 'CAN'){
									_obj.listDisplayOptions.push('Other Bank');
									}
									var optionDialogList = require('/utils/OptionDialog').showOptionDialog(evt.source.htmlElementLabel,_obj.listDisplayOptions,-1);
								optionDialogList.show();
								
								setTimeout(function(){
									activityIndicator.hideIndicator();
								},200);
								
								optionDialogList.addEventListener('click',function(evtDialog){
									if(optionDialogList.options[evtDialog.index] !== L('btn_cancel'))
									{
										if(optionDialogList.options[evtDialog.index] === 'ADD BANK')
										{
											try{
											if(_obj.pmc == 'ACH')
											{
									    		require('/js/ach/AddBankModal').AddBankModal();	 
											}
											else
											{
												require('/js/deb/AddBankModal').AddBankModal();
											}
											}catch(e){}
										}
										else if(optionDialogList.options[evtDialog.index] === 'Other Bank')
										{
											
											/*var alertDialog = require('/utils/AlertDialog').showAlert('', 'Functionality is in progress', [L('btn_ok')]);

											alertDialog.show();*/
											
											// Add other bank as modal popup
											var alertDialog = require('/utils/AlertDialog').showAlert('', 'Ensure that you mention the remittance transaction reference number and your name in the description field while initiating the remittance amount from your domestic bank account to our bank account.', [L('btn_ok')]);
											Ti.API.info("Check 21");
											alertDialog.show();
									
											alertDialog.addEventListener('click', function(e) {
												alertDialog.hide();
												if (e.index === 0 || e.index === "0") {
													alertDialog = null;
													
													var otherBankView = Ti.UI.createScrollView(_obj.style.otherBankView);
													var otherBankMainView = Ti.UI.createView(_obj.style.otherBankMainView);
													var txtOtherBank = Ti.UI.createTextField(_obj.style.txtOtherBank);
													txtOtherBank.hintText = 'Other Bank';
													_obj.paymentDetailsView.children[evt.source.child].text = txtOtherBank.value;
													var btnView = Titanium.UI.createView(_obj.style.btnView);
													var btnCancel = Titanium.UI.createButton(_obj.style.btnCancel);
													btnCancel.title = L('btn_cancel');
													var btnProceed = Ti.UI.createButton(_obj.style.btnProceed);
													btnProceed.title = L('btn_proceed');
													
													otherBankMainView.add(txtOtherBank);
													btnView.add(btnCancel);
													btnView.add(btnProceed);
													otherBankMainView.add(btnView);
													otherBankView.add(otherBankMainView);
													_obj.transferView.add(otherBankView);
													
													btnProceed.addEventListener('click',function(e){
														
														if(txtOtherBank.value === '')
														{ 
															
															require('/utils/AlertDialog').showAlert('','Please enter Bank Name',[L('btn_ok')]).show();
												    		txtOtherBank.value = '';
												    		txtOtherBank.focus();
												    		return;
														}
														else if(require('/lib/toml_validations').checkBeginningSpace(txtOtherBank.value) === false)
														{
														
															require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Bank Name',[L('btn_ok')]).show();
															txtOtherBank.value = '';
															txtOtherBank.focus();
															return;
														}
														else if(require('/lib/toml_validations').checkinLastSpace(txtOtherBank.value) === false)
														{
															
															require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Bank Name',[L('btn_ok')]).show();
												    		txtOtherBank.value = '';
												    		txtOtherBank.focus();
															return;
														}
														else
														{
															
															
															//_obj.bankDetails.push(txtOtherBank.value+'~~');
															_obj.bankDetailsSel =  txtOtherBank.value;
															_obj.paymentDetailsView.children[evt.source.child].text = txtOtherBank.value;

															Ti.API.info("_obj.bankDetailsSel:",+_obj.bankDetailsSel);
															_obj.transferView.remove(otherBankView);
															otherBankMainView.remove(txtOtherBank);
															btnView.remove(btnCancel);
															btnView.remove(btnProceed);
															otherBankMainView.remove(btnView);
															otherBankView.remove(otherBankMainView);
															
															otherBankView = null;
															btnCancel = null;
															btnProceed = null;
															btnView = null;
															otherBankMainView = null;
														}
														
														
													});
													
													btnCancel.addEventListener('click',function(e){
														_obj.paymentDetailsView.children[evt.source.child].text = evt.source.isMandatoryField === 'Y' ? 'Select ' + evt.source.htmlElementLabel+'*' : 'Select ' + evt.source.htmlElementLabel;
														
														_obj.transferView.remove(otherBankView);
														otherBankMainView.remove(txtOtherBank);
														btnView.remove(btnCancel);
														btnView.remove(btnProceed);
														otherBankMainView.remove(btnView);
														otherBankView.remove(otherBankMainView);
														
														otherBankView = null;
														btnCancel = null;
														btnProceed = null;
														btnView = null;
														otherBankMainView = null;
													});
													
												}
											});
										}
										else
										{
											_obj.paymentDetailsView.children[evt.source.child].text = optionDialogList.options[evtDialog.index]; //other bank -->optionList--->PaymentDetails
											
											_obj.bankDetailsSel = _obj.bankDetails[evtDialog.index];
											Ti.API.info("_obj.bankDetailsSel of list of bank",+_obj.bankDetailsSel);
											/*if(_obj.pmc == 'CIP')
											{
												require('/utils/AlertDialog').showAlert('', 'Ensure that you mention the remittance transaction reference number and your name in the description field while initiating the remittance amount from your domestic bank account to our bank account.', [L('btn_ok')]).show();
											}*/
											
											optionDialogList = null;
										}
									}
									else
									{
										optionDialogList = null;
										_obj.paymentDetailsView.children[evt.source.child].text = evt.source.isMandatoryField === 'Y' ? 'Select ' + evt.source.htmlElementLabel+'*' : 'Select ' + evt.source.htmlElementLabel;
									}
								});
								}catch(e){}
								}
							}
							xhr = null;
							activityIndicator.hideIndicator();
						}
				
						function xhrError(response) {
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr = null;
						}
					});	
				}
				else
				{
					
				}
			}
			}catch(e){}
		}
		
		if(origSplit[0] === 'US')
		{
			_obj.infoTextView.add(_obj.imgInfo);
			_obj.infoTextView.add(_obj.lblInfo);
			_obj.quickPayView.add(_obj.infoTextView);
			_obj.infoTextView.height = 0;
		}
		
		_obj.quickPayView.add(_obj.alertView);
		_obj.quickPayView.add(_obj.lblPaymentHeader);
		_obj.paymodeView.add(_obj.lblPaymode);
		_obj.paymodeView.add(_obj.imgPaymode);
		_obj.quickPayView.add(_obj.paymodeView);
		_obj.quickPayView.add(_obj.borderViewP1);
		
		_obj.quickPayView.add(_obj.paymentDetailsView);
		
		_obj.transactionDetailsView.add(_obj.lblTransactionHeader);
		_obj.receiverView.add(_obj.lblReceiver);
		_obj.receiverView.add(_obj.imgReceiver);
		_obj.transactionDetailsView.add(_obj.receiverView);
		_obj.transactionDetailsView.add(_obj.borderViewP2);
		_obj.purposeView.add(_obj.lblPurpose);
		_obj.purposeView.add(_obj.imgPurpose);
		_obj.transactionDetailsView.add(_obj.purposeView);
		_obj.transactionDetailsView.add(_obj.borderViewP3);
		_obj.transactionDetailsView.add(_obj.txtAmount);
		_obj.transactionDetailsView.add(_obj.borderViewP4);
		_obj.transactionDetailsView.add(_obj.lblExchangeRate);
		
		if(origSplit[0] !== 'UK'){                            //changes done for CMN 36
		_obj.transactionDetailsView.add(_obj.lblNote);
		}
		
		
		
		if(origSplit[0] !== 'AUS' && origSplit[0] !== 'UK'&& origSplit[0] !== 'US'){       //changes done for CMN 36
		_obj.transactionDetailsView.add(_obj.lblPt1);
		}
		/*if(origSplit[0] === 'US')
		{
			_obj.transactionDetailsView.add(_obj.lblPt2);	
		}*/
		
		//added by sanjivani on 18 Jan 17 for Bug no.1827
		_obj.txtAmount.addEventListener('change', function(){
			//setTimeout(function(){ 
			if(_obj.txtAmount.value > 2000)
			{
			Ti.API.info("_obj.txtAmount.value",+_obj.txtAmount.value);
			if(_obj.paymodeSelected === "POLIFRT" || _obj.paymodeSelected === 'ACHGFXI')
		     {
				//alert()
			var alertDialog = require('/utils/AlertDialog').showAlert('','You can send a maximum of AUD 2000 per transaction through this paymode. If you wish to send higher amount select the other paymode.', [L('btn_ok')]);
			alertDialog.show();
		     }
			}
			//},300);
		});
		
		if(origSplit[0] !== 'UK' && origSplit[0] !== 'US'){                 //changes done for CMN 36
		_obj.transactionDetailsView.add(_obj.lblPt3);
		}
		
		_obj.quickPayView.add(_obj.transactionDetailsView);
		
		_obj.benefitsView.add(_obj.lblBenefits);
		_obj.benefitsView.add(_obj.imgBenefits);
		_obj.quickPayView.add(_obj.benefitsView);
		_obj.quickPayView.add(_obj.promoView);
		
		var timer = setInterval(function(){
									
			try{	
				if(_obj.transactionInfoResult !== null)
				{
					if(_obj.transactionInfoResult.txnParameter[0].transactionInsurance === 'Yes')
					{
						//_obj.recommendedView.add(_obj.lblRecommended);
						//_obj.recommendedView.add(_obj.imgRecommended);
						//_obj.quickPayView.add(_obj.recommendedView);
						_obj.quickPayView.add(_obj.assuranceView);
					}
					
					_obj.quickPayView.add(_obj.btnSendMoney);
					
					var xhr = require('/utils/XHR_BCM');
			
					xhr.call({
						url : TiGlobals.appURLBCM,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"'+TiGlobals.bcmConfig+'",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"source":"'+TiGlobals.bcmSource+'",'+
							'"type":"socialInfo",'+
							'"platform":"'+TiGlobals.osname+'",'+
							'"corridor":"'+origSplit[0]+'"'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
			
					function xhrSuccess(evt) {
						if(evt.result.status === "S")
						{   
							try{
							if(evt.result.response[0].socialInfo.referamount !== '0')
							{
								_obj.lblMORBannerTxt.text = 'Avail Benefit worth Rs. '+evt.result.response[0].socialInfo.referamount+' for every NRI friend that you refer';
								
								_obj.MORBannerView.add(_obj.imgMORBanner);
								_obj.MORTxtView.add(_obj.lblMORBannerHead);
								_obj.MORTxtView.add(_obj.lblMORBannerTxt);
								_obj.MORBannerView.add(_obj.MORTxtView);
								_obj.quickPayView.add(_obj.MORBannerView);
							}
							}catch(e){Ti.API.info(e);}
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
						}
						xhr = null;
					}
					
					function xhrError(evt) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
					
					clearInterval(timer);
					timer = null;
				}
			}
			catch(e){Ti.API.info(e);}
		}, 200);
		
		_obj.receiverView.addEventListener('click',function(e){
			activityIndicator.showIndicator();
			if(TiGlobals.ipAddress == '127.0.0.1'){
				ipCheck();

	         } else{
			//CMN84a
			var start = 1;
	        var max = 100;
	        var accountNo = 0;
	
	//_obj.tblRecipients.setData([]);
		
		    var xhr1 = require('/utils/XHR');

		  xhr1.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BENEFICIARYLISTING",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"'+start+'",'+
				'"rangeTo":"'+max+'"'+
				'}',
			success : xhrSuccess1,
			error : xhrError1,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess1(e) {
			
			require('/utils/Console').info('Result ======== ' + e.result);
				_obj.receiverOptions = [];
				_obj.receiverDetails = [];
				//_obj.recData = [];
				if(e.result.responseFlag === "S")
				{
					try{ // if not added
						for(var i=0; i<e.result.RecieverData.length; i++)
						{
							_obj.receiverOptions.push(e.result.RecieverData[i].nickName);
							_obj.receiverDetails.push(e.result.RecieverData[i].nickName +'~' + e.result.RecieverData[i].parentChildFlag +'~' + Ti.App.Properties.getString('ownerId') +'~' + Ti.App.Properties.getString('loginId') +'~' + e.result.RecieverData[i].firstName +'~' + e.result.RecieverData[i].lastName +'~' + e.result.RecieverData[i].dob +'~' + e.result.RecieverData[i].micr); //CMN 78 //updated on 22Nov-17 for CMN84a
						    _obj.recData.push(e.result.RecieverData[i]);
						}
					}catch(e){
						Ti.API.info(e);
					}
					
					_obj.receiverOptions.push('ADD RECEIVER');
					
					var optionDialogReceiver = require('/utils/OptionDialog').showOptionDialog('Receiver',_obj.receiverOptions,-1);
					optionDialogReceiver.show();
					
					setTimeout(function(){
						activityIndicator.hideIndicator();
					},200);
					
					optionDialogReceiver.addEventListener('click',function(evt){
						//var recDtls = null;
						if(optionDialogReceiver.options[evt.index] !== L('btn_cancel'))
						{
							if(optionDialogReceiver.options[evt.index] === 'ADD RECEIVER')
							{
								require('/js/recipients/AddRecipientModal').AddRecipientModal();
							}
							else
							{
								_obj.lblReceiver.text = optionDialogReceiver.options[evt.index];
								_obj.receiverSel = _obj.receiverDetails[evt.index];
								//_obj.recData.push(e.result.receiverData[i]);
								Ti.API.info("_obj.recData.length",+_obj.recData.length);
								//Ti.API.info("_obj.RecieverData.length",+_obj.RecieverData.length);
								_obj.params = _obj.recData[evt.index];
						
								
								Ti.API.info("IN RECEIVER",_obj.receiverSel);
								optionDialogReceiver = null;
	          
	                           var receiverSplit = _obj.receiverSel.split('~');
		
		                       var rMICR =  receiverSplit[7];   //CMN84a
				
								//CMN84a
								if(origSplit[0] == 'CAN' && _obj.pmc == 'CIP') {
								if(rMICR == ' ' || rMICR == 'F'){ 	
								var dialog = Ti.UI.createAlertDialog({
                                     buttonNames: ['Continue', 'Skip & Book'],  //0 , 1
                                     message: 'Dear Customer, please update your beneficiary details to ensure smooth processing of your future transaction',
                                   });
									dialog.show();
									
									dialog.addEventListener('click',function(e){ 
						            if(e.index == 0)
						              {
						              	//alert(_obj.recData[evt.index]);
									    require('/js/recipients/EditRecipientModal').EditRecipientModal(_obj.params);//(recData[evt.index]);
									    
								      }
							});
						  }
						 }
						}
					}
						else
						{
							try{
							optionDialogReceiver = null;
							_obj.lblReceiver.text = 'Select/Add Receiver*';
							}catch(e){}
						}
					});	
				}
		
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_viewrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr1 = null;
		}
    
		function xhrError1(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr1 = null;
		}

					
			//
			
		
		
			}
		});
		
		_obj.purposeView.addEventListener('click',function(e){
			if(TiGlobals.ipAddress == '127.0.0.1'){
				ipCheck();
            }
	         
			if(_obj.lblPaymode.text === 'Select Your Payment Mode*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a paymode code',[L('btn_ok')]).show();
				_obj.quickPayView.scrollTo(0,0);
	    		return;
			}
			
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETPURPOSELIST",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					//'"paymodeCode":"'+_obj.pmc+'",'+
					'"paymodeCode": "'+_obj.paymodeSelected+'",'+
					'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'",'+
					'"purposeCode":"",'+
					'"purposeDesc":"",'+
					'"destCountry":"'+countryCode[0]+'",'+
			    	'"destCurrency":"'+countryCode[1]+'",'+
					'"origCountry":"'+origSplit[0]+'",'+ 
					'"origCurrency":"'+origSplit[1]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
			
			function xhrSuccess(evt) {
				require('/utils/Console').info('Result ======== ' + evt.result);
				activityIndicator.hideIndicator();
				require('/utils/Console').info('Result ======== ');
				if(evt.result.responseFlag === "S")
				{
					// Populate Purpose
					_obj.purposeOptions = [];
					require('/utils/Console').info('Result ???????');
					for(var i=0;i<evt.result.purposeListData.length;i++)
					{
						_obj.purposeOptions.push(evt.result.purposeListData[i].purposeLabel);
						_obj.purposeCodeOptions.push(evt.result.purposeListData[i].purposeCode+'~'+evt.result.purposeListData[i].purposeDesc);
					}
					
					var optionDialogPurpose = require('/utils/OptionDialog').showOptionDialog('Purpose',_obj.purposeOptions,-1);
					optionDialogPurpose.show();
					
					optionDialogPurpose.addEventListener('click',function(evt){
						if(optionDialogPurpose.options[evt.index] !== L('btn_cancel'))
						{
							_obj.lblPurpose.text = optionDialogPurpose.options[evt.index];
							_obj.purposeCode = _obj.purposeCodeOptions[evt.index];
							optionDialogPurpose = null;
						}
						else
						{
							optionDialogPurpose = null;
							_obj.purposeCode = null;
							_obj.lblPurpose.text = 'Select Purpose of Remittance*';
						}
					});
				}
				else
				{
					require('/utils/Console').info('Result ******** ');
					if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
					{
						require('/utils/Console').info('Result ------ ');
						require('/lib/session').session();
						destroy_transfer();
					}
					else
					{
						require('/utils/Console').info('Result +++++++ ');
						require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
					}
				}
				
				xhr = null;
			}
	
			
			function xhrError(evt) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
		
		function getExchangeRate(paymode,amt)
		{
			var xhr = require('/utils/XHR');
	
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETEXCHANGERATE",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"originatingCurrency":"'+origSplit[1]+'",'+
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'",'+
					'"paymodeCode":"'+_obj.pmc+'",'+
					'"amount":"'+amt+'",'+
					'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				require('/utils/Console').info('Result ======== ' + e.result);
				try{
				if(e.result.responseFlag === "S")
				{
					//require('/utils/Console').info(e.result);
					_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = ' + e.result.exchangeRate + ' ' + destSplit[1];
				}
				xhr = null;
				}catch(e){}
			}
	
			function xhrError(e) {
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		
		_obj.paymodeView.addEventListener('click',function(e){
			try{
			// Populate paymodes
			_obj.paymentOptions = [];
			_obj.paymodeCode = [];
			_obj.paymodeLabel = [];
			
			if(_obj.transactionInfoResult !== null)
			{
				for(var i=0;i<_obj.transactionInfoResult.corridorData.length;i++)
				{
					_obj.paymodeLabel.push(_obj.transactionInfoResult.corridorData[i].paymodeLabel);
					_obj.paymentOptions.push(_obj.transactionInfoResult.corridorData[i].paymodeName);
					_obj.paymodeCode.push(_obj.transactionInfoResult.corridorData[i].paymodeCode);
				}
				
				var optionDialogPaymode = require('/utils/OptionDialog').showOptionDialog('Paymodes',_obj.paymodeLabel,-1);
				optionDialogPaymode.show();
				
				optionDialogPaymode.addEventListener('click',function(evt){
					try{
					if(optionDialogPaymode.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblPaymode.text = optionDialogPaymode.options[evt.index];
						Ti.API.info("Paymode Lable is:" +_obj.lblPaymode.text );
						_obj.paymodeSelected = _obj.paymodeCode[evt.index];
						try{
						if(origSplit[0] === 'US' && _obj.paymodeSelected.indexOf('WIRE') > -1){
							try{
							_obj.pmc = 'WIREFRT';
							TiGlobals.paymode = 'WIREFRT';  //CMN 70-b
							//_obj.paymodecheck=true;
							notedispaly(TiGlobals.paymode);
                             }catch(e){}
                          }
						else if(_obj.paymodeSelected.indexOf('ACH') > -1)
						{
							try{
							_obj.pmc = 'ACH';
							TiGlobals.paymode ='ACH';
							//_obj.paymodecheck=true;
							notedispaly(TiGlobals.paymode);
							}catch(e){}
						}
						else if(_obj.paymodeSelected.indexOf('WIRE') > -1)
						{
							try{
							_obj.pmc = 'WIRE';
							TiGlobals.paymode ='WIRE';
							}catch(e){}
						}
						else if(_obj.paymodeSelected.indexOf('POLI') > -1)
						{
							try{
							_obj.pmc = 'POLI';
							TiGlobals.paymode ='POLI';
							}catch(e){}
						}
						else if(_obj.paymodeSelected.indexOf('DEB') > -1)
						{
							try{
							_obj.pmc = 'DEB';
							TiGlobals.paymode = 'DEB';
							}catch(e){}
						}
						else if(_obj.paymodeSelected.indexOf('CIP') > -1)
						{
							try{
							_obj.pmc = 'CIP';
							TiGlobals.paymode = 'CIP';
							}catch(e){}
						}

						else if(_obj.paymodeSelected.indexOf('POLIFRT') > -1)
						{
							try{
							_obj.pmc = 'POLIFRT';
							TiGlobals.paymode = 'POLIFRT';
							}catch(e){}
						}
						else if(_obj.paymodeSelected.indexOf('CCARD') > -1)
						{
							try{
							_obj.pmc = 'CCARD';
							TiGlobals.paymode = 'CCARD';
							require('/utils/AlertDialog').showAlert('','Additional charges may be levied by your issuing bank for using card. Charges may differ from one bank to another.',[L('btn_ok')]).show();
							//return;
							}catch(e){}
						}
						else
						{
							_obj.pmc = '';
							TiGlobals.paymode = '';
							return;
						}
					
						//*************Sanju code starts here
						//if(origSplit[0]=='UAE'&& _obj.pmc == 'CCARD')
						if(origSplit[0]=='UAE' || origSplit[0]=='FRA'  )
						{
							if(_obj.pmc !== 'CCARD'){
							//activityIndicator.showIndicator();
							var xhr = require('/utils/XHR');
					
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"GETTRANSACTIONPAYMODEINFO",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"paymodeCode": "'+_obj.paymodeSelected+'",'+
									//'"paymodeCode": "'+_obj.pmc+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'"'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
					
							function xhrSuccess(e) {
								try{
								_obj.paymodeResult = e.result;
								createUIElements(e.result);
								xhr = null;
                                }catch(e){}
								
								/////////////// Get Exchange Rate Min Max /////////////////
								
								var xhrExchange = require('/utils/XHR');
							
								xhrExchange.call({
									url : TiGlobals.appURLTOML,
									get : '',
									post : '{' +
										'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
										'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
										'"partnerId":"'+TiGlobals.partnerId+'",'+
										'"channelId":"'+TiGlobals.channelId+'",'+
										'"ipAddress":"'+TiGlobals.ipAddress+'",'+
										'"originatingCountryCode":"'+origSplit[0]+'",'+ 
										'"originatingCurrencyCode":"'+origSplit[1]+'",'+
										'"destinationCountryCode":"'+destSplit[0]+'",'+
										'"destinationCurrencyCode":"'+destSplit[1]+'",'+
										'"payModeCode":"'+_obj.paymodeSelected+'",'+
										//'"paymodeCode": "'+_obj.pmc+'",'+
										'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
										'}',
									success : xhrExchangeSuccess,
									error : xhrExchangeError,
									contentType : 'application/json',
									timeout : TiGlobals.timer
								});
								
								function xhrExchangeSuccess(evt) {
									try{
									require('/utils/Console').info('Result 1======== ' + JSON.stringify(evt.result));
									activityIndicator.hideIndicator();
									
									var minExchange = [];
									var maxExchange = [];
									
									if(evt.result.responseFlag === "S")
									{
										Ti.API.info("Welcome1"); 
                                          try{
										
				     						Ti.API.info("out of  UAE-----Welcome3");
										for(var s=0; s<evt.result.exchangeRateDtls.length;s++)
				     					{
				     						minExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeFrom));
				     						maxExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeTo));
				     					}
				     					
				     					minExchange.sort(function(a,b){return a-b;});
				     					maxExchange.sort(function(a,b){return b-a;});
				     					
				     					_obj.minExchange = minExchange[0];
				     					_obj.maxExchange = maxExchange[0];
				     					//}
				     					if(origSplit[0] === 'US')
										{
											_obj.lblInfo.text = 'Enjoy ' + _obj.paymodeSelected + ' - Guaranteed rate upto '+origSplit[1] + ' '+ _obj.maxExchange + ' per transaction with Remit2India';
											_obj.infoTextView.height = Ti.UI.SIZE;
										}
										
										activityIndicator.showIndicator();
										var xhr1 = require('/utils/XHR');
										xhr1.call({
											url : TiGlobals.appURLTOML,
											get : '',
											post : '{' +
												'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
												'"requestName":"GETEXCHANGERATE",'+
												'"partnerId":"'+TiGlobals.partnerId+'",'+
												'"channelId":"'+TiGlobals.channelId+'",'+
												'"ipAddress":"'+TiGlobals.ipAddress+'",'+
												'"originatingCountry":"'+origSplit[0]+'",'+ 
												'"originatingCurrency":"'+origSplit[1]+'",'+
												'"destinationCountry":"'+destSplit[0]+'",'+
												'"destinationCurrency":"'+destSplit[1]+'",'+
												'"paymodeCode":"'+_obj.paymodeSelected+'",'+
												//'"paymodeCode": "'+_obj.pmc+'",'+
												'"amount":"'+_obj.minExchange+'",'+
												'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
												'}',
											success : xhr1Success,
											error : xhr1Error,
											contentType : 'application/json',
											timeout : TiGlobals.timer
										});
						
										function xhr1Success(evtG1) {
											try{
												require('/utils/Console').info('Result ======== ' + evtG1.result);
												activityIndicator.hideIndicator();
												
												if(evtG1.result.responseFlag === "S")
												{
													_obj.guaranteedExchangeResult = evtG1.result;
													
													_obj.lblExchangeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteedExchangeResult.exchangeRate + ' ' + destSplit[1];
													
												}
												xhr1 = null;
											}catch(e){Ti.API.info(e);}
										}
						
										function xhr1Error(evtG1) {
											try{
												activityIndicator.hideIndicator();
												require('/utils/Network').Network();
												xhr1 = null;

											}catch(e){}
										}
										}catch(e){}		
									}
									xhrExchange = null;
								}catch(e){Ti.API.info(e);}
						}
								function xhrExchangeError(evt) {
									activityIndicator.hideIndicator();
									require('/utils/Network').Network();
									xhrExchange = null;
								}//end for copy
							}
					
							function xhrError(e) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
							
							optionDialogPaymode = null;
						   }
						else{
							var xhrExchange = require('/utils/XHR');
							
							xhrExchange.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"originatingCountryCode":"'+origSplit[0]+'",'+ 
									'"originatingCurrencyCode":"'+origSplit[1]+'",'+
									'"destinationCountryCode":"'+destSplit[0]+'",'+
									'"destinationCurrencyCode":"'+destSplit[1]+'",'+
									'"payModeCode":"'+_obj.paymodeSelected+'",'+
									//'"paymodeCode": "'+_obj.pmc+'",'+
									'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
									'}',
								success : xhrExchangeSuccess,
								error : xhrExchangeError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
							
							function xhrExchangeSuccess(evt) {
								require('/utils/Console').info('Result 1======== ' + JSON.stringify(evt.result));
								activityIndicator.hideIndicator();
								
								var minExchange = [];
								var maxExchange = [];
								
								if(evt.result.responseFlag === "S")
								{
									Ti.API.info("Welcome1"); 
                                        try{
									
			     						Ti.API.info("out of  UAE-----Welcome3");
									for(var s=0; s<evt.result.exchangeRateDtls.length;s++)
			     					{
			     						minExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeFrom));
			     						maxExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeTo));
			     					}
			     					
			     					minExchange.sort(function(a,b){return a-b;});
			     					maxExchange.sort(function(a,b){return b-a;});
			     					
			     					_obj.minExchange = minExchange[0];
			     					_obj.maxExchange = maxExchange[0];
			     					//}
			     					if(origSplit[0] === 'US')
									{
										_obj.lblInfo.text = 'Enjoy ' + _obj.paymodeSelected + ' - Guaranteed rate upto '+origSplit[1] + ' '+ _obj.maxExchange + ' per transaction with Remit2India';
										_obj.infoTextView.height = Ti.UI.SIZE;
									}
									
									activityIndicator.showIndicator();
									var xhr1 = require('/utils/XHR');

									xhr1.call({
										url : TiGlobals.appURLTOML,
										get : '',
										post : '{' +
											'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
											'"requestName":"GETEXCHANGERATE",'+
											'"partnerId":"'+TiGlobals.partnerId+'",'+
											'"channelId":"'+TiGlobals.channelId+'",'+
											'"ipAddress":"'+TiGlobals.ipAddress+'",'+
											'"originatingCountry":"'+origSplit[0]+'",'+ 
											'"originatingCurrency":"'+origSplit[1]+'",'+
											'"destinationCountry":"'+destSplit[0]+'",'+
											'"destinationCurrency":"'+destSplit[1]+'",'+
											'"paymodeCode":"'+_obj.paymodeSelected+'",'+
											//'"paymodeCode": "'+_obj.pmc+'",'+
											'"amount":"'+_obj.minExchange+'",'+
											'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
											'}',
										success : xhr1Success,
										error : xhr1Error,
										contentType : 'application/json',
										timeout : TiGlobals.timer
									});
					
									function xhr1Success(evtG1) {
										try{
											require('/utils/Console').info('Result ======== ' + evtG1.result);

											activityIndicator.hideIndicator();
											
											if(evtG1.result.responseFlag === "S")
											{

												_obj.guaranteedExchangeResult = evtG1.result;
												
												_obj.lblExchangeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteedExchangeResult.exchangeRate + ' ' + destSplit[1];
												
											}
											xhr1 = null;
										}catch(e){Ti.API.info(e);}
									}
					
									function xhr1Error(evtG1) {
										try{
											activityIndicator.hideIndicator();
											require('/utils/Network').Network();
											xhr1 = null;

										}catch(e){}
									}
									}catch(e){}		
								}
								xhrExchange = null;
							}
					
							function xhrExchangeError(evt) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhrExchange = null;
							}//end for copy
							
						}
							}
						
			
						//*************Sanju code ends here
						else if (origSplit[0] !=='UAE' && origSplit[0] !=='FRA' )
						{
						
						//activityIndicator.showIndicator();
						var xhr = require('/utils/XHR');
				
						xhr.call({
							url : TiGlobals.appURLTOML,
							get : '',
							post : '{' +
								'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
								'"requestName":"GETTRANSACTIONPAYMODEINFO",'+
								'"partnerId":"'+TiGlobals.partnerId+'",'+
								'"channelId":"'+TiGlobals.channelId+'",'+
								'"paymodeCode": "'+_obj.paymodeSelected+'",'+
								//'"paymodeCode": "'+_obj.pmc+'",'+
								'"ipAddress":"'+TiGlobals.ipAddress+'"'+
								'}',
							success : xhrSuccess,
							error : xhrError,
							contentType : 'application/json',
							timeout : TiGlobals.timer
						});
				
						function xhrSuccess(e) {
							try{
							_obj.paymodeResult = e.result;
							createUIElements(e.result);
							xhr = null;

							}catch(e){}
							/////////////// Get Exchange Rate Min Max /////////////////
							
							var xhrExchange = require('/utils/XHR');
						    try{
							xhrExchange.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"originatingCountryCode":"'+origSplit[0]+'",'+ 
									'"originatingCurrencyCode":"'+origSplit[1]+'",'+
									'"destinationCountryCode":"'+destSplit[0]+'",'+
									'"destinationCurrencyCode":"'+destSplit[1]+'",'+
									'"payModeCode":"'+_obj.paymodeSelected+'",'+
									//'"paymodeCode": "'+_obj.pmc+'",'+
									'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
									'}',
								success : xhrExchangeSuccess,
								error : xhrExchangeError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
							}catch(e){}
							function xhrExchangeSuccess(evt) {
								require('/utils/Console').info('Result 1======== ' + JSON.stringify(evt.result));
								activityIndicator.hideIndicator();
								
								var minExchange = [];
								var maxExchange = [];
								
								if(evt.result.responseFlag === "S")
								{
									try{
									Ti.API.info("Welcome1"); 
									
			     						Ti.API.info("out of  UAE-----Welcome3");
									for(var s=0; s<evt.result.exchangeRateDtls.length;s++)
			     					{
			     						minExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeFrom));
			     						maxExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeTo));
			     					}
			     					
			     					minExchange.sort(function(a,b){return a-b;});
			     					maxExchange.sort(function(a,b){return b-a;});
			     					
			     					_obj.minExchange = minExchange[0];
			     					_obj.maxExchange = maxExchange[0];
			     					//}
			     					if(origSplit[0] === 'US')
									{
										_obj.lblInfo.text = 'Enjoy ' + _obj.paymodeSelected + ' - Guaranteed rate upto '+origSplit[1] + ' '+ _obj.maxExchange + ' per transaction with Remit2India';
										_obj.infoTextView.height = Ti.UI.SIZE;
									}
									
									activityIndicator.showIndicator();
									var xhr1 = require('/utils/XHR');

									xhr1.call({
										url : TiGlobals.appURLTOML,
										get : '',
										post : '{' +
											'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
											'"requestName":"GETEXCHANGERATE",'+
											'"partnerId":"'+TiGlobals.partnerId+'",'+
											'"channelId":"'+TiGlobals.channelId+'",'+
											'"ipAddress":"'+TiGlobals.ipAddress+'",'+
											'"originatingCountry":"'+origSplit[0]+'",'+ 
											'"originatingCurrency":"'+origSplit[1]+'",'+
											'"destinationCountry":"'+destSplit[0]+'",'+
											'"destinationCurrency":"'+destSplit[1]+'",'+
											'"paymodeCode":"'+_obj.paymodeSelected+'",'+
											//'"paymodeCode": "'+_obj.pmc+'",'+
											'"amount":"'+_obj.minExchange+'",'+
											'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
											'}',
										success : xhr1Success,
										error : xhr1Error,
										contentType : 'application/json',
										timeout : TiGlobals.timer
									});
					
									function xhr1Success(evtG1) {
										try{
											require('/utils/Console').info('Result ======== ' + evtG1.result);
											activityIndicator.hideIndicator();
											
											if(evtG1.result.responseFlag === "S")
											{

												_obj.guaranteedExchangeResult = evtG1.result;
												
												_obj.lblExchangeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteedExchangeResult.exchangeRate + ' ' + destSplit[1];
												
								
											}
											xhr1 = null;
										}catch(e){Ti.API.info(e);}
									}
					
									function xhr1Error(evtG1) {
										try{
											activityIndicator.hideIndicator();
											require('/utils/Network').Network();
											xhr1 = null;

										}catch(e){Ti.API.info(e);}
									}
									}catch(e){}		
								}
								xhrExchange = null;
							}
					
							function xhrExchangeError(evt) {

								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhrExchange = null;
							}
						}
				
						function xhrError(e) {

							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr = null;
						}
						
						optionDialogPaymode = null;
					}
					else
					{
						optionDialogPaymode = null;
						_obj.lblPaymode.text = 'Select Your Payment Mode*';
						_obj.paymentDetailsView.removeAllChildren();
					}
						}catch(e){}//({event ({{
						}
						}catch(e){
							Ti.API.info(e);
						}
				
				}); //click event ends here
			//}
		}
		}catch(e){}
		
		});
		
		//CMN 70-b				
if((_obj.pmc1 === null) || (_obj.pmc1 !== 'WIREFRT') ||(_obj.pmc1 !== 'WIRE') ||(_obj.pmc1 !== 'ACH')){
							_obj.pmc1= 'WIRE';
							 notedispaly(_obj.pmc1);
						}
				   function notedispaly(ev){   //Added for CMN70b
				   	if(origSplit[0]=='US'){
						_obj.transactionDetailsView.add(_obj.lblPt4);
						_obj.transactionDetailsView.add(_obj.lblPt5);

					_obj.state = Ti.App.Properties.getString('state');
						Ti.API.info("STATE:",_obj.state);
						Ti.API.info("STATE1:",_obj.pmc1);
						Ti.API.info("STATE1:",ev);
						
						    if(TiGlobals.usLicsState.indexOf(_obj.state ) != -1 ){ //License
							Ti.API.info("LICENSE--:");
							  _obj.transactionDetailsView.remove(_obj.lblPt4);
						 	  _obj.lblPt5.hide();
		                      _obj.transactionDetailsView.remove(_obj.lblPt5);
		                      
						 	  	if(TiGlobals.paymode == 'WIREFRT' || _obj.pmc1== 'WIRE'){
						 	  	_obj.lblPt4.top = 10;	
						 	    _obj.lblPt4.text = '\u00B7 In case of non-receipt of funds within 2 working days for any reason, MoneyDart may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.';
		                        _obj.transactionDetailsView.add(_obj.lblPt4);
		                      }
			 	  	          if(TiGlobals.paymode == 'ACH'){
						 	  	   _obj.transactionDetailsView.remove(_obj.lblPt4);
						 	  	   _obj.lblPt4.text = '\u00B7 The exchange rate offered for Account transfer is a guaranteed exchange rate if the Guaranteed Rate option is selected. The same shall be displayed in the pre confirmation page.';
								   _obj.lblPt5.text = '\u00B7You can only use your existing verified bank account to transfer funds using Guaranteed Rate option.';
								   _obj.lblPt5.show();
								   _obj.transactionDetailsView.add(_obj.lblPt4);
								   _obj.transactionDetailsView.add(_obj.lblPt5);
								    
								}
						 	 
						 }
						 
						 if(TiGlobals.usLicsState.indexOf(_obj.state ) == -1){  //non-license
						 	Ti.API.info("NONLICENSE--:");
						 	 _obj.transactionDetailsView.remove(_obj.lblPt4);
						 	 _obj.lblPt5.hide();
		                        // _obj.lblPt5.height = 0;
		                     _obj.transactionDetailsView.remove(_obj.lblPt5);
						 	    if(TiGlobals.paymode == 'WIREFRT' || _obj.pmc1== 'WIRE'){
						 	     _obj.lblPt4.top = 10;	
						 	  	 _obj.lblPt4.text = '\u00B7 In case of non-receipt of funds within 2 working days for any reason, TTSPL may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.';
		                        _obj.transactionDetailsView.add(_obj.lblPt4);
			 	  	
						 	  }
						 	  if(TiGlobals.paymode == 'ACH'){
						 	  	  _obj.transactionDetailsView.remove(_obj.lblPt4);
						 	  	  _obj.lblPt4.text = '\u00B7 The exchange rate offered for Account transfer is a guaranteed exchange rate if the Guaranteed Rate option is selected. The same shall be displayed in the pre confirmation page.';
								  _obj.lblPt5.text = '\u00B7 You can only use your existing verified bank account to transfer funds using Guaranteed Rate option.';
								  _obj.lblPt5.show();
								  _obj.transactionDetailsView.add(_obj.lblPt4);
								  _obj.transactionDetailsView.add(_obj.lblPt5);
								    
								}
						 }
					
					  	}
					 }
					 
		_obj.btnSendMoney.addEventListener('click',function(e){
			
			if(TiGlobals.ipAddress == '127.0.0.1'){
				ipCheck();
               } 
	         
			// CMN 55 by pallavi(14 april)
			TiGlobals.usKycDtlFlag = false;
			
			try{
			var conf = [];
			
			// Purpose List
			
			conf['purposeData'] = _obj.purposeOptions.join();
			
			// Define dynamic fields
			
			conf['accountId'] = '';
			conf['bankName'] =  '';
			conf['bankBranch'] = '';
			conf['accountNo'] = '';
			
			if(_obj.lblPaymode.text === 'Select Your Payment Mode*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a paymode code',[L('btn_ok')]).show();  //for cmn-55
				_obj.quickPayView.scrollTo(0,0); 
	    		return;
			}
			else
			{
				conf['paymode'] = _obj.paymodeSelected;
				conf['paymodeshort'] = _obj.pmc;
				//conf[]
			}
			//Sanjivani code 06 oct for UAE
			
			if(_obj.pmc !== 'CCARD')
			{
               try{
				for(var i=0; i<_obj.paymodeResult.paymodeDetails[0].htmlFormFields.length; i++)
				{
					if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementType === 'TextBox')
					{
						var rangeSplit = _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].minMaxLength.split('~');
						
						if(_obj.paymentDetailsView.children[i*2].value === '' && _obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].isMandatoryField === 'Y')
						{
							require('/utils/AlertDialog').showAlert('','Please enter '+_obj.paymentDetailsView.children[i*2].htmlElementLabel,[L('btn_ok')]).show();
				    		_obj.paymentDetailsView.children[i*2].value = '';
				    		_obj.paymentDetailsView.children[i*2].focus();
				    		_obj.quickPayView.scrollTo(0,0);
							return;
						}
						else if(_obj.paymentDetailsView.children[i*2].allowableCharacters !== '')
						{
							if(require('/lib/toml_validations').customValidation(_obj.paymentDetailsView.children[i*2].value,_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel,_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].allowableCharacters) === false)
							{
								_obj.paymentDetailsView.children[i*2].value = '';
					    		_obj.paymentDetailsView.children[i*2].focus();
					    		_obj.quickPayView.scrollTo(0,0);
								return;
							}	
						}
						else if(_obj.paymentDetailsView.children[i*2].value.length < rangeSplit[0] || _obj.paymentDetailsView.children[i*2].value.length > rangeSplit[1])
						//if(_obj.paymentDetailsView.children[i*2].value.length < rangeSplit[0] || _obj.paymentDetailsView.children[i*2].value.length > rangeSplit[1])
						{
							require('/utils/AlertDialog').showAlert('',_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel + ' should be of minimum '+rangeSplit[0]+' and maximum '+rangeSplit[1]+' characters',[L('btn_ok')]).show();
				    		_obj.paymentDetailsView.children[i*2].value = '';
				    		_obj.paymentDetailsView.children[i*2].focus();
				    		_obj.quickPayView.scrollTo(0,0);
							return;
						}
						else
						{
							
						}
						
						if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel === 'Bank Name')
						{
							conf['bankName'] = _obj.paymentDetailsView.children[i*2].value;	
							Ti.API.info("Bank name is:" +conf['bankName'] );
						}
						else if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel === 'Account Number')
						//if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel === 'Account Number')
						{
							conf['accountNo'] = _obj.paymentDetailsView.children[i*2].value;
							Ti.API.info("Bank Account Number is:" +conf['accountNo'] );
						}
						else if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel === 'Bank Branch and City')
						//if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementLabel === 'Bank Branch and City')
						{
							conf['bankBranch'] = _obj.paymentDetailsView.children[i*2].value;	
						}
						else
						{
							
						}
					}
					//else if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementType === 'List')
					if(_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].htmlElementType === 'List')
					{
						if((_obj.paymentDetailsView.children[i*2].text === 'Select ' + _obj.paymentDetailsView.children[i*2].htmlElementLabel || _obj.paymentDetailsView.children[i*2].text === 'Select ' + _obj.paymentDetailsView.children[i*2].htmlElementLabel+'*') && (_obj.paymodeResult.paymodeDetails[0].htmlFormFields[i].isMandatoryField === 'Y'))
						{
							require('/utils/AlertDialog').showAlert('','Please select '+_obj.paymentDetailsView.children[i*2].htmlElementLabel,[L('btn_ok')]).show();
							_obj.quickPayView.scrollTo(0,0);
				    		return;
						}
						else
						{
							/*conf['bankName'] = _obj.paymentDetailsView.children[i*2].value;
							conf['accountNo'] = _obj.paymentDetailsView.children[i*2].value;
							conf['bankBranch'] = _obj.paymentDetailsView.children[i*2].value;*/
							require('/utils/Console').info('_obj.bankDetailsSel = ' + _obj.bankDetailsSel);
							var bankSplit = _obj.bankDetailsSel.split('~');
							conf['bankName'] = bankSplit[0];
								
							Ti.API.info("Bank name 2 is:" +conf['bankName'] );
							conf['accountNo'] = bankSplit[1];
							
							
							Ti.API.info("Bank Account Number 2 is:" +conf['accountNo'] );
							conf['accountId'] = bankSplit[2];
							
						}
					}
					else
					{
						
					}
				}
				}catch(e){}
			}
			//}
			//sanjivani 06 oct code ends here
			// Receiver
			if(_obj.lblReceiver.text === 'Select/Add Receiver*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a receiver',[L('btn_ok')]).show();
				_obj.quickPayView.scrollTo(0,0);
	    		return;		
			}
			else
			{
				var receiverSplit = _obj.receiverSel.split('~');
				Ti.API.info("IN RECEIVERSPLIT***************",receiverSplit);
				conf['receiverNickName'] = receiverSplit[0];
				Ti.App.Properties.setString('receiverNickName',conf['receiverNickName']);
				                         
				conf['receiverParentChildflag'] = receiverSplit[1];
				conf['receiverOwnerId'] = receiverSplit[2];
				conf['receiverLoginid'] = receiverSplit[3];
				
				conf['receiverFName'] = receiverSplit[4];
				Ti.App.Properties.setString('receiverFName',conf['receiverFName']);
				
				conf['receiverLName'] = receiverSplit[5];
				Ti.App.Properties.setString('receiverLName',conf['receiverLName']);
				
				conf['receiverDOB'] = receiverSplit[6];
				Ti.App.Properties.setString('receiverDOB',conf['receiverDOB']);
				
				
				conf['receiverMicr'] =  receiverSplit[7];   //CMN84a - 23Nov17
				Ti.App.Properties.setString('receiverMicr',conf['receiverMicr']);
				
			}
			
			// Purpose
			if(_obj.lblPurpose.text === 'Select Purpose of Remittance*')
			{
				require('/utils/AlertDialog').showAlert('','Please select purpose of remittance',[L('btn_ok')]).show();
				_obj.quickPayView.scrollTo(0,0);
	    		return;		
			}
			else
			{
				conf['purpose'] = _obj.purposeCode;
				 var purposeSplit = _obj.purposeCode.split('~');
                conf['purpose1'] = purposeSplit[1];	
			}
			
			// Amount
			try{
			if(_obj.txtAmount.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please add an amount to be transferred',[L('btn_ok')]).show();
				_obj.txtAmount.value = '';
				_obj.txtAmount.focus();
				_obj.quickPayView.scrollTo(0,60);
	    		return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAmount.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Amount',[L('btn_ok')]).show();
	    		_obj.txtAmount.value = '';
	    		_obj.txtAmount.focus();
	    		_obj.quickPayView.scrollTo(0,60);
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtAmount.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Amount',[L('btn_ok')]).show();
	    		_obj.txtAmount.value = '';
	    		_obj.txtAmount.focus();
	    		_obj.quickPayView.scrollTo(0,60);
				return;
			}
			else if(_obj.txtAmount.value.search("[^0-9]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Amount must be a Numeric Value',[L('btn_ok')]).show();
	    		_obj.txtAmount.value = '';
	    		_obj.txtAmount.focus();
	    		_obj.quickPayView.scrollTo(0,60);
				return;
			}
			else if(parseInt(_obj.txtAmount.value) < parseInt(_obj.minExchange))
			{
				require('/utils/AlertDialog').showAlert('','Please add an amount greater than '+_obj.minExchange,[L('btn_ok')]).show();
	    		_obj.txtAmount.focus();
	    		_obj.quickPayView.scrollTo(0,60);
				return;
			}
			else if(parseInt(_obj.txtAmount.value) > parseInt(_obj.maxExchange))
			{
				require('/utils/AlertDialog').showAlert('','Please add an amount less than '+_obj.maxExchange,[L('btn_ok')]).show();
	    		_obj.txtAmount.focus();
	    		_obj.quickPayView.scrollTo(0,60);
				return;
			}
			else if((_obj.fxfwVoucherSel === 1 && origSplit[0] === 'UK') && (parseInt(_obj.txtAmount.value) < parseInt(_obj.freewayMinAmt)))
			{
				require('/utils/AlertDialog').showAlert('','Freeway program is applicable on transactions above ' + _obj.freewayMinAmt,[L('btn_ok')]).show();
	    		_obj.txtAmount.value = '';
	    		_obj.txtAmount.focus();
	    		_obj.quickPayView.scrollTo(0,60);
				return;
			}
			else
			{  //pallavi
				conf['amount'] = _obj.txtAmount.value;
			    Ti.App.Properties.setString('amount',conf['amount']);
				Ti.API.info("------------<<<",conf['amount']);
				TiGlobals.sendamt = conf['amount'];
				Ti.API.info("------------<<<",TiGlobals.sendamt);
				conf['exchangeRate'] = _obj.lblExchangeRate.text;
			}
			}catch(e){}
			
			// Promo code
			conf['agentSpecialCode'] = _obj.txtPromoCode1.value;
			conf['promoCode'] = _obj.txtPromoCode2.value;
			
			// Voucher Fx/Fw
			if(_obj.fxfwVoucherSel === 1)
			{
				if((_obj.fxfwVoucherSel === 1) && (parseInt(_obj.voucherCount) > parseInt(_obj.voucherMaxQty)))
				{
					require('/utils/AlertDialog').showAlert('','You cannot subscribe to more than ' + _obj.voucherMaxQty + ' vouchers.',[L('btn_ok')]).show();
					return;
				}
				else
				{
					
				}
			}
			
			// MOR
			try{
			if(parseInt(_obj.transactionInfoResult.txnParameter[0].morRefferalAmnt) > 0 && _obj.transactionInfoResult.txnParameter[0].morRefferalAmnt !== '')
			{
				if(parseInt(_obj.txtRedeemAmount.value) > parseInt(_obj.morRefferalAmnt))
				{
					require('/utils/AlertDialog').showAlert('','You cannot redeem to more than Rs. ' + _obj.morRefferalAmnt,[L('btn_ok')]).show();
		    		_obj.txtRedeemAmount.value = '';
		    		_obj.txtRedeemAmount.focus();
		    		return;
				}
				else
				{
					conf['morRefferalAmnt'] = _obj.txtRedeemAmount.value;
				}
			}
			else
			{
				conf['morRefferalAmnt'] = '0';
			}	
			}catch(e){}
			
				
			
			//if(origSplit[0] !== 'US'){
				try{
		    
				
			if(_obj.transactionInfoResult.txnParameter[0].isKYCDetailsPresent === 'No')
			{
				if(_obj.isKYCDetailsPresent === 'No')
				{
					if(origSplit[0] === 'US'){
						require('/js/kyc/us/KYCModal').KYCModal();
						Ti.API.info("US KYC");
					}
					else if(origSplit[0] === 'UK'){
						require('/js/kyc/uk/KYCModal').KYCModal();
					}
					else if(origSplit[0] === 'UAE'){
						require('/js/kyc/uae/KYCModal').KYCModal();
					}
					else if(origSplit[0] === 'AUS'){
						require('/js/kyc/aus/KYCModal').KYCModal();
					}
					else if(origSplit[0] !== 'US'|| origSplit[0] !=='UK' || origSplit[0] !== 'UAE' || origSplit[0] !== 'AUS' ) {
						require('/js/kyc/others/KYCModal').KYCModal();
						Ti.API.info("Other KYC");
					}
					else {
						
					}
					return;
				}
			}
			}catch(e){}
			
			try{
			if(  _obj.transactionInfoResult.txnParameter[0].isFirstTransaction === 'Yes')
			{
				if(_obj.isFirstTransaction === 'Yes')
				{
					require('/js/kyc/KYCCallingModal').KYCCallingModal();
					return;
				}
			}
			
			// if(_obj.transactionInfoResult.txnParameter[0].isCustNationalityPresent === 'No')
			if(TiGlobals.isNationality === 'No')
				{
					var conf = _obj.transactionInfoResult.txnParameter[0].isCustNationalityPresent;
					//setTimeout(function(){
						require('/utils/PageController').pageController('updateCDOB');
					//},300);
					return;
				}
			
			}catch(e){}
			try{
			if(_obj.isOccupationPresent === 'No')
			{			
				setTimeout(function(){
					require('/js/transfer/OccupationModal').OccupationModal();
				},300);
				return;
			}
			}catch(e){}
			
			// Mobile Alerts
			conf['mobileAlerts'] = _obj.mobileAlerts;
			
			// Transaction Insurance
			conf['transactionInsurance'] = _obj.transactionInsurance;
			
			// fxVoucherRedeem
			conf['fxVoucherRedeem'] = _obj.fxVoucherRedeem;
			
			// programType
			conf['programType'] = 'QuickPaySingleTxn';
			
			// Voucher Code
			conf['voucherCode'] = _obj.voucherCode;
			
			// Voucher Quantity
			conf['voucherQty'] = _obj.voucherCount;
			
			conf['txnRefId'] = '';
			
			// FirstTxn
			
			conf['contactDate'] = _obj.contactDate;
			conf['timeFrom'] = _obj.timeFrom;
			conf['timeTo'] = _obj.timeTo;
			conf['timeFrom1'] = _obj.timeFrom1;
			conf['timeTo1'] = _obj.timeTo1;
			
			//added by sanjivani 25-10-16 for paymode label
			conf['paymodeLabel'] = _obj.lblPaymode.text ; //bug.694
			
			//Added by Sanjivani on 07-march-2017 for CMN 41
			conf['fname'] ='';
			conf['lname'] = '';
			
			//IP ISSUE
			TiGlobals.ipAddress=='127.0.0.1';
			
			if(TiGlobals.ipAddress=='127.0.0.1'){
		 	//alert('Dear User, for security reason please login again and book the transaction. Sorry for the inconvenience caused.');
	       
	        if(Ti.App.Properties.getString('loginStatus') !== '0'){
	 Ti.App.Properties.setString('loginStatus','0');
	 Ti.API.info("Session expired in activity tracker");
	 var alertDialog = require('/utils/AlertDialog').showAlert('', 'Dear User, for security reason please login again and book the transaction. Sorry for the inconvenience caused.', [L('btn_ok')]);
     alertDialog.show();
     alertDialog.addEventListener('click', function(e) {
     	try{
			alertDialog.hide();
			//setTimeout(function(){
			    Ti.App.fireEvent('loginData');
			    try{
			    require('/utils/RemoveViews').removeAllScrollableViews(); // Move to dashboard
			    }
			    catch(e){
			    	Ti.API.info("in catch");
			    }
				alertDialog=null;
				 }catch(e){}
			});
       
 }
	        }
			Ti.App.Properties.setString('paymode',conf['paymode']);
			// Call Preconfirmation
			
			require('/utils/Console').info( 
			'\paymode label ='	+ conf['paymodeLabel']+	 //bug.694
			'\paymode = ' + conf['paymode'] + 
			'\naccountId = ' + conf['accountId'] +       //bank account id
			'\nbankName = ' + conf['bankName'] +         //for bank name
			'\nbankBranch = ' + conf['bankBranch'] +     //bankBranch
			'\naccountNo = ' + conf['accountNo'] + 
			'\nreceiverNickName = ' + conf['receiverNickName'] +
			'\nreceiverOwnerId = ' + conf['receiverOwnerId'] +
			'\nreceiverLoginid = ' + conf['receiverLoginid'] +
			'\nreceiverFName = ' + conf['receiverFName'] +
			'\nreceiverLName = ' + conf['receiverLName'] +
			'\nreceiverParentChildflag = ' + conf['receiverParentChildflag'] +
			'\npurpose = ' + conf['purpose'] + 
			//'\namount = ' + conf['amount'] + 
			'\namount = ' + TiGlobals.sendamt +   //pallavi
			'\nexchangeRate = ' + conf['exchangeRate'] +
			'\npromoCode = ' + conf['promoCode'] + 
			'\nagentSpecialCode = ' + conf['agentSpecialCode'] + 
			'\nmorRefferalAmnt = ' + conf['morRefferalAmnt'] + 
			'\nmobileAlerts = ' + conf['mobileAlerts'] + 
			'\ntransactionInsurance = ' + conf['transactionInsurance'] + 
			'\nfxVoucherRedeem = ' + conf['fxVoucherRedeem'] + 
			'\nprogramType = ' + conf['programType'] + 
			'\nvoucherCode = ' + conf['voucherCode'] + 
			'\nvoucherQty = ' + conf['voucherQty'] +
			'\ntxnRefId = ' + conf['txnRefId'] +
			'\ncontactDate = ' + conf['contactDate'] + 
			'\ntimeFrom = ' + conf['timeFrom'] + 
			'\ntimeTo = ' + conf['timeTo'] + 
			'\ntimeFrom1 = ' + conf['timeFrom1'] + 
			'\ntimeTo1 = ' + conf['timeTo1'])+	
			'\TiGlobals.ipAddress=' +TiGlobals.ipAddress;
		/*	'\nrecvBankName =' +conf['recvBankName']+
			'\nrecvBankBranch ='+conf['recvBankBranch']);*/
          	
			require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
		}catch(e){
			//Ti.API.info("exception handled in transfer money and exceptio is-->",+e);
		}
		});
	} // quickPay()
	
	quickPay();
	_obj.quickFlag = 1;
	
	function changekycFlag()
	{
		_obj.isKYCDetailsPresent = 'Yes';
	}
	
	Ti.App.addEventListener('changekycFlag',changekycFlag);
	
	function changefirstTxnFlag(evt)
	{
		var fTxnSplit = evt.data.split('||');
		
		_obj.contactDate = fTxnSplit[0];
		_obj.timeFrom = fTxnSplit[1];
		_obj.timeTo = fTxnSplit[2];
		_obj.timeFrom1 = fTxnSplit[3];
		_obj.timeTo1 = fTxnSplit[4];
		
		_obj.isFirstTransaction = 'No';
	}
	Ti.App.addEventListener('changefirstTxnFlag',changefirstTxnFlag);
	
	function changeOccupationFlag()
	{
		try{
		_obj.isOccupationPresent = 'Yes';
		}catch(e){}
	}
	Ti.App.addEventListener('changeOccupationFlag',changeOccupationFlag);
	
		
	// One click Pay View
	
	_obj.oneClickPayView = Ti.UI.createView(_obj.style.oneClickPayView);
	
	function oneClickPay()
	{
		
		_obj.lblLastTransaction = Ti.UI.createLabel(_obj.style.lblLastTransaction);
		_obj.lblLastTransaction.text = 'This was your last transaction';
		
		_obj.tblOneClick = Ti.UI.createTableView(_obj.style.tableView);
		_obj.tblOneClick.separatorColor = '#FFF';
		
		_obj.oneClickPayView.add(_obj.lblLastTransaction);
		_obj.oneClickPayView.add(_obj.tblOneClick);
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETONECLICKTXNLIST",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				activityIndicator.hideIndicator();
				
				if(e.result.txnListData.length === 0)
				{
					_obj.lblLastTransaction.text = 'No Transactions';
					return;
				}
				
				for(var i=0; i<e.result.txnListData.length;i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 58,
						className : 'ocp',
						recName:e.result.txnListData[i].recvNickName,
						recFName:e.result.txnListData[i].recvFirstName,  //Added on 22 Dec 17 for one click pay defect fix
						recLName:e.result.txnListData[i].recvLastName,   //
						recBankName:e.result.txnListData[i].recvBankName,
						recPurpose:e.result.txnListData[i].fircPurpose,
						recAmount:e.result.txnListData[i].sendOrgAmount,
						origCurrency:e.result.txnListData[i].origCurrency,
						destCurrency:e.result.txnListData[i].destCurrency,
						rtrn:e.result.txnListData[i].rtrn,
						paymodeName:e.result.txnListData[i].payModeName,
						paymode:e.result.txnListData[i].payModeCode,
						paymodeLabel:e.result.txnListData[i].payModeLabel,  //Added on 28th Nov 17 during development of CMN84a
						bankName:e.result.txnListData[i].bankName,
						bankBranch:e.result.txnListData[i].bankBranch,
						accountNo:e.result.txnListData[i].accountNo,
						accountId:e.result.txnListData[i].accountId,
						recOwnerId:e.result.txnListData[i].recvOwnerId,
						recLoginId:e.result.txnListData[i].recvLoginId,
						recParentChildFlag:e.result.txnListData[i].recvParentChildFlag,
						mobileAlerts:'No',
						transactionInsurance:'',
						fxVoucherRedeem:'no',
						specialPromoCode:'',
						morRefferalAmnt:'0',
						programType:'OneClickTxn'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var detailView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#828283',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailView1 = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '55%',
						height : 56,
						backgroundColor : '#6f6f6f'
					});
					
					var detailNameView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblReceiverName = Ti.UI.createLabel({
						text:e.result.txnListData[i].recvNickName, 
						top : 0,
						left : 20,
						right : 20,
						height : Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal16'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var lblPurpose = Ti.UI.createLabel({
						text:e.result.txnListData[i].recvBankName, 
						top : 3,
						left : 20,
						right : 20,
						height : Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var detailView2 = Ti.UI.createView({
						top : 0,
						left : 1,
						width : '45%',
						height : 56,
						backgroundColor : '#6f6f6f'
					});
					
					var detailSendView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblSendTxt = Ti.UI.createLabel({
						text:'Send Amount', 
						top : 0,
						left : 20,
						right : 30,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblAmount = Ti.UI.createLabel({
						text:e.result.txnListData[i].sendOrgAmount +' '+e.result.txnListData[i].origCurrency, 
						top : 2,
						left : 20,
						right : 30,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var separatorView = Ti.UI.createView({
						top : 56,
						left : 0,
						right : 0,
						height : 2,
						backgroundColor : '#fff'
					});
					
					var arrowView = Ti.UI.createView({
						top : 0,
						right : 0,
						width : 30,
						height : 56,
						backgroundColor : '#464646'
					});
					
					var imgArrow = Ti.UI.createImageView({
						image:'/images/arrow_tbl.png',
						width : 10,
						height : 17
					});
					
					detailNameView.add(lblReceiverName);
					detailNameView.add(lblPurpose);
					detailView1.add(detailNameView);
					detailView.add(detailView1);
					detailSendView.add(lblSendTxt);
					detailSendView.add(lblAmount);
					detailView2.add(detailSendView);
					arrowView.add(imgArrow);
					detailView2.add(arrowView);
					detailView.add(detailView2);
					detailView.add(separatorView);
					row.add(detailView);
					_obj.tblOneClick.appendRow(row);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		
		_obj.tblOneClick.addEventListener('click',function(e){
			require('/js/transfer/OneClickPayModal').OneClickPayModal(e.row);
		});
	}
	
	
	// Saved Transaction View
	
	_obj.savedTransactionView = Ti.UI.createView(_obj.style.savedTransactionView);
	
	function savedTxn()
	{
		_obj.lblLastSavedTransaction = Ti.UI.createLabel(_obj.style.lblLastTransaction);
		_obj.lblLastSavedTransaction.text = 'This was your last saved transaction';
		
		_obj.tblSavedTxn = Ti.UI.createTableView(_obj.style.tableView);
		_obj.tblSavedTxn.separatorColor = '#FFF';
		
		_obj.savedTransactionView.add(_obj.lblLastSavedTransaction);
		_obj.savedTransactionView.add(_obj.tblSavedTxn);
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETINCOMPLETEDTRANSACTIONDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"strCountryCode":"'+origSplit[0]+'",'+ 
				'"strCurrencyCode":"'+origSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				activityIndicator.hideIndicator();
				
				if(e.result.transactionData.length === 0)
				{
					_obj.lblLastTransaction.text = 'No Saved Transactions';
					return;
				}
				
				for(var i=0; i<e.result.transactionData.length;i++)
				{
					
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 58,
						className : 'ocp',
						recName:e.result.transactionData[i].recvNickName,
						recFName:e.result.transactionData[i].firstName,
						recLName:e.result.transactionData[i].lastName,
						recBankName:e.result.transactionData[i].recvBankName,
						recPurpose:e.result.transactionData[i].fircPurpose,
						recAmount:e.result.transactionData[i].sendOrgAmount,
						origCurrency:origSplit[1],
						destCurrency:destSplit[1],
						rtrn:e.result.transactionData[i].txnRefId,
						paymodeName:e.result.transactionData[i].payModeName,
						paymodeLabel:e.result.transactionData[i].payModeLabel,  //Added on 28th Nov 17 during development of CMN84a
						paymode:e.result.transactionData[i].payModeCode,
						bankName:e.result.transactionData[i].bankName,
						bankBranch:e.result.transactionData[i].bankBranch,
						accountNo:e.result.transactionData[i].accountNo,
						accountId:e.result.transactionData[i].accountId,
						recOwnerId:e.result.transactionData[i].recvOwnerId,
						recLoginId:e.result.transactionData[i].recvLoginId,
						recParentChildFlag:e.result.transactionData[i].recvParentChildFlag,
						mobileAlerts:'No',
						transactionInsurance:'',
						fxVoucherRedeem:'',
						specialPromoCode:'',
						morRefferalAmnt:'0',
						programType:'PendingTxns'
					});
					
					if(e.result.transactionData[0].hasOwnProperty('accountId')){
						row.accountId = e.result.transactionData[i].accountId;
						Ti.API.info("row.accountId",+row.accountId);
					}
					else{
						Ti.API.info("row.accountId",+row.accountId);
						row.accountId =" ";
					}
					
					//if(e.result.nostroDetails[0].hasOwnProperty('field1')){
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var detailView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#828283',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailView1 = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '55%',
						height : 56,
						backgroundColor : '#6f6f6f'
					});
					
					var detailNameView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					
					var lblReceiverName = Ti.UI.createLabel({
						text:e.result.transactionData[i].recvNickName, 
						top : 0,
						left : 20,
						right : 20,
						height : Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal16'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var lblPurpose = Ti.UI.createLabel({
						text:e.result.transactionData[i].recvBankName, 
						top : 3,
						left : 20,
						right : 20,
						height : Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var detailView2 = Ti.UI.createView({
						top : 0,
						left : 1,
						width : '45%',
						height : 56,
						backgroundColor : '#6f6f6f'
					});
					
					var detailSendView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblSendTxt = Ti.UI.createLabel({
						text:'Send Amount', 
						top : 0,
						left : 20,
						right : 30,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblAmount = Ti.UI.createLabel({
						text:e.result.transactionData[i].sendOrgAmount +' '+origSplit[1], 
						top : 2,
						left : 20,
						right : 30,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var separatorView = Ti.UI.createView({
						top : 56,
						left : 0,
						right : 0,
						height : 2,
						backgroundColor : '#fff'
					});
					
					var arrowView = Ti.UI.createView({
						top : 0,
						right : 0,
						width : 30,
						height : 56,
						backgroundColor : '#464646'
					});
					
					var imgArrow = Ti.UI.createImageView({
						image:'/images/arrow_tbl.png',
						width : 10,
						height : 17
					});
					
					detailNameView.add(lblReceiverName);
					detailNameView.add(lblPurpose);
					detailView1.add(detailNameView);
					detailView.add(detailView1);
					detailSendView.add(lblSendTxt);
					detailSendView.add(lblAmount);
					detailView2.add(detailSendView);
					arrowView.add(imgArrow);
					detailView2.add(arrowView);
					detailView.add(detailView2);
					detailView.add(separatorView);
					row.add(detailView);
					_obj.tblSavedTxn.appendRow(row);
				}
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show(); //done on 22nd DEC for blank popup
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		
		_obj.tblSavedTxn.addEventListener('click',function(e){
			require('/js/transfer/SavedTxnModal').SavedTxnModal(e.row);
		});
	}
	
	_obj.scrollableView.views = [_obj.quickPayView,_obj.oneClickPayView,_obj.savedTransactionView];
	
	function changeTabs(selected)
	{
		if(selected === 'quick')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '30%';
			
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '30%';
			
			_obj.lblQuickPay.color = TiFonts.FontStyle('whiteFont');
			_obj.lblQuickPay.backgroundColor = '#6F6F6F';
			_obj.lblOneClickPay.color = TiFonts.FontStyle('blackFont');
			_obj.lblOneClickPay.backgroundColor = '#E9E9E9';
			_obj.lblSavedTransactions.color = TiFonts.FontStyle('blackFont');
			_obj.lblSavedTransactions.backgroundColor = '#E9E9E9';
		}
		else if(selected === 'one')
		{
			_obj.tabSelView.left = '30%';
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '30%';
			
			_obj.tabSelIconView.left = '30%';
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '30%';
			
			_obj.lblQuickPay.color = TiFonts.FontStyle('blackFont');
			_obj.lblQuickPay.backgroundColor = '#E9E9E9';
			_obj.lblOneClickPay.color = TiFonts.FontStyle('whiteFont');
			_obj.lblOneClickPay.backgroundColor = '#6F6F6F';
			_obj.lblSavedTransactions.color = TiFonts.FontStyle('blackFont');
			_obj.lblSavedTransactions.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '39%';
			
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '40%';
			
			_obj.lblQuickPay.color = TiFonts.FontStyle('blackFont');
			_obj.lblQuickPay.backgroundColor = '#E9E9E9';
			_obj.lblOneClickPay.color = TiFonts.FontStyle('blackFont');
			_obj.lblOneClickPay.backgroundColor = '#E9E9E9';
			_obj.lblSavedTransactions.color = TiFonts.FontStyle('whiteFont');
			_obj.lblSavedTransactions.backgroundColor = '#6F6F6F';
		}
	}
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'quick' && _obj.tab !== 'quick')
		{
			_obj.scrollableView.scrollToView(0);
		}
		
		if(e.source.sel === 'one' && _obj.tab !== 'one')
		{
			_obj.scrollableView.scrollToView(1);
		}
		
		if(e.source.sel === 'saved' && _obj.tab !== 'saved')
		{
			_obj.scrollableView.scrollToView(2);
		}
	});
	
	_obj.scrollableView.addEventListener('scrollend',function(e){
		if(e.currentPage === 0)
		{
			if(_obj.quickFlag === 0)
			{
				quickPay();
				_obj.quickFlag = 1;
			}
		}
		
		if(e.currentPage === 1)
		{
			if(_obj.oneClickFlag === 0)
			{
				oneClickPay();
				_obj.oneClickFlag = 1;
			}
		}
		
		if(e.currentPage === 2)
		{
			if(_obj.savedTxnFlag === 0)
			{
				savedTxn();
				_obj.savedTxnFlag = 1;
			}
		}
	});
	
	_obj.scrollableView.addEventListener('scroll',function(e){
		if(e.currentPage === 0)
		{
			changeTabs('quick');
			_obj.tab = 'quick';
		}
		
		if(e.currentPage === 1)
		{
			changeTabs('one');
			_obj.tab = 'one';
		}
		
		if(e.currentPage === 2)
		{
			changeTabs('saved');
			_obj.tab = 'saved';
		}
	});
	
	function getTransactionInfo()
	{
		// For US check occupation
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETTRANSACTIONINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"payModeCode":"",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			require('/utils/Console').info('Result ======== ' + e.result);
			_obj.transactionInfoResult = e.result;
			
			if(e.result.responseFlag === "S")
			{
				
				if(_obj.transactionInfoResult.txnParameter[0].isKYCDetailsPresent === 'No')
				{
					_obj.isKYCDetailsPresent = 'No';
				}
				else
				{
					_obj.isKYCDetailsPresent = 'Yes';
				}
				
				if(_obj.transactionInfoResult.txnParameter[0].isFirstTransaction === 'Yes')
				{
					_obj.isFirstTransaction = 'Yes';
				}
				else
				{
					_obj.isFirstTransaction = 'No';
				}
				try{
				if(_obj.transactionInfoResult.txnParameter[0].isOccupationPresent === 'Yes')
				{
					_obj.isOccupationPresent = 'Yes';
				}
				else
				{
					_obj.isOccupationPresent = 'No';
					
					setTimeout(function(){
						require('/js/transfer/OccupationModal').OccupationModal();
					},300);
				}
				if(_obj.transactionInfoResult.txnParameter[0].isCustNationalityPresent === 'Yes')
				{
					TiGlobals.isNationality = 'Yes';
				}
				else
				{
					TiGlobals.isNationality = 'No';
					
				}
				}catch(e){}
				
                         _obj.fxVoucherRedeem = _obj.transactionInfoResult.txnParameter[0].fxVoucherRedeem;
				_obj.morRefferalAmnt = _obj.transactionInfoResult.txnParameter[0].morRefferalAmnt;
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			activityIndicator.hideIndicator();
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getTransactionInfo();
	
	function destroy_transfer() {
		try {
			require('/utils/Console').info('############## Remove transfer start ##############');

			require('/utils/RemoveViews').removeViews(_obj.transferView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_transfer', destroy_transfer);
			Ti.App.removeEventListener('changekycFlag',changekycFlag);
			Ti.App.removeEventListener('changefirstTxnFlag',changefirstTxnFlag);
			require('/utils/Console').info('############## Remove transfer end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}

	Ti.App.addEventListener('destroy_transfer', destroy_transfer);

	return _obj.transferView;

};// TransferMoney()

module.exports = TransferMoney;
