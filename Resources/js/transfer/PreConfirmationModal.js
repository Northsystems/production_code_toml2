exports.PreConfirmationModal = function(conf)
{
	var getState = null;
	var getCountry = null;
	require('/lib/analytics').GATrackScreen('Transfer Money Pre-Confirmation');
	
	var _obj = {
		style : require('/styles/transfer/PreConfirmation').PreConfirmation,
		winPreConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblAddressHeader : null,
		lblAddress : null,
		lblDateHeader : null,
		lblDate : null,
		lblLimitHeader : null,
		lblLimit : null,
		paymentView : null,
		tblPaymentDetails : null,
		lblAmountConvertedHeader : null,
		lblAmountConverted : null,
		convertedView : null,
		tblConverted : null,
		lblRecipientAmountHeader : null,
		lblRecipientAmount : null,
		recipientAmountView : null,
		lblSummary : null,
		lblSummaryReceipt : null,
		summaryView : null,
		imgIndicator : null,
		indicatorView : null,
		tblSummary : null,
		imgInfo : null,
		lblInfo : null,
		//added on 30June17 for CMN66
		lblInfo1:null,
		lblInfo3:null,  //For CMN 70(Part 4)on 24th aug
		lblInfo5:null,
		lblInfo2:null,
		lblInfo6 : null, //CMN 76
		infoTextView1:null,
		infoTextView2:null,
		infoTextView3:null,
		imgInfo1:null,
		//
		infoTextView : null,
		imgTerms : null,
		lblTerms : null,
		lblTerms1 : null,
		ukTerms : null,
		termsView : null,

		terms : null,
		isFRT : 'Y',
		
		lblLimit1 : null,
		lblLimit2 : null,
		lblLimit3 : null,
		lblTransDetails : null,
		state:null,
		
		//CMN70-4
		receiveDtlView:null,
        receiveDtlHeader:null,
        tblReceiverDetails:null,
        receiverToRecHeader:null,
        receiverToReceive:null,
        tblToReceive:null,
        
        lblCanAddr:null  //added on 8-Dec-17
		};
		//
		var xmlhttp = Ti.Network.createHTTPClient();
	              xmlhttp.onload = function()
	              {
		           var hostipInfo = JSON.parse(xmlhttp.responseText);
		           TiGlobals.ipAddress = hostipInfo.ip;
                   xmlhttp = null;
	              };
	
	             xmlhttp.onerror = function(e)
	             {
		            // require('/utils/AlertDialog').toast('Error in genrating IP Address.');
	             };
	             xmlhttp.open('GET', 'https://geoip.nekudo.com/api',true);
	             xmlhttp.send();
	    //         
	//For CMN 41
	var purposeShortSplit = conf['purpose'].split('~');
    conf['purposeShort']=purposeShortSplit[1];

	
	// FRT
	var paymodeData = Ti.App.Properties.getString('paymodeData');
	
	for(var f=0; f<paymodeData.length; f++)
	{
		if(paymodeData[f].paymodeCode === conf['paymodeshort'] || paymodeData[f].paymodeCode === conf['paymode']) //added on 7 dec 17
		{
			_obj.isFRT = paymodeData[f].isFRTPaymode;
		}
	}
	
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winPreConfirmation = Ti.UI.createWindow(_obj.style.winPreConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winPreConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Pre-Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblAddressHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
	_obj.lblAddressHeader.text = 'Our Correspondence Address';
	
	 //For CMN70
	_obj.state = Ti.App.Properties.getString('state');
	
	_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
	
        if (origSplit[0] =='US' && TiGlobals.usLicsState.indexOf(_obj.state ) != -1 ){
       	    
       	    // _obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
	     _obj.lblAddress.text = 'Moneydart Global Services Inc DBA Xpress Money \n1000 Woodbridge Center Drive\nWoodbridge, NJ 07095,\nTel: 1-888-736-4886\nwww.remit2india.com';
	    }
       else if (origSplit[0] =='US' && TiGlobals.usLicsState.indexOf(_obj.state ) == -1 ){
             //_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
	     _obj.lblAddress.text = 'TOM TECHNOLOGY SERVICES PRIVATE LTD\nLevel-4, B Wing Reliable Tech Park,\nAiroli, New Mumbai-400 708, India';
             }
             //CMN84
             else if (origSplit[0] == 'CAN'){
             _obj.lblCanAddr = Ti.UI.createLabel(_obj.style.lblAddress);
               if(Ti.Platform.osname === 'Android'){
             		 _obj.lblCanAddr.font = TiFonts.FontStyle('lblNormal14');
             		}else{
             			 _obj.lblCanAddr.font = TiFonts.FontStyle('lblSwissBold12');
             		}
              
               _obj.lblCanAddr.text = 'UAE Exchange Canada Inc | Head Office';
               _obj.lblAddress.text = 'Suite 239,7025 Tomken Road, \nMississauga, ON-L5S 1R6 \nTel: 1-888-736-4886 \nwww.remit2india.com';
	           //_obj.lblAddress.text = 'UAE Exchange Canada Inc \n9288 34 Ave NW, Edmonton, AB T6E 5P2, Canada \nTel: 1-888-736-4886 \nwww.remit2india.com';
               _obj.lblAddress.top = 0;
            
              }
             else if(origSplit[0] != 'UK' ){    //Done for query raised on 25th Sept
            _obj.lblAddress.text = 'TimesofMoney Ltd.\nLevel-4, B Wing Reliable Tech Park,\nAiroli, New Mumbai-400 708, India';
          
             }
        
	 //cmn 9 
	 _obj.lblAddress1 = Ti.UI.createLabel(_obj.style.lblAddress);
     _obj.lblAddress1.text ='TimesofMoney, Inc.\n830, Stewart Dr # 256 Sunnyvale, \nCA 94085';



	_obj.lblDateHeader = Ti.UI.createLabel(_obj.style.lblDateHeader);
	_obj.lblDateHeader.text = 'Today\'s Date & Time';
	//Today's Date & Time
    //14 Nov, 2017 16:50:08 (IST)
	_obj.lblDate = Ti.UI.createLabel(_obj.style.lblDate);
    var dateFormat = Date().split(' ');  //Tue Jul 25 2017 17:01:39 GMT+0530 (IST)-orignal  //
	 //4.16(IST) GMT +0530-requirement for bug:739 on 25th july
	Ti.API.info("TIME IN PRECONF:--",dateFormat[5]); //GMT+0530
	Ti.API.info("TIME IN PRECONF:--",dateFormat[6]); //(IST)
	_obj.lblDate.text = dateFormat[0]+' '+dateFormat[1]+' '+dateFormat[2]+' '+dateFormat[3]+' '+dateFormat[4]+''+dateFormat[6]+' '+dateFormat[5];
	
	if (origSplit[0] == 'CAN'){
             	if(conf['paymode'] == 'WIRE' || conf['paymode'] == 'CIP'){
	_obj.lblDate.text = dateFormat[2]+' '+ dateFormat[1]+', '+dateFormat[3]+' '+dateFormat[4]+' '+dateFormat[6];
	}
	}
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	//_obj.globalView.add(_obj.headerView);

	_obj.globalView.add(_obj.mainView);
	_obj.winPreConfirmation.add(_obj.globalView);
	_obj.winPreConfirmation.open();
	if(TiGlobals.usKycDtlFlag == false){
		
	try{
	//Added by sanjivani on 28-04-17 for CMN 55
	 activityIndicator.showIndicator();
		   var xhr1 = require('/utils/XHR');
	
			xhr1.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"requestName":"DISPLAYPHOTOIDFORMCHECK",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				    '"originatingCountry":"'+origSplit[0]+'",'+ 
				    '"originatingCurrency":"'+origSplit[1]+'",'+
				    '"destinationCountry":"'+destSplit[0]+'",'+
				    '"destinationCurrency":"'+destSplit[1]+'",'+
				    '"paymodeCode":"'+conf['paymode']+'",'+
					'"amount":"'+conf['amount']+'"'+
					'}',
				success : xhrSuccess11,
				error : xhrError11,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess11(e1) {
				
				require('/utils/Console').info('Result ======== ' + JSON.stringify(e1.result));
				activityIndicator.hideIndicator();
				 Ti.App.Properties.setString('responseCode ',e1.result.responseArray[0].responseCode );
				 TiGlobals.responseCode = e1.result.responseArray[0].responseCode;
			   // alert(TiGlobals.responseCode);
				Ti.API.info("**---------------------IN PASSPORTDRIVING",JSON.stringify(e1.result.responseArray[0].responseCode));

				 if(origSplit[0] == 'US'){
				 	
 	            if(TiGlobals.responseCode === '01' ||TiGlobals.responseCode === '02' ){
				 TiGlobals.usKycDtlFlag = true;
				 require('/js/kyc/KYCModal').KYCModal(conf);
				 destroy_preconfirmation();
			   
				}else if(TiGlobals.responseCode === '00'){   //invalid session
					
					//require('/utils/AlertDialog').showAlert('Invalid Session',e1.result.Message,[L('btn_ok')]).show();
					         if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast("Invalid Session");
									}
									else
									{
										require('/utils/AlertDialog').iOSToast("Invalid Session");
									}
					destroy_preconfirmation();
			   
			   }else if(TiGlobals.responseCode === '03'){  //not applicable
			   	
			   	//require('/utils/AlertDialog').showAlert('Not Applicable',e1.result.Message,[L('btn_ok')]).show();
			   	/* if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast("Not Applicable");
									}
									else
									{
										require('/utils/AlertDialog').iOSToast("Not Applicable");
									}*/
			    preconf();
				}
				else{
					
				}		
                }
             else{
             	preconf();
         }
				
				xhr1 = null;
			}
	
			function xhrError11(e1) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr1 = null;
			}
			}catch(e){
            Ti.API.info(e);
                 }
			
     
  }
  else{
  	preconf();
  }
	// Check pre-confirmation
	
	function preconf()
	{
		try{
		_obj.globalView.add(_obj.headerView);
		}catch(e){}
		
		activityIndicator.showIndicator();
	
		
		var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"PRECONFTXN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"originatingCurrency":"'+origSplit[1]+'",'+
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'",'+
					'"paymodeCode":"'+conf['paymode']+'",'+
				    '"receiverNickName":"'+conf['receiverNickName']+'",'+
				    '"receiverOwnerId":"'+conf['receiverOwnerId']+'",'+
				    '"receiverLoginid":"'+conf['receiverLoginid']+'",'+
				    '"receiverParentChildflag":"'+conf['receiverParentChildflag']+'",'+
				    '"amount":"'+conf['amount']+'",'+
				    '"accountId":"'+conf['accountId']+'",'+
				    '"bankName":"'+conf['bankName']+'",'+
				    '"bankBranch":"'+conf['bankBranch']+'",'+
				    '"accountNo":"'+conf['accountNo']+'",'+
				    '"purpose":"'+conf['purpose']+'",'+
				    '"personalMessage":"",'+
				    '"promoCode":"'+conf['promoCode']+'",'+
				    '"mobileAlerts":"'+conf['mobileAlerts']+'",'+
				    '"transactionInsurance":"'+conf['transactionInsurance']+'",'+
				    '"fxVoucherRedeem":"'+conf['fxVoucherRedeem']+'",'+
				    '"agentSpecialCode":"'+conf['agentSpecialCode']+'",'+
				    '"morRefferalAmnt":"'+conf['morRefferalAmnt']+'",'+
				    '"programType":"'+conf['programType']+'",'+ 
				    '"txnRefId":"'+conf['txnRefId']+'",'+
				    '"contactDate":"'+conf['contactDate']+'",'+
				    '"timeFrom":"'+conf['timeFrom']+'",'+
				    '"timeTo":"'+conf['timeTo']+'",'+
				    '"timeFrom1":"'+conf['timeFrom1']+'",'+
				    '"timeTo1":"'+conf['timeTo1']+'",'+
				    '"voucherCode":"'+conf['voucherCode']+'",'+
				    '"voucherQty":"'+conf['voucherQty']+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				
				try{
				require('/utils/Console').info('Result ======== ' + e.result);
				
				activityIndicator.hideIndicator();
				
				if(e.result.message === "Please select KYC Contact Details")
				{
					
					require('/js/kyc/KYCCallingModal').KYCCallingModal();
					
					setTimeout(function(){
						destroy_preconfirmation();
					},500);
					return;
				}
			
				
				if(e.result.message === 'You have exceeded the compliance limit')
				{
					_obj.lblLimitHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
					_obj.lblLimitHeader.text = e.result.message;
					
					_obj.lblLimit1 = Ti.UI.createLabel(_obj.style.lblAddress);
					_obj.lblLimit1.text = 'You have reached your prescribed transaction limit. In order to send higher amounts, please fill in our KYC Form.';
					
					_obj.lblLimit2 = Ti.UI.createLabel(_obj.style.lblAddress);
					_obj.lblLimit2.text = "Remit2India allows you to send a minimum of AUD50 per transaction and a maximum of AUD2000 per transaction. In case you want to send amounts more than the specified limits. Kindly transfer funds through other paymodes.";
					
					_obj.lblLimit3 = Ti.UI.createLabel(_obj.style.lblAddress);
					_obj.lblLimit3.text ="Remit2India allows you to send a minimum of AUD50 per transaction and a maximum of AUD2000 per transaction. Maximum of AUD4000 can be sent in a day. In case you want to send amounts more than the specified limits. Kindly transfer funds through other pay modes.";
						
					_obj.lblTransDetails = Ti.UI.createLabel(_obj.style.lblAddressHeader);
					_obj.lblTransDetails.text = 'Transaction Details';
					
					_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnSubmit.title = 'KYC Form';
	
					
					//_obj.mainView.add(_obj.lblLimit);
					try{
					if((conf['paymode'] === "POLIFRT") || (conf['paymode'] === 'ACHGFXI')){
						if(conf['amount'] > 2000){
						
							_obj.mainView.add(_obj.lblTransDetails);
							_obj.mainView.add(_obj.lblLimit2);
							
						}
						else{
							_obj.mainView.add(_obj.lblTransDetails);
							_obj.mainView.add(_obj.lblLimit3);
							
						}
					}
					
					if((conf['paymode'] !== "POLIFRT") && (conf['paymode'] !== 'ACHGFXI'))
					{
						
						if(origSplit[0] != 'UK'){//  && origSplit[0] != 'CAN'){ //Done for CMN 84 ON 14th nov   //Done for query raised on 25th Sept
					_obj.mainView.add(_obj.lblAddressHeader); //1
						if(origSplit[0] == 'CAN'){     //added on 8Dec-17 for CMN84a
						 _obj.mainView.add(_obj.lblCanAddr);
						}
						_obj.mainView.add(_obj.lblAddress);//2
						_obj.mainView.add(_obj.lblDateHeader);//3
						_obj.mainView.add(_obj.lblDate);//4
						}
						
						_obj.mainView.add(_obj.lblLimitHeader);
						_obj.mainView.add(_obj.lblLimit1);
						_obj.mainView.add(_obj.btnSubmit);	
					    
						_obj.btnSubmit.addEventListener('click',function(e){
							
							Ti.API.info("-----CONF AMOUNT1----",TiGlobals.sendamt);
							
							require('/js/transfer/LimitEnhancementModal').LimitEnhancementModal(conf);
							
							setTimeout(function(){
								destroy_preconfirmation();
							},500);
						});
					}}catch(e){
						//Ti.API.info(e);
					}
					
					return;
				}
				
				//try{
				if(e.result.responseFlag === 'S')
				{
					if(origSplit[0] != 'UK'){    //Done for query raised on 25th Sept
					_obj.mainView.add(_obj.lblAddressHeader); //1
					if(origSplit[0] == 'CAN'){     //added on 8Dec-17 for CMN84a
						 _obj.mainView.add(_obj.lblCanAddr);
						}
					_obj.mainView.add(_obj.lblAddress);//2
					_obj.mainView.add(_obj.lblDateHeader);//3
					_obj.mainView.add(_obj.lblDate);//4
					}
					_obj.paymentView = Ti.UI.createView(_obj.style.paymentView);
					_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
					_obj.lblPaymentHeader.text = 'Payment Details';
					
					  if(origSplit[0] == 'CAN'){ //CMN84a
				       if(conf['paymode'] == 'WIRE' || conf['paymode'] == 'CIP'){
							_obj.paymentView.top =  '10';
							}
						}
					//Added by Sanjivani on 23Aug17 for CMN70-4
				    _obj.receiveDtlView = Ti.UI.createView(_obj.style.convertedView);
					_obj.receiveDtlHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);//grey tab
					_obj.receiveDtlHeader.text = 'Receiver Details';
					_obj.receiveDtlView.add(_obj.receiveDtlHeader);
					
					_obj.receiverToReceive = Ti.UI.createView(_obj.style.paymentView);
					_obj.receiverToRecHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
					_obj.receiverToRecHeader.text = 'Receiver to Receive';
					_obj.receiverToReceive.add(_obj.receiverToRecHeader);
					_obj.receiverToReceive.top = 0;
					
					_obj.actionView = Ti.UI.createView(_obj.style.actionView);
					_obj.lblEdit = Ti.UI.createLabel(_obj.style.lblAction);
					_obj.lblEdit.text = 'EDIT';
					_obj.lblEdit.sel = 'edit';
					_obj.lblSeparator1 = Ti.UI.createLabel(_obj.style.lblAction);
					_obj.lblSeparator1.text = '-';
					Ti.API.info('Paymode code for bug.695 is:'+conf['paymode']);  //by Sanjivani on 26 Oct 16
					Ti.API.info('Paymode code for bug.695 is:'+conf['paymodeCode']);
					//if(conf['paymodeCode'] !== 'CCARD')//By BCM
					if(conf['paymode'] !== 'CCARD')// by Sanjivani on 26 Oct 16
					{
						_obj.lblSave = Ti.UI.createLabel(_obj.style.lblAction);
						_obj.lblSave.text = 'SAVE';
						_obj.lblSave.sel = 'save';
						_obj.lblSave.color = '#FFF';
						_obj.lblSeparator2 = Ti.UI.createLabel(_obj.style.lblAction);
						_obj.lblSeparator2.text = '-';
					}
					_obj.lblCancel = Ti.UI.createLabel(_obj.style.lblAction);
					_obj.lblCancel.text = 'CANCEL';	
					_obj.lblCancel.sel = 'cancel';
					
					_obj.tblPaymentDetails = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblPaymentDetails.height = 0;
					
					_obj.tblReceiverDetails = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblReceiverDetails.height = 0;
					
					_obj.tblToReceive = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblToReceive.height = 0;
					if(origSplit[0] == 'UK' || (origSplit[0] == 'CAN' && (conf['paymode'] === 'CIP' || conf['paymode'] === 'WIRE'))){
					for(var i=0; i<=9; i++)
					{
						var rowPD = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : 'transparent',
							className : 'bank_account_details'
						});
						
						if(TiGlobals.osname !== 'android')
						{
							rowPD.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeyPD = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValuePD = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblKeyToReceive = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblKeyToReceive1 = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font:TiFonts.FontStyle('lblSwissBold14'),
		                    color:TiFonts.FontStyle('blackFont')
						});
						//_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
						var lblValueToReceive = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						
						
						 switch(i)
						{
					   case 0:
								if(e.result.hasOwnProperty('paymodeData'))
								{
									lblKeyPD.text = 'Payment Mode:';
									/*if(e.result.paymodeData[0].paymodeDesc === 'Wire FRT'){
										e.result.paymodeData[0].paymodeDesc = 'Wire Transfer';
										//Ti.API.info("PAYMODE:---",e.result.paymodeData[0].paymodeDesc);
									lblValuePD.text = e.result.paymodeData[0].paymodeDesc;
									}
									else{
										lblValuePD.text = e.result.paymodeData[0].paymodeDesc;
									}*/
									lblValuePD.text = conf['paymodeLabel']; //CMN84a _ added after confirmation from Kanchan on Call
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
								}
					
							break;
							
							case 1:
								if(e.result.hasOwnProperty('purpose'))
								{
									var purposeSplit = e.result.purpose.split('~');
									lblKeyPD.text = 'Purpose of Remittance:';
								    lblValuePD.text = conf['purpose1'];
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
								}//
								
							break;
							
							case 2:
							        try{
									lblKeyPD.text = 'Bank Account Details:';//
									//lblValuePD.text = 'Clarification pending';
									lblValuePD.height = Ti.UI.SIZE;
									rowPD.height = 70;
									if(conf['paymode'] === 'DEBFRT' ||conf['paymode'] === 'DEB'){
									lblValuePD.text = conf['bankName'];
									//rowPD.height = 60; 	
									}
									else if(conf['paymode'] === 'WIRE' || conf['paymode'] === 'WIREFRT' ){
									lblValuePD.text = conf['bankName'] + '\n'+ conf['bankBranch'];
									lblValuePD.bottom = 5;
									}
									else if(conf['paymode'] === 'CIP'){
									lblValuePD.text = conf['bankName'] + '\n'+ conf['accountNo'];	
									}
									else{}
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 70);
								}catch(e){}
						
								
							break;
							
							case 3:
									lblKeyPD.text = 'Name:';
									lblValuePD.text = conf['receiverFName']+' '+conf['receiverLName'];
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblReceiverDetails.appendRow(rowPD);
									_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 60);
								
							break;
							
							case 4:
							//Bank Account Details:
								if(e.result.hasOwnProperty('receiverData'))
							     {
									lblKeyPD.text = 'Bank Account Details:';
								   // lblValuePD.text = 'Clarification pending';
									
									lblValuePD.height = Ti.UI.SIZE;
									lblValuePD.bottom = 15;
									//rowPD.height = Ti.UI.SIZE;
									//rowPD.height = 100;
									var bankBranch = e.result.receiverData[0].recvBankBranch;

									//var bankBranch = "e.result.recvBankBranch , receiverData[0], ssfs " ;
									var n = bankBranch.indexOf(",");
									if(n == -1){
									rowPD.height =100;	
									lblValuePD.text = e.result.receiverData[0].recvBankName + '\n'+bankBranch + '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 100);
									}
									else{
									
									var bankBranchSplit = bankBranch.split(',');
									if(bankBranchSplit.length == 2){
									rowPD.height = 120;		
									lblValuePD.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 120);
									}
	                                else if(bankBranchSplit.length == 3){
	                                rowPD.height = 140;
	                               	lblValuePD.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+bankBranchSplit[2]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 140);//120
									}
									else if(bankBranchSplit.length == 4){
									rowPD.height = 160;	
	                               	lblValuePD.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+bankBranchSplit[2] + '\n'+bankBranchSplit[3]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 160);
									}
									else if(bankBranchSplit.length == 5){
									rowPD.height = 180;	
	                               	lblValuePD.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+bankBranchSplit[2] + '\n'+bankBranchSplit[3] + '\n'+bankBranchSplit[4]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 180);
									}  
									else{}   
        
									}
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblReceiverDetails.appendRow(rowPD);
									//_obj.tblReceiverDetails.height = parseInt(_obj.tblReceiverDetails.height + 120);//120
								}
						break;
							
							
							case 5:
									lblKeyToReceive.text = 'Indicative Exchange Rate:';
									//lblValueToReceive.text ='Clarification pending';
									lblValueToReceive.text = '1 ' + origSplit[1] + ' = ' + e.result.exchangeRate + ' ' +destSplit[1];
									rowPD.add(lblKeyToReceive);
									rowPD.add(lblValueToReceive);
									_obj.tblToReceive.appendRow(rowPD);
									_obj.tblToReceive.height = parseInt(_obj.tblToReceive.height + 60);
					   			
							break;
							
							case 6:
							        lblKeyToReceive.text = 'Sending Amount:';
									lblValueToReceive.text = origSplit[1] +' '+conf['amount'];
									rowPD.add(lblKeyToReceive);
									rowPD.add(lblValueToReceive);
									_obj.tblToReceive.appendRow(rowPD);
									_obj.tblToReceive.height = parseInt(_obj.tblToReceive.height + 60);
							break;
							
							case 7:
							        lblKeyToReceive.text = 'Net Applicable Fee:';
									lblValueToReceive.text = origSplit[1] + ' ' +e.result.netApplicableFee ;
									
									rowPD.add(lblKeyToReceive);
									rowPD.add(lblValueToReceive);
									_obj.tblToReceive.appendRow(rowPD);
									_obj.tblToReceive.height = parseInt(_obj.tblToReceive.height + 60);
								
							break;
							
							case 8:
							        lblKeyToReceive1.text = 'Net Transfer Amount:';
									lblValueToReceive.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
									rowPD.add(lblKeyToReceive1);
									rowPD.add(lblValueToReceive);
									_obj.tblToReceive.appendRow(rowPD);
									_obj.tblToReceive.height = parseInt(_obj.tblToReceive.height + 60);
								
							break;
							
							case 9:
							        lblKeyToReceive.text = 'Indicative Destination Amount';
									lblValueToReceive.text = destSplit[1] + ' ' + e.result.convertedAmount;
									
									rowPD.add(lblKeyToReceive);
									rowPD.add(lblValueToReceive);
									_obj.tblToReceive.appendRow(rowPD);
									_obj.tblToReceive.height = parseInt(_obj.tblToReceive.height + 60);
								
							break;
							
							
					
						}	//end of switch
						}//end of for 
						}//uk end of IF
						//else{
					if(origSplit[0] !== 'UK' ){  //CMN84a
					  if( origSplit[0] == 'CAN' && (conf['paymode'] == 'CIP' || conf['paymode'] == 'WIRE' )) {
						}else{
					for(var i=0; i<=2; i++)
					{
						var rowPD = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : 'transparent',
							className : 'bank_account_details'
						});
						
						if(TiGlobals.osname !== 'android')
						{
							rowPD.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeyPD = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValuePD = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						switch(i)
						{
							case 0:
								if(e.result.hasOwnProperty('transferAmount'))
								{
									//Done for CMN 70(part 3)on 23rd aug
									if(origSplit[0] === 'US'){
									lblKeyPD.text = 'Total Amount';
									}else{
										lblKeyPD.text = 'Transfer Amount';
									}
									
									lblValuePD.text = origSplit[1] + ' ' + e.result.transferAmount;
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
								}
							break;
							
							case 1:
								if(e.result.hasOwnProperty('programFee') && e.result.programFee !== '')
								{
									var voucher = '';
									
									if(e.result.hasOwnProperty('benefitsData') && e.result.benefitsData !== '')
									{ 
										if(e.result.benefitsData[0].hasOwnProperty('ep'))
										{
											voucher = '(FXVoucher)';
										}
										else if(e.result.benefitsData[0].hasOwnProperty('fw'))
										{
											voucher = '(Freeway Voucher)';
										}  
									}
									
									lblKeyPD.text = 'Program Fee ' + voucher;
									lblValuePD.text = e.result.programFee;
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
								}
							break;
							
							case 2:
							    if(e.result.hasOwnProperty('netApplicableFee'))
								{
									if(e.result.netApplicableFee >= 0.0)
									{
										//Done for CMN 70(part 3)on 23rd aug
									 if(origSplit[0] === 'US'){
									lblKeyPD.text = 'Transfer Fee';
									}else{
										lblKeyPD.text = 'Net Applicable Fee';
									}
									lblValuePD.text = e.result.netApplicableFee;
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
									}
								}
							break;
						}
					}//end of else
					
					} //Canada if check
					}
	             // if((origSplit[0] !== 'UK') && (origSplit[0] !== 'CAN' && conf['paymode'] !== 'CIP') && ( origSplit[0] !== 'CAN' && conf['paymode'] !== 'WIRE')) {  //CMN84a
			      if((origSplit[0] !== 'UK') || (origSplit[0] !== 'CAN' && conf['paymode'] !== 'CIP') && ( origSplit[0] !== 'CAN' && conf['paymode'] !== 'WIRE')) {  //CMN84a
							
				  //  if( origSplit[0] !== 'UK' || origSplit[0] == 'CAN' && (conf['paymode'] != 'CIP' && conf['paymode'] != 'WIRE')){  //CMN84a  -21Nov17 
					//if( origSplit[0] !== 'UK'){	//CMN70-4
					if(origSplit[0] === 'US'){   //// added by pallavi for CMN70
					_obj.convertedView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblAmountConvertedHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblAmountConvertedHeader.text = 'Transfer Amount';
					_obj.lblAmountConverted = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblAmountConverted.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
					}else{
					_obj.convertedView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblAmountConvertedHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);//grey tab
					_obj.lblAmountConvertedHeader.text = 'Amount to be converted';
					_obj.lblAmountConverted = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblAmountConverted.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
					}
					_obj.tblConverted = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblConverted.height = 0;
					//if( origSplit[0] !== 'UK'){
					for(var i=0; i<=3; i++)
					{
						var rowCon = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : 'transparent',
							className : 'bank_account_details'
						});
						if(TiGlobals.osname !== 'android')
						{
							rowCon.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeyCon = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValueCon = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						switch(i)
						{
							case 0:
								if(e.result.hasOwnProperty('exchangeRate'))
								{
									lblKeyCon.text = 'Exchange Rate';
									lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.exchangeRate;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}
								
							break;
							
							case 1:
								if(e.result.hasOwnProperty('netExchangeRate'))
								{
									lblKeyCon.text = 'Exchange Rate with Benefit';
									lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.netExchangeRate;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}
							break;
							
							case 2:
								if(e.result.hasOwnProperty('convertedAmount'))
								{
									lblKeyCon.text = 'Converted Amount';
									lblValueCon.text = destSplit[1] + ' ' + e.result.convertedAmount;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}	
							break;
							
							case 3:
								if(e.result.hasOwnProperty('serviceTax'))
								{
									if((origSplit[0] === 'US' && ((conf['paymode'] === 'ACHGFXFRT') || (conf['paymode'] === 'WIREFRT'))) || (origSplit[0] === 'UK' && (conf['paymode'] === 'DEBFRT')))
									{
										if(_obj.isFRT === 'Y')
										{
											lblKeyCon.text = 'Service Tax';
											lblValueCon.text = '(-) ' + destSplit[1] + ' ' + e.result.serviceTax;
											
											rowCon.add(lblKeyCon);
											rowCon.add(lblValueCon);
											_obj.tblConverted.appendRow(rowCon);
											_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
										}
									}
									
									//Commented for CMN 35
									/*if(origSplit[0] === 'AUS' && ((conf['paymode'] === 'POLIFRT') || (conf['paymode'] === 'POLIFR')))
									{
										if(_obj.isFRT === 'Y')
										{
											lblKeyCon.text = 'Service Tax';
											lblValueCon.text = '(-) ' + destSplit[1] + ' ' + e.result.serviceTax;
											
											rowCon.add(lblKeyCon);
											rowCon.add(lblValueCon);
											_obj.tblConverted.appendRow(rowCon);
											_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
										}
									}*/
									
									
								}
							break;
						}
						
					}
				
					_obj.SwachhBharatView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblSwachhBharatHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblSwachhBharatHeader.text = 'Swachh Bharat Cess';
					_obj.lblSwachhBharatAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblSwachhBharatAmount.text = destSplit[1] + ' ' + e.result.sbcTax;
					
					//if(origSplit[0] === 'US'&& origSplit[0] !== 'UK'){ //CMN70-4
					if(origSplit[0] === 'US'){
					_obj.recipientAmountView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblRecipientAmountHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblRecipientAmountHeader.text = 'Total Amount to Receiver';
					_obj.lblRecipientAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblRecipientAmount.text = destSplit[1] + ' ' + e.result.indicativeAmountRecipient;
						
					}else{
					_obj.recipientAmountView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblRecipientAmountHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblRecipientAmountHeader.text = 'Amount to Recipient';
					_obj.lblRecipientAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblRecipientAmount.text = destSplit[1] + ' ' + e.result.indicativeAmountRecipient;
					}
					_obj.recipientAmountToView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblRecipientAmountToHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblRecipientAmountToHeader.text = 'Indicative Destination Amount';
					_obj.lblRecipientAmountTo = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblRecipientAmountTo.text = destSplit[1] + ' ' + e.result.convertedAmount;
					
					/*_obj.summaryView = Ti.UI.createView(_obj.style.summaryView);
					_obj.lblSummary = Ti.UI.createLabel(_obj.style.lblSummary);
					_obj.lblSummary.text = 'Summary of pre-confirmation';
					_obj.lblSummaryReceipt = Ti.UI.createLabel(_obj.style.lblSummaryReceipt);
					_obj.lblSummaryReceipt.text = 'Not a Receipt';*/
					
					//_obj.indicatorView = Ti.UI.createView(_obj.style.indicatorView);
					//_obj.imgIndicator = Ti.UI.createImageView(_obj.style.imgIndicator);
					
					_obj.tblSummary = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblSummary.height = 0;
					_obj.tblSummary.separatorColor = '#595959';
					_obj.tblSummary.backgroundColor = '#303030';
					
					for(var i=0; i<=4; i++)
					{
						var rowSummary = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : '#303030',
							className : 'bank_account_details'
						});
						
						if(TiGlobals.osname !== 'android')
						{
							rowSummary.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeySummary = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValueSummary = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						switch(i)
						{
							case 0:
								if(e.result.hasOwnProperty('receiverData'))
								{
									lblKeySummary.text = 'Remittance transfer request received for';
									lblValueSummary.text = e.result.receiverData[0].firstName +' '+ e.result.receiverData[0].lastname +' '+ e.result.receiverData[0].micr;
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
							
							case 1:
								if(e.result.hasOwnProperty('receiverData'))
								{
									lblKeySummary.text = 'Delivery Mode';
									lblValueSummary.text = e.result.receiverData[0].deliveryModeCodeDesc;
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
							
							case 2:
								if(e.result.hasOwnProperty('purpose'))

								{
								var purposeSplit = e.result.purpose.split('~');

                                                                //Ti.API.info("Response is:------>",purposeSplit[1]);
								lblKeySummary.text = 'Purpose of Remittance';

								//lblValueSummary.text = purposeSplit[1];
                               // conf['purpose1'] = lblValueSummary.text;
                                lblValueSummary.text = conf['purpose1'];
								rowSummary.add(lblKeySummary);

								rowSummary.add(lblValueSummary);

								_obj.tblSummary.appendRow(rowSummary);

								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);

								}
							break;
							
							
							case 3:
								if(e.result.hasOwnProperty('paymodeData'))
								{
									lblKeySummary.text = 'Sending option selected';
									/*if(e.result.paymodeData[0].paymodeDesc === 'Wire FRT'){
										e.result.paymodeData[0].paymodeDesc = 'Wire Transfer';
										//Ti.API.info("PAYMODE:---",e.result.paymodeData[0].paymodeDesc);
									lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
									}
									else{
										lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
									}*/
									lblValueSummary.text = conf['paymodeLabel'];
									
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
							
							case 4:
								if(e.result.hasOwnProperty('deliveryDate') && (origSplit[0] === 'US'))
								{
									lblKeySummary.text = 'Date Available: ' + e.result.deliveryDate;
									lblValueSummary.text = '(The money may be available sooner to the receiver)';
									lblValueSummary.font = TiFonts.FontStyle('lblNormal12');
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
						}
						
					}
					}//end of if
					_obj.infoTextView1 = Ti.UI.createView(_obj.style.infoTextView);
					_obj.infoTextView2 = Ti.UI.createView(_obj.style.infoTextView);
					_obj.infoTextView3 = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo1 = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo1 = Ti.UI.createLabel(_obj.style.lblInfo1);
                                                _obj.lblInfo6 = Ti.UI.createLabel(_obj.style.lblInfo1); //CMN 76
						
						_obj.lblInfo2 = Ti.UI.createLabel(_obj.style.lblInfo2);
						if(TiGlobals.osname === 'android')
								{
									_obj.lblInfo2.font = TiFonts.FontStyle('lblNormal14');
								}//for CMN 76
			     _obj.lblInfo6.text = 'Receiver may receive less due to fees charged by receiver'+'s bank and foreign taxes. The Service Charge has been computed and deducted from the converted amount based on the final exchange rate as indicated above. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.';
				 _obj.lblInfo1.text = 'Receiver may receive less due to fees charged by receiver'+'s bank and foreign taxes. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.';
					
					 var text =  'Remit2India services in the UK is offered by UAE Exchange UK Limited, a company authorised by the Financial Conduct Authority, and having its registered office at 14-15 Carlisle Street, London W1D 3BS. This transaction is subject to the Terms & Conditions. For questions or complaints write to us at the aforesaid address, or contact us at: 08000163404.  Email : info@remit2india.com';
					var attr = Ti.UI.createAttributedString({
						    text: text,
						    attributes: [
						        {
						            type: Ti.UI.ATTRIBUTE_FOREGROUND_COLOR,
						            value: 'red',
						            range: [text.indexOf('Terms & Conditions'), ('Terms & Conditions').length]
						        }
						    ]
						});
					 var ukTerms = Ti.UI.createLabel({
											 	       top:0,
                                                       left:20,
                                                       right:20,
                                                       textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
											    	   font:TiFonts.FontStyle('lblNormal12'),
											    	   color:TiFonts.FontStyle('blackFont'),
												       attributedString: attr
											         });
					 
					/* if((Ti.Platform.osname === 'iphone')||(Ti.Platform.osname === 'ipad')){
					  	ukTerms.top=68;
					  }*/
					  
					  ukTerms.addEventListener('click',function(e){
						require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','txn','');
					});
					
					// _obj.lblInfo2.top=20;
					 _obj.lblInfo2.text= '**the credit date may get impacted if the transaction completion requires fulfillment of compliance requirements if any.';
  			      if(origSplit[0] === 'US')
					{
						_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
						
						if(conf['paymodeshort'] === "WIRE" || conf['paymodeshort'] === "WIREFRT" || conf['paymode'] === "WIRE"|| conf['paymode'] === "WIREFRT")
						{
						//_obj.lblInfo.text = "You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TOML may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.";	
						//changing on 04-July-17
						    // _obj.lblInfo.text = 'You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TOMPL may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.'; 
						//changing on 21-Aug-17
						 if (TiGlobals.usLicsState.indexOf(_obj.state ) != -1 ){
							 _obj.lblInfo.text = 'You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, Moneydart Global Services Inc DBA Xpress Money may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.'; 
                            }
                         else{
                             _obj.lblInfo.text = 'You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TTSPL may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.'; 
                            }
							 
			
						}
					
					}
					
					if(origSplit[0] === 'UK')
					{
						_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
						
						//_obj.lblInfo.text = "Our Nostro account details have changed from Axis Bank to Karnataka Bank. Kindly note the same before initiating the fund transfers.";
					    //_obj.lblInfo.text = "Our Nostro account details have changed from Karnataka Bank to Axis Bank. Kindly note the same before initiating the fund transfers.";//added on 02-may-17 by sanjivani CMN56
					    _obj.lblInfo.text ='Our Nostro account details have changed from Axis Bank to Barclays Bank. Kindly note the same before initiating the fund transfers';//added on 02-Aug-17 by sanjivani

					}
					
					if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
					{
						_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
						
						_obj.lblInfo.text = "Our Correspondent Bank details have changed. Kindly note the same before initiating the fund transfers.";
					}
					
					_obj.btnServiceTax = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnServiceTax.backgroundColor = '#000';
					_obj.btnServiceTax.title = 'Service Tax';
					_obj.btnServiceTax.bottom = 20;
					
					_obj.btnServiceTax.addEventListener('click',function(e){
						require('/js/transfer/ServiceTaxModal').ServiceTaxModal();
					});
					
					_obj.termsView = Ti.UI.createView(_obj.style.termsView);
					_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
					_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
					_obj.lblTerms1 = Ti.UI.createLabel(_obj.style.lblTerms1);
					
					if(origSplit[0] === 'US')
					{
						_obj.lblTerms.text = 'I agree to the ';
						_obj.lblTerms1.text = 'Terms & Conditions';
					}
					else if (origSplit[0] === 'UK')
					{
						_obj.lblTerms.text = 'I agree to the ';
						_obj.lblTerms1.text = 'Terms & Conditions';   //for CMN 70 e
						//_obj.lblTerms.text = 'I hereby declare and confirm that the information provided herein above, including but not limited to my Bank account and/or transaction details are correct.';
					}
					
					_obj.terms = 'N';
					_obj.imgTerms.addEventListener('click',function(e){
						if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
						{
							_obj.imgTerms.image = '/images/checkbox_sel.png';
							_obj.terms = 'Y';
						}
						else
						{
							_obj.imgTerms.image = '/images/checkbox_unsel.png';
							_obj.terms = 'N';
						}
					});
					
					_obj.lblTerms.addEventListener('click',function(e){
						if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
						{
							_obj.imgTerms.image = '/images/checkbox_sel.png';
							_obj.terms = 'Y';
						}
						else
						{
							_obj.imgTerms.image = '/images/checkbox_unsel.png';
							_obj.terms = 'N';
						}
					});
					
					_obj.lblTerms1.addEventListener('click',function(e){
						require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','txn','');
					});
			_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnSubmit.title = 'CONFIRM';
					
					_obj.paymentView.add(_obj.lblPaymentHeader);
					_obj.actionView.add(_obj.lblEdit);
					_obj.actionView.add(_obj.lblSeparator1);
					if(Ti.Platform.osname === 'android'){
					if(conf['paymode'] !== 'CCARD')
					{
						_obj.actionView.add(_obj.lblSave);
						_obj.actionView.add(_obj.lblSeparator2);
					}
                                         }
                                         else{
                                             if(conf['paymodeCode'] !== 'CCARD')
					{
						_obj.actionView.add(_obj.lblSave);
						_obj.actionView.add(_obj.lblSeparator2);
					}
                                        }

					_obj.actionView.add(_obj.lblCancel);
					_obj.paymentView.add(_obj.actionView);
				
					_obj.actionView.addEventListener('click',function(e){
						if(e.source.sel === 'edit')
						{
							require('/js/transfer/EditTransactionModal').EditTransactionModal(conf);
							destroy_preconfirmation();
						}
						
						if(e.source.sel === 'save')
						{
							activityIndicator.showIndicator();
							
							var xhr = require('/utils/XHR');
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"SAVEINCOMPLETETRANSACTION",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'",'+
									'"paymodeCode":"'+conf['paymode']+'",'+
								    '"receiverNickName":"'+conf['receiverNickName']+'",'+
								    '"receiverOwnerId":"'+conf['receiverOwnerId']+'",'+
								    '"receiverLoginid":"'+conf['receiverLoginid']+'",'+
								    '"amount":"'+conf['amount']+'",'+
								    '"accountId":"'+conf['accountId']+'",'+
								    '"bankName":"'+conf['bankName']+'",'+
								    '"bankBranch":"'+conf['bankBranch']+'",'+
								    '"bankAccNo":"'+conf['accountNo']+'",'+
								    '"purpose":"'+conf['purpose']+'",'+
								    '"checkNumber":"",'+
								    '"checkBankName":"",'+
								    '"checkBankBranch":"",'+
								    '"clearingHouseId":"",'+
								    '"clearingHouseAddress":""'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
					
							function xhrSuccess(e) {
								activityIndicator.hideIndicator();
								if(e.result.responseFlag === "S")
								{
									if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast(e.result.message);
									}
									else
									{
										require('/utils/AlertDialog').iOSToast(e.result.message);
									}
									
									require('/utils/RemoveViews').removeAllScrollableViews();
									destroy_preconfirmation();
								}
								else
								{
									if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
									{
										require('/lib/session').session();
										destroy_preconfirmation();
									}
									else
									{
										require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
									}
								}
							}
					
							function xhrError(e) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
						}
						
						if(e.source.sel === 'cancel')
						{
							var alertDialog = Ti.UI.createAlertDialog({
								buttonNames:[L('btn_yes'), L('btn_no')],
								message:L('cancel_transaction')
							});
						
							alertDialog.show();
							
							alertDialog.addEventListener('click', function(e){
								alertDialog.hide();
								if(e.index === 0 || e.index === "0")
								{
									require('/utils/RemoveViews').removeAllScrollableViews();
									destroy_preconfirmation();
									alertDialog = null;
								}
							});
						}
					});
					
					
					_obj.mainView.add(_obj.paymentView);
					_obj.mainView.add(_obj.tblPaymentDetails);
					
					if( origSplit[0] == 'UK' || (origSplit[0] == 'CAN' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'CIP'))) ){
					_obj.mainView.add(_obj.receiveDtlView);  //CMN70-4
					_obj.mainView.add(_obj.tblReceiverDetails);
					
					_obj.mainView.add(_obj.receiverToReceive);  //CMN70-4
					_obj.mainView.add(_obj.tblToReceive);
					}
					else{
					_obj.convertedView.add(_obj.lblAmountConvertedHeader);
					_obj.convertedView.add(_obj.lblAmountConverted);
					_obj.mainView.add(_obj.convertedView);
					_obj.mainView.add(_obj.tblConverted);
					
					if(origSplit[0] === 'US'&& ((conf['paymode'] === 'ACHGFXFRT') || (conf['paymode'] === 'WIREFRT'))) //|| (origSplit[0] === 'UK' && (conf['paymode'] === 'DEBFRT')))  //updated on 29Aug17 for CMN70-part4 requirement
					{
						if(_obj.isFRT === 'Y')
						{
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatHeader);
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatAmount);
							_obj.mainView.add(_obj.SwachhBharatView);
						}
					}
					
					/*if(origSplit[0] === 'AUS' && ((conf['paymode'] === 'POLIFRT') || (conf['paymode'] === 'POLIFR')))
					{
						if(_obj.isFRT === 'Y')
						{
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatHeader);
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatAmount);
							_obj.mainView.add(_obj.SwachhBharatView);
						}
					}*/
					

				//	if(_obj.isFRT === 'N')
					//{
					 // if((origSplit[0] === 'AUS' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'POLI'))) ||(origSplit[0] === 'UK' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'DEB')/*||(conf['paymode'] === 'CIP')*/)))
					 if(origSplit[0] === 'AUS' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'POLI')))
					{
					 	_obj.recipientAmountToView.add(_obj.lblRecipientAmountToHeader);
						_obj.recipientAmountToView.add(_obj.lblRecipientAmountTo);
						_obj.mainView.add(_obj.recipientAmountToView);
				   //}
					}else //if(_obj.isFRT === 'Y')
					{
						_obj.recipientAmountView.add(_obj.lblRecipientAmountHeader);
						_obj.recipientAmountView.add(_obj.lblRecipientAmount);
						_obj.mainView.add(_obj.recipientAmountView);
					}
				
					//_obj.summaryView.add(_obj.lblSummary);
					//_obj.summaryView.add(_obj.lblSummaryReceipt);
					//_obj.mainView.add(_obj.summaryView);
					//_obj.indicatorView.add(_obj.imgIndicator);
					//_obj.mainView.add(_obj.indicatorView);
					_obj.mainView.add(_obj.tblSummary);
					
					}
					

					//CMN66-30June17
					//_obj.infoTextView1.add(_obj.imgInfo1);  //CMN84a
					if(origSplit[0] == 'UK' || origSplit[0] == 'US' ){ //CMN 76
						         _obj.infoTextView1.add(_obj.imgInfo1);
			                      _obj.infoTextView1.add(_obj.lblInfo1);
							
						}/*else {
							if( (origSplit[0] == 'CAN' && conf['paymode'] !== 'WIRE') || (origSplit[0] == 'CAN' && conf['paymode'] !== 'CIP')){
							//_obj.infoTextView1.add(_obj.imgInfo1);
							//_obj.infoTextView1.add(_obj.lblInfo6);
							}else{
							_obj.infoTextView1.add(_obj.imgInfo1);
							_obj.infoTextView1.add(_obj.lblInfo6);
							}
							
						}*/
						
					if( (origSplit[0] !== 'UK') &&( origSplit[0] !== 'US') ){  //CMN84a
						if( origSplit[0] == 'CAN' && (conf['paymode'] == 'CIP' || conf['paymode'] == 'WIRE' )) {
							
						}else{
					_obj.infoTextView1.add(_obj.imgInfo1);
					_obj.infoTextView1.add(_obj.lblInfo6);
					}
					}
					if(origSplit[0] === 'UK'){ //CMN 40 part 4 added on 24th aug
					_obj.infoTextView3.add(ukTerms);
					_obj.lblInfo2.top = 20;
					}
					
					//if(conf['paymode'] != 'CIP'  && conf['paymode'] != 'WIRE' || origSplit[0] != 'CAN'  ){  //CMN84a
					//if((conf['paymode'] != 'CIP' || conf['paymode'] != 'WIRE' ) ){  //CMN84a
					if( origSplit[0] == 'CAN' && (conf['paymode'] == 'CIP' || conf['paymode'] == 'WIRE' )) {
							
						}else{	
					_obj.infoTextView2.add(_obj.lblInfo2);
					}
					//_obj.infoTextView2.add(_obj.lblInfo2);
					_obj.mainView.add(_obj.infoTextView1);
					_obj.mainView.add(_obj.infoTextView3);
					_obj.mainView.add(_obj.infoTextView2);
				    //_obj.mainView.add(_obj.lblInfo2);
				    //setTimeout(function(){
				    	
				    
                    if(origSplit[0] === 'US')
					{
						if(conf['paymodeshort'] === "WIRE"|| conf['paymodeshort'] === "WIREFRT" || conf['paymode'] === "WIRE"|| conf['paymode'] === "WIREFRT" ){
						//_obj.infoTextView.add(_obj.imgInfo);
						_obj.infoTextView.add(_obj.lblInfo);
						_obj.mainView.add(_obj.infoTextView);
						}
					}
					
					if(origSplit[0] === 'UK')
					{
						//_obj.infoTextView.add(_obj.imgInfo);
						_obj.infoTextView.add(_obj.lblInfo);
						_obj.mainView.add(_obj.infoTextView);
					}
					
					if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
					{
						//_obj.infoTextView.add(_obj.imgInfo);
						_obj.infoTextView.add(_obj.lblInfo);
						_obj.mainView.add(_obj.infoTextView);
					}
					//},400);
					//if(_obj.isFRT === 'Y' && origSplit[0] === 'US')//Commenting on 03July17 for CMN66
					//{
						//CMN 76
						
						//if(origSplit[0] != 'UK' || origSplit[0] != 'US' || origSplit[0] != 'CAN'){
							//if(origSplit[0] != 'US'){ 
					    if(origSplit[0] != 'UK' && origSplit[0] != 'US'){//} || origSplit[0] != 'CAN'){
						//if(conf['paymode'] != 'CIP'  && conf['paymode'] != 'WIRE' || origSplit[0] != 'CAN' && (origSplit[0] !== 'UK') &&( origSplit[0] !== 'US') ){  //CMN84a
					     if( origSplit[0] == 'CAN' && (conf['paymode'] == 'CIP' || conf['paymode'] == 'WIRE' )) {
					    }else{ _obj.mainView.add(_obj.btnServiceTax);}
						}
						
					//}
					
					if(origSplit[0] === 'US')
					{
						_obj.termsView.add(_obj.imgTerms);
						_obj.termsView.add(_obj.lblTerms);
						_obj.termsView.add(_obj.lblTerms1);
						_obj.mainView.add(_obj.termsView);
					}
					else if (origSplit[0] === 'UK')
					{                                  //for CMN 70 e
						//if(conf['paymodeshort'] == 'DEB' || conf['paymodeshort'] == 'DEBFRT')
						//{
							_obj.termsView.add(_obj.imgTerms);
							_obj.termsView.add(_obj.lblTerms);
							_obj.termsView.add(_obj.lblTerms1);
							_obj.mainView.add(_obj.termsView);
						//}
					}
					
					_obj.mainView.add(_obj.btnSubmit);
					
					_obj.btnSubmit.addEventListener('click',function(e){
					
						if(origSplit[0] === 'US')
						{
							if(_obj.terms === 'N')
							{
								require('/utils/AlertDialog').showAlert('','Please agree to the terms and conditions to proceed',[L('btn_ok')]).show();
					    		return;
							}
						}
						
						if(origSplit[0] === 'UK')
						{
							if(conf['paymodeshort'] == 'DEB' || conf['paymodeshort'] == 'DEBFRT' || conf['paymode'] == 'DEB' || conf['paymode'] == 'DEBFRT')
							{
								if(_obj.terms === 'N')
								{
									require('/utils/AlertDialog').showAlert('','Please agree to the terms and conditions to proceed',[L('btn_ok')]).show();
						    		return;
								}
							}
						}
						
						//Ti.API.info("conf is:"+ conf[0]);
						try{
						require('/js/transfer/ConfirmationModal').ConfirmationModal(conf);
						}
						catch(e){
						Ti.API.info(e);
						}
						destroy_preconfirmation();
					});
				}
				
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_preconfirmation();
					}
					else
					{
						var alertDialog = Ti.UI.createAlertDialog({
							buttonNames:[L('btn_ok')],
							message:e.result.message
						});
					
						alertDialog.show();
						
						alertDialog.addEventListener('click', function(e){
							alertDialog.hide();
							if(e.index === 0 || e.index === "0")
							{
								require('/utils/RemoveViews').removeAllScrollableViews();
								destroy_preconfirmation();
								alertDialog = null;
							}
						});
					}
				}
				}catch(e){
					
					//Ti.API.info("Exception is:---",e);
				}
			}  //end of preconftxn sucess response
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		
		
	 
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				require('/utils/RemoveViews').removeAllScrollableViews();
				destroy_preconfirmation();
				alertDialog = null;
			}
		});
	});
	
	_obj.winPreConfirmation.addEventListener('androidback',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				require('/utils/RemoveViews').removeAllScrollableViews();
				destroy_preconfirmation();
				alertDialog = null;
			}
		});
	});
	
	function destroy_preconfirmation()
	{
		try{
			
			require('/utils/Console').info('############## Remove preconf start ##############');
			
			_obj.winPreConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winPreConfirmation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_preconfirmation',destroy_preconfirmation);
			require('/utils/Console').info('############## Remove preconf end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_preconfirmation', destroy_preconfirmation);
}; // Login()

