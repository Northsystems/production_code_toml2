exports.ServiceTaxModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Service Tax Details');
	
	var _obj = {
		style : require('/styles/transfer/ServiceTax').ServiceTax,
		winServiceTax : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		serviceTaxView : null,
		tblServiceTax : null
	};
	
	_obj.winServiceTax = Titanium.UI.createWindow(_obj.style.winServiceTax);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Service Tax';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Service Tax ///////////////////
	
	_obj.serviceTaxView = Ti.UI.createScrollView(_obj.style.serviceTaxView);
	
	_obj.tblServiceTax = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblServiceTax.height = 300;
	
	function populateServiceTax() {
		for(var i=0; i<=5; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				backgroundColor : 'transparent',
				className : 'service_tax'
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var lblHead = Ti.UI.createLabel({
				top : 0,
				height : 40,
				left : 0,
				right : 0,
				textAlign: 'center',
				font : TiFonts.FontStyle('lblBold14'),
				color : TiFonts.FontStyle('whiteFont'),
				backgroundColor:'#656565'
			});
			
			var lblBody = Ti.UI.createLabel({
				top : 0,
				width:Ti.UI.SIZE,
				textAlign: 'center',
				font : TiFonts.FontStyle('lblNormal14'),
				color : TiFonts.FontStyle('greyFont')
			});
			
			switch(i)
			{
				case 0:
					lblHead.text = 'Up to Rs. 1,00,000';
					row.height = 40;
					row.add(lblHead);
				break;
				
				case 1:
					//lblBody.text = 'Minimum INR 35 or 0.14% of the transaction amount';
					//added by sanjivani on 22Jan-2017 for defect no.1607
					//lblBody.text = 'Minimum INR 35 or 0.15% of the transaction amount';
					//added by sanjivanifor CMN66
					lblBody.text = '0.18% of the gross amount of currency exchanged subject to the minimum tax of Rs 45';
                    lblBody.height = 40;
					row.height = 40;
					row.add(lblBody);
				break;
				
				case 2:
				
					lblHead.text = 'INR 1,00,000 to INR 10,00,000';
					row.height = 40;
					row.add(lblHead);
				break;
				
				case 3:
				   ///added by sanjivanifor CMN66 on 30-june-17
					//lblBody.text = 'INR 140 (for 1 lakh) +0.07% of the balance amount converted above 1,00,000';
					lblBody.text = 'Rs 180+0.09% of the gross amount of currency exchanged for an amount of rupees exceeding Rs 100,000/';;
					row.height = 60;
					lblBody.height = 60;
					row.add(lblBody);
				break;
				
				case 4:
					lblHead.text = 'INR 10,00,000 above';
					row.height = 40;
					row.add(lblHead);
				break;
				
				case 5:
					//lblBody.text = 'INR 770 (for 10 lakhs) + 0.014% of the balance amount converted above 10,00,000, subject to maximum INR 7000';
					//added by sanjivani on 22Jan-2017 for defect no.1607
					lblBody.text = 'INR 770 (for 10 lakhs) + 0.015% of the balance amount converted above 10,00,000, subject to maximum INR 7000';
                     //added by sanjivani for CMN66&revised CMN66 on 30-june-17
                     lblBody.text ='Rs 990+0.018% of the gross amount of currency exchanged for an amount of rupees exceeding Rs 10,00,000/, subject to maximum amount of Rs 10,800/';
					row.height = 80;
					lblBody.height = 80;
					row.add(lblBody);
				break;
			}
			
			_obj.tblServiceTax.appendRow(row);
		}
	}
	
	populateServiceTax();
	
	_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
	//changed by Sanjivani for CMN66 on 30-June-17
	//_obj.lblInfo.text = '*The revised service tax of 14% will be applicable for all the transactions processed from 1st June 2015';
	//_obj.lblInfo.text = 'GST is recoverable from any foreign currency transfer into India.';
	_obj.lblInfo.text = 'Service Charge is recoverable from any foreign currency transfer into India.';//Done on 29th aug for cmn70-5
	
	_obj.mainView.add(_obj.serviceTaxView);
	_obj.serviceTaxView.add(_obj.tblServiceTax);
	_obj.serviceTaxView.add(_obj.lblInfo);
	_obj.globalView.add(_obj.mainView);
	_obj.winServiceTax.add(_obj.globalView);
	_obj.winServiceTax.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_service_tax();
	});
	
	function destroy_service_tax()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove Service Tax start ##############');
			
			_obj.winServiceTax.close();
			require('/utils/RemoveViews').removeViews(_obj.winServiceTax);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_service_tax',destroy_service_tax);
			require('/utils/Console').info('############## Remove Service Tax end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_service_tax', destroy_service_tax);
}; // ServiceTaxModal()
