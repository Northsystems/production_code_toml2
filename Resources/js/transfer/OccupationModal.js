exports.OccupationModal = function()
{
	require('/lib/analytics').GATrackScreen('Occupation');
	
	var _obj = {
		style : require('/styles/transfer/Occupation').Occupation,
		winOccupation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblOccupation : null,
		imgOccupation : null,
		occupationView : null,
		borderView1 : null,
		txtOccupation : null,
		borderView2 : null,
		btnSubmit : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winOccupation = Ti.UI.createWindow(_obj.style.winOccupation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winOccupation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Occupation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.occupationView = Ti.UI.createView(_obj.style.occupationView);
	_obj.lblOccupation = Ti.UI.createLabel(_obj.style.lblOccupation);
	_obj.lblOccupation.text = 'Select Occupation*';
	_obj.imgOccupation = Ti.UI.createImageView(_obj.style.imgOccupation);
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtOccupation = Ti.UI.createTextField(_obj.style.txtOccupation);
	_obj.txtOccupation.hintText = 'Other Occupation*';
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtOccupation.setTop(0);
	_obj.txtOccupation.setHeight(0);
	_obj.borderView2.setHeight(0);
	_obj.txtOccupation.visible = false;
	_obj.borderView2.visible = false;
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.headerView.add(_obj.lblHeader);
	//_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.occupationView.add(_obj.lblOccupation);
	_obj.occupationView.add(_obj.imgOccupation);
	_obj.mainView.add(_obj.occupationView);
	_obj.mainView.add(_obj.borderView1);
	_obj.mainView.add(_obj.txtOccupation);
	_obj.mainView.add(_obj.borderView2);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winOccupation.add(_obj.globalView);
	_obj.winOccupation.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = require('/utils/AlertDialog').showAlert('', L('occuption_mandatory'), [L('btn_yes'), L('btn_no')]);
		alertDialog.show();

		alertDialog.addEventListener('click', function(e) {
			alertDialog.hide();
			if (e.index === 0 || e.index === "0") {
				alertDialog = null;
				destroy_occupation();
			}
		});
	});
	
	// Occupation List
	
	_obj.occupationView.addEventListener('click',function(e){
		activityIndicator.showIndicator();
		var occupationOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"ocupationlisting",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].ocupationlisting.length;i++)
				{
					occupationOptions.push(e.result.response[0].ocupationlisting[i]);
				}
				
				var optionDialogOccupation = require('/utils/OptionDialog').showOptionDialog('Occupation',occupationOptions,-1);
				optionDialogOccupation.show();
				
				optionDialogOccupation.addEventListener('click',function(evt){
					if(optionDialogOccupation.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblOccupation.text = optionDialogOccupation.options[evt.index];
						optionDialogOccupation = null;
						
						if(_obj.lblOccupation.text === 'Others')
						{
							_obj.txtOccupation.setTop(15);
							_obj.txtOccupation.setHeight(35);
							_obj.borderView2.setHeight(1);
							_obj.txtOccupation.visible = true;
							_obj.borderView2.visible = true;
						}
						else
						{
							_obj.txtOccupation.setTop(0);
							_obj.txtOccupation.setHeight(0);
							_obj.borderView2.setHeight(0);
							_obj.txtOccupation.visible = false;
							_obj.borderView2.visible = false;
						}
					}
					else
					{
						optionDialogOccupation = null;
						_obj.lblOccupation.text = 'Select Occupation*';
						_obj.txtOccupation.setTop(0);
						_obj.txtOccupation.setHeight(0);
						_obj.borderView2.setHeight(0);
						_obj.txtOccupation.visible = false;
						_obj.borderView2.visible = false;
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_occupation();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		if(_obj.lblOccupation.text === 'Select Occupation*')
		{
			require('/utils/AlertDialog').showAlert('','Please select your occupation',[L('btn_ok')]).show();
    		_obj.lblOccupation.text = 'Select Occupation*';
			return;
		}
		else 
		{
			if(_obj.lblOccupation.text === 'Others')
			{
				if(_obj.txtOccupation.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter occupation',[L('btn_ok')]).show();
		    		_obj.txtOccupation.value = '';
		    		_obj.txtOccupation.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtOccupation.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of occupation',[L('btn_ok')]).show();
		    		_obj.txtOccupation.value = '';
		    		_obj.txtOccupation.focus();
					return;
				}
				else if(_obj.txtOccupation.value.search("[^a-zA-Z]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in occupation',[L('btn_ok')]).show();
		    		_obj.txtOccupation.value = '';
		    		_obj.txtOccupation.focus();
					return;
				}
				else
				{
				}
			}
		}
  		
  		var occupation = _obj.lblOccupation.text;
	
		if(occupation === 'Others')
		{
			occupation = _obj.txtOccupation.value;
		}
		
		activityIndicator.showIndicator();
		
		/*var xhr1 = require('/utils/XHR');
		xhr1.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhr1Success,
			error : xhr1Error,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhr1Success(e1) {
			
			if(e1.result.responseFlag === "S")
			{*/
				var xhr2 = require('/utils/XHR');
				xhr2.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"UPDATEUSERDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						/*'"loginId":"'+e1.result.loginId+'",'+
						'"ownerId":"'+e1.result.ownerId+'",'+
						'"sessionId":"'+e1.result.sessionId+'",'+
						'"referenceNo":"'+e1.result.referenceNo+'",'+*/
						'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				        '"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				        '"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
						'"referenceNo":"'+Ti.App.Properties.getString('referenceNo')+'",'+
						'"firstName":"",'+
						'"lastName":"",'+
						'"mobileNo":"",'+ 
						'"resPhoneNo":"",'+ 
						'"alternateNo":"",'+
						'"dob":"",'+
						'"address":"",'+ 
						'"state":"",'+
						'"city":"",'+
						'"zip":"",'+
						'"country":"",'+
						'"rmailId":"",'+
						'"gender":"",'+
						'"knowAboutUs":"",'+
						'"nationality":"",'+
						'"occupation":"'+occupation+'",'+
						'"fieldsToUpdate":{"mobileNoFlag":"N","resPhoneNoFlag":"N","alternateNoFlag":"N","firstNameFlag":"N","lastNameFlag":"N","emailIdFlag":"N","nationalityFlag":"N","occupationFlag":"Y","stateFlag":"N","cityFlag":"N","pinCodeFlag":"N"}'+
						'}',
					success : xhr2Success,
					error : xhr2Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhr2Success(e2) {
					
					if(e2.result.responseFlag === "S")
					{
						activityIndicator.hideIndicator();
						Ti.API.info(e2.result.message);
						
						/*if(TiGlobals.osname === 'android')
						{
							require('/utils/AlertDialog').toast(e2.result.message);
						}
						else
						{
							require('/utils/AlertDialog').iOSToast(e2.result.message);
						}*/
						try{
						
						Ti.App.fireEvent('changeOccupationFlag');
						
						destroy_occupation();
						}catch(e){}
					}
					else
					{
						if(e2.result.message === L('invalid_session'))
						{
							require('/lib/session').session();
							destroy_occupation();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e2.result.message,[L('btn_ok')]).show();
						}
						
					}
					xhr2 = null;
				}
		
				function xhr2Error(e2) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr2 = null;
				}	
			/*}
			else
			{
				require('/utils/AlertDialog').showAlert('',e1.result.message,[L('btn_ok')]).show();
			}
			xhr1 = null;
		}

		function xhr1Error(e1) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr1 = null;
		}*/
	});
	
	_obj.winOccupation.addEventListener('androidback',function(e){
		require('/utils/AlertDialog').showAlert('', L('occuption_mandatory'), [L('btn_ok')]).show();
	});
	
	function destroy_occupation()
	{
		try{
			
			require('/utils/Console').info('############## Remove occupation start ##############');
			
			_obj.winOccupation.close();
			require('/utils/RemoveViews').removeViews(_obj.winOccupation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_occupation',destroy_occupation);
			require('/utils/Console').info('############## Remove occupation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_occupation', destroy_occupation);
}; // Login()
