exports.ConfirmationModal = function(conf)
{
	require('/lib/analytics').GATrackScreen('Transfer Money Confirmation');
	
	var _obj = {
		style : require('/styles/transfer/Confirmation').Confirmation,
		winConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		imgInfo : null,
		lblInfo : null,
		//added on 30June17 for CMN66
		lblInfo1:null,
		lblInfo2:null,
		lblInfo3:null,  //CMN 70part 4 on 24th aug
		lblInfo5:null,
		lblInfo6:null,  //CMN 76
		infoTextView1:null,
		infoTextView2:null,
		infoTextView3 :null,
		imgInfo1:null,
		//
		infoTextView : null,
		lblAddressHeader : null,
		lblAddress : null,
		lblPaymentHeader : null,
		tblPaymentDetails : null,
		tblPaymentDetailsuk : null, //Done on 29th aug cmn70-4
		lblAmountConvertedHeader : null,
		lblAmountConverted : null,
		convertedView : null,
		tblConverted : null,
		tblConverteduk : null,
		lblRecipientAmountHeader : null,
		lblRecipientAmount : null,
		recipientAmountView : null,
		lblSummary : null,
		lblSummaryReceipt : null,
		summaryView : null,
		imgIndicator : null,
		indicatorView : null,
		tblSummary : null,
		tblSummaryuk : null,
		txtTraceNo : null,
		btnSubmit : null,
		webView : null,//for uk-sofort page
		
		isFRT : 'Y',
		confButton : null,
		responseCode : null,
		
		//CMN70
		txtComplOnline:null,
		txtComplHotline:null,
		txtComplTele:null,
		txtComplInPerson:null,
		txtComplMD:null,
		txtComplMD1:null,
		txtComplCFPB:null,
		txtBldComplaint2:null,
		complaintTxtUnderline:null,
		
		//CMN70
		USstateFlagConf:false,
		state:null,
		
		//CMN 70
		txtConsumer:null,
		lblAddressHeaderconf:null,
		lblAddressconf:null,
		lblDateHeaderconf:null,
		lblDateconf:null,
		lblDateHeaderconfuk:null,
		lblDateconfuk:null,
		
		receiverView : null,
		lblReceiverHeader:null,
		tblReceiveruk:null,
		nostroView:null,
		lblNostroHeader:null,
		tblNostrouk:null,
		
		//CMN84
		 bankView: null,
	     lblbank: null,
	     imgBank : null,
	     borderViewBank : null,
	     hasPropertyDeb:false,
	     hasPropertyPoli:false,
	     hasPropertyCCARD:false,
		
	};
	
	var xmlhttp = Ti.Network.createHTTPClient();
	              xmlhttp.onload = function()
	              {
		           var hostipInfo = JSON.parse(xmlhttp.responseText);
		           TiGlobals.ipAddress = hostipInfo.ip;
                   xmlhttp = null;
	              };
	
	             xmlhttp.onerror = function(e)
	             {
		            // require('/utils/AlertDialog').toast('Error in genrating IP Address.');
	             };
	             xmlhttp.open('GET', 'https://geoip.nekudo.com/api',true);
	             xmlhttp.send();
	             
  //CMN70	
  _obj.state = Ti.App.Properties.getString('state');

  if (TiGlobals.usLicsState.indexOf(_obj.state ) != -1){
        _obj.USstateFlagConf = true;
  } 
   
    _obj.confButton = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.confButton.title = 'BOOK ANOTHER TRANSACTION';
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	// FRT
	var paymodeData = Ti.App.Properties.getString('paymodeData');
	
	for(var f=0; f<paymodeData.length; f++)
	{
		if(paymodeData[f].paymodeCode === conf['paymodeshort'] || paymodeData[f].paymodeCode === conf['paymode']) //added on 7 dec 17 for save transaction
		{
			_obj.isFRT = paymodeData[f].isFRTPaymode;
		}
	}
	
	_obj.winConfirmation = Ti.UI.createWindow(_obj.style.winConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
		_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
        _obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);

	_obj.globalView.add(_obj.mainView);
	_obj.winConfirmation.add(_obj.globalView);
	_obj.winConfirmation.open();
	
	function navigateURL(params)
	{
		var winNavigate = Ti.UI.createWindow(_obj.style.winConfirmation);
		var globalView = Ti.UI.createView(_obj.style.globalView);	
		var headerView = Ti.UI.createView(_obj.style.headerView);
		var lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
		lblHeader.text = 'Confirmation';
		var imgClose = Ti.UI.createImageView(_obj.style.imgClose);
		var headerBorder = Ti.UI.createView(_obj.style.headerBorder);
		var mainView = Ti.UI.createScrollView(_obj.style.mainView);
		var webView = Ti.UI.createWebView(_obj.style.webView);
		
		if(conf['paymodeshort'] === 'DEB' || conf['paymode'] === 'DEB' || conf['paymodeshort'] === 'DEBFRT' || conf['paymode'] === 'DEBFRT'){
		if(_obj.hasPropertyDeb == true)
		{
			//if(_obj.hasPropertyDeb == true ){
			  if(Ti.Platform.osname === 'android'){
                 var activityIndicator = new ActivityIndicator(_obj.winConfirmation);
	
				_obj.globalView = Ti.UI.createView(_obj.style.globalView);
				
				_obj.headerView = Ti.UI.createView(_obj.style.headerView);
				
				_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
				_obj.lblHeader.text = 'Confirmation';
				
				_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
				
				_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
				
				_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
				_obj.webView = Ti.UI.createWebView(_obj.style.webView);
					
			params.debURL += "?hash="+params.debHash;
			params.debURL += "&rtrn="+params.rtrn;
			params.debFields = params.debFields.split("|");
			params.debData = params.debData.split("|");
			for(var d = 0; d<params.debFields.length;d++)
			{
				params.debURL += "&"+params.debFields[d]+"="+params.debData[d];
			}
			Ti.API.info("URL---",params.debURL);
			_obj.webView.url = params.debURL;
	     }else{
			     	params.debURL += "?hash="+params.debHash;
					params.debURL += "&rtrn="+params.rtrn;
					params.debFields = params.debFields.split("|");
					params.debData = params.debData.split("|");
					for(var d = 0; d<params.debFields.length;d++)
					{
						params.debURL += "&"+params.debFields[d]+"="+params.debData[d];
					}
					Ti.API.info("URL---",params.debURL);
					webView.url = params.debURL;
	         }
         }
		}
		else if(conf['paymodeshort'] === 'POLI' || conf['paymode'] === 'POLIFRT' || conf['paymodeshort'] === 'POLIFRT' || conf['paymode'] === 'POLI' ){
		 if(_obj.hasPropertyPoli == true)
		{
			//if(_obj.hasPropertyPoli == true ){
			try{
	 		//_obj.winConfirmation.add(_obj.confButton);
			//_obj.mainView.add(_obj.confButton);
			//_obj.globalView.add(_obj.confButton);
			
			//webView.width  = 200; 
			 if(Ti.Platform.osname === 'android'){
			              webView.height = 600;
			           }
			webView.url = params.navigateURL;
			 var val = null;
			 var val1 = null;
			webView.addEventListener('load',function(e) {
				try{
			     val = webView.evalJS("document.getElementById('rdTitle').innerHTML"); 
			     val1 = webView.evalJS("document.getElementById('err_msg').innerHTML"); 
			    // if(val! == null){
			    if(val == 'Your transaction is confirmed!' || val1 == 'Your transaction has been unsuccessful / aborted'){
			     	if(Ti.Platform.osname === 'android'){
			              webView.height = 450;
			           }else{
				          webView.height = 450;
			            }
			    	 mainView.add(_obj.confButton);
			     }
				//var val = webview.evalJS('document.title'); 
			    //Ti.API.info( "VALUE in is-> " + val );
			    //alert("VALUE is-> " + val);
			    }catch(e){}
			});
			//Ti.API.info( "VALUE out is-> " + val );
              
			_obj.confButton.addEventListener('click',function(e){
				if(val == 'Your transaction is confirmed!' || val1 == 'Your transaction has been unsuccessful / aborted'){
					
					require('/utils/RemoveViews').removeAllScrollableViews();
					winNavigate.close();
					headerView.remove(lblHeader);
					headerView.remove(imgClose);
					headerView.remove(headerBorder);
					globalView.remove(headerView);
					mainView.remove(webView);
					globalView.remove(mainView);
					winNavigate.remove(globalView);
					
					winNavigate = null;
					globalView = null;	
					headerView = null;
					lblHeader = null;
					imgClose = null;
					headerBorder = null;
					mainView = null;
					webView = null;
					
					destroy_confirmation();
						
				}
				else{
					//alert("My transaction is failed !!!!!!");
				}
					
				});
			}
			catch(e){
				Ti.API.info(e);
			}
		 }	
		}
		else if(conf['paymodeshort'] === 'CCARD' || conf['paymode'] === 'CCARD')
		{
                   if(_obj.hasPropertyCCARD === true){    
			//webView.url = Ti.Filesystem.resourcesDirectory + '/Resource/deviceDetect.html';
   if (TiGlobals.osname === 'android') {
	webView.url = 'web.php';
	   //Kalpesh code
	  webView.addEventListener('load',function(e){
	    var data = {
		navigateURL : params.navigateURL,
		encryptedMsg : params.encryptedMsg,
		merchantId : params.merchantId
	};
	webView.evalJS('foo(\'' + JSON.stringify(data) + '\')');
});
	}else{		
			// working code
		webView.url = Ti.Filesystem.resourcesDirectory + '/js/transfer/web.html';
			
			  webView.addEventListener('load',function(e){
	    var data = {
		navigateURL : params.navigateURL,
		encryptedMsg : params.encryptedMsg,
		merchantId : params.merchantId
	       };
	    webView.evalJS('foo(\'' + JSON.stringify(data) + '\')');
        });
		   }	
			/*var navigateURL=params.navigateURL;
			var merchantId=params.merchantId;
			webView.url = 'web.html';
			webView.evalJS('test(navigateURL,merchantId);');*/
			
            /*webView.url("POST", "var navigateURL='" + params.navigateURL + "';");
            webView.evalJS("var encryptedMsg='" + params.encryptedMsg+ "';");
			webView.evalJS("var merchantId='" + params.merchantId + "';");*/
             
			//webView.html='<!DOCTYPE html><html><form name="form_1" action="<?=$ccardURL?>" method="POST"><input type="hidden" name=params.navigateURL value=params.merchantId."|".params.encryptedMsg /></form><html>';
				
		}	
		}
		else
		{
			
		}
		
	 if(Ti.Platform.osname === 'android' && (conf['paymodeshort'] === 'DEB' || conf['paymode'] === 'DEB') ){
				_obj.headerView.add(_obj.lblHeader);
				_obj.headerView.add(_obj.imgClose);
				_obj.headerView.add(_obj.headerBorder);
				_obj.globalView.add(_obj.headerView);
			    _obj.mainView.add(_obj.webView);
				_obj.globalView.add(_obj.mainView);
				
				_obj.winConfirmation.add(_obj.globalView);
		//_obj.winConfirmation.add(webView);
		_obj.winConfirmation.open();
		
		
		_obj.imgClose.addEventListener('click',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_confirmation();
	});
	
	_obj.winConfirmation.addEventListener('androidback',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_confirmation();
	});

  }
  else{  
  	
		headerView.add(lblHeader);
		headerView.add(imgClose);
		headerView.add(headerBorder);
		globalView.add(headerView);
		mainView.add(webView);
		//mainView.add(_obj.confButton);
		globalView.add(mainView);
		winNavigate.add(globalView);
		winNavigate.open();
		
		winNavigate.addEventListener('androidback',function(evt){
			/*var alertDialog = Ti.UI.createAlertDialog({
				buttonNames:[L('btn_yes'), L('btn_no')],
				message:L('cancel_transaction')
			});
		
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(e){
				alertDialog.hide();
				if(e.index === 0 || e.index === "0")
				{*/
					require('/utils/RemoveViews').removeAllScrollableViews();
					winNavigate.close();
					headerView.remove(lblHeader);
					headerView.remove(imgClose);
					headerView.remove(headerBorder);
					globalView.remove(headerView);
					mainView.remove(webView);
					globalView.remove(mainView);
					winNavigate.remove(globalView);
					
					winNavigate = null;
					globalView = null;
					headerView = null;
					lblHeader = null;
					imgClose = null;
					headerBorder = null;
					mainView = null;
					webView = null;
					
					destroy_confirmation();
					//alertDialog = null;
				/*}
			});*/
		});
		
		imgClose.addEventListener('click',function(evt){
			/*var alertDialog = Ti.UI.createAlertDialog({
				buttonNames:[L('btn_yes'), L('btn_no')],
				message:L('cancel_transaction')
			});
		
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(e){
				alertDialog.hide();
				if(e.index === 0 || e.index === "0")
				{*/
					require('/utils/RemoveViews').removeAllScrollableViews();
					winNavigate.close();
					headerView.remove(lblHeader);
					headerView.remove(imgClose);
					headerView.remove(headerBorder);
					globalView.remove(headerView);
					mainView.remove(webView);
					globalView.remove(mainView);
					winNavigate.remove(globalView);
					
					winNavigate = null;
					globalView = null;	
					headerView = null;
					lblHeader = null;
					imgClose = null;
					headerBorder = null;
					mainView = null;
					webView = null;
					
					destroy_confirmation();
					//alertDialog = null;
				/*}
			});*/
		});
	 }	
	}
	
	
	// Check confirmation
	
	//function confirmation(conf)
	function confirmation()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 10000000) + 10000) +'",'+
				'"requestName":"BOOKTXN",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				//'"channelId":"MobileApp",'+
				'"channelId":"'+TiGlobals.channel+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+conf['paymode']+'",'+
				
			    '"receiverNickName":"'+conf['receiverNickName']+'",'+
			    '"receiverOwnerId":"'+conf['receiverOwnerId']+'",'+
			    '"receiverLoginid":"'+conf['receiverLoginid']+'",'+
			    '"receiverParentChildflag":"'+conf['receiverParentChildflag']+'",'+
			    '"amount":"'+conf['amount']+'",'+
			    '"accountId":"'+conf['accountId']+'",'+
			    '"bankName":"'+conf['bankName']+'",'+
			    '"bankBranch":"'+conf['bankBranch']+'",'+
			    '"accountNo":"'+conf['accountNo']+'",'+
			    '"purpose":"'+conf['purpose']+'",'+
			    '"personalMessage":"",'+
			    '"promoCode":"'+conf['promoCode']+'",'+
			    '"mobileAlerts":"'+conf['mobileAlerts']+'",'+
			    '"transactionInsurance":"'+conf['transactionInsurance']+'",'+
			    '"fxVoucherRedeem":"'+conf['fxVoucherRedeem']+'",'+
			    '"agentSpecialCode":"'+conf['agentSpecialCode']+'",'+
			    '"MORRefferalAmnt":"'+conf['morRefferalAmnt']+'",'+
			    '"programType":"'+conf['programType']+'",'+ 
			    '"txnRefId":"'+conf['txnRefId']+'",'+
			    '"contactDate":"'+conf['contactDate']+'",'+
			    '"timeFrom":"'+conf['timeFrom']+'",'+
			    '"timeTo":"'+conf['timeTo']+'",'+
			    '"timeFrom1":"'+conf['timeFrom1']+'",'+
			    '"timeTo1":"'+conf['timeTo1']+'",'+
			    '"voucherCode":"'+conf['voucherCode']+'",'+
			    '"voucherQty":"'+conf['voucherQty']+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			require('/utils/Console').info('Result ======== ' + e.result);
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === 'S')
			{
				activityIndicator.hideIndicator();
				TiGlobals.usKycDtlFlag = false;//cmn55
				//27 feb yodlle start
				/*//comment start
				if(conf['paymodeshort'] === 'ACH')
				{
					_obj.responseCode = e.result.responseCode;
					//_obj.responseCode = '1167';
					require('/utils/Console').info('responseCode is ======== ' + e.result.responseCode);
					if(e.result.responseCode =='1188'){
					var params = {
						issuerId : e.result.samlResponseArray.issuerId,
						SAMLResponse : e.result.samlResponseArray.SAMLResponse,
						navigateURL : e.result.navigateURL,
						TARGET : e.result.samlResponseArray.TARGET ,
						CONSUMER_URL : e.result.samlResponseArray.CONSUMER_URL
						
					};
					}
					else if(e.result.responseCode == '1167'){
						//yodleeBankName //routingNo //url
						//$routingNo=$ctReturnArray['bankData'][0]['routingNo'];
				var params = {
						routingNo : e.result.bankData.routingNo,
						yodleeBankName : e.result.yodleeBankName,
						//navigateURL : response.navigateURL,
						
					};
				}
				else{
					
				}
				navigateURL(params);
				}//end of ACH if
				else  //comment end*/
		       //activityIndicator.hideIndicator();
		       
		       Ti.API.info("-----------------PAY",conf['paymode']);
		Ti.API.info("-----------------PAY1",conf['paymodeshort']);
		Ti.API.info("-----------------PAY2",conf['paymodeCode']);
				if(conf['paymodeshort'] === 'DEB'|| conf['paymode'] === 'DEB' || conf['paymodeshort'] === 'DEBFRT'|| conf['paymode'] === 'DEBFRT') //changed for saved transaction process ON 13TH NOV
				{
					if(e.result.hasOwnProperty('debDetails') && e.result.debDetails !== ''){
						if(e.result.debDetails[0][0].hasOwnProperty('url'))
							{
					_obj.hasPropertyDeb = true;
					var params = {
						debHash : e.result.debDetails[0][0].hash,
						debFields : e.result.debDetails[0][0].parameterName,
						debData : e.result.debDetails[0][0].parameterValues,
						//navigateURL : e.result.debDetails[0][0].url
						debURL : e.result.debDetails[0][0].url,
						rtrn : e.result.rtrn
						
					};
					//require('/utils/Console').info('Result ======== ' + debURL);
					navigateURL(params);
					}
				 }	
				}
	               //else if(conf['paymodeshort'] === 'POLI' || conf['paymode'] === 'POLIFRT') //changed for saved transaction process ON 13TH NOV
				else if(conf['paymodeshort'] === 'POLI' || conf['paymodeshort'] === 'POLIFRT' || conf['paymode'] === 'POLI' || conf['paymode'] === 'POLIFRT' )
				{
			 if(e.result.hasOwnProperty('navigateURL')){
			 	  _obj.hasPropertyPoli = true;
					if(e.result)
					var params = {
						navigateURL : e.result.navigateURL
					};
					//Ti.API.info("IN POLI----------",JSON.stringify(params));
					navigateURL(params);
					}
				}
				else if(conf['paymodeshort'] === 'CCARD')
				{
					if(e.result.hasOwnProperty('navigateURL')){
					_obj.hasPropertyCCARD = true;
                        Ti.API.info("e.result.navigateURL",+e.result.navigateURL);
						Ti.API.info(" e.result.merchantId",+ e.result.merchantId);
						Ti.API.info("e.result.encryptedMsg",+e.result.encryptedMsg);
					var params = {
						navigateURL : e.result.navigateURL,
						merchantId : e.result.merchantId,
						encryptedMsg : e.result.encryptedMsg
					};
					
					navigateURL(params);
				 }
				}
                           else
				{
					
				}
				
				conf['txnRefId'] = e.result.rtrn;
				
				try{   //added on 11th aug 
				_obj.lblAddressHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
				_obj.lblAddressHeader.text = 'Your transaction is confirmed!';
				}catch(e){}
				
				_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);  //Done on 28th aug CMN70-4
				_obj.lblAddress.text = 'Thank you '+Ti.App.Properties.getString('firstName')+' '+Ti.App.Properties.getString('lastName')+' for choosing Remit2India for sending money to India. Remittance Transaction Reference Number (RTRN) has to be provided for each transaction and use the same for all communications with us. An email confirmation will be sent to ' + Ti.App.Properties.getString('loginId').toLowerCase();
				
	//Added by pallavi on 10th Aug for Address on confirmation page
				
	_obj.lblAddressHeaderconf = Ti.UI.createLabel(_obj.style.lblAddressHeader);
	_obj.lblAddressHeaderconf.text = 'Our Correspondence Address';
	
	 //For CMN70
	_obj.state = Ti.App.Properties.getString('state');
        if (origSplit[0] =='US' && TiGlobals.usLicsState.indexOf(_obj.state ) != -1 ){
       	    
       	     _obj.lblAddressconf = Ti.UI.createLabel(_obj.style.lblAddress);
	     _obj.lblAddressconf.text = 'Moneydart Global Services Inc DBA Xpress Money \n1000 Woodbridge Center Drive\nWoodbridge, NJ 07095,\nTel: 1-888-736-4886\nwww.remit2india.com';
	    } 
	    //CMN84
             else if (origSplit[0] == 'CAN'){
             	if(conf['paymode'] == 'WIRE'){
             		_obj.txtComplMD = Ti.UI.createLabel(_obj.style.lblAddress);
             		if(Ti.Platform.osname === 'Android'){
             		_obj.txtComplMD.font =TiFonts.FontStyle('lblNormal14');
             		}else{
             			_obj.txtComplMD.font =TiFonts.FontStyle('lblSwissBold12');
             		}
		             _obj.txtComplMD.text = "UAE Exchange Canada Inc | Head Office";
               _obj.lblAddressconf = Ti.UI.createLabel(_obj.style.lblAddress);
               try{
               _obj.lblAddressconf.top = 0;
               }catch(e){}
	          _obj.lblAddressconf.text = 'Suite 239 7025 Tomken Road,\nMississauga, ON-L5S 1R6 \nTel: 1-888-736-4886 \nwww.remit2india.com';
               }
               else if (conf['paymode'] == 'CIP'){
               _obj.txtComplMD = Ti.UI.createLabel(_obj.style.lblAddress);
             	if(Ti.Platform.osname === 'Android'){
             		_obj.txtComplMD.font =TiFonts.FontStyle('lblNormal14');
             		}else{
             			_obj.txtComplMD.font =TiFonts.FontStyle('lblSwissBold12');
             		}
		             _obj.txtComplMD.text = "UAE Exchange Canada Inc | Head Office";
		         _obj.lblAddressconf = Ti.UI.createLabel(_obj.style.lblAddress);
		         try{
               _obj.lblAddressconf.top = 0;
               }catch(e){}
	          _obj.lblAddressconf.text = 'Suite 239 7025 Tomken Road,\nMississauga,ON-L5S 1R6 \nTel: 1-888-736-4886 \nwww.remit2india.com';
                    }
              }
       else{
             _obj.lblAddressconf = Ti.UI.createLabel(_obj.style.lblAddress);   //For CMN 70 part3 on 23rd aug
	     _obj.lblAddressconf.text = 'TOM TECHNOLOGY SERVICES PRIVATE LTD\nLevel-4, B Wing Reliable Tech Park,\nThane Belapur Road,Airoli,\n New Mumbai-400 708, India\nTel: 1-888-736-4886\nwww.remit2india.com';
             }
	
	_obj.lblDateHeaderconf = Ti.UI.createLabel(_obj.style.lblDateHeader);
	_obj.lblDateHeaderconf.text = 'Today\'s Date & Time';
	
	_obj.lblDateconf = Ti.UI.createLabel(_obj.style.lblDate);
	
	//Done on 28th aug cmn70-4
	_obj.lblDateHeaderconfuk = Ti.UI.createLabel(_obj.style.lblDateHeader);
	_obj.lblDateHeaderconfuk.text = 'Date and Time Stamp ';
	
	_obj.lblDateconfuk = Ti.UI.createLabel(_obj.style.lblDate);
    
    if(origSplit[0] == 'UK' || origSplit[0] == 'CAN'){  //Done on 28th aug cmn70-4
		var dateFormat = Date().split(' ');  //Tue Jul 25 2017 17:01:39 GMT+0530 (IST)-orignal
	   //requirement in cmn -7th July 2017 17:12:02 IST
		_obj.lblDateconfuk.text =dateFormat[2]+' '+dateFormat[1]+', '+dateFormat[3]+' '+dateFormat[4]+' '+dateFormat[6];
		Ti.API.info("TIME IN PRECONF:--",_obj.lblDateconf.text);
	}else if(origSplit[0] != 'UK' || origSplit[0] != 'CAN'){  //Done for CMN 84 ON 14th nov
		var dateFormat = Date().split(' ');  //Tue Jul 25 2017 17:01:39 GMT+0530 (IST)-orignal
	 //4.16(IST) GMT +0530-requirement for bug:739 on 25th july
	Ti.API.info("TIME IN PRECONF:--",dateFormat[5]); //GMT+0530
	Ti.API.info("TIME IN PRECONF:--",dateFormat[6]); //(IST)
	_obj.lblDateconf.text = dateFormat[0]+' '+dateFormat[1]+' '+dateFormat[2]+' '+dateFormat[3]+' '+dateFormat[4]+''+dateFormat[6]+' '+dateFormat[5];
	Ti.API.info("TIME IN PRECONF:--",_obj.lblDateconf.text);
		
	}			
				var lblKeyPDrtrn = Ti.UI.createLabel({   //Done on 29th aug cmn70-4
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						text:'RTRN No.'
					});
					var lblValuePDrtrn = Ti.UI.createLabel({
						top : 5,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
				if(e.result.hasOwnProperty('rtrn'))
							{
								if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //Done on 29th aug cmn70-4
								lblValuePDrtrn.text = e.result.rtrn;                 //CMN 84-a
							}
							}
				_obj.paymentView = Ti.UI.createView(_obj.style.paymentView);
				_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
				_obj.lblPaymentHeader.textAlign = 'center';
				//_obj.lblPaymentHeader.text = 'Provisional Receipt'; //Done on 28th aug CMN70-4
				_obj.lblPaymentHeader.text = 'Payment Details';
				 
				
	             if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){	//CMN 84-a
	            
	            _obj.tblPaymentDetailsuk = Ti.UI.createTableView(_obj.style.tableView); //Done on 29th aug cmn70-4
				_obj.tblPaymentDetailsuk.height = 0; 			
				
				for(var i=0; i<=2; i++)
				{
					var rowPD = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowPD.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyPD = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValuePD = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
				
					switch(i)
					{
						case 0:
						    if(e.result.hasOwnProperty('paymodeData'))
							{
								lblKeyPD.text = 'Payment Mode:';
								/*lblValuePD.text = e.result.paymodeData[0].paymodeDesc;
								if(e.result.paymodeData[0].paymodeDesc === 'Wire FRT'){
										e.result.paymodeData[0].paymodeDesc = 'Wire Transfer';
									lblValuePD.text = e.result.paymodeData[0].paymodeDesc;
									}else{
										lblValuePD.text = e.result.paymodeData[0].paymodeDesc;
									}*/
									lblValuePD.text =  conf['paymodeLabel'];  //CMN84a _ added after getting confirmation from Kanchan on Call paymode label descreapency fix
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetailsuk.appendRow(rowPD);
								_obj.tblPaymentDetailsuk.height = parseInt(_obj.tblPaymentDetailsuk.height + 60);
							}
						break;
						
						case 1:
						  if(e.result.hasOwnProperty('purpose'))
							{
								var purposeSplit = e.result.purpose.split('~');
								lblKeyPD.text = 'Purpose of Remittance:';
								lblValuePD.text = conf['purpose1'];
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetailsuk.appendRow(rowPD);
								_obj.tblPaymentDetailsuk.height = parseInt(_obj.tblPaymentDetailsuk.height + 60);
							}
						break;
						case 2:
						       try{
								rowPD.height = 70;//80
								lblKeyPD.text = 'Bank Account Details:';
								lblValuePD.height = Ti.UI.SIZE;
								    if(conf['paymode'] === 'DEBFRT' ||conf['paymode'] === 'DEB'){
									lblValuePD.text = conf['bankName'];
									}
									else if(conf['paymode'] === 'WIRE' || conf['paymode'] === 'WIREFRT' ){
									lblValuePD.text = conf['bankName'] + '\n'+ conf['bankBranch'];
									lblValuePD.bottom = 5;
									}
									else if(conf['paymode'] === 'CIP'){
									lblValuePD.text = conf['bankName'] + '\n'+ conf['accountNo'];	
									}
									else{}
									
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetailsuk.appendRow(rowPD);
								_obj.tblPaymentDetailsuk.height = parseInt(_obj.tblPaymentDetailsuk.height + 70); //80
								}catch(e){}
						
						break;
					
					}
					
				}

				}
				
				_obj.tblPaymentDetails = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblPaymentDetails.height = 0;
				
				for(var i=0; i<=3; i++)
				{
					var rowPD = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowPD.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyPD = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValuePD = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					switch(i)
					{
						case 0:
						    
							if(e.result.hasOwnProperty('rtrn'))
							{
								if(origSplit[0] != 'UK'){ //Done on 29th aug cmn70-4
								lblKeyPD.text = 'RTRN No.';
								lblValuePD.text = e.result.rtrn;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
							}
						break;
						
						case 1:
							if(e.result.hasOwnProperty('transferAmount'))
							{
								//Done for CMN 70(part 3)on 23rd aug
								if(origSplit[0] === 'US'){
								lblKeyPD.text = 'Total Amount';
								}else{
									lblKeyPD.text = 'Transfer Amount';
								}
								lblValuePD.text = origSplit[1] + ' ' + e.result.transferAmount;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
						break;
						
						case 2:
							if(e.result.hasOwnProperty('programFee') && e.result.programFee !== '')
							{
								var voucher = '';
								
								if(e.result.hasOwnProperty('benefitsData') && e.result.benefitsData !== '')
								{ 
									if(e.result.benefitsData[0].hasOwnProperty('ep'))
									{
										voucher = '(FXVoucher)';
									}
									else if(e.result.benefitsData[0].hasOwnProperty('fw'))
									{
										voucher = '(Freeway Voucher)';
									}  
								}
								
								lblKeyPD.text = 'Program Fee ' + voucher;
								lblValuePD.text = e.result.programFee;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
						break;
						
						case 3:
						    if(e.result.hasOwnProperty('netApplicableFee'))
							{
								if(e.result.netApplicableFee >= 0.0)
								{
								//Done for CMN 70(part 3)on 23rd aug
								 if(origSplit[0] === 'US'){
								lblKeyPD.text = 'Transfer Fee';
								}else{
									lblKeyPD.text = 'Net Applicable Fee';
								}
								lblValuePD.text = e.result.netApplicableFee;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							   }
							}
						break;
					}
				}
				
				_obj.convertedView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblAmountConvertedHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
                if(origSplit[0] === 'US'){  // added by sanjivani for CMN70
                _obj.lblAmountConvertedHeader.text = 'Transfer Amount';
				}
				else if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
				_obj.lblAmountConvertedHeader.text = 'Receiver Details';
				}
				else{
				   _obj.lblAmountConvertedHeader.text = 'Amount to be converted';
				}				

				
				_obj.lblAmountConverted = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblAmountConverted.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
				
				//Done on 29th aug for CMN70-4
				if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
					
				_obj.tblConverteduk = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblConverteduk.height = 0;
				
				for(var i=0; i<=1; i++)
				{
					var rowCon = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowCon.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyCon = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValueCon = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					switch(i)
					{
						case 0:
							 if(e.result.hasOwnProperty('receiverData'))
							{
								lblKeyCon.text = 'Name:';
								lblValueCon.text = e.result.receiverData[0].firstName +' '+ e.result.receiverData[0].lastname;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverteduk.appendRow(rowCon);
								_obj.tblConverteduk.height = parseInt(_obj.tblConverteduk.height + 60);
							}
							
						break;
						
						case 1:
							if(e.result.hasOwnProperty('receiverData'))
							{
								rowCon.height = 100;
                                lblKeyCon.text = 'Bank Account Details:';
								lblValueCon.text = e.result.receiverData[0].recvBankName;
								lblValueCon.height = Ti.UI.SIZE;
									lblValueCon.bottom = 15;
									var bankBranch = e.result.receiverData[0].recvBankBranch;
									var n = bankBranch.indexOf(",");
									if(n == -1){
									rowCon.height =100;	
									lblValueCon.text = e.result.receiverData[0].recvBankName + '\n'+bankBranch + '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblConverteduk.height = parseInt(_obj.tblConverteduk.height + 100);
									}
									else{
									
									var bankBranchSplit = bankBranch.split(',');
									if(bankBranchSplit.length == 2){
									rowCon.height = 120;		
									lblValueCon.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblConverteduk.height = parseInt(_obj.tblConverteduk.height + 120);
									}
	                                else if(bankBranchSplit.length == 3){
	                                rowCon.height = 140;
	                               	lblValueCon.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+bankBranchSplit[2]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblConverteduk.height = parseInt(_obj.tblConverteduk.height + 140);//120
									}
									else if(bankBranchSplit.length == 4){
									rowCon.height = 160;	
	                               	lblValueCon.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+bankBranchSplit[2] + '\n'+bankBranchSplit[3]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblConverteduk.height = parseInt(_obj.tblConverteduk.height + 160);
									}
									else if(bankBranchSplit.length == 5){
									rowCon.height = 180;	
	                               	lblValueCon.text = e.result.receiverData[0].recvBankName + '\n'+bankBranchSplit[0]+'\n'+bankBranchSplit[1]+ '\n'+bankBranchSplit[2] + '\n'+bankBranchSplit[3] + '\n'+bankBranchSplit[4]+ '\n'+e.result.receiverData[0].recvBankAccntNumber;
									_obj.tblConverteduk.height = parseInt(_obj.tblConverteduk.height + 180);
									}  
									else{}   
        
									}
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverteduk.appendRow(rowCon);
								
							}
						break;
						
					}
					
				}
				}
				
				_obj.receiverView = Ti.UI.createView(_obj.style.paymentView);
				_obj.lblReceiverHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
				_obj.lblReceiverHeader.textAlign = 'center';
				//_obj.lblPaymentHeader.text = 'Provisional Receipt'; //Done on 28th aug CMN70-4
				_obj.lblReceiverHeader.text = 'Receiver to Receive';
				_obj.receiverView.top=0;
					//Done on 29th aug for CMN70-4
				if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){//CMN 84-a
					
				_obj.tblReceiveruk = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblReceiveruk.height = 0;
				
				for(var i=0; i<=4; i++)
				{
					var rowCon = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowCon.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyCon = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValueCon = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					switch(i)
					{
						case 0:
							if(e.result.hasOwnProperty('exchangeRate'))
							{
								lblKeyCon.text = 'Indicative Exchange Rate:';
								lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.exchangeRate;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblReceiveruk.appendRow(rowCon);
								_obj.tblReceiveruk.height = parseInt(_obj.tblReceiveruk.height + 60);
							}
							
						break;
						
						case 1:
							if(e.result.hasOwnProperty('transferAmount'))
							{
								lblKeyCon.text = 'Sending Amount:';
								lblValueCon.text = origSplit[1] + ' ' + e.result.transferAmount;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblReceiveruk.appendRow(rowCon);
								_obj.tblReceiveruk.height = parseInt(_obj.tblReceiveruk.height + 60);
							}
						break;
						
						case 2:
							 if(e.result.hasOwnProperty('netApplicableFee'))
							{
								if(e.result.netApplicableFee >= 0.0)
								 {
								   lblKeyCon.text = 'Net Applicable Fee:';
								 }
								lblValueCon.text = origSplit[1] + ' ' +e.result.netApplicableFee;

								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblReceiveruk.appendRow(rowCon);
								_obj.tblReceiveruk.height = parseInt(_obj.tblReceiveruk.height + 60);
							}
						break;
						case 3:
						    lblKeyCon.font = TiFonts.FontStyle('lblNormal14');
						    lblKeyCon.color = TiFonts.FontStyle('blackFont');
							lblKeyCon.text = 'Net Transfer Amount :';  //made bold
								lblValueCon.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblReceiveruk.appendRow(rowCon);
								_obj.tblReceiveruk.height = parseInt(_obj.tblReceiveruk.height + 60);
							
						break;	
						case 4:
						
								lblKeyCon.text = 'Indicative Destination Amount:'; 
								lblValueCon.text = destSplit[1] + ' ' + e.result.convertedAmount;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblReceiveruk.appendRow(rowCon);
								_obj.tblReceiveruk.height = parseInt(_obj.tblReceiveruk.height + 60);
								
							break;	
					}
					
				}
				}
				
				_obj.nostroView = Ti.UI.createView(_obj.style.paymentView);
				_obj.lblNostroHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
				//_obj.lblNostroHeader.textAlign = 'center';
				//_obj.lblPaymentHeader.text = 'Provisional Receipt'; //Done on 28th aug CMN70-4
				_obj.lblNostroHeader.text = 'Remittance Transfer Details';
				_obj.nostroView.top = 0;
				_obj.tblConverted = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblConverted.height = 0;
				
				for(var i=0; i<=3; i++)
				{
					var rowCon = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowCon.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyCon = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValueCon = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					switch(i)
					{
						case 0:
							if(e.result.hasOwnProperty('exchangeRate'))
							{
								lblKeyCon.text = 'Exchange Rate';
								lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.exchangeRate;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverted.appendRow(rowCon);
								_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
							}
							
						break;
						
						case 1:
							if(e.result.hasOwnProperty('netExchangeRate'))
							{
								lblKeyCon.text = 'Exchange Rate with Benefit';
								lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.netExchangeRate;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverted.appendRow(rowCon);
								_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
							}
						break;
						
						case 2:
							if(e.result.hasOwnProperty('convertedAmount'))
							{
								lblKeyCon.text = 'Converted Amount';
								lblValueCon.text = destSplit[1] + ' ' + e.result.convertedAmount;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverted.appendRow(rowCon);
								_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
							}	
						break;
						
						case 3:
							if(e.result.hasOwnProperty('serviceTax'))
							{
								if(_obj.isFRT === 'Y' && ((origSplit[0] === 'US' && ((conf['paymode'] === 'ACHGFXFRT') || (conf['paymode'] === 'WIREFRT')) )|| (origSplit[0] === 'UK' && (conf['paymode'] === 'DEBFRT'))))
								{
									lblKeyCon.text = 'Service Tax';
									lblValueCon.text = '(-) ' + destSplit[1] + ' ' + e.result.serviceTax;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}
							}
						break;
					}
					
				}
				
				_obj.SwachhBharatView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblSwachhBharatHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				_obj.lblSwachhBharatHeader.text = 'Swachh Bharat Cess';
				_obj.lblSwachhBharatAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblSwachhBharatAmount.text = destSplit[1] + ' ' + e.result.sbcTax;
				
				_obj.recipientAmountView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblRecipientAmountHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				
				if(origSplit[0] === 'US'){  //CMN70
				  _obj.lblRecipientAmountHeader.text = 'Total Amount to Receiver';
				}
				else{  //CMN 84-a
				_obj.lblRecipientAmountHeader.text = 'Amount to Recipient';
				}
				_obj.lblRecipientAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblRecipientAmount.text = destSplit[1] + ' ' + e.result.indicativeAmountRecipient;
				
				_obj.recipientAmountToView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblRecipientAmountToHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				_obj.lblRecipientAmountToHeader.text = 'Indicative Destination Amount';
				_obj.lblRecipientAmountTo = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblRecipientAmountTo.text = destSplit[1] + ' ' + e.result.convertedAmount;
				
				/*////comment start
				_obj.summaryView = Ti.UI.createView(_obj.style.summaryView);
				_obj.lblSummary = Ti.UI.createLabel(_obj.style.lblSummary);
				_obj.lblSummary.text = 'Summary of confirmation';
				_obj.lblSummaryReceipt = Ti.UI.createLabel(_obj.style.lblSummaryReceipt);
				_obj.lblSummaryReceipt.text = 'Not a Receipt';
				
				_obj.indicatorView = Ti.UI.createView(_obj.style.indicatorView);
				_obj.imgIndicator = Ti.UI.createImageView(_obj.style.imgIndicator); 
				*/////comment end
				
				_obj.tblSummary = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblSummary.height = 0;
				_obj.tblSummary.separatorColor = '#595959';
				_obj.tblSummary.backgroundColor = '#303030';
				
				_obj.tblSummaryuk = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblSummaryuk.height = 0;
				_obj.tblSummaryuk.separatorColor = '#595959';
				_obj.tblSummaryuk.backgroundColor = '#303030';
				
				for(var i=0; i<=4; i++)
				{
					var rowSummary = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : '#303030',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowSummary.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeySummary = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValueSummary = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					switch(i)
					{
						case 0:
							if(e.result.hasOwnProperty('receiverData'))
							{
								lblKeySummary.text = 'Remittance transfer request received from';
								lblValueSummary.text = e.result.receiverData[0].firstName +' '+ e.result.receiverData[0].lastname +' '+ e.result.receiverData[0].micr;
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 1:
							if(e.result.hasOwnProperty('receiverData'))
							{
								lblKeySummary.text = 'Delivery Mode';
								lblValueSummary.text = e.result.receiverData[0].deliveryModeCodeDesc;
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 2:
							if(e.result.hasOwnProperty('purpose'))
							{
								var purposeSplit = e.result.purpose.split('~');
									
								lblKeySummary.text = 'Purpose of Remittance';
								//lblValueSummary.text = purposeSplit[1];	
								lblValueSummary.text = conf['purpose1'];
								//lblKeySummary.text = 'Purpose of Remittance';

									//lblValueSummary.text= e.result.purpose;

									//Ti.API.info("Purpose on confirmation is:" + lblValueSummary.text);
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 3:
							if(e.result.hasOwnProperty('paymodeData'))
							{
								lblKeySummary.text = 'Sending option selected';
								/*lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
								if(e.result.paymodeData[0].paymodeDesc === 'Wire FRT'){
										e.result.paymodeData[0].paymodeDesc = 'Wire Transfer';
										//Ti.API.info("PAYMODE:---",e.result.paymodeData[0].paymodeDesc);
									lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
									}else{
										lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
									}*/
									
									lblValueSummary.text = conf['paymodeLabel'];
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 4:
							if(e.result.hasOwnProperty('deliveryDate') && (origSplit[0] === 'US'))
							{
								lblKeySummary.text = 'Date Available: ' + e.result.deliveryDate;
								lblValueSummary.text = '(The money may be available sooner to the receiver)';
								lblValueSummary.font = TiFonts.FontStyle('lblNormal12');
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
					}
					
				}
				
				// Nostro Details
				
				if(e.result.hasOwnProperty('nostroDetails'))
				{
					if(e.result.nostroDetails.length>0)
					{
						//for(var i=4; i>=0; i--)
						for(var i=5; i>=0; i--)  //added on 31Aug,17 for nostro
						{
							var rowSummary = Ti.UI.createTableViewRow({
								top : 0,
								left : 0,
								right : 0,
								height : 60,
								backgroundColor : '#303030',
								className : 'bank_account_details'
							});
							
							if(TiGlobals.osname !== 'android')
							{
								rowSummary.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
							}
							
							var lblKeySummary = Ti.UI.createLabel({
								top : 10,
								left : 20,
								height : 15,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal12'),
								color : TiFonts.FontStyle('greyFont')
							});
							
							var lblValueSummary = Ti.UI.createLabel({
								top : 30,
								height : 20,
								left : 20,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal14'),
								color : TiFonts.FontStyle('whiteFont')
							});
							
							switch(i)
							{
							case 5:
								
								try{
									//for Bank Account Number
									var nostroSplit = e.result.nostroDetails[0].field1.split('|');
									 //For UAE Receiver's Correspondent value with <br> as asked by toml to do
							         if(nostroSplit[1].indexOf('<br>')==-1){   
								        lblKeySummary.text = nostroSplit[0]; 
							            lblValueSummary.text = nostroSplit[1];
								      }
                                    else
                                      {
                                       var nostroSplit1 =nostroSplit[1].split('<br>');
                                          lblKeySummary.text = nostroSplit[0]; 
							              lblValueSummary.text = nostroSplit1[0]+nostroSplit1[1];
                                      }
                              
							           
									   rowSummary.add(lblKeySummary);
									   rowSummary.add(lblValueSummary);
									if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
									_obj.tblSummaryuk.appendRow(rowSummary);
									_obj.tblSummaryuk.height = parseInt(_obj.tblSummaryuk.height + 60);
									}else{
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}
								}catch(e){}
							break;
							
								case 4:
									
									try{
										//for Receiver's Correspondent
										var nostroSplit = e.result.nostroDetails[0].field2.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
									_obj.tblSummaryuk.appendRow(rowSummary);
									_obj.tblSummaryuk.height = parseInt(_obj.tblSummaryuk.height + 60);
									}else{
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}
									}catch(e){}
								break;
								
								case 3:
									try{
										//for SWIFT Code
										var nostroSplit = e.result.nostroDetails[0].field3.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
									_obj.tblSummaryuk.appendRow(rowSummary);
									_obj.tblSummaryuk.height = parseInt(_obj.tblSummaryuk.height + 60);
									}else{
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}
									}catch(e){}
								break;
								
								case 2:
									try{
										//for Fed Routing Number
										var nostroSplit = e.result.nostroDetails[0].field4.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
									_obj.tblSummaryuk.appendRow(rowSummary);
									_obj.tblSummaryuk.height = parseInt(_obj.tblSummaryuk.height + 60);
									}else{
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}
									}catch(e){}
								break;
								
								case 1:
									try{
										//for Bank account name
										var nostroSplit = e.result.nostroDetails[0].field5.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
									_obj.tblSummaryuk.appendRow(rowSummary);
									_obj.tblSummaryuk.height = parseInt(_obj.tblSummaryuk.height + 60);
									}else{
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}
									}catch(e){}
								break;
								
								case 0:
									try{
										
										var nostroSplit = e.result.nostroDetails[0].field6.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
									_obj.tblSummaryuk.appendRow(rowSummary);
									_obj.tblSummaryuk.height = parseInt(_obj.tblSummaryuk.height + 60);
									}else{
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}
									}catch(e){}
								break;
							}	
						}
					}	
				}
				
				        _obj.infoTextView1 = Ti.UI.createView(_obj.style.infoTextView);
					_obj.infoTextView2 = Ti.UI.createView(_obj.style.infoTextView);
					_obj.infoTextView3 = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo1 = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo1 = Ti.UI.createLabel(_obj.style.lblInfo1);
						_obj.lblInfo6 = Ti.UI.createLabel(_obj.style.lblInfo1);  //CMN 76
						
						_obj.lblInfo2 = Ti.UI.createLabel(_obj.style.lblInfo2);
						if(TiGlobals.osname === 'android')
								{
									_obj.lblInfo2.font = TiFonts.FontStyle('lblNormal14');
								} //for CMN 76
				      _obj.lblInfo6.text = 'Receiver may receive less due to fees charged by receiver'+'s bank and foreign taxes. The Service Charge has been computed and deducted from the converted amount based on the final exchange rate as indicated above. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.';
					
					 _obj.lblInfo1.text = 'Receiver may receive less due to fees charged by receiver'+'s bank and foreign taxes.We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.';
					 var text = 'Remit2India services in the UK is offered by UAE Exchange UK Limited, a company authorised by the Financial Conduct Authority, and having its registered office at 14-15 Carlisle Street, London W1D 3BS. This transaction is subject to the Terms & Conditions. For questions or complaints write to us at the aforesaid address, or contact us at: 08000163404.  Email : info@remit2india.com';
					 //CMN 70 part 4 on 24th aug
						var attr = Ti.UI.createAttributedString({
						    text: text,
						    attributes: [
						        {
						            type: Ti.UI.ATTRIBUTE_FOREGROUND_COLOR,
						            value: 'red',
						            range: [text.indexOf('Terms & Conditions'), ('Terms & Conditions').length]
						        }
						    ]
						});
				var ukTerms = Ti.UI.createLabel({
											 	    top:0,
													left:20,
													right:20,
													textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
											    	font:TiFonts.FontStyle('lblNormal12'),
													color:TiFonts.FontStyle('blackFont'),
												    attributedString: attr
											   });
					 
				
					
					  ukTerms.addEventListener('click',function(e){
						require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','txn','');
					});
					
					// _obj.lblInfo3.text = 'Remit2India services in the UK is offered by UAE Exchange UK Limited, a company authorised by the Financial Conduct Authority, and having its registered office at 14-15 Carlisle Street, London W1D 3BS. This transaction is subject to the Terms & Conditions. For questions or complaints write to us at the aforesaid address, or contact us at: 08000163404.  Email : info@remit2india.com';
					
					 //_obj.lblInfo5.text ='For questions or complaints write to us at the aforesaid address, or contact us at: 08000163404.  Email : info@remit2india.com';
					
                    // _obj.lblInfo2.top=10;
					 _obj.lblInfo2.text= '**the credit date may get impacted if the transaction completion requires fulfillment of compliance requirements if any.';
  			      if(origSplit[0] === 'US')
				{
					_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
					_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
					_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
					
					if(conf['paymodeshort'] === "WIRE" || conf['paymodeshort'] === "WIREFRT" ||conf['paymode'] === "WIRE" || conf['paymode'] === "WIREFRT")
						{
							//_obj.lblInfo.text = "You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TOML may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.";	
						     //changing on 04-July-17
						     _obj.lblInfo.text = 'You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TOMPL may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.'; 
						    //changing on 21-Aug-17
						   if (TiGlobals.usLicsState.indexOf(_obj.state ) != -1 ){
							 _obj.lblInfo.text = 'You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, Moneydart Global Services Inc DBA Xpress Money may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.'; 
                            }
                         else{
                             _obj.lblInfo.text = 'You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TTSPL may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.'; 
                            }
						}
					/*else if(conf['paymodeshort'] === "ACH")
					{
						_obj.lblInfo.text = "Recipient may receive less due to fees charged by recipient's bank and foreign taxes. Service Tax is applicable as per the Service Tax (Amendment) rules, 2012 of the Government of India & is recoverable from any foreign currency transfer into India. The Service Tax shall be computed and deducted from the converted amount based on the final exchange rate as indicated in the final receipt. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.";
					}*/
					
					
					// Error Rights
		
					_obj.errorRightsView = Ti.UI.createView(_obj.style.collapsibleView);
					_obj.errorRightsView.top = 20;
					_obj.lblErrorRights = Ti.UI.createLabel(_obj.style.lblCollapsible);
					_obj.lblErrorRights.text = 'Your Error Resolution Rights:';
					_obj.imgErrorRights = Ti.UI.createImageView(_obj.style.imgCollapsible);
					
					_obj.errorRightsTextView = Ti.UI.createView(_obj.style.collapsibleTextView);
					_obj.errorRightsTextView.height = 0;
					 
					_obj.errorRightsTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					_obj.errorRightsTxt.height = Ti.UI.SIZE;
					_obj.errorRightsTxt.text =  'You have a right to dispute errors in your transaction. If you think there is an error, contact us within 180 days at 1-888-736-4886 or www.remit2india.com. You can also contact us for a written explanation of your rights.\n\n Note: You may feel free to use the above number to report frauds.';
					
					_obj.errorRightsView.addEventListener('click',function(e){
						if(_obj.imgErrorRights.image === '/images/transfer_hide.jpg')
						{
							_obj.imgErrorRights.image ='/images/transfer_show.jpg';
							_obj.errorRightsTextView.height = Ti.UI.SIZE;
						}
						else
						{
							_obj.imgErrorRights.image ='/images/transfer_hide.jpg';
							_obj.errorRightsTextView.height = 0;
						}
					});
					
					// Cancellation Rights
						
					_obj.cancelRightsView = Ti.UI.createView(_obj.style.collapsibleView);
					_obj.cancelRightsView.top = 2;
					_obj.lblCancelRights = Ti.UI.createLabel(_obj.style.lblCollapsible);
					_obj.lblCancelRights.text = 'Your Cancellation Rights:';
					_obj.imgCancelRights = Ti.UI.createImageView(_obj.style.imgCollapsible);
					
					_obj.cancelRightsTextView = Ti.UI.createView(_obj.style.collapsibleTextView);
					_obj.cancelRightsTextView.height = 0;
					 
					_obj.cancelRightsTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					_obj.cancelRightsTxt.height = Ti.UI.SIZE;
					
					if(_obj.USstateFlagConf == true ){  //CMN70
					  _obj.cancelRightsTxt.text='You can cancel for a full refund within 30 minutes of payment, unless the funds have been picked up or deposited.';
					  }
                      else{
                     _obj.cancelRightsTxt.text =  'You can cancel for a full refund within 3 business days of the date of booking of the remittance transfer request (including the day of booking of the remittance transfer request).';
					}
                                        _obj.cancelRightsView.addEventListener('click',function(e){
						if(_obj.imgCancelRights.image === '/images/transfer_hide.jpg')
						{
							_obj.imgCancelRights.image ='/images/transfer_show.jpg';
							_obj.cancelRightsTextView.height = Ti.UI.SIZE;
						}
						else
						{
							_obj.imgCancelRights.image ='/images/transfer_hide.jpg';
							_obj.cancelRightsTextView.height = 0;
						}
					});
					
					// Complaint
						
					_obj.complaintView = Ti.UI.createView(_obj.style.collapsibleView);
					_obj.complaintView.top = 2;
					_obj.lblComplaint = Ti.UI.createLabel(_obj.style.lblCollapsible);
					_obj.lblComplaint.text = 'Complaint Resolution:';
					_obj.imgComplaint = Ti.UI.createImageView(_obj.style.imgCollapsible);
					//_obj.complaintTextView
					_obj.complaintTextView = Ti.UI.createView(_obj.style.collapsibleTextView);
					_obj.complaintTextView.height = 0;
					 
					_obj.complaintTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					_obj.complaintTxt.height = Ti.UI.SIZE;
					
					//Added for CMN 70 on 10th AUG
					_obj.txtConsumer = Ti.UI.createLabel(_obj.style.txtBldComplaint);
					_obj.txtConsumer.text = 'CONSUMER FINANCIAL PROTECTION BUREAU (CFPB):\n'+
										        '855-411-2372\n'+
												'855-729-2372 (TTY/TDD)\n'+
												'www.consumerfinance.gov';
					
					//Added by Sanjivani for CMN 70 on 21-July-2017
					_obj.txtComplOnline = Ti.UI.createLabel(_obj.style.txtBldComplaint);
					_obj.txtComplOnline.text = "Online:";
					
		            _obj.txtComplHotline = Ti.UI.createLabel(_obj.style.txtBldComplaint);
		            _obj.txtComplHotline.text = "Hotline:";
		            
		            _obj.txtComplTele = Ti.UI.createLabel(_obj.style.txtBldComplaint);
		            _obj.txtComplTele.text = "Telephone Number:";
		            
		            _obj.txtComplInPerson = Ti.UI.createLabel(_obj.style.txtBldComplaint);
		            _obj.txtComplInPerson.text = "In Person or U.S. Mail:" ;
		           
		            
		            _obj.complaintTxtUnderline = Ti.UI.createLabel(_obj.style.collapsibleTxt);
		            
		          
					/*function underline(_label, _word) {
                        if (Ti.Platform.name === 'iPhone OS') {
                        var text = _label.getText();
                        var attr = Titanium.UI.iOS.createAttributedString({
                        text : text,
                        attributes : [{
                        type : Titanium.UI.iOS.ATTRIBUTE_UNDERLINES_STYLE,
                        value : Titanium.UI.iOS.ATTRIBUTE_UNDERLINE_STYLE_SINGLE,
                        range : [text.indexOf(_word), _word.length]
                         }]
                           });
                           _obj.complaintTxtUnderline.attributedString = attr;
                      //_label.setAttributedString(attr); // Why doesn't this work?
                        }

                 if (Ti.Platform.osname === 'android') {
                      var text = _label.text.split(_word);
                      _label.setHtml(text[0] + '<u>' + _word + '</u>' + text[1]);
                      _label.setText(undefined);
                    }
                   }*/

                _obj.complaintTxtUnderline.text = 'If you are unable to resolve the issue with Moneydart Global Services Inc. d/b/a Xpress Money, you should file a complaint with the Consumer Financial Protection Bureau (CFPB).';
                 //_obj.complaintTxtUnderline.attributedString = attr;
                 //underline(_obj.complaintTxtUnderline, "Consumer Financial Protection Bureau (CFPB)");
                
					
					_obj.complaintView.addEventListener('click',function(e){
						if(_obj.imgComplaint.image === '/images/transfer_hide.jpg')
						{
							_obj.imgComplaint.image ='/images/transfer_show.jpg';
							_obj.complaintTextView.height = Ti.UI.SIZE;
						}
						else
						{
							_obj.imgComplaint.image ='/images/transfer_hide.jpg';
							_obj.complaintTextView.height = 0;
						}
					});
				}
				
				if(origSplit[0] === 'UK')
				{
					_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
					_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
					_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
					
					//_obj.lblInfo.text = "Our Nostro account details have changed from Axis Bank to Karnataka Bank. Kindly note the same before initiating the fund transfers.";
				   //_obj.lblInfo.text = "Our Nostro account details have changed from Karnataka Bank to Axis Bank. Kindly note the same before initiating the fund transfers."; //added on 02-may-17 by sanjivani
                   _obj.lblInfo.text ='Our Nostro account details have changed from Axis Bank to Barclays Bank. Kindly note the same before initiating the fund transfers';//added on 02-Aug-17 by sanjivani //CMN70
				}
				
				if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
				{
					_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
					_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
					_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
					
					_obj.lblInfo.text = "Our Correspondent Bank details have changed. Kindly note the same before initiating the fund transfers.";
				}
				
				_obj.btnServiceTax = Ti.UI.createButton(_obj.style.btnSubmit);
				_obj.btnServiceTax.backgroundColor = '#000';
				_obj.btnServiceTax.title = 'Service Tax';
				_obj.btnServiceTax.bottom = 20;
				
				_obj.btnServiceTax.addEventListener('click',function(e){
					require('/js/transfer/ServiceTaxModal').ServiceTaxModal();
				});
				
				_obj.mainView.add(_obj.lblAddressHeader);
				_obj.mainView.add(_obj.lblAddress);
		         
		         if(origSplit[0] =='CAN'){  //Done for CMN 84 ON 15th nov
		         _obj.mainView.add(_obj.lblAddressHeaderconf);
		         _obj.mainView.add(_obj.txtComplMD);
				_obj.mainView.add(_obj.lblAddressconf);	
				_obj.mainView.add(_obj.lblDateHeaderconf);
				_obj.mainView.add(_obj.lblDateconfuk);
				_obj.mainView.add(lblKeyPDrtrn);
			   _obj.mainView.add(lblValuePDrtrn);//CMN 84-a
		        }
				
				 if (origSplit[0] =='US'){ 
				_obj.mainView.add(_obj.lblAddressHeaderconf);
				_obj.mainView.add(_obj.lblAddressconf);
				_obj.mainView.add(_obj.lblDateHeaderconf);
				_obj.mainView.add(_obj.lblDateconf);
			   }
			   
			   if( origSplit[0] == 'UK'){ //Done on 28th aug cmn70-4
			   	_obj.mainView.add(_obj.lblDateHeaderconfuk);
				_obj.mainView.add(_obj.lblDateconfuk);
				_obj.mainView.add(lblKeyPDrtrn);
			   _obj.mainView.add(lblValuePDrtrn); //Done on 29th aug cmn70-4
			   	
			   }

				_obj.paymentView.add(_obj.lblPaymentHeader);
				if( origSplit[0] == 'UK'|| origSplit[0] === 'CAN'){ //CMN 84-a
				_obj.mainView.add(_obj.paymentView);  //Done on 28th aug cmn70-4
				_obj.mainView.add(_obj.tblPaymentDetailsuk); //Done on 29th aug cmn70-4
				}else{
				_obj.mainView.add(_obj.tblPaymentDetails);
				}
				
				_obj.convertedView.add(_obj.lblAmountConvertedHeader);
				if( origSplit[0] != 'UK'){  //Done on 29th aug cmn70-4
					Ti.API.info("-------------------OY UK=");
				_obj.convertedView.add(_obj.lblAmountConverted);      //CMN 84-a
				}
				
				if(origSplit[0] != 'CAN'){
					Ti.API.info("-------------------OY UK=");
					_obj.convertedView.add(_obj.lblAmountConverted);      //CMN 84-a
				}
				_obj.mainView.add(_obj.convertedView);
				
				if( origSplit[0] === 'UK' || origSplit[0] === 'CAN'){  //Done on 29th aug cmn70-4
				_obj.mainView.add(_obj.tblConverteduk);                 //CMN 84-a
				_obj.receiverView.add(_obj.lblReceiverHeader);
				_obj.mainView.add(_obj.receiverView);
				_obj.mainView.add(_obj.tblReceiveruk);
				
				if(origSplit[0] === 'CAN' && conf['paymode'] === 'CIP'){
				 _obj.borderViewBank = Ti.UI.createView(_obj.style.borderView);
			    _obj.mainView.add(_obj.borderViewBank);
			    }
			    
				if(origSplit[0] === 'UK'||( origSplit[0] === 'CAN' && conf['paymode'] != 'CIP')){
					_obj.nostroView.add(_obj.lblNostroHeader);
					_obj.mainView.add(_obj.nostroView);
				} 
				
				
				}else{
				_obj.mainView.add(_obj.tblConverted);	
				}
				
				if(_obj.isFRT === 'Y' && ((origSplit[0] === 'US' && ((conf['paymode'] === 'ACHGFXFRT') || (conf['paymode'] === 'WIREFRT')) )|| (origSplit[0] === 'UK' && (conf['paymode'] === 'DEBFRT'))))
				{
					_obj.SwachhBharatView.add(_obj.lblSwachhBharatHeader);
					_obj.SwachhBharatView.add(_obj.lblSwachhBharatAmount);
					_obj.mainView.add(_obj.SwachhBharatView);
				}
				
				/*//comment start
				if(_obj.isFRT === 'Y' && (origSplit[0] === 'US' || origSplit[0] === 'UK'))
				{
					_obj.recipientAmountView.add(_obj.lblRecipientAmountHeader);
					_obj.recipientAmountView.add(_obj.lblRecipientAmount);
					_obj.mainView.add(_obj.recipientAmountView);
				}//comment end*/
				
				//if(_obj.isFRT === 'N'|| (origSplit[0] === 'AUS' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'POLI'))))
					if((origSplit[0] === 'AUS' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'POLI')))/*//comment start ||(origSplit[0] === 'UK' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'DEB') //comment start||(conf['paymode'] === 'CIP')//comment end))//comment end*/)
					{                                                                                              //comment on 29th aug for CMN70-4
						_obj.recipientAmountToView.add(_obj.lblRecipientAmountToHeader);
						_obj.recipientAmountToView.add(_obj.lblRecipientAmountTo);
						_obj.mainView.add(_obj.recipientAmountToView);
					}//else //if(_obj.isFRT === 'Y')//comment on 29th aug for CMN70-4
					else if(origSplit[0] != 'UK' || origSplit[0] != 'CAN')
					{/*                                                              //CMN 84
						_obj.recipientAmountView.add(_obj.lblRecipientAmountHeader);
						_obj.recipientAmountView.add(_obj.lblRecipientAmount);
						_obj.mainView.add(_obj.recipientAmountView);*/
					}
				/*	//comment start
				_obj.summaryView.add(_obj.lblSummary);
				_obj.summaryView.add(_obj.lblSummaryReceipt);
				_obj.mainView.add(_obj.summaryView);
				_obj.indicatorView.add(_obj.imgIndicator);
				_obj.mainView.add(_obj.indicatorView);//comment end*/
				if(origSplit[0] === 'UK' || origSplit[0] === 'CAN'){ //CMN 84-a
					_obj.mainView.add(_obj.tblSummaryuk);
				}else{
				_obj.mainView.add(_obj.tblSummary);
				}
				
				if(conf['paymode'] === 'CIP')
				{
					/*if(origSplit[0] === 'CAN'){   //CMN84--commented for CMN 84-a
			    
	              
	              _obj.bankView = Ti.UI.createView(_obj.style.countryView);
	              _obj.lblbank = Ti.UI.createLabel(_obj.style.lblCountry);
	              _obj.imgBank = Ti.UI.createImageView(_obj.style.imgCountry);
	              _obj.borderViewBank = Ti.UI.createView(_obj.style.borderView);
			
	                _obj.lblbank.text  = 'Select Bank name';		
	                _obj.bankView .add(_obj.lblbank);
	                _obj.bankView .add(_obj.imgBank);
					_obj.mainView.add(_obj.bankView);
					_obj.mainView.add(_obj.borderViewBank);
			
                   _obj.bankView.addEventListener('click',function(){
                   //	else if(evt.source.webServiceToCall === 'GETCIPTRACEBANKLIST')
						//{
							activityIndicator.showIndicator();
		                    var bankOptions = [];
		                    var xhr = require('/utils/XHR');

							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"GETCIPTRACEBANKLIST",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"payModeCode":"'+conf['paymode']+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'"'+
									
									
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
							
						function xhrSuccess(response) {
							require('/utils/Console').info('Result ======== ' + response.result);
							try{
				               activityIndicator.hideIndicator();
				    if(response.result.responseFlag === "S")
				     {
					for(var i=0; i<response.result.bankDtls.length; i++)
					{
						bankOptions.push(response.result.bankDtls[i].bankName);
					}
					
					var optionDialogCity = require('/utils/OptionDialog').showOptionDialog('Bank Name',bankOptions,-1);
					optionDialogCity.show();
					
					optionDialogCity.addEventListener('click',function(evt){
						if(optionDialogCity.options[evt.index] !== L('btn_cancel'))
						{
							_obj.lblbank.text = optionDialogCity.options[evt.index];
							optionDialogCity = null;
						}
						else
						{
							_obj.lblbank.text = null;
							_obj.lblbank.text  = 'Select Bank name';	
						}
					});	
				}
				else
			    {
			    	try{
				if(response.result.message === L('invalid_session') || response.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',response.result.message,[L('btn_ok')]).show();
				}
				}catch(e){}
			}
			xhr = null;
			}catch(e){}
			
			}
				
						function xhrError(response) {
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr = null;
						}
					
					});
					
					} //end of Canada CIP */
					_obj.txtTraceNo = Ti.UI.createTextField(_obj.style.txtTraceNo);
                                  _obj.txtTraceNo.hintText = 'Internet Banking Trace Number';
					_obj.txtTraceNo.maxLength = 25;
					_obj.traceBorderView = Ti.UI.createView(_obj.style.borderView);
					
					_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnSubmit.title = 'SUBMIT';
					
					_obj.mainView.add(_obj.txtTraceNo);
					_obj.mainView.add(_obj.traceBorderView);
					_obj.mainView.add(_obj.btnSubmit);
					
					_obj.btnSubmit.addEventListener('click',function(e){
						if(_obj.txtTraceNo.value === '')
						{
							require('/utils/AlertDialog').showAlert('','Please enter Internet Banking Trace Number',[L('btn_ok')]).show();
				    		_obj.txtTraceNo.value = '';
				    		_obj.txtTraceNo.focus();
				    		return;
						}
						else if(_obj.txtTraceNo.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('','Internet Banking Trace Number must contain a numeric value',[L('btn_ok')]).show();
				    		_obj.txtTraceNo.value = '';
				    		_obj.txtTraceNo.focus();
				    		return;
						}
						else
						{
							
						}
						
						activityIndicator.showIndicator();
						var xhr = require('/utils/XHR');
						xhr.call({
							url : TiGlobals.appURLTOML,
							get : '',
							post : '{' +
								'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
								'"requestName":"UPDATECIPTRACENUMBER",'+
								'"partnerId":"'+TiGlobals.partnerId+'",'+
								'"channelId":"'+TiGlobals.channelId+'",'+
								'"ipAddress":"'+TiGlobals.ipAddress+'",'+
								'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
								'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
								'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
								'"originatingCurrency":"'+origSplit[1]+'",'+
								'"rtrn":"'+conf["txnRefId"]+'",'+
								'"bankTxnNo":"'+_obj.txtTraceNo.value+'"'+
								'}',
							success : xhrSuccess,
							error : xhrError,
							contentType : 'application/json',
							timeout : TiGlobals.timer
						});
				
						function xhrSuccess(e) {
							require('/utils/Console').info('Result ======== ' + e.result);
							
							if(e.result.responseFlag === "S")
							{
								activityIndicator.hideIndicator();
								
								if(TiGlobals.osname === 'android')
								{
									require('/utils/AlertDialog').toast(e.result.message);
								}
								else
								{
									require('/utils/AlertDialog').iOSToast(e.result.message);
								}
							}
							else
							{
								if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
								{
									require('/lib/session').session();
									destroy_confirmation();
								}
								else
								{
									require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
								}
							}
							xhr = null;
						}
				
						function xhrError(e) {
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr = null;
						}
					});
				}
				
				_obj.btnBAT = Ti.UI.createButton(_obj.style.btnSubmit);
				_obj.btnBAT.title = 'BOOK ANOTHER TRANSACTION';
				
				//CMN66-30June17
					_obj.infoTextView1.add(_obj.imgInfo1);
					
					if(origSplit[0] == 'UK' || origSplit[0] == 'US' ){  //CMN 76
						
			                      _obj.infoTextView1.add(_obj.lblInfo1);
							
					}else if(origSplit[0] != 'CAN') { //CMN 84-a
							_obj.infoTextView1.add(_obj.lblInfo6);
						}
					
					if(origSplit[0] === 'UK'){  //CMN 70 part 4 on 24th aug
					_obj.infoTextView3.add(ukTerms);
					_obj.lblInfo2.top = 20;
					}
					if(origSplit[0] != 'CAN') {  //CMN84-a
					_obj.infoTextView2.add(_obj.lblInfo2);
					_obj.mainView.add(_obj.infoTextView1);
					_obj.mainView.add(_obj.infoTextView3);
					_obj.mainView.add(_obj.infoTextView2);
					}
				if(origSplit[0] === 'US')
				{
					//_obj.infoTextView.add(_obj.imgInfo);
					_obj.infoTextView.add(_obj.lblInfo);
					_obj.mainView.add(_obj.infoTextView);
				}
				
				if(origSplit[0] === 'UK')
				{
					//_obj.infoTextView.add(_obj.imgInfo);
					_obj.infoTextView.add(_obj.lblInfo);
					_obj.mainView.add(_obj.infoTextView);
				}
				
				if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
				{
					//_obj.infoTextView.add(_obj.imgInfo);
					_obj.infoTextView.add(_obj.lblInfo);
					_obj.mainView.add(_obj.infoTextView);
				}
				
				/*if(origSplit[0] === 'US')  //Commenting on 03July17 for CMN66
				{
					if(_obj.isFRT === 'Y')
					{
						_obj.mainView.add(_obj.btnServiceTax);
					}
				}*/
				//CMN 76

					if(origSplit[0] != 'UK' && origSplit[0] != 'US'){ //CMN84a
						 if( origSplit[0] == 'CAN' && (conf['paymode'] == 'CIP' || conf['paymode'] == 'WIRE' )) {
					    }else{ _obj.mainView.add(_obj.btnServiceTax);}
						}
				
				if(origSplit[0] === 'US')
				{
					_obj.errorRightsView.add(_obj.lblErrorRights);
					_obj.errorRightsView.add(_obj.imgErrorRights);
					_obj.mainView.add(_obj.errorRightsView);
					_obj.errorRightsTextView.add(_obj.errorRightsTxt);
					_obj.mainView.add(_obj.errorRightsTextView);
					
					_obj.cancelRightsView.add(_obj.lblCancelRights);
					_obj.cancelRightsView.add(_obj.imgCancelRights);
					_obj.mainView.add(_obj.cancelRightsView);
					if (_obj.state !='Arkansas'){   //added by sanjivani on 08Sept17 for CMN70b
					_obj.cancelRightsTextView.add(_obj.cancelRightsTxt);
					}
					_obj.mainView.add(_obj.cancelRightsTextView);
					
					_obj.complaintView.add(_obj.lblComplaint);
					_obj.complaintView.add(_obj.imgComplaint);
					
				  //Updated on 31Aug17 for CMN70A to remove compiant tab from non-license states
				  /* if(_obj.USstateFlagConf == false && _obj.state !== 'California'){
					_obj.mainView.add(_obj.complaintView);
					_obj.mainView.add(_obj.complaintTextView);
					}*/
					
					var textOnline =Ti.UI.createLabel(_obj.style.collapsibleTxt);
				    var textHotline =Ti.UI.createLabel(_obj.style.collapsibleTxt);
					var txtComplTele = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					var txtComplInPerson = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					
					textOnline.top = 0;
					textHotline.top = 0;
					txtComplTele.top = 0;
					txtComplInPerson.top = 0;
					textOnline.height = Ti.UI.SIZE;
					textHotline.height = Ti.UI.SIZE;
					txtComplTele.height = Ti.UI.SIZE;
					txtComplInPerson.height = Ti.UI.SIZE;
					
					//var state = Ti.App.Properties.getString('state');
					Ti.API.info(_obj.state);
					
					// _obj.complaintTextView.add(_obj.complaintTxt); 
					// _obj.complaintTxt.text = 'For questions or complaints about us, please contact:';
					if(_obj.USstateFlagConf == true && _obj.state != 'California'){  //CMN70 from distribution to stage
					
					//added by sanjivani on 08Sept17 for CMN70b
					if(_obj.state == 'Arkansas'){
					_obj.complaintTxt.text='You can cancel your Transaction for a full refund within 30 minutes of authorizing your Transaction, unless the funds have already been paid out to the Recipient or Service Company. After 30 minutes, we generally do not provide refunds unless we did not process your Transaction according to your instructions or we are unable to pay out the Transaction. To request a refund, please contact our Customer Service.\n\n'+
					      'We will make every effort not to debit your Payment Instrument after we have received your request for cancellation. However, in some cases, we may have initiated an irreversible request for funds from your financial institution prior to receiving your request for cancellation. In such cases, your Payment Instrument may be debited even if you have cancelled your Transaction but we will refund your money usually within four (4) business days after we have received the funds from your financial institution.\n\n'+
					      'Refunds will be credited to the same Payment Instrument used to pay for the Transaction. Refunds are only made in U.S. Dollars. Refund amounts will not be adjusted to account for changes in the value of the U.S. Dollar or foreign currency from the time your Transaction was submitted.\n\n'+
					      'For questions or complaints about Moneydart Global Services Inc DBAXpress Money, contact 1-888-736-4886 or info@remit2india.com. If you still have an unresolved complaint regarding the company'+'s money transmission activity, please direct your complaint to:'; 
	                }
					else{
					_obj.complaintTxt.text ='For questions or complaints about Moneydart Global Services Inc DBAXpress Money, contact 1-888-736-4886 or info@remit2india.com. If you still have an unresolved complaint regarding the company'+'s money transmission activity, please direct your complaint to:'; 
					}
					_obj.complaintTxt.bottom=10;
					_obj.cancelRightsTextView.add(_obj.complaintTxt);//CMN70
					}
					
					if(_obj.state == 'California'){
						var nonLicText =Ti.UI.createLabel(_obj.style.collapsibleTxt);
						    nonLicText.height = Ti.UI.SIZE;
						    nonLicText.text = 'You, the customer, are entitled to a refund of the money to be transmitted as a result of this agreement if Moneydart Global Services Inc. DBAXpress Money ("Xpress Money”) does not forward the money received from you within 10 days of the date of its receipt, or does not give instructions committing an equivalent amount of money to the person designated by you within 10 days of the receipt of the funds from you unless otherwise instructed by you. If your instructions as to when the moneys shall be forwarded or transmitted are not complied with and the money has not yet been forwarded or transmitted, you have a right to a refund of your money. If you want a refund, you must mail or deliver your written request to Moneydart Global Services Inc., DBA Xpress Money at 1000 Woodbridge Center Drive,Woodbridge, NJ-07095. If you don’t receive your refund, you maybe entitled to your money back plus penalty of up to $1,000 and attorney’s fees pursuant to section 2012 of the CaliforniaCode.';
						     
						var nonLicTextBld = Ti.UI.createLabel(_obj.style.txtBldComplaint);
                            nonLicTextBld.text = 'RIGHT TO REFUND';
                        var complaintTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					        complaintTxt.height = Ti.UI.SIZE;
                            complaintTxt.text ='For questions or complaints about Moneydart Global Services Inc DBAXpress Money, contact 1-888-736-4886 or info@remit2india.com. If you still have an unresolved complaint regarding the company'+'s money transmission activity, please direct your complaint to:'; 
					        complaintTxt.bottom=10;
					        
					   var txt =  'Department of Business Oversight'+'\n'+
                                  'ConsumerServices, 1515 K Street, Suite 200,'+'\n'+
                                  'Sacramento,CA 95814'+'\n'+
                                  '1-BOD-622-0620'+'\n'+
                                  'Consumer.complaint@dfi.ca.gov';  //CMN 70 for California(distribution to stage)
                                  
                         txtComplInPerson.text = txt;          
                        _obj.cancelRightsTextView.add(nonLicTextBld);
                        _obj.cancelRightsTextView.add(nonLicText);
                        _obj.cancelRightsTextView.add(complaintTxt);
                        _obj.cancelRightsTextView.add(txtComplInPerson);
                        
                          
					}
					switch(_obj.state)
					{
						case 'Alabama':
							 textOnline.text = 'www.ago.state.al.us/Page-Consumer- Protection';
							 textHotline.text =  '1-800-392-5658';
							 txtComplTele.text =  '334-242-7335';
							 var txt =  'Office of Alabama Attorney General Luther Strange'+'\n'+
                                                      'Consumer Protection Section Post Office'+'\n'+
                                                      'Box 300152\n'+
                                                      'Montgomery, Alabama 36130\n';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline); 
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
					
						case 'Alaska':
					          var txt =     'Alaska Department of Commerce, Community, and Economic Development\n\n'+
                                                      'For complaints and other concerns about money services provided by Moneydart Global Services Inc. d/b/a Xpress Money:\n\n'+
			
			                                          'Alaska Department of Commerce, Community and Economic\n'+
			                                          'Development Division of Banking and Securities\n'+
			                                          'Money Service Businesses\n'+
			                                          '550 W. 7th Avenue, Suite 1850\n'+
			                                          'Anchorage, AK 99501\n'+
			                                          'Tel: (907) 269-4584\n'+
			                                          'Fax:(907) 269-1066\n'+
			                                          'Email: moneytransmitters@alaska.gov\n';
			                    txtComplInPerson.text = txt;                       
							   _obj.cancelRightsTextView.add(txtComplInPerson);   
                            
					  break;
					  
					  case 'Arizona':
					        var txt ='Moneydart Global Services Inc. d/b/a Xpress Money is a licensed money service business with the State of Arizona. If Moneydart is non-responsive and if any consumer has any complaints or concerns about an Arizona Moneydart transaction or authorized delegate, they may contact the Arizona Department of Financial Institutions, 2910 North 44th Street, Suite 310, Phoenix, AZ 85018; Telephone: (800) 544-0708 Fax:(602) 381-1225. An Arizona complaint form may be found\n'+
                                      'at:azdfi.gov/Consumers/Complaints/Forms/ConsumerComplaintForm.pdf.\n'; 
                                 txtComplInPerson.text = txt;                       
							    _obj.cancelRightsTextView.add(txtComplInPerson);    
					  break;
					
                     case 'Colorado':
                           var txt = 'COLORADO DIVISION OF BANKING, COLORADO MONEY TRANSMITTERS ACT CUSTOMER NOTICE\n'+
			                         'Entities other than FDIC insured financial institutions that conduct money transmission activities in Colorado, including the sale of money orders,transfer of funds, and other instruments for the payment of money or credit, are required to be licensed by the Colorado Division of Banking pursuant to the Money Transmitters Act, Title 12, Article 52, Colorado Revised Statutes.\n\n'+
			
			                         'If you have a Question about or Problem with YOUR TRANSACTION -THE MONEY YOU SENT, You must contact the Money Transmitter who processed your transaction for assistance. The Division of Banking does not have access to this information.\n\n'+
			
			                         'If you are a Colorado Resident and have a Complaint about Moneydart Global Services Inc. d/b/a Xpress Money, ALL complaints must be submitted in writing. Please fill out the Complaint Form provided on the Colorado Division of Banking’s website and return it and any documentation supporting the complaint via mail or email to the Division of Banking at: Colorado Division of Banking, 1560 Broadway, Suite 975,Denver, CO 80202, Main Office Telephone: (303) 894-2320 Fax: (303) 861-2126 email: DORA_BankingWebsite@state.co.us, website:ww.dora.colorado.gov/dob.\n\n'+
			
			                         'Section 12-52-116, C.R.S. requires that money transmitters and money order companies post this notice in a conspicuous, well-lighted location visible to customers.\n';
                                 txtComplInPerson.text = txt;                       
							    _obj.cancelRightsTextView.add(txtComplInPerson);
                      break;
                      
                      case 'Connecticut':
                           textOnline.text = 'www.state.ct.us/dob';
                          // textOnline.top = 10;
                           _obj.txtComplTele.text = 'Phone Number:';
						   txtComplTele.text = '860-713-6100';
                           var txt = 'Connecticut Department of Banking\n'+
			                         '260 Constitution Plaza\n'+
			                         'Hartford, CT 06103-1800'; 
			               txtComplInPerson.text = txt;
			                 _obj.cancelRightsTextView.add(txtComplInPerson);
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele); 
							 _obj.cancelRightsTextView.add(txtComplTele);                    
					  break;
					  
					  case 'Delaware':
					        textOnline.text = 'banking.delaware.gov/';
                            txtComplTele.text = '302-739-4235';
					        var txt ='Delaware Office of State Bank Commissioner';
					        txtComplInPerson.text = txt;
			                 _obj.cancelRightsTextView.add(txtComplInPerson);
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele); 
							 _obj.cancelRightsTextView.add(txtComplTele);   
					  break;
					  
					  case 'Florida':
					        var txt = 'Moneydart Global Services Inc. d/b/a Xpress Money is a licensed money transmitter under Chapter 560, Florida Statutes. For suspected violations of Chapter 560, Florida Statutes, call Moneydart Global Services, Inc. d.b.a Xpress Money at 866-372-3874 or contact the Florida Office of Financial Regulation, 200 East Gaines Street, Tallahassee, FL 32399-0376, (800) 848-3792.';
					        txtComplInPerson.text = txt;
			               _obj.cancelRightsTextView.add(txtComplInPerson);	
					
					  break;
					  case 'Georgia':
					       _obj.txtComplCFPB = Ti.UI.createLabel(_obj.style.txtBldComplaint);
		                   _obj.txtComplCFPB.height = Ti.UI.SIZE;
		                   _obj.txtComplCFPB.top = 2;
		                   textOnline.top = 2;
		                   
		                   _obj.txtComplCFPB.text = "Consumer Financial Protection Bureau (CFPB)";
		                  var txt ='If you are unable to resolve the issue with Moneydart Global Services Inc. d/b/a Xpress Money, you should file a complaint with the Consumer Financial Protection Bureau (CFPB).';
                          var txt1 = 'The CFPB is a federal agency established by Congress to protect consumers by carrying out Federal consumer financial laws. Go to the following page on the CFPB'+'\''+'s website to learn more: http://www.consumerfinance.gov/blog/now-accepting-money-transfer-complaints/';
                            txtComplInPerson.text = txt;
                            textOnline.text = txt1;
                           _obj.cancelRightsTextView.add(txtComplInPerson);
                           _obj.cancelRightsTextView.add(_obj.txtComplCFPB);
                           _obj.cancelRightsTextView.add(textOnline); 
                       
					  break;
					  
					  case 'Idaho':
					       textOnline.text = 'finance.idaho.gov/';
                           txtComplTele.text = '208-332-8000';
					       var txt = 'Idaho Department of Finance';
					        txtComplInPerson.text = txt;
			                _obj.cancelRightsTextView.add(txtComplInPerson);
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         
							_obj.cancelRightsTextView.add(_obj.txtComplTele); 
							_obj.cancelRightsTextView.add(txtComplTele);   
					  break;
					  
					  case 'Illinois':
					        var txt = 'Moneydart Global Services Inc. d/b/a Xpress Money is licensed under the Illinois Money Transmitter Act. Please contact the Illinois Department of Financial Institutions, toll free at (877) 710-5331 for suspected violation of the Illinois Transmitters of Money Act';
					        txtComplInPerson.text = txt;
			               _obj.cancelRightsTextView.add(txtComplInPerson);	
					
					  break;
					  
					  case 'Iowa':
					       textOnline.text = 'www.iowa.gov/government/ag/protecting_consumers/index.html';
                           txtComplTele.text = '515-281-5926';
                           textHotline.text =  '1-888-777-4590';
					       var txt = 'Lowa Attorney General Consumer Protection Division \n'+
			                         '1305 E. Walnut Street\n'+
			                         'Des Moines, IA 50319 ';
					         txtComplInPerson.text = txt;
					         _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline); 
                             _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline); 
							 
					  break;	
					  
					  case 'Kentucky':
					       textOnline.text = 'ag.ky.gov/civil/consumerprotection/complaints/ ';
                           textHotline.text =  '1-888-432-9257';
                           txtComplTele.text = '502 696-5389 ';
					       var txt = 'Consumer Protection Division\n'+
				                     '1024 Capital Center Drive\n'+
				                     'Frankfort, KY 40601';
					         txtComplInPerson.text = txt;
					        
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline); 
                             _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline);
                             _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
							 
					  break;
					  
					//  case 'Louisiana Office of Financial Institutions': 
	                   case 'Louisiana': 
					   var txt = 'Louisiana Office of Financial Institutions';
					         txtComplInPerson.text = txt;
					         textOnline.text =  'www.ofi.louisiana.gov/';
                             txtComplTele.text = '888-525-9414';
                             _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
							 _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);
                             _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele);
                             
					   break;	
					   
					   case 'Maine':
					    var txt = 'Maine Bureau of Consumer Credit Protection';
					         txtComplInPerson.text = txt;
					         textOnline.text =  'www.credit.maine.gov/';
                             txtComplTele.text = '207-624-8527\n'+
				               '800-332-8529 (ME Only)';
				             
				             _obj.cancelRightsTextView.add(txtComplInPerson);
							 _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);
                             _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele);
                             
					   break; 
					   
					   case 'Maryland':
					   var txt ='The Commissioner of Financial Regulation for the State of Maryland will accept all questions or complaints from Maryland residents regarding Moneydart Global Services Inc. d/b/a Xpress Money, NMLS ID 1026711 at:\n\n'+
                              'Commissioner of Financial Regulation,\n'+
				              'Attention: Consumer Services Unit,\n'+
				              '500 North Calvert Street, Suite 402,\n'+
				              'Baltimore, Maryland 21202\n'+ 
				              'Walk in hours: 9:00 am – 4:00 pm\n\n'+
				              'You may also contact Maryland via telephone at:\n'+
				              'Main telephone number: (888) 784-0136\n'+
				              'Consumer Service Unit Phone number: (410) 230-6077\n\n'+
				              'You can also fax to:\n'+
				              'Attention: Consumer Services Unit\n'+
				              'Fax Number: (410) 333-3866\n\n'+
				              'You can also email/scan your document to: DLFRComplaints-DLLR@maryland.gov\n';
	 
	                           txtComplInPerson.text = txt;
	                           _obj.cancelRightsTextView.add(txtComplInPerson); 
					   break;
					   
					   case 'Michigan':
					         var txt = 'Michigan Department of Insurance & Financial Services';
					         txtComplInPerson.text = txt;
					         textOnline.text =  'http://www.michigan.gov/dleg';
                             txtComplTele.text = '877-999-6442';
				                
                             _obj.cancelRightsTextView.add(txtComplInPerson);
							 _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);
                             _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele);
                             
					   break; 
					   
					   case 'Missouri':
					         var txt = 'Missouri Division of Finance';
					         txtComplInPerson.text = txt;
					         textOnline.text =  'www.finance.mo.gov/';
                             txtComplTele.text = '573-751-3242';
				                
                             _obj.cancelRightsTextView.add(txtComplInPerson);
							 _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);
                             _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele);
                         
					   break;
					   
					   case 'Minnesota':
							 textOnline.text = 'www.ag.state.mn.us/consumer/complaint.asp';
							 textHotline.text =  '1-800-657-3787';
							 txtComplTele.text =  '651 296-3353';
							 var txt =  'Office of Minnesota Attorney General\n'+ 
			                            '1400 Bremer Tower\n'+
			                            '445 Minnesota Street\n'+
			                            'St. Paul, MN 55101';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline); 
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
					   
					 case 'Montana':
					        var txt = 'For consumers located in Montana, please contact Moneydart Global Services Inc. d/b/a Xpress Money directly.';
					        txtComplInPerson.text = txt;
			               _obj.cancelRightsTextView.add(txtComplInPerson);	
					
					  break;
					    
					  case 'New Hampshire':
							 textOnline.text = 'doj.nh.gov/consumer/complaints/index.htm';
							 textHotline.text =  '1-888-468-4454';
							 txtComplTele.text =  '603-271-3641 ';
							 var txt =  'OFFICE OF THE ATTORNEY GENERAL CONSUMER PROTECTION AND ANTITRUST BUREAU\n'+
			                             '33 CAPITOL STREET\n'+
			                             'CONCORD, NEW HAMPSHIRE 03301';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline); 
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
						
						case 'New Jersey':
							 textOnline.text = 'www.njconsumeraffairs.gov/ocp/';
							 txtComplTele.text =  '973-504-6200 ';
							 var txt =  'Division of Consumer Affairs Office of Consumer Protection\n'+
			                            'Regulated Business Section\n'+
			                            'P.O. Box 45028\n'+
			                            'Newark, New Jersey 07101';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
						
						case 'New Mexico':         //added on 23th aug for CMN 70
							 var txt =  'New Mexico Regulation & Licensing Department\n'+
										'Toney Anaya Building,\n'+
										'2550 Cerrillos Road,\n'+
										'Santa Fe, New Mexico 87505\n'+
										'Contact: (505) 476-4885\n'+ 
										'Fax (505) 476-4670\n\n'+ 
										 
										'Click link to download the complaint form\n'+
										'Please email the complaint form to:\n'+
										'david.mora@state.nm.us\n'+
										'link: http://www.rld.state.nm.us/financialinstitutions/File_a_Complaint.aspx';

                              txtComplInPerson.text = txt;
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
 
                       case 'North Carolina':
					        var txt = 'The North Carolina Commissioner of Banks Consumer Services Division will accept all questions or complaints from North Carolina residents regarding Moneydart Global Services, Inc. d/b/a Xpress Money at: 4309 Mail Service Center, Raleigh, N.C. 27699-4309 or call (919) 733-3016, Fax (919) 733-691';
					        txtComplInPerson.text = txt;
			               _obj.cancelRightsTextView.add(txtComplInPerson);	
					
					   break;
					   
					     case 'North Dakota':
							 textOnline.text = 'www.nd.gov/dfi/';
							 txtComplTele.text =  '701-328-9933';
							 var txt =  'North Dakota Department of Financial Institutions';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(txtComplInPerson);
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 
						break; 
						
					   case 'Oregon':
							 textOnline.text = 'www.cbs.state.or.us/dfcs/';
							 txtComplTele.text =  '866-814-9710 (OR Only)';
							 var txt =  'Oregon Division of Finance & Corporate Securities';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(txtComplInPerson);
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 
						break;
					   
					   case 'Oklahoma':
							 textOnline.text = 'www.oag.state.ok.us/oagweb.nsf/ccomp.html';
							 txtComplTele.text =  '405-521-3921';
							 var txt =  'Office of Attorney General Public Protection Unit\n'+  
			                            'Attn: Investigative Analyst\n'+ 
			                            '313 NE 21st Street\n'+ 
			                            'Oklahoma City, OK 73105';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
						
						case 'Pennsylvania':
							 textOnline.text = 'www.attorneygeneral.gov/complaints.aspx?id=451';
							 textHotline.text =  '1-800-441-2555';
							 txtComplTele.text =  '717-787-3391 ';
							 var txt =  'Office of Attorney General\n'+
			                            'Bureau of Consumer Protection\n'+ 
			                            '14th Floor, Strawberry Square\n'+
			                            'Harrisburg, PA 17120';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline); 
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
						
						 case 'Rhode Island':
							 textOnline.text = 'www.riag.ri.gov/civil/consumer/';
							 txtComplTele.text =  '401-274-4400';
							 var txt =  'Office of the Attorney General Consumer Protection Unit\n'+ 
				                        '150 South Main Street\n'+
				                        'Providence, Rhode Island 02903';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);
						break;
						
						case 'South Carolina':
					        var txt = 'For consumers in South Carolina, please contact Moneydart Global Service Inc. d/b/a Xpress Money directly.';
					        txtComplInPerson.text = txt;
			              _obj.cancelRightsTextView.add(txtComplInPerson);	
					
					    break;
					    
					    case 'Texas':
					         _obj.txtComplMD = Ti.UI.createLabel(_obj.style.txtBldComplaint);
		                     _obj.txtComplMD.text = "Moneydart Global Services Inc. d/b/a Xpress Money ";//Texas
		            
		                     _obj.txtComplMD1 = Ti.UI.createLabel(_obj.style.txtBldComplaint);
		                     _obj.txtComplMD1.top= 0 ;
		                     _obj.txtComplMD1.text = "Moneydart Global Services Inc. d/b/a Xpress Money consumer assistance at 866-372-3874,";
		                     var txt = 'is licensed under the laws of the State of Texas and by state law are subject to regulatory oversight by the Texas Department of Banking. Any consumer wishing to file a complaint against Moneydart Global Services Inc. d//b/a Xpress Money should contact the Texas Department of Banking.\n'+
			                          'If you have a complaint, first contact';
                             //var txt1 = 'If you have a complaint, first contact';
                             var txt2 = 'if you still have an unresolved complaint regarding the company'+'s money transmitter activity, please direct your complaint to: Texas Department of Banking, 2601 N. Lamar Blvd., Austin, TX 78705, phone: 1-877-276-5554 (toll-free), www.dob.texas.gov';  
                              txtComplInPerson.text = txt;
                              textOnline.text = txt1;
                              txtComplTele.text = txt2;
                            _obj.cancelRightsTextView.add(_obj.txtComplMD);
                            _obj.cancelRightsTextView.add(txtComplInPerson);
                            //_obj.cancelRightsTextView.add(textOnline);
                            _obj.cancelRightsTextView.add(_obj.txtComplMD1);
                            _obj.cancelRightsTextView.add(txtComplTele); 
                           
					   break;
					   
					   case 'Tennessee':
							 textOnline.text = 'tn.gov/consumer/index.shtml';
							 textHotline.text =  '1-800-342-8385';
							 txtComplTele.text =  '615-741-4737';
							 var txt =  'Consumer Affairs\n'+
		                                '500 James Robertson Pkwy\n'+
		                                'Nashville, TN 37243-0600';
                              txtComplInPerson.text = txt;
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         
							_obj.cancelRightsTextView.add(_obj.txtComplHotline);
							_obj.cancelRightsTextView.add(textHotline); 
							_obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
							_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);
					  break;
					  
					  case 'Utah':
							 textOnline.text = 'attorneygeneral.utah.gov/against_fraud.html';
							 textHotline.text =  '1-800-244-4636';
							 txtComplTele.text =  '801-530-6601';
							 var txt =  'Division of Consumer Protection\n'+
			                            '160 East 300 South, 2nd Floor\n'+
			                            'P.O. Box 146704\n'+
			                            'Salt Lake City, Utah 84114-6704';
                              txtComplInPerson.text = txt;
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         
							_obj.cancelRightsTextView.add(_obj.txtComplHotline);
							_obj.cancelRightsTextView.add(textHotline); 
							_obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
							_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);
					 break;
					
					 case 'Vermont':
							 textOnline.text = 'www.uvm.edu/consumer/?Page=complaint.html';
							 textHotline.text =  '1-800-649-2424';
							 txtComplTele.text =  '802-828-3171';
							 var txt = 'Consumer Assistance Program\n'+ 
			                           '146 University Place\n'+
			                           'Burlington, VT 05405';
                              txtComplInPerson.text = txt;
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         
							_obj.cancelRightsTextView.add(_obj.txtComplHotline);
							_obj.cancelRightsTextView.add(textHotline); 
							_obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
							_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);
					  break;	
					
					 case 'Virginia':
							 textOnline.text = 'www.scc.virginia.gov/bfi/index.aspx';
							
							 var txt = '800-552-794\n\n'+ 
				                       '800-552-7945 (VA only)';
                              txtComplTele.text = txt;
                              
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         

							/*_obj.cancelRightsTextView.add(_obj.txtComplHotline);
							_obj.cancelRightsTextView.add(textHotline);*/ 
							_obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
							/*_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);*/
					  break;
					  
					  case 'Washington':
					        var txt ='Entities other than FDIC insured financial institutions that conduct money transmission activities in Washington State, including the sale of payment of money or credit are required to be licensed. If there are questions or concerns regarding a transaction with Moneydart Global Services Inc. d.b.a Xpress Money please contact the Washington State Department of Financial Institutions at (360) 902-8703 or by mail at: Department of Consumer Services, 150 Israel Road Southwest, Olympia, WA 98504-1200. To file a complaint, please provide: the name of the company, office location, and who you contacted at the company; your name, mailing address, and ten digit phone numbers; and a simple explanation of the problem, with a list of events in chronological order, making sure to include names and dates. Be specific and as brief as possible. For electronic submission, please complete the form found on the website: www.dfi.wa.gov under Consumer Information: File a Complaint.\n';
                           txtComplInPerson.text = txt;
			              _obj.cancelRightsTextView.add(txtComplInPerson);	
					
					  break;		
						
					  case 'Wisconsin':
							 textOnline.text = 'www.doj.state.wi.us/dls/ConsProt/newcp.asp';
							 textHotline.text =  '1-800-998-0700';
							 txtComplTele.text =  '608-266-1852 ';
							 var txt = 'Department of Financial Institutions\n'+ 
			                           'Office of Consumer Affairs \n'+ 
			                           'PO Box 8041\n'+
			                           'Madison, WI 53708-8041\n';
                              txtComplInPerson.text = txt;
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         
							_obj.cancelRightsTextView.add(_obj.txtComplHotline);
							_obj.cancelRightsTextView.add(textHotline); 
							_obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
							_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);
					   break;		

                       case 'Wyoming':
                             textOnline.text = 'http://attorneygeneral.state.wy.us/consumer.htm';
							 textHotline.text =  '1-800-438-5799 ';
							 txtComplTele.text =  '307-777-5833';
							 var txt = 'Wyoming Attorney General'+'s Office\n'+ 
			                           'Consumer Protection Unit 123 Capitol\n'+ 
			                           'Building Cheyenne, WY 82002\n';
                              txtComplInPerson.text = txt;
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);                         
							_obj.cancelRightsTextView.add(_obj.txtComplHotline);
							_obj.cancelRightsTextView.add(textHotline); 
							_obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
							_obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							_obj.cancelRightsTextView.add(txtComplInPerson);
					   break;	
					   
					 
                       //added by sanjivani on 07Sept17 for CMN70b
                       case 'Arkansas' :
                       var txt = 'Arkansas State Bank Department\n'+
                                 '501-324-9019\n'+
                                 'http://www.banking.arkansas.gov/consumer-info/file-a-complaint\n';
                                 
                                  txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson); 

                       break;
                       case 'Hawaii' :
                       var txt = 'Hawaii Department of Commerce and Consumer Affairs \n'+
                                 'Link: http://cca.hawaii.gov/dfi/file-a-complaint/ \n\n'+
                                 'If by mail:\n\n'+
                                 'Division of Financial Institutions\n'+
                                 'Department of Commerce and Consumer Affairs\n'+
                                 'P.O. Box 2054\n'+
                                 'Honolulu, HI 96805\n\n'+
                                 'If in person or by courier:\n'+
                                 'Division of Financial Institutions\n'+
                                 'Department of Commerce and Consumer Affairs\n'+
                                 '335 Merchant Street, Room 221\n'+
                                 'Honolulu, HI 96813\n'+
                                 'Telephone: 808-587-3222';
                                
                                txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson); 
                                  
                       break;


                       case 'Indiana' :
                       var txt = 'Indiana Department of Financial Institutions\n'+
                                 'Link: http://www.in.gov/dfi/files/Complaint%20Form%202016-10.pdf\n\n'+
                                 'Department of Financial Institutions\n'+
                                 '30 South Meridian Street,\n'+
                                 'Suite 300 Indianapolis,\n'+
                                 'IN 46204\n'+
                                 'Telephone: 317-232-3955\n'+
                                 'Toll-free: 800-382-4880\n'+
                                 'Fax: 317-232-7655\n'+
                                 'Email: dficomplaints@dfi.IN.gov\n';
                                 
                                 txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson);

                       break;
                       
                      case 'Kansas' :
                       var txt = 'Kansas Office of the State Bank Commissioner\n'+
                                 'Link: http://www.osbckansas.org/consumers/complaints.html\n'+
                                 'Office of the State Bank Commissioner\n'+
                                 'Attn: Complaints\n'+
                                 '700 SW Jackson, Suite 300\n'+
                                 'Topeka, Ks. 66603\n'+
                                 'Phone: 785-296-2266\n'+
                                 'Fax - 785-296-6037\n'+
                                 'E-Mail - complaints@osbckansas.org\n';
                                 
                                 txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson);
                       
                        break;
                        

                       case 'Mississippi' :
                        var txt ='http://www.dbcf.state.ms.us/complaints.asp\n'+
                                 'MS Department of Banking and Consumer Finance\n'+
                                 'P.O. Box 12129\n'+
                                 'Jackson, MS 39236-2129\n'+
                                 '(601) 321-6901\n';
                        
                        
                                txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson);    
                       break;
  
                       case 'Nebraska' :
                       var txt = 'Nebraska Department of Banking and Finance\n'+
                                 'Link: https://ndbf.nebraska.gov/consumers/complaints\n\n'+
                                 'By Mail to:\n\n'+
                                 'The Nebraska Department of Banking and Finance,\n'+
                                 'P.O. Box 95006, Lincoln,\n'+
                                 'NE 68509-5006 \n\n'+
                                 'Or\n\n'+
                                 'By private mail couriers to:\n'+
                                 'The Nebraska Department of Banking and Finance,\n'+
                                 '1526 K Street, Suite 300, Lincoln,\n'+
                                 'NE 68508-2732.\n'+
                                 'Phone: (402) 471-2171\n';
                                 
                                  txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson);
                                 
                       
                       break;

                       case 'South Dakota' :
                       var txt = 'South Dakota Division of Banking\n'+
                                 'Link: http://dlr.sd.gov/banking/consumers/consumer_complaint_reference_list.aspx\n'+
                                 'South Dakota Division of Banking\n'+
                                 '1601 N. Harrison Avenue, Suite 1\n'+
                                 'Pierre, SD 57501\n'+
                                 'Phone: 605.773.3421\n'+
                                 'Fax: 866.326.7504\n';
                                 
                                 txtComplInPerson.text = txt;
                                _obj.cancelRightsTextView.add(txtComplInPerson);
                                 
                       
                       
                       break;
                       
                       //Added on 10-Nov-17 for CMN87
                       case 'West Virginia' :
                       var txt = 'West Virginia Division of Financial Institutions';
                            txtComplInPerson.text = txt;
                      
                            txtComplTele.text =  '304-558-2294 ';
                            textOnline.text = 'http://www.dfi.wv.gov/Pages/default.aspx';
                            _obj.cancelRightsTextView.add(txtComplInPerson);
                            _obj.cancelRightsTextView.add(_obj.txtComplTele);
							_obj.cancelRightsTextView.add(txtComplTele); 
                            _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                            _obj.cancelRightsTextView.add(textOnline);
                       
                       break;
                       //CMN 91
                       case 'Ohio' : 
                      
                            _obj.txtComplOnline.text = 'Website:';
							 textOnline.text = 'www.com.ohio.gov/fin/';
							 _obj.txtComplHotline.text = 'Email:';
							 textHotline.text =  'webdfi-oca@com.ohio.gov';
							 txtComplTele.text =  '1-866-278-0003';
							 var txt =  'Ohio Department of Commerce'+'\n'+
                                                      'Division of Financial Institutions'+'\n'+
                                                      'Office of Consumer Affairs\n'+
                                                      '77 South High Street, 21st floor\n'+
                                                      'Columbus, Ohio 43215-6120\n';
                              txtComplInPerson.text = txt;
                             _obj.cancelRightsTextView.add(_obj.txtComplOnline);
                             _obj.cancelRightsTextView.add(textOnline);                         
							 _obj.cancelRightsTextView.add(_obj.txtComplHotline);
							 _obj.cancelRightsTextView.add(textHotline); 
							 _obj.cancelRightsTextView.add(_obj.txtComplTele);
							 _obj.cancelRightsTextView.add(txtComplTele); 
							 _obj.cancelRightsTextView.add(_obj.txtComplInPerson);
							 _obj.cancelRightsTextView.add(txtComplInPerson);
						break;
                       
                       break;
                       
						    
					
					}//Switch ends here
                      _obj.cancelRightsTextView.add(_obj.txtConsumer);
                     //_obj.mainView.add(_obj.complaintTextView);
				
				}
				_obj.mainView.add(_obj.btnBAT);
				
				_obj.btnBAT.addEventListener('click',function(e){
					destroy_confirmation();
					require('/utils/RemoveViews').removeAllScrollableViews();
					require('/utils/PageController').pageController('transfer');
				});
			
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_confirmation();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
				
				setTimeout(function(){
					destroy_confirmation();
					require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
				},1000);
			}
		}
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	
	}
	
	confirmation();
	
	_obj.imgClose.addEventListener('click',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_confirmation();
	});
	
	_obj.winConfirmation.addEventListener('androidback',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_confirmation();
	});
	
	function destroy_confirmation()
	{
		try{
			
			require('/utils/Console').info('############## Remove conf start ##############');
			
			_obj.winConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winConfirmation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_confirmation',destroy_confirmation);
			require('/utils/Console').info('############## Remove conf end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_confirmation', destroy_confirmation);
}; // Login()
