exports.LocationModal = function()
{
	require('/lib/analytics').GATrackScreen('Location');
	
	var _obj = {
		style : require('/styles/Location').Location,
		winLocation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tblLocation : null,
		
		dataOriginCountry : null,
		resultOriginCountry : null,
	};
	
	_obj.winLocation = Ti.UI.createWindow(_obj.style.winLocation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winLocation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.globalView.backgroundImage = null;
	_obj.globalView.backgroundColor = '#FFF';
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Location';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.tblLocation = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblLocation.top = 40;
	_obj.tblLocation.height = TiGlobals.platformHeight-60;
	
	_obj.tblLocation.addEventListener('click',function(e){
		try{
		if(e.row.rw === 1)
		{
			var sourceSplit = e.row.origCtyCurCode.split('~');
			Ti.App.Properties.setString('sourceCountryCurCode',sourceSplit[0]+'-'+sourceSplit[1]);
			
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR');

			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETCORRIDORDTL",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"corridorRequestType":"2",'+ 
					'"origCtyCurCode":"'+Ti.App.Properties.getString('sourceCountryCurCode')+'",'+
					'"destCtyCurCode":"'+Ti.App.Properties.getString('destinationCountryCurCode')+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				require('/utils/Console').info('Result ======== ' + e.result);
				
				activityIndicator.hideIndicator();
				
				if(e.result.responseFlag === "S")
				{
					// FRT
					Ti.App.Properties.setString('paymodeData',e.result.corridorData[0].origCtyCurData[0].paymodeData);
					
					//Source Country
					Ti.App.Properties.setString('sourceCountryCurCode',e.result.corridorData[0].origCtyCurData[0].origCtyCurCode);
					Ti.App.Properties.setString('sourceCountryCurName',e.result.corridorData[0].origCtyCurData[0].origCtyCurName);
					Ti.App.Properties.setString('sourceCountryInfoEmail',e.result.corridorData[0].origCtyCurData[0].infoEmail);
					Ti.App.Properties.setString('sourceCountryISDN',e.result.corridorData[0].origCtyCurData[0].isdnCode);
					Ti.App.Properties.setString('sourceCountryTollFree',e.result.corridorData[0].origCtyCurData[0].tollFreeNo);  
					
					//
					try{
					var selCorr= Ti.App.Properties.getString('sourceCountryCurName').split('~');
		            var  userCorr = Ti.App.Properties.getString('country');
		            var orig = selCorr[0];
					
		//alert(userCorr);
		//alert(orig);
		           Ti.API.info('userCorr ',userCorr);
					Ti.API.info('selCorr',orig);
	
					//added on 28 Dec 17
					 if(Ti.App.Properties.getString('loginStatus') !== '0'){ //check if user is logged in
	 					//if(TiGlobals.mpinPg == true){
							// setTimeout(
	 						//function(){
							if(Ti.App.Properties.getString('country') !== orig){
								 require('/js/updateuserdetails').addressUpdate(Ti.App.Properties.getString('firstName'));
						 }
							// },300);
	  					//}	
					}
					}catch(e){}
					//
					
					// First empty
					
					Ti.App.Properties.setString('indicativePaymodeName','');
					Ti.App.Properties.setString('indicativePaymodeCode','');
			        Ti.App.Properties.setString('guaranteedPaymodeName','');
			        Ti.App.Properties.setString('guaranteedPaymodeCode','');
			        
			        for(var i=0; i<e.result.corridorData[0].origCtyCurData[0].paymodeData.length; i++)
					{
			        
						if(e.result.corridorData[0].origCtyCurData[0].paymodeData[i].fxRateIndicativeOrGuaranteed === '01')
				        {
				        	Ti.App.Properties.setString('indicativePaymodeName',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeLabel);
							Ti.App.Properties.setString('indicativePaymodeCode',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeCode);
				        }
				        
				        if(e.result.corridorData[0].origCtyCurData[0].paymodeData[i].fxRateIndicativeOrGuaranteed === '02')
				        {	 
				        	Ti.App.Properties.setString('guaranteedPaymodeName',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeLabel);
				        	Ti.App.Properties.setString('guaranteedPaymodeCode',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeCode);
				        }
					}
					
					//Destination Country
					//Ti.App.Properties.setString('destinationCountryCurName',e.result.corridorData[1].destCtyCurData[0].destCtyCurName);
					//Ti.App.Properties.setString('destinationCountryCurCode',e.result.corridorData[1].destCtyCurData[0].destCtyCurCode);
					//Ti.App.Properties.setString('destinationCountryISDN',e.result.corridorData[1].destCtyCurData[0].isdnCode);
					
					Ti.App.fireEvent('changeLocation');
					Ti.App.fireEvent('menuAnimate');
					require('/utils/RemoveViews').removeAllScrollableViews();
					destroy_location();
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		}catch(e){}
	});
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	
	function populateOriginCountry() {
		activityIndicator.showIndicator();
		
		_obj.dataOriginCountry = [];
		_obj.tblLocation.setData([]);

		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCORRIDORDTL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"corridorRequestType":"3",'+ 
				'"origCtyCurCode":"",'+
				'"destCtyCurCode":""'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			_obj.resultOriginCountry = e.result;
			require('/utils/Console').info('Result ======== ' + _obj.resultOriginCountry);
			
			if(_obj.resultOriginCountry.responseFlag === "S")
			{
				var zoneArr = [];
				var countryArr = [];
			
				for(var i=0; i<_obj.resultOriginCountry.corridorData[0].origCtyCurData.length; i++)
				{
					if(zoneArr.indexOf(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName) === -1)
					{
						if(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName === "")
						{
							if(zoneArr.indexOf('Others') === -1)
							{
								if(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry === 'Y')
								{
									zoneArr.push('Others');
								}
							}
						}
						else if(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry === 'Y')  //added for zone name hide
						{
							zoneArr.push(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName);
						}else{}
					}
					_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName === "" ? countryArr.push('Others##'+_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurName +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurCode +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry) : countryArr.push(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurName +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurCode +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry);
				}
				
				for(var z=0; z<zoneArr.length; z++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 45,
						backgroundColor : 'transparent',
						className : 'origin_country',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblCountryName = Ti.UI.createLabel({
						text : zoneArr[z],
						height : Ti.UI.SIZE,
						left : 20,
						right : 20,
						textAlign : 'center',
						font : TiFonts.FontStyle('lblNormal16'),
						color : TiFonts.FontStyle('redFont')
					});

					row.add(lblCountryName);
					_obj.dataOriginCountry.push(row);
					
					for(var c=0; c<countryArr.length-1; c++)
					{
						var splitArr = countryArr[c].split('##');
						var splitCountryArr = splitArr[1].split('~');
						
						if(zoneArr[z] === splitArr[0])
						{
							if(splitArr[3] === "Y")
							{
								var row = Ti.UI.createTableViewRow({
									top : 0,
									left : 0,
									right : 0,
									height : 45,
									backgroundColor : 'transparent',
									className : 'origin_country',
									origCtyCurCode : splitArr[2],
									countryName : splitCountryArr[0],
									rw:1
								});
								
								if(TiGlobals.osname !== 'android')
								{
									row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
								}
								
								var lblCountryName = Ti.UI.createLabel({
									text : splitCountryArr[0],
									height : Ti.UI.SIZE,
									left : 20,
									right : 20,
									textAlign : 'center',
									font : TiFonts.FontStyle('lblNormal14'),
									color : TiFonts.FontStyle('greyFont')
								});
			
								row.add(lblCountryName);
								if(countryName[0] === splitCountryArr[0])
								{
									var imgCheck = Ti.UI.createImageView({
										image : '/images/check_red.png',
										right : 20,
										height : 25,
										width : 25,
										zIndex:5
									});
									
									row.add(imgCheck);
								}
								_obj.dataOriginCountry.push(row);
							}
						}
					}
				}
				
				activityIndicator.hideIndicator();
				_obj.tblLocation.setData(_obj.dataOriginCountry);
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',_obj.resultOriginCountry.message,[L('btn_ok')]).show();
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	populateOriginCountry();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.tblLocation);
	_obj.globalView.add(_obj.mainView);
	_obj.winLocation.add(_obj.globalView);
	_obj.winLocation.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_location();
	});
	
	_obj.winLocation.addEventListener('androidback', function(){
		destroy_location();
	});
	
	function destroy_location()
	{
		try{
			
			require('/utils/Console').info('############## Remove location start ##############');
			
			_obj.winLocation.close();
			require('/utils/RemoveViews').removeViews(_obj.winLocation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_location',destroy_location);
			require('/utils/Console').info('############## Remove location end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_location', destroy_location);
}; // Login()
