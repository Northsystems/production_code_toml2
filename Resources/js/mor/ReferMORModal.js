exports.ReferMORModal = function()
{
	require('/lib/analytics').GATrackScreen('Refer Friends');
	
	var _obj = {
		style : require('/styles/mor/ReferMOR').ReferMOR, // style
		winReferMOR : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tblRefer : null,
	};
	
	_obj.winReferMOR = Ti.UI.createWindow(_obj.style.winReferMOR);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winReferMOR);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Refer Friends';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.tblRefer = Ti.UI.createTableView(_obj.style.tableView);
	
	var ht = TiGlobals.osname === 'android' ? ((parseInt(TiGlobals.platformHeight) - (40))/3) : ((parseInt(TiGlobals.platformHeight) - (60))/3);
	require('/utils/Console').info(Math.ceil(ht));
	function referEarn()
	{
		for(var i=0; i<3; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : ht,
				backgroundColor : i%2 === 0 ? '#efeff0' : '#e3e3e3',
				className : 'refer',
				rw:i
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var imgView = Ti.UI.createImageView({
				width : 175,
				height : 75
			});
			
			switch (i)
			{
				case 0:
					imgView.image = '/images/earn_manual.png';
				break;
				
				case 1:
					imgView.image = '/images/earn_email.png';
				break;
				
				case 2:
					imgView.image = '/images/earn_social.png';
				break;
				
			}

			var imgArrow = Ti.UI.createImageView({
				image : '/images/link_arrow_red.png',
				right : 20,
				width : 10,
				height : 15
			});
			
			row.add(imgView);
			row.add(imgArrow);
			_obj.tblRefer.appendRow(row);
		}
	}
	
	referEarn();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.tblRefer);
	_obj.globalView.add(_obj.mainView);
	_obj.winReferMOR.add(_obj.globalView);
	_obj.winReferMOR.open();
	
	_obj.tblRefer.addEventListener('click', function(event){
		switch (event.row.rw)
		{
			case 0:
				// Manual Refer
				if(Titanium.Network.online)
				{
					require('/js/mor/ManualLIModal').ManualModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 1:
				// Email Refer
				if(Titanium.Network.online)
				{
					require('/js/mor/ReferEmailLIModal').ReferEmailModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 2:
				// Social Refer
				if(Titanium.Network.online)
				{
					require('/js/mor/ReferSocialLIModal').ReferSocialModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
		}
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_referemor();
	});
	
	_obj.winReferMOR.addEventListener('androidback',function(e){
		destroy_referemor();
	});
	
	function destroy_referemor()
	{
		try{
			
			require('/utils/Console').info('############## Remove refer mor start ##############');
			
			_obj.winReferMOR.close();
			require('/utils/RemoveViews').removeViews(_obj.winReferMOR);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_referemor',destroy_referemor);
			require('/utils/Console').info('############## Remove refer mor end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_referemor', destroy_referemor);
}; // ReferMORModal()