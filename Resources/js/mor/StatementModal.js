exports.StatementModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Statement');
	
	var _obj = {
		style : require('/styles/mor/Statement').Statement, // style
		winStatement : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lbl1 : null,
		lbl2 : null,
		selectBorder : null,
		lblSelect : null,
		imgSelect : null,
		selectView : null,
		tblStatement : null,
		selectBorder1 : null,
		btnReminder : null,
		
		selected : [],
		selectedValue : [],
	};
	
	//Ti.API.info("morDtls.length",+params.morDtls.length);
	_obj.winStatement = Ti.UI.createWindow(_obj.style.winStatement);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winStatement);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Statement';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lbl1 = Ti.UI.createLabel(_obj.style.lbl);
	_obj.lbl1.text = '\u00B7 The statement below reflects only customers you have referred and who have registered and / or transacted with us.';
	
	_obj.lbl2 = Ti.UI.createLabel(_obj.style.lbl);
	_obj.lbl2.text = '\u00B7 "Registered users" have signed up however have not initiated a transaction. To send a reminder select the users and click on "Send reminder" button.';
	
	_obj.selectBorder = Ti.UI.createView(_obj.style.headerBorder);
	_obj.selectBorder.top = 15;
	
	_obj.selectView = Ti.UI.createView(_obj.style.selectView);
	_obj.lblSelect = Ti.UI.createLabel(_obj.style.lblSelect);
	_obj.lblSelect.text = 'Select All';
	_obj.imgSelect = Ti.UI.createImageView(_obj.style.imgSelect);
	
	_obj.tblStatement = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.selectBorder1 = Ti.UI.createView(_obj.style.headerBorder);
	_obj.selectBorder1.top = 0;
	
	_obj.btnReminder = Ti.UI.createButton(_obj.style.btnReminder);
	_obj.btnReminder.title = 'SEND REMINDER';
	
	_obj.selectView.addEventListener('click', function(event){
		if(_obj.imgSelect.image === '/images/checkbox_unsel.png')
		{
			_obj.imgSelect.image = '/images/checkbox_sel.png';
			_obj.selected = [];
			_obj.selectedValue = [];
			
			for(var i=0; i<params.morDtls.length; i++)
			{
				imgCheck[i].image = '/images/checkbox_white_sel.png';
				_obj.selected.push(i);
				_obj.selectedValue.push(imgCheck[i].val);
			}
		}
		else
		{
			_obj.imgSelect.image = '/images/checkbox_unsel.png';
			_obj.selected = [];
			_obj.selectedValue = [];
			for(var i=0; i<params.morDtls.length; i++)
			{
				imgCheck[i].image = '/images/checkbox_white_unsel.png';
				
			}
		}
	});
	
	var imgCheck = [];
	
	function statement()
	{
		try{
		if(params.responseFlag === 'S')
		{   
			try{
			_obj.tblStatement.height = (params.morDtls.length*163);
			
			for(var i=0; i<params.morDtls.length; i++)
			{
				var row = Ti.UI.createTableViewRow({
					top : 0,
					left : 0,
					right : 0,
					height : 163,
					backgroundColor : 'transparent',
					className : 'payment_details',
					val : params.morDtls[i].refreeName + '~' + params.morDtls[i].refreeEmail,
					rw : i
				});
				
				if(TiGlobals.osname !== 'android')
				{
					row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
				}
				
				var topView = Ti.UI.createView({
					top : 0,
					left : 0,
					right : 0,
					height : 50,
					backgroundColor : '#6f6f6f'
				});
				
				var detailNameView = Ti.UI.createView({
					top : 0,
					left : 0,
					right : 60,
					height : 50
				});
				
				var nameView = Ti.UI.createView({
					left : 0,
					right:0,
					height : Ti.UI.SIZE,
					layout:'vertical'
				});
				
				var lblName = Ti.UI.createLabel({
					text : params.morDtls[i].refreeName,
					top : 0,
					left : 20,
					right : 20,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					textAlign: 'left',
					font : TiFonts.FontStyle('lblNormal14'),
					color : TiFonts.FontStyle('whiteFont')
				});
				
				var dt = params.morDtls[i].refDate.split('-');
				
				var lblDate = Ti.UI.createLabel({
					text : 'Referred Date ' + dt[0]+'/'+require('/utils/Date').monthNo(dt[1])+'/'+dt[2],
					top : 2,
					left : 20,
					right : 20,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					textAlign: 'left',
					font : TiFonts.FontStyle('lblNormal12'),
					color : TiFonts.FontStyle('greyFont')
				});
				
				imgCheck[i] = Ti.UI.createImageView({
					image:'/images/checkbox_white_unsel.png',
					right : 20,
					width : 20,
					height : 20,
					val : params.morDtls[i].refreeName + '~' + params.morDtls[i].refreeEmail
				});
				
				var bottomView = Ti.UI.createView({
					top : 50,
					left : 0,
					right : 0,
					height : 56,
					backgroundColor : '#fff',
					layout:'horizontal',
					horizontalWrap:false
				});
				
				var emailView = Ti.UI.createView({
					top : 0,
					left : 0,
					width : '100%',
					height : 56,
					backgroundColor : '#fff'
				});
				
				var emailMainView = Ti.UI.createView({
					left : 0,
					right:0,
					height : Ti.UI.SIZE,
					layout:'vertical'
				});
				
				var lblEmailHead = Ti.UI.createLabel({
					text : 'Email Address',
					top : 0,
					left : 20,
					right : 20,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					textAlign: 'left',
					font : TiFonts.FontStyle('lblNormal12'),
					color : TiFonts.FontStyle('greyFont')
				});
				
				var lblEmailTxt = Ti.UI.createLabel({
					text : params.morDtls[i].refreeEmail,
					top : 2,
					left : 20,
					right : 20,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					textAlign: 'left',
					font : TiFonts.FontStyle('lblNormal14'),
					color : TiFonts.FontStyle('blackFont')
				});
				
				var borderView1 = Ti.UI.createView({
					top : 106,
					left : 0,
					right : 0,
					height : 1,
					backgroundColor : '#eee'
				});
				
				var bottomView1 = Ti.UI.createView({
					top : 107,
					left : 0,
					right : 0,
					height : 56,
					backgroundColor : '#fff',
					layout:'horizontal',
					horizontalWrap:false
				});
				
				var detailStatusView = Ti.UI.createView({
					top : 0,
					left : 0,
					width:'100%',
					height : 56,
					backgroundColor : '#fff'
				});
				
				var statusView = Ti.UI.createView({
					left : 0,
					right:0,
					height : Ti.UI.SIZE,
					layout:'vertical'
				});
				
				var lblStatusHead = Ti.UI.createLabel({
					text : 'Transaction Status',
					top : 0,
					left : 20,
					right : 20,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					textAlign: 'left',
					font : TiFonts.FontStyle('lblNormal12'),
					color : TiFonts.FontStyle('greyFont')
				});
				
				var lblStatus = Ti.UI.createLabel({
					text : params.morDtls[i].txnStatus,
					top : 2,
					left : 20,
					right : 20,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					textAlign: 'left',
					font : TiFonts.FontStyle('lblNormal14'),
					color : TiFonts.FontStyle('blackFont')
				});
				
				nameView.add(lblName);
				nameView.add(lblDate);
				detailNameView.add(nameView);
				topView.add(detailNameView);
				topView.add(imgCheck[i]);
				emailMainView.add(lblEmailHead);
				emailMainView.add(lblEmailTxt);
				emailView.add(emailMainView);
				bottomView.add(emailView);
				statusView.add(lblStatusHead);
				statusView.add(lblStatus);
				detailStatusView.add(statusView);
				bottomView1.add(detailStatusView);
				row.add(topView);
				row.add(bottomView);
				row.add(borderView1);
				row.add(bottomView1);
				_obj.tblStatement.appendRow(row);
			}
		}catch(e){}
			
		}
		else
		{
			_obj.tblStatement.height = 50;
			
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : 50,
				backgroundColor : 'transparent',
				className : 'payment_details',
				rw:''
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var lblMessage = Ti.UI.createLabel({
				text : 'You have not referred anybody',
				left : 20,
				right : 20,
				height:Ti.UI.SIZE,
				width:Ti.UI.SIZE,
				textAlign: 'center',
				font : TiFonts.FontStyle('lblNormal14'),
				color : TiFonts.FontStyle('greyFont')
			});
			
			row.add(lblMessage);
			_obj.tblStatement.appendRow(row);
		}
		}catch(e){}
	}
	
	statement();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.lbl1);
	_obj.mainView.add(_obj.lbl2);
	_obj.mainView.add(_obj.selectBorder);
	try{
	if(params.responseFlag === 'S')
	{
		try{
		if(params.morDtls.length > 0){
		_obj.selectView.add(_obj.lblSelect);
		_obj.selectView.add(_obj.imgSelect);
		_obj.mainView.add(_obj.selectView);
		_obj.mainView.add(_obj.tblStatement);
	    _obj.mainView.add(_obj.selectBorder1);
	    _obj.mainView.add(_obj.btnReminder);
	   
	}
	
	else{ //added by sanjivani on 23 march 17 for phase 3 defect
	
		_obj.lblSelect.text = "You do not have any referrals !";
		_obj.selectView.add(_obj.lblSelect);
		//_obj.selectView.add(_obj.imgSelect);
		_obj.mainView.add(_obj.selectView);
		_obj.mainView.add(_obj.tblStatement);
	    _obj.mainView.add(_obj.selectBorder1);
		
	}
	}catch(e){}
	}
	}catch(e){}
	/*_obj.mainView.add(_obj.tblStatement);
	_obj.mainView.add(_obj.selectBorder1);
	if(params.responseFlag === 'S')
	{
		_obj.mainView.add(_obj.btnReminder);
	}*/
	_obj.globalView.add(_obj.mainView);
	_obj.winStatement.add(_obj.globalView);
	_obj.winStatement.open();
	
	_obj.btnReminder.addEventListener('click', function(evt){
		if(_obj.selected.length === 0)
		{
			require('/utils/AlertDialog').showAlert('','Please select at least one record',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		var friendDataArray = {
		    frdReferList: []
		};
		
		for(var i=0; i<_obj.selected.length; i++)
		{
			var refSplit = [];
			refSplit = _obj.selectedValue[i].split('~');
			
			friendDataArray.frdReferList.push({ 
		        "refreeName" : refSplit[0], 
				"refreeEmail" : refSplit[1]
		    });
		}
		
		require('/utils/Console').info(JSON.stringify(friendDataArray.frdReferList));
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"SENDREFERRALPROGREFREEREMINDER",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"referData":'+JSON.stringify(friendDataArray.frdReferList)+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(require('/lib/toml_validations').removeHTMLTags(e.result.message));
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(require('/lib/toml_validations').removeHTMLTags(e.result.message));
				}
				
				setTimeout(function(){
					destroy_statement();
				},1000);
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_statement();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.tblStatement.addEventListener('click', function(evt){
		if(evt.row.rw !== '')
		{
			if(imgCheck[evt.row.rw].image === '/images/checkbox_white_sel.png')
			{
				_obj.imgSelect.image = '/images/checkbox_unsel.png';
				imgCheck[evt.row.rw].image = '/images/checkbox_white_unsel.png';
				
				//Remove element from array
				var index = _obj.selected.indexOf(evt.row.rw);
				_obj.selected.splice(index, 1);
				
				var index1 = _obj.selectedValue.indexOf(imgCheck[evt.row.rw].val);
				_obj.selectedValue.splice(index1, 1);
				
				require('/utils/Console').info(_obj.selected.join());
				require('/utils/Console').info(_obj.selectedValue.join());
			}
			else
			{
				imgCheck[evt.row.rw].image = '/images/checkbox_white_sel.png';
				_obj.selected.push(evt.row.rw);
				_obj.selectedValue.push(imgCheck[evt.row.rw].val);
				
				require('/utils/Console').info(_obj.selected.join());
				require('/utils/Console').info(_obj.selectedValue.join());
			}
		}
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_statement();
	});
	
	_obj.winStatement.addEventListener('androidback',function(e){
		destroy_statement();
	});
	
	function destroy_statement()
	{
		try{
			
			require('/utils/Console').info('############## Remove refer mor start ##############');
			
			_obj.winStatement.close();
			require('/utils/RemoveViews').removeViews(_obj.winStatement);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_statement',destroy_statement);
			require('/utils/Console').info('############## Remove refer mor end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_statement', destroy_statement);
}; // StatementModal()
