function Location(_self)
{
	//require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));
	
	var _obj = {
		style : require('/styles/Location').Location,
		globalView : null,
		mainView : null,
		tblLocation : null,
		lblTxt1 : null,
		lblTxt2 : null,
		lblTxt3 : null,
		locationHorizontalView : null,
		locationMainView : null,
		locationTxtView : null,
		lblCountry : null,
		imgCountry : null,
		countryView : null,
		borderView : null,
		locationBottomView : null,
		
		dataOriginCountry : null,
		resultOriginCountry : null,
	};
	
	//////////////////////////// Create UI ////////////////////////////
  
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	if(TiGlobals.osname !== 'android')
	{
		_obj.globalView.top = 20;	
	}
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.tblLocation = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblLocation.zIndex = 1000;
	_obj.tblLocation.backgroundColor = '#fff';
	_obj.tblLocation.top = TiGlobals.platformHeight;
	_obj.tblLocation.height = TiGlobals.platformHeight-20;
	
	_obj.locationTxtView = Ti.UI.createView(_obj.style.locationTxtView);
	
	_obj.locationMainView = Ti.UI.createView(_obj.style.locationMainView);
	
	_obj.lblTxt1 = Ti.UI.createLabel(_obj.style.lblTxt1);
	_obj.lblTxt1.text = 'Transfer Money to';
	
	_obj.locationHorizontalView = Ti.UI.createView(_obj.style.locationHorizontalView);
	_obj.lblTxt2 = Ti.UI.createLabel(_obj.style.lblTxt2);
	_obj.lblTxt2.text = 'India ';
	_obj.lblTxt3 = Ti.UI.createLabel(_obj.style.lblTxt3);
	_obj.lblTxt3.text = 'From';
	
	_obj.locationBottomView = Ti.UI.createView(_obj.style.locationBottomView);
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCountry.text = 'Select Country';
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderView = Ti.UI.createView(_obj.style.borderView);
	
	_obj.locationMainView.add(_obj.lblTxt1);
	_obj.locationHorizontalView.add(_obj.lblTxt2);
	_obj.locationHorizontalView.add(_obj.lblTxt3);
	_obj.locationMainView.add(_obj.locationHorizontalView);
	_obj.locationTxtView.add(_obj.locationMainView);
	_obj.mainView.add(_obj.locationTxtView);
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.locationBottomView.add(_obj.countryView);
	_obj.locationBottomView.add(_obj.borderView);
	_obj.mainView.add(_obj.locationBottomView);
	_obj.globalView.add(_obj.mainView);
	_obj.globalView.add(_obj.tblLocation);
	
	var animateViewTop = Ti.UI.createAnimation({
	    top:0,
	    duration:200
	});
	
	var animateViewBottom = Ti.UI.createAnimation({
	    top:TiGlobals.platformHeight,
	    duration:200
	});
	//populateOriginCountry();
	_obj.countryView.addEventListener('click',function(e){
		populateOriginCountry();
	});
	
	_obj.tblLocation.addEventListener('click',function(e){
		try{
		if(e.row.rw === 1)
		{
			var sourceSplit = e.row.origCtyCurCode.split('~');
			Ti.App.Properties.setString('sourceCountryCurCode',sourceSplit[0]+'-'+sourceSplit[1]);
			
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR');

			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETCORRIDORDTL",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"corridorRequestType":"2",'+ 
					'"origCtyCurCode":"'+Ti.App.Properties.getString('sourceCountryCurCode')+'",'+
					'"destCtyCurCode":"'+Ti.App.Properties.getString('destinationCountryCurCode')+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				require('/utils/Console').info('Result ======== ' + e.result);
				//alert(e.result);
				
				activityIndicator.hideIndicator();
				
				if(e.result.responseFlag === "S")
				{
					try{
					// FRT
					Ti.App.Properties.setString('paymodeData',e.result.corridorData[0].origCtyCurData[0].paymodeData);
					
					//Source Country
					Ti.App.Properties.setString('sourceCountryCurCode',e.result.corridorData[0].origCtyCurData[0].origCtyCurCode);
					//alert(Ti.App.Properties.getString('sourceCountryCurCode'));
					Ti.App.Properties.setString('sourceCountryCurName',e.result.corridorData[0].origCtyCurData[0].origCtyCurName);
					Ti.App.Properties.setString('sourceCountryInfoEmail',e.result.corridorData[0].origCtyCurData[0].infoEmail);
					Ti.App.Properties.setString('sourceCountryISDN',e.result.corridorData[0].origCtyCurData[0].isdnCode);
					Ti.App.Properties.setString('sourceCountryTollFree',e.result.corridorData[0].origCtyCurData[0].tollFreeNo);  
					
					// First empty
					
					Ti.App.Properties.setString('indicativePaymodeName','');
					Ti.App.Properties.setString('indicativePaymodeCode','');
			        Ti.App.Properties.setString('guaranteedPaymodeName','');
			        Ti.App.Properties.setString('guaranteedPaymodeCode','');
					
					for(var i=0; i<e.result.corridorData[0].origCtyCurData[0].paymodeData.length; i++)
					{
			        
						if(e.result.corridorData[0].origCtyCurData[0].paymodeData[i].fxRateIndicativeOrGuaranteed === '01')
				        {
				        	Ti.App.Properties.setString('indicativePaymodeName',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeLabel);
							Ti.App.Properties.setString('indicativePaymodeCode',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeCode);
				        }
				        
				        if(e.result.corridorData[0].origCtyCurData[0].paymodeData[i].fxRateIndicativeOrGuaranteed === '02')
				        {	 
				        	Ti.App.Properties.setString('guaranteedPaymodeName',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeLabel);
				        	Ti.App.Properties.setString('guaranteedPaymodeCode',e.result.corridorData[0].origCtyCurData[0].paymodeData[i].paymodeCode);
				        }
					}
					
					//Destination Country
					//Ti.App.Properties.setString('destinationCountryCurName',e.result.corridorData[1].destCtyCurData[0].destCtyCurName);
					//Ti.App.Properties.setString('destinationCountryCurCode',e.result.corridorData[1].destCtyCurData[0].destCtyCurCode);
					//Ti.App.Properties.setString('destinationCountryISDN',e.result.corridorData[1].destCtyCurData[0].isdnCode);
					
					// Open dashboard
					
					Ti.App.fireEvent('changeLocation');
					_obj.tblLocation.animate(animateViewBottom);
					
					setTimeout(function(){
						Ti.App.fireEvent('sections',{'data':'dashboard'});
						destroy_location();
					},300);
				}catch(e){
					
				}
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		}catch(e){}
	});
	
	function populateOriginCountry() {
		activityIndicator.showIndicator();
		
		_obj.dataOriginCountry = [];
		_obj.tblLocation.setData([]);

		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCORRIDORDTL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"corridorRequestType":"3",'+ 
				'"origCtyCurCode":"",'+
				'"destCtyCurCode":""'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			_obj.resultOriginCountry = e.result;
			require('/utils/Console').info('Result ======== ' + _obj.resultOriginCountry);
			
			if(_obj.resultOriginCountry.responseFlag === "S")
			{
				var zoneArr = [];
				var countryArr = [];
			
				for(var i=0; i<_obj.resultOriginCountry.corridorData[0].origCtyCurData.length; i++)
				{
					if(zoneArr.indexOf(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName) === -1)
					{
						if(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName === "")
						{
							if(zoneArr.indexOf('Others') === -1)
							{
								if(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry === 'Y')
								{
									zoneArr.push('Others');	
								}
							}
						}
						else if(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry === 'Y')  //added for zone name hide
						{
							zoneArr.push(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName);
						}else{}
					}
					_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName === "" ? countryArr.push('Others##'+_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurName +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurCode +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry) : countryArr.push(_obj.resultOriginCountry.corridorData[0].origCtyCurData[i].zoneName +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurName +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].origCtyCurCode +'##'+ _obj.resultOriginCountry.corridorData[0].origCtyCurData[i].isActiveRegncountry);
				}
				
				for(var z=0; z<zoneArr.length; z++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 45,
						backgroundColor : 'transparent',
						className : 'origin_country',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblCountryName = Ti.UI.createLabel({
						text : zoneArr[z],
						height : Ti.UI.SIZE,
						left : 20,
						right : 20,
						textAlign : 'center',
						font : TiFonts.FontStyle('lblNormal16'),
						color : TiFonts.FontStyle('redFont')
					});

					row.add(lblCountryName);
					_obj.dataOriginCountry.push(row);
					
					for(var c=0; c<countryArr.length-1; c++)
					{
						var splitArr = countryArr[c].split('##');
						var splitCountryArr = splitArr[1].split('~');
						
						if(zoneArr[z] === splitArr[0])
						{
							if(splitArr[3] === "Y")
							{
								var row = Ti.UI.createTableViewRow({
									top : 0,
									left : 0,
									right : 0,
									height : 45,
									backgroundColor : 'transparent',
									className : 'origin_country',
									origCtyCurCode : splitArr[2],
									countryName : splitCountryArr[0],
									rw:1
								});
								
								if(TiGlobals.osname !== 'android')
								{
									row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
								}
								
								var lblCountryName = Ti.UI.createLabel({
									text : splitCountryArr[0],
									height : Ti.UI.SIZE,
									left : 20,
									right : 20,
									textAlign : 'center',
									font : TiFonts.FontStyle('lblNormal14'),
									color : TiFonts.FontStyle('greyFont')
								});
			
								row.add(lblCountryName);
								_obj.dataOriginCountry.push(row);
							}
						}
					}
				}
				
				activityIndicator.hideIndicator();
				_obj.tblLocation.setData(_obj.dataOriginCountry);
				
				_obj.tblLocation.animate(animateViewTop);
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',_obj.resultOriginCountry.message,[L('btn_ok')]).show();
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function destroy_location()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove disclaimer start ##############');
			
			_self.remove(_obj.globalView);
			require('/utils/RemoveViews').removeViews(_obj.globalView);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_location',destroy_location);
			require('/utils/Console').info('############## Remove disclaimer end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_location', destroy_location);
	
	return _obj.globalView;
	
}; // Location()

module.exports = Location;
