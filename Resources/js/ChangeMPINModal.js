exports.ChangeMPINModal = function(){
	
	require('/lib/analytics').GATrackScreen('Change mPIN');
	
	var _obj = {
		style : require('/styles/CreateMPIN').CreateMPIN,
		winChangeMPIN : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		done : [],
		txtMPIN : [],
		doneConfirm : [],
		txtMPINConfirm : [],
		doneNew : [],
		txtMPINNew : [],
		btnSubmit : null,
		
	};
	
	_obj.winChangeMPIN = Ti.UI.createWindow(_obj.style.winCreateMPIN);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winChangeMPIN);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Change mPIN';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	// mPIN Old View
	
	_obj.lblEnterMPIN = Ti.UI.createLabel(_obj.style.lblMPIN);
	_obj.lblEnterMPIN.text = 'Enter old mPIN';
	
	_obj.mPINView = Ti.UI.createView(_obj.style.mPINView);
	
	for(var i=0; i<6; i++)
	{
		_obj.done[i] = Ti.UI.createButton(_obj.style.done);
	
		_obj.done[i].addEventListener('click',function(){
			
			for(var j=0; j<6; j++)
			{
				_obj.txtMPIN[j].blur();
			}
		});
		
		_obj.txtMPIN[i] = Ti.UI.createTextField(_obj.style.txtMPIN);
		_obj.txtMPIN[i].left = 0;
		_obj.txtMPIN[i].sel = i;
		_obj.txtMPIN[i].editable = true;
		
		if(i>0)
		{
			_obj.txtMPIN[i].left = '4%';
			_obj.txtMPIN[i].editable = false;
		}
		
		_obj.txtMPIN[i].width = '12%';
		_obj.txtMPIN[i].maxLength = 1;
		_obj.txtMPIN[i].passwordMask = true;
		_obj.txtMPIN[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMPIN[i].keyboardToolbar = [_obj.done[i]];
		
		_obj.mPINView.add(_obj.txtMPIN[i]);
		
		_obj.txtMPIN[i].addEventListener('change',function(){
			if(this.value !== '')
			{
				if(this.sel<5)
				{
					_obj.txtMPIN[this.sel+1].editable = true;
					_obj.txtMPIN[this.sel+1].focus();
				}
			}
		});
	}
	
	// mPIN new View
	
	_obj.lblNewMPIN = Ti.UI.createLabel(_obj.style.lblMPIN);
	_obj.lblNewMPIN.text = 'Enter new mPIN';
	
	_obj.mPINNewView = Ti.UI.createView(_obj.style.mPINView);
	
	for(var i=0; i<6; i++)
	{
		_obj.doneNew[i] = Ti.UI.createButton(_obj.style.done);
	
		_obj.doneNew[i].addEventListener('click',function(){
			
			for(var j=0; j<6; j++)
			{
				_obj.txtMPINNew[j].blur();
			}
		});
		
		_obj.txtMPINNew[i] = Ti.UI.createTextField(_obj.style.txtMPIN);
		_obj.txtMPINNew[i].left = 0;
		_obj.txtMPINNew[i].sel = i;
		_obj.txtMPINNew[i].editable = true;
		
		if(i>0)
		{
			_obj.txtMPINNew[i].left = '4%';
			_obj.txtMPINNew[i].editable = false;
		}
		
		_obj.txtMPINNew[i].width = '12%';
		_obj.txtMPINNew[i].maxLength = 1;
		_obj.txtMPINNew[i].passwordMask = true;
		_obj.txtMPINNew[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMPINNew[i].keyboardToolbar = [_obj.doneNew[i]];
		
		_obj.mPINNewView.add(_obj.txtMPINNew[i]);
		
		_obj.txtMPINNew[i].addEventListener('change',function(){
			if(this.value !== '')
			{
				if(this.sel<5)
				{
					_obj.txtMPINNew[this.sel+1].editable = true;
					_obj.txtMPINNew[this.sel+1].focus();
				}
			}
		});
	}
	
	// Confirm
	
	_obj.lblConfirmMPIN = Ti.UI.createLabel(_obj.style.lblMPIN);
	_obj.lblConfirmMPIN.text = 'Confirm mPIN';
	
	_obj.mPINConfirmView = Ti.UI.createView(_obj.style.mPINView);
	
	for(var i=0; i<6; i++)
	{
		_obj.doneConfirm[i] = Ti.UI.createButton(_obj.style.done);
	
		_obj.doneConfirm[i].addEventListener('click',function(){
			
			for(var j=0; j<6; j++)
			{
				_obj.txtMPINConfirm[j].blur();
			}
		});
		
		_obj.txtMPINConfirm[i] = Ti.UI.createTextField(_obj.style.txtMPIN);
		_obj.txtMPINConfirm[i].left = 0;
		_obj.txtMPINConfirm[i].sel = i;
		_obj.txtMPINConfirm[i].editable = true;
		
		if(i>0)
		{
			_obj.txtMPINConfirm[i].left = '4%';
			_obj.txtMPINConfirm[i].editable = false;
		}
		
		_obj.txtMPINConfirm[i].width = '12%';
		_obj.txtMPINConfirm[i].maxLength = 1;
		_obj.txtMPINConfirm[i].passwordMask = true;
		_obj.txtMPINConfirm[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMPINConfirm[i].keyboardToolbar = [_obj.doneConfirm[i]];
		
		_obj.mPINConfirmView.add(_obj.txtMPINConfirm[i]);
		
		_obj.txtMPINConfirm[i].addEventListener('change',function(){
			if(this.value !== '')
			{
				if(this.sel<5)
				{
					_obj.txtMPINConfirm[this.sel+1].editable = true;
					_obj.txtMPINConfirm[this.sel+1].focus();
				}
			}
		});
	}
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblEnterMPIN);
	_obj.mainView.add(_obj.mPINView);
	_obj.mainView.add(_obj.lblNewMPIN);
	_obj.mainView.add(_obj.mPINNewView);
	_obj.mainView.add(_obj.lblConfirmMPIN);
	_obj.mainView.add(_obj.mPINConfirmView);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winChangeMPIN.add(_obj.globalView);
	_obj.winChangeMPIN.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_mpin();
	});
	
	_obj.winChangeMPIN.addEventListener('androidback',function(e){
		destroy_mpin();
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		if(!(require('/utils/Debounce').debounce(e.source))){return;}
		
		var mpin = '';
		var mpin_new = '';
		var mpin_confirm = '';
		
		for(var i=0; i<6; i++)
		{
			mpin = mpin + _obj.txtMPIN[i].value;
			mpin_new = mpin_new + _obj.txtMPINNew[i].value;
			mpin_confirm = mpin_confirm + _obj.txtMPINConfirm[i].value;
		}
		
		if(mpin.length<6)
  		{
  			require('/utils/AlertDialog').showAlert('',L('validateOldMPIN'),[L('btn_ok')]).show();
			return;
  		}
  		else if(mpin_new.length<6)
  		{
  			require('/utils/AlertDialog').showAlert('',L('validateNewMPIN'),[L('btn_ok')]).show();
			return;
  		}
  		else if(mpin_new !== mpin_confirm)
  		{
  			require('/utils/AlertDialog').showAlert('',L('confirmMPIN'),[L('btn_ok')]).show();
  			
  			for(var i=0; i<6; i++)
			{
				_obj.txtMPINNew[i].value = '';
				_obj.txtMPINConfirm[i].value = '';
			}
  			
			return;
  		}
  		else
		{
			var cipher = 'rijndael-128';
			var mode = 'ecb';
			var iv = null;
			var base64 = require('/lib/Crypt/Base64');
			
			require('/lib/Crypt/AES').Crypt(null,null,null, key, cipher, mode);
			var ciphertext1 = require('/lib/Crypt/AES').Encrypt(true, mpin, iv, key, cipher, mode);
			ciphertext1 = base64.encode(ciphertext1);
			
			var ciphertext2 = require('/lib/Crypt/AES').Encrypt(true, mpin_new, iv, key, cipher, mode);
			ciphertext2 = base64.encode(ciphertext2);
			
			var ciphertext3 = require('/lib/Crypt/AES').Encrypt(true, mpin_confirm, iv, key, cipher, mode);
			ciphertext3 = base64.encode(ciphertext3);
			
			activityIndicator.showIndicator();
			
			var xhr = require('/utils/XHR');
			var base64 = require('/lib/Crypt/Base64');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"CHANGEMPIN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"mPin":"'+ciphertext1+'",'+
					'"newMPin":"'+ciphertext2+'",'+
					'"reEnterNewMPin":"'+ciphertext3+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					destroy_mpin();
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_mpin();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xmlhttp = null;
			}
		}	
	});
	
	function destroy_mpin()
	{
		try{
			
			require('/utils/Console').info('############## Remove change mpin start ##############');
			
			_obj.winChangeMPIN.close();
			require('/utils/RemoveViews').removeViews(_obj.winChangeMPIN);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_mpin',destroy_mpin);
			require('/utils/Console').info('############## Remove change mpin end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_mpin', destroy_mpin);

};// ChangeMPINModal()