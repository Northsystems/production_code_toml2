exports.StaticPagesModal = function(page,section,url_slug)
{
	require('/lib/analytics').GATrackScreen(page);

	var _obj = {
		style : require('/styles/StaticPages').StaticPages, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		webView : null,
		currCode : null
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
		Ti.API.info(countryName[0]+'-'+countryName[1]+'-'+countryCode[0]+'-'+countryCode[1]+'-origSplit[0] = '+origSplit[0]+'-'+origSplit[1]);
		//United States-US Dollar-US-USD-origSplit[0] = US-USD
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	/*'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"originatingCurrency":"'+origSplit[1]+'",'+
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'",'+*/
	
	// CreateUI
	

   // orgCountry=US&orgCurrency=USD&orgCountryName=United+States';

	_obj.winModal = Ti.UI.createWindow(_obj.style.winModal);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = page;
	//Ti.API.info("Hi in static page" + _obj.lblHeader.text);
	
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack); //Event added by Sanjivani on 16Nov16 for bug.490
    _obj.imgBack.addEventListener('click',function(){
	//_obj.winModal.close();
	destroy_receiving_options();
	});
	var termsCondition;
	//Ti.App.Properties.setString('https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_US.jsp?origCountryCode=US&origCurrencyCode=USD&destCountryCode=IND&senderCountry=United%20States', termsCondition);
   _obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.enableZoomControls = true;
	_obj.webView.scalesPageToFit = false;
	//_obj.webView.scalesPageToFit = true;
	//Ti.API.info("PAGES:---",page);
	if(page == 'Privacy Policy'){
	_obj.webView.url = TiGlobals.staticPagesURL + 'pages.php?url_slug=privacy-policy&corridor='+_obj.currCode[0];
	}else if(page == 'Terms & Conditions')
	{
	//_obj.webView.url='https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry='+countryCode[0]+'&orgCurrency='+countryCode[1]+'&orgCountryName='+countryName[0];

	if(_obj.currCode[0] == 'US'){
   _obj.webView.url='https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_US.jsp?origCountryCode=US&origCurrencyCode=USD&destCountryCode=IND&senderCountry=United%20States';

	    }
	else if(_obj.currCode[0] == 'AUS'){
		//_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-aus.jsp?orgCountry=AUS&orgCurrency=AUD&orgCountryName=Australia';
		_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_AUS.jsp?origCountryCode=AUS&origCurrencyCode=AUD&destCountryCode=IND&senderCountry=Australia';
	}
	else if(_obj.currCode[0] == 'UK'){
		//_obj.webView.url ='https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-uk.jsp?orgCountry=UK&orgCurrency=GBP&orgCountryName=United%20Kingdom';
		_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_UK.jsp?origCountryCode=UK&origCurrencyCode=GBP&destCountryCode=IND&senderCountry=United%20Kingdom';
	}
	else if(_obj.currCode[0] == 'CAN'){  //CMN 84 
		//alert('Canada');
		// _obj.webView.url =  'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions-can.jsp';
	      //_obj.webView.url ='http://testnew.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry=CAN&orgCurrency=CAD&orgCountryName=Canada ';
           _obj.webView.url ='https://www.remit2india.com/sendmoneytoindia/Terms_and_conditions_CAN.jsp?origCountryCode=CAN&origCurrencyCode=CAD&destCountryCode=IND&senderCountry=Canada';
	}
	else{
		_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry='+countryCode[0]+'&orgCurrency='+countryCode[1]+'&orgCountryName='+countryName[0];
	}
	}
	else{
		
	}
	//_obj.webView.url = TiGlobals.staticPagesURL + 'master-static.php?section='+section+'&url_slug='+url_slug+'&corridor='+_obj.currCode[0];
	//_obj.webView.url = TiGlobals.staticPagesURL + 'terms-condition.php?section=about&url_slug=terms-and-conditions&corridor='+_obj.currCode[0];
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	/*var scrollView = Ti.UI.createScrollView({
		  showVerticalScrollIndicator: true,
		  showHorizontalScrollIndicator: false,
		  //height: '80%',
		  //width: '80%'
		});*/  //created and commented by sanjivani
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	//_obj.mainView.add(webview);
	//scrollView.add(_obj.mainView);
	_obj.globalView.add(_obj.mainView);
	//_obj.globalView.add(scrollView);
	_obj.winModal.add(_obj.globalView);
	_obj.winModal.open();
	
	function destroy_receiving_options() {
		try {
			require('/utils/Console').info('############## Remove win modal start ##############');
			_obj.winModal.close();
			require('/utils/RemoveViews').removeViews(_obj.winModal,_obj.globalView);
			_obj = null;

			require('/utils/Console').info('############## Remove win modal end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}
}; // StaticPagesModal()
