exports.AddBankPreConfirmationModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Bank Account PreConfirmation DEB');
	
	var _obj = {
		style : require('/styles/deb/AddBankPreConfirmation').AddBankPreConfirmation,
		winAddBankPreConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblSectionHead : null,
		sectionHeaderBorder : null,
		tblDetails : null,
		btnSubmit : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winAddBankPreConfirmation = Titanium.UI.createWindow(_obj.style.winAddBankPreConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddBankPreConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Bank Account Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'NetBanking Transfers';
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);

	/////////////////// Bank Account Pre Confirmation ///////////////////
	
	_obj.addBankPreConfirmationView = Ti.UI.createScrollView(_obj.style.addBankPreConfirmationView);
	
	_obj.tblDetails = Ti.UI.createTableView(_obj.style.tableView);
	
	for(var i=0; i<=6; i++)
	{
		var row = Ti.UI.createTableViewRow({
			top : 0,
			left : 0,
			right : 0,
			height : 56,
			backgroundColor : '#FFF'
		});
		
		if(TiGlobals.osname !== 'android')
		{
			row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
		}
		
		var lblBankDetails1 = Ti.UI.createLabel({
			height : 16,
			top : 10,
			left : 20,
			right : 20,
			textAlign : 'left',
			font : TiFonts.FontStyle('lblNormal14'),
			color : TiFonts.FontStyle('greyFont')
		});

		var lblBankDetails2 = Ti.UI.createLabel({
			//height : 16, 
			height : 20,       //enabled later changed for  bug:99 
			top : 30,
			bottom:10,
			left : 20,
			right : 20,
			textAlign : 'left',
			font : TiFonts.FontStyle('lblNormal14'),
			color : TiFonts.FontStyle('blackFont')
		});
		
		switch(i)
		{
			case 0:
				lblBankDetails1.text = 'First Name';
				lblBankDetails2.text = params.firstName;
			break;
			
			case 1:
				lblBankDetails1.text = 'Last Name';
				lblBankDetails2.text = params.lastName;
			break;
			
			case 2:
				lblBankDetails1.text = 'Bank Name';
				lblBankDetails2.text = params.bankName;
			break;
			
			case 3:
				lblBankDetails1.text = 'Nick Name for Your Bank Account';
				lblBankDetails2.text = params.nickName;
			break;
			
			case 4:
				lblBankDetails1.text = 'Account Type';
				lblBankDetails2.text = 'Savings';
			break;
			
			case 5:
				lblBankDetails1.text = 'BLZ Code/Sort Code';
				lblBankDetails2.text = params.sortCode;
			break;
			
			case 6:
				lblBankDetails1.text = 'Account Number';
				lblBankDetails2.text = params.accountNumber;
			break;
		}
		
		row.add(lblBankDetails1);
		row.add(lblBankDetails2);
		_obj.tblDetails.appendRow(row);
	}
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'CONFIRM';
	_obj.addBankPreConfirmationView.add(_obj.tblDetails);
	_obj.addBankPreConfirmationView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.addBankPreConfirmationView);
	_obj.globalView.add(_obj.mainView);
	_obj.winAddBankPreConfirmation.add(_obj.globalView);
	_obj.winAddBankPreConfirmation.open();
	
	_obj.btnSubmit.addEventListener('click',function(e){
		require('/js/deb/AddBankConfirmationModal').AddBankConfirmationModal(params);
		destroy_addbankpreconfirmation();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_addbankpreconfirmation();
				alertDialog = null;
			}
		});
	});
	
	_obj.winAddBankPreConfirmation.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_addbankpreconfirmation();
				alertDialog = null;
			}
		});
	});
	
	function destroy_addbankpreconfirmation()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove bank pre confirmation start ##############');
			
			_obj.winAddBankPreConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddBankPreConfirmation);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addbankpreconfirmation',destroy_addbankpreconfirmation);
			require('/utils/Console').info('############## Remove bank pre confirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addbankpreconfirmation', destroy_addbankpreconfirmation);
}; // AddBankPreConfirmationModal()
