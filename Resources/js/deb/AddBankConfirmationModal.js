exports.AddBankConfirmationModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Bank Account Confirmation DEB');
	
	var _obj = {
		style : require('/styles/deb/AddBankConfirmation').AddBankConfirmation,
		winAddBankConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		addBankConfirmationFinalView : null,
		webView : null, 
		btnSendMoney : null,
		
		//added by sanjivani on 25-march-17
		   lblMail:null,
		   lblSuccess:null,
           lblContent1:null,
           lblContent2:null,
           lblContent3:null
	};
	
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	
	_obj.winAddBankConfirmation = Titanium.UI.createWindow(_obj.style.winAddBankConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddBankConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Bank Account Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

   
    //Added by sanjivani on 25 march 17
     /*_obj.lblMail = Ti.UI.createLabel(_obj.style.lblMail);
     _obj.lblMail.text = Ti.App.Properties.getString('sourceCountryInfoEmail');*/
     
     
     _obj.lblSuccess = Ti.UI.createLabel(_obj.style.lblHeader1);
	 _obj.lblSuccess.text = 'Bank Account Addition Successful!';
	//lblHeader1 lblConfirm
	
	_obj.lblContent1 = Ti.UI.createLabel(_obj.style.lblConfirm);
	_obj.lblContent1.text = 'You have successfully added your Bank Account';
	

/*_obj.lblContent2 = createLabel();
function createLabel() {

    var label = Ti.UI.createLabel({
        top: 50,
        attributedString: Ti.UI.createAttributedString({
            text: 'Should you need any clarifications, please write to us at',
            attributes: [{
                type: Ti.UI.ATTRIBUTE_LINK,
                value: Ti.App.Properties.getString('sourceCountryInfoEmail'),
                range: [14, 29]
            }]
        })
    });

    label.addEventListener('link', function(e) {
        Ti.Platform.openURL(e.url);
    });

    return label;
}*/
	_obj.lblContent2 = Ti.UI.createLabel(_obj.style.lblConfirm);
	//_obj.lblContent2.text = 'Should you need any clarifications, please write to us at info@remit2india.com. You can also call UK toll free number 0800-0163404.';
	_obj.lblContent2.text = 'Should you need any clarifications, please write to us at '+Ti.App.Properties.getString('sourceCountryInfoEmail')+'.';
    
    _obj.lblContent3 = Ti.UI.createLabel(_obj.style.lblConfirm);
    _obj.lblContent3.text = 'You can also call '+countryCode[0]+' toll free number '+Ti.App.Properties.getString('sourceCountryTollFree') +'.';
	
	/////////////////// Bank Account Confirmation ///////////////////
	
	_obj.addBankConfirmationFinalView = Ti.UI.createScrollView(_obj.style.addBankConfirmationFinalView);
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'bank_account_confirmation_mobile.php?acc_type=deb&email='+Ti.App.Properties.getString('sourceCountryInfoEmail')+'&phone='+Ti.App.Properties.getString('sourceCountryTollFree');
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSendMoney.bottom = 20;
	_obj.btnSendMoney.title = 'SEND MONEY NOW';
	
	_obj.globalView.add(_obj.mainView);
	_obj.winAddBankConfirmation.add(_obj.globalView);
	_obj.winAddBankConfirmation.open();
	
	function successConfirmation()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"SENDERBANKACCADD",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"currencyCode":"'+countryCode[1]+'",'+
				'"paymodeCode":"DEB",'+
				'"firstName":"'+params.firstName+'",'+
			    '"lastName":"'+params.lastName+'",'+
			    '"bankName":"'+params.bankName+'",'+
			    '"bankAccNickName":"'+params.nickName+'",'+
			    '"referenceNo":"'+params.referenceNo+'",'+
			    '"accountType":"'+params.accountType+'",'+
			    '"accountNumber":"'+params.accountNumber+'",'+
			    '"sortCode":"'+params.sortCode+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				//_obj.addBankConfirmationFinalView.add(_obj.webView); //by sanjivani 24-march-17
				_obj.addBankConfirmationFinalView.add(_obj.lblSuccess);
	            _obj.addBankConfirmationFinalView.add(_obj.lblContent1);
	            _obj.addBankConfirmationFinalView.add(_obj.lblContent2);
	            _obj.addBankConfirmationFinalView.add(_obj.lblContent3);
	            
				_obj.addBankConfirmationFinalView.add(_obj.btnSendMoney);
				_obj.mainView.add(_obj.addBankConfirmationFinalView);
				
				_obj.btnSendMoney.addEventListener('click',function(e){
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						destroy_addbankconfirmation();
						require('/utils/RemoveViews').removeAllScrollableViews();
						require('/utils/PageController').pageController('transfer');
					}
					else
					{
						if(Ti.App.Properties.getString('mPIN') === '1')
						{
							require('/js/Passcode').Passcode();
						}
						else
						{
							require('/js/LoginModal').LoginModal();
						}
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addbankconfirmation();
				}
				else
				{
					var alertDialog = require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]);
					alertDialog.show();
			
					alertDialog.addEventListener('click', function(e) {
						alertDialog.hide();
						if (e.index === 0 || e.index === "0") {
							alertDialog = null;
							destroy_addbankconfirmation();
						}
					});
				}
			}
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	successConfirmation();
		
	_obj.imgClose.addEventListener('click',function(e){
		destroy_addbankconfirmation();
	});
	
	_obj.winAddBankConfirmation.addEventListener('androidback', function(){
		destroy_addbankconfirmation();
	});
	
	function destroy_addbankconfirmation()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove bank confirmation start ##############');
			
			_obj.winAddBankConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddBankConfirmation);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addbankconfirmation',destroy_addbankconfirmation);
			require('/utils/Console').info('############## Remove bank confirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addbankconfirmation', destroy_addbankconfirmation);
}; // AddBankConfirmationModal()
