function Main()
{
	try{
	////////// Include Style //////////
	var style = require('/styles/Main').Main;
	
	Ti.App.Properties.setString('pg','dashboard');
	
	var _self = null;
	var _headerView = null;
	var _headerBorder = null;
	var _lblLogo = null;
	var _exchangeView = null;
	var _exchangeMainView = null;
	var _lblExchangeTxt = null;
	var _exchangeRateView = null;
	var _imgCurrency = null;
	var _lblCurrency = null;
	var _imgExchangeArrow = null;
	
	// Main body
	
	var _footerView = null;
	var _imgMenu = null;
	var _imgRegister = null;
	var _imgLogin = null;
	var _registerView = null;
	var _loginView = null;
	
	_self = Ti.UI.createWindow (style.self);
	
	ActivityIndicator = require('/utils/ActivityIndicator');
	activityIndicator = new ActivityIndicator(_self);
	
	_headerView = Ti.UI.createView(style.headerView);
	_headerBorder = Ti.UI.createView(style.headerBorder); 
	_lblLogo = Ti.UI.createView(style.lblLogo);
	_exchangeView = Ti.UI.createView(style.exchangeView);
	_exchangeMainView = Ti.UI.createView(style.exchangeMainView);
	_exchangeMainView.opacity = 0;
	_lblExchangeTxt = Ti.UI.createLabel(style.lblExchangeTxt);
	_lblExchangeTxt.text = '';
	_exchangeRateView = Ti.UI.createView(style.exchangeRateView);
	_imgCurrency = Ti.UI.createImageView(style.imgCurrency);
	_lblCurrency = Ti.UI.createLabel(style.lblCurrency);
	_lblCurrency.text = '';
	_imgExchangeArrow = Ti.UI.createImageView(style.imgExchangeArrow);
	
	_exchangeView.addEventListener('click',function(e){
		require('/utils/PageController').pageController('exchange');
	});
	
	_scrollableView = Ti.UI.createScrollableView(style.scrollableView);
	
	_footerView = Ti.UI.createView(style.footerView);
	_registerView = Ti.UI.createView(style.registerView);
	//NL
	_imgRegister = Ti.UI.createImageView(style.imgRegister);
	//LI
	_lblWelcome = Ti.UI.createLabel(style.lblWelcome);
	_lblWelcome.text = 'Welcome';
	_lblName = Ti.UI.createLabel(style.lblName);
	_lblName.text = Ti.App.Properties.getString('firstName') +' '+ Ti.App.Properties.getString('lastName'); 
	
	_imgMenu = Ti.UI.createImageView(style.imgMenu);
	
	_loginView = Ti.UI.createView(style.loginView);
	
	//NL
	_imgLogin = Ti.UI.createImageView(style.imgLogin);
	//LI
	_customerView = Ti.UI.createView(style.customerView);
	_lblCustomer = Ti.UI.createLabel(style.lblCustomer);
	_lblCustomer.text = 'Customer ID: ';
	_lblCustomerId = Ti.UI.createLabel(style.lblCustomerId); 
	_lblCustomerId.text = Ti.App.Properties.getString('ownerId');
	_lblLastLogin = Ti.UI.createLabel(style.lblLastLogin);
	_lblLastLogin.text = 'Last login: ' + Ti.App.Properties.getString('lastLoginTime');
	 
	_headerView.add(_lblLogo);
	_exchangeMainView.add(_lblExchangeTxt);
	_exchangeRateView.add(_imgCurrency);
	_exchangeRateView.add(_lblCurrency);
	_exchangeMainView.add(_exchangeRateView);
	_exchangeView.add(_exchangeMainView);
	_exchangeView.add(_imgExchangeArrow);
	_headerView.add(_exchangeView);
	_headerView.add(_headerBorder);
	
	_footerView.add(_registerView);
	_footerView.add(_imgMenu);
	_footerView.add(_loginView);
	
	_loginView.addEventListener('click',function(e){
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{
			var strCopy = 'Customer ID\n'+_lblCustomerId.text+'\n\n'+_lblLastLogin.text;
			var alertDialog = require('/utils/AlertDialog').showAlert('',strCopy, [L('btn_copy'),L('btn_cancel')]);
			alertDialog.show();
		
			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if(e.index === 0 || e.index === "0") {
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Details copied to clipboard');
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Details copied to clipboard');
					}
					Ti.UI.Clipboard.clearText();
					Ti.UI.Clipboard.setText(strCopy);
				}
				alertDialog=null;
			});
		}
		else
		{
			if(Ti.App.Properties.getString('mPIN') === '1')
			{
				require('/js/Passcode').Passcode();
			}
			else
			{
				require('/js/LoginModal').LoginModal();
			}
		}		
	});
	
	_registerView.addEventListener('click',function(e){
		if(Ti.App.Properties.getString('loginStatus') === '0') 
		{
			require('/js/RegisterModal').RegisterModal();
		}
	});
	
	require('/utils/Console').info(Ti.App.Properties.getString('loginStatus'));
	
	_registerView.add(_lblWelcome);
	_registerView.add(_lblName);
	_customerView.add(_lblCustomer);
	_customerView.add(_lblCustomerId);
	_loginView.add(_customerView);
	_loginView.add(_lblLastLogin);
	_registerView.add(_imgRegister);
	_loginView.add(_imgLogin);
	
	function showLogin()
	{
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{
			_lblWelcome.show();
			_lblName.show();
			_lblCustomer.show();
			_lblCustomerId.show();
			_customerView.show();
			_lblLastLogin.show();
			
			_imgRegister.hide();
			_imgLogin.hide();
			
		}
		else
		{
			_lblWelcome.hide();
			_lblName.hide();
			_lblCustomer.hide();
			_lblCustomerId.hide();
			_customerView.hide();
			_lblLastLogin.hide();
			
			_imgRegister.show();
			_imgLogin.show();
		}
	}
	
	showLogin();
	
	function loginData(e)
	{
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{
			_lblName.text = Ti.App.Properties.getString('firstName') +' '+ Ti.App.Properties.getString('lastName');
			_lblCustomer.text = 'Customer ID: '; 
			_lblCustomerId.text = Ti.App.Properties.getString('ownerId');
			_lblLastLogin.text = Ti.App.Properties.getString('lastLoginTime');
			
			showLogin();
			
			Ti.App.fireEvent('logout');
		}
		else
		{
			showLogin();
			Ti.App.fireEvent('logout');
		}
	}
	
	Ti.App.addEventListener('loginData',loginData);
	
 	
	if(Ti.Platform.osname === 'android'){
	////Ti.API.info("********Inside if********");
    var platformTools = require('bencoding.android.tools').createPlatform(),
    wasInForeGround = true; //initially it was true
	    setInterval(function() 
	    {
	    	//Ti.API.info("********Inside set interval ---if---********");
	        var isInForeground = platformTools.isInForeground();
	        
	        //if (isInForeground == true)
	        if (wasInForeGround !== isInForeground) 
	        {
	        	 //Ti.App.fireEvent(isInForeground ? 'resumed' : 'paused');
	            if(isInForeground == true){
	            	
	            	////Ti.API.info("********Resume Called********");
	            	//Ti.API.info("****",TiGlobals.mytime);
	            	
	            	clearTimeout(TiGlobals.mytime);
	            	
	            } else {
	 					if(Ti.App.Properties.getString('loginStatus') !== '0'){
	 									
										TiGlobals.mytime = setTimeout(function(){
											
							
							    		TiGlobals.myEvent =  1;
							    		////Ti.API.info("1..Value of TiGlobals.myEvent in else timeout pause is",+TiGlobals.myEvent);
									    Ti.App.Properties.setString('loginStatus','0');
									    ////Ti.API.info("********setting loginstatus to 0********");
									    
									    Ti.App.fireEvent('loginData');
									    
									    if(Ti.App.Properties.getString('loginStatus') === '0' && TiGlobals.myEvent == '1')
									    {
											////Ti.API.info("Session expired");
											//getDate();
											function getDate() {
                      var currentTime = new Date();
                      var hours = currentTime.getHours();
                      var minutes = currentTime.getMinutes();
                      var month = currentTime.getMonth() + 1;
                      var day = currentTime.getDate();
                      var year = currentTime.getFullYear();
                  
                      return month+"/"+day+"/"+year+" - "+hours +":"+minutes;
                  };
		//Ti.API.info("Session expired",getDate());
										    var alertDialog = require('/utils/AlertDialog').showAlert(' ', 'Your session has expired. Please log in again.', [L('btn_ok')]);
									        alertDialog.show();
									        alertDialog.addEventListener('click', function(e) {
											    alertDialog.hide();
											    setTimeout(function(){
												    if(Ti.App.Properties.getString('mPIN') === '1'){
													    require('/js/Passcode').Passcode();
												     }else {
													    require('/js/LoginModal').LoginModal();	
												     }
											   	 },200);
											   	alertDialog=null;
									    	});
									  	  }
									  	  //},10000);
									  	 // },240000);  //4min
									  	 // },600000);  //10min
									  	   // },420000); //7min
									  	// },900000);  //15 min
									  //	},1080000); //18 min
									  //	},1800000);  //30 min---n
									  	},2700000);  //45 min
						}           	
	            }
	            wasInForeGround = isInForeground;
	           	        }
	    // },6000);  //5 sec  trial--100,200,1000,4000,4500,4900,5500,6000,7000,8000,10000,13000,15000,20000,50000,60000
	   // }, 10000);   //sec 10---n
	    },15000);  // 15 sec
	
	}
	
	
    _self.add(_headerView);
    _self.add(_scrollableView);
    _self.add(_footerView);
    
     //Activity tracker
    if(Ti.Platform.osname === 'android'){
   _self.addEventListener('touchstart', require('/utils/activity-tracker').didActivity);
   }
  else{
   //_self.addEventListener('touchstart', require('/utils/activity-tracker').didActivity);
}
//win.open();
    //IP ISSUE    
try{ 
	var xmlhttp = Ti.Network.createHTTPClient();
	              xmlhttp.onload = function()
	              {
		           var hostipInfo = JSON.parse(xmlhttp.responseText);
		           TiGlobals.ipAddress = hostipInfo.ip;
                   xmlhttp = null;
	              };
	
	             xmlhttp.onerror = function(e)
	             {
		            // require('/utils/AlertDialog').toast('Error in genrating IP Address.');
	             };
	             xmlhttp.open('GET', 'https://geoip.nekudo.com/api',true);
	             xmlhttp.send();
          }catch(e){}
	             
    function displayRates(e)
    {
    	require('/utils/Console').info(e.data);
    	//Ti.API.info("+++++++++++++++++++++++",e.data);
    	
    	//#61.22-INR-Indicative#59.16-INR-Guaranteed
    	//var strSplit = e.data.split('#');
    	//require('/utils/Console').info(strSplit.length);
    	////Ti.API.info("________________",strSplit);
    	//var valSplit = strSplit[1].split('-');
    		
		//_lblExchangeTxt.text = valSplit[2] + ' Rate';     //Header of dashboard dispalys guaranteed or indicatine rates
    	
    	var strSplit = e.data.split('-');
    	//Ti.API.info("________________",strSplit);
    	
    	_lblExchangeTxt.text = strSplit[0] + ' Rate';
		_lblCurrency.text = strSplit[1]+'*';   
		////Ti.API.info("??????????????",valSplit[0]);   
		_exchangeMainView.opacity = 1;
    	
    	/*if(strSplit.length === 2)
    	{
    		// Only one rate present
    		
    		var valSplit = strSplit[1].split('-');
    		
    		_lblExchangeTxt.text = valSplit[2] + ' Rate';
			_lblCurrency.text = valSplit[0]+'*';
			_exchangeMainView.opacity = 1;
    	}
    	else if(strSplit.length === 3)
    	{
    		var count = 1;
    		
    		function animate(opacity)
    		{
    			_exchangeMainView.animate({
				    opacity:opacity,
	    			duration:1500
				},function(){
					
					if(opacity === 1)
					{
						animate(0);	
					}
					else
					{
						if(count%2 === 0)
						{
							var valSplit = strSplit[2].split('-');
							_lblExchangeTxt.text = valSplit[2] + ' Rate';
							_lblCurrency.text = valSplit[0]+'*';
						}
						else
						{
							var valSplit = strSplit[1].split('-');
							_lblExchangeTxt.text = valSplit[2] + ' Rate';
							_lblCurrency.text = valSplit[0]+'*';	
						}
						count = count + 1;
						animate(1);
					}
				});
    		}
    		
    		animate(1);
    		
    	}
    	else
    	{
    		
    	}*/
    }
    
    Ti.App.addEventListener('displayRates',displayRates);
    
    function menuAnimate()
    {
    	if(TiGlobals.menuOpen === '0')
		{
			Ti.App.fireEvent('animateTop');
			_imgMenu.image = '/images/menu_close.png';
		}
		else
		{
			Ti.App.fireEvent('animateBottom');
			_imgMenu.image = '/images/menu.png';
		}
    }
    
    Ti.App.addEventListener('menuAnimate',menuAnimate);
    
    
    _imgMenu.clickTime = null;
 
	 _imgMenu.addEventListener('click',function(e){
	 	
	 	var currentTime = new Date();
	    if (currentTime - _imgMenu.clickTime < 600) {
	        return;
	    };
	    _imgMenu.clickTime = currentTime;
	 	
    	menuAnimate();
	});
	
	var Menu = require('/js/Menu');
	var menu = new Menu(_self);
	_self.add(menu);
	
	if(Ti.App.Properties.getString('sourceCountryCurCode') === '0')
	{
		sections({data:'location'});
	}
	else
	{
		sections({data:'dashboard'});
	}
	
    function sections(evt)
	{
		switch (evt.data)
		{
			case 'dashboard':
				var Dashboard = require('/js/Dashboard');
				var dash = new Dashboard(_self);
				_scrollableView.addView(dash);
			break;
			
			case 'location':
				var Location = require('/js/Location');
				var location = new Location(_self);
				_self.add(location);
			break;
			
			case 'exchange':
				var ExchangeRate = require('/js/ExchangeRate');
				var exchangeRate = new ExchangeRate(_self);
				_scrollableView.addView(exchangeRate);
				_scrollableView.scrollToView(exchangeRate);
			break;
			
			case 'transfer':
				var TransferMoney = require('/js/transfer/TransferMoney');
				var transfer = new TransferMoney(_self);
				_scrollableView.addView(transfer);
				_scrollableView.scrollToView(transfer);
			break;
			
			case 'myaccount':
				var MyAccount = require('/js/MyAccount');
				var myaccount = new MyAccount(_self);
				_scrollableView.addView(myaccount);
				_scrollableView.scrollToView(myaccount);
			break;
			
			case 'about':
				var About = require('/js/About');
				var about = new About(_self);
				_scrollableView.addView(about);
				_scrollableView.scrollToView(about);
			break;
			
			case 'how':
				var How = require('/js/How');
				var how = new How(_self);
				_scrollableView.addView(how);
				_scrollableView.scrollToView(how);
			break;
			
			case 'terms':
				var Terms = require('/js/Terms');
				var terms = new Terms(_self);
				_scrollableView.addView(terms);
				_scrollableView.scrollToView(terms);
			break;
			
			case 'privacy':
				var Privacy = require('/js/Privacy');
				var privacy = new Privacy(_self);
				_scrollableView.addView(privacy);
				_scrollableView.scrollToView(privacy);
			break;
			
			case 'power':
				var Power = require('/js/Power');
				var power = new Power(_self);
				_scrollableView.addView(power);
				_scrollableView.scrollToView(power);
			break;
			
			case 'contact':
				var Contact = require('/js/Contact');
				var contact = new Contact(_self);
				_scrollableView.addView(contact);
				_scrollableView.scrollToView(contact);
			break;
			
			case 'faq':
				var FAQ = require('/js/FAQ');
				var faq = new FAQ(_self);
				_scrollableView.addView(faq);
				_scrollableView.scrollToView(faq);
			break;
			
			case 'sending_options':
				var SendingOptions = require('/js/SendingOptions');
				var sending_options = new SendingOptions(_self);
				_scrollableView.addView(sending_options);
				_scrollableView.scrollToView(sending_options);
			break;
			
			case 'receiving_options':
				var ReceivingOptions = require('/js/ReceivingOptions');
				var receiving_options = new ReceivingOptions(_self);
				_scrollableView.addView(receiving_options);
				_scrollableView.scrollToView(receiving_options);
			break;
			
			case 'delivery_time':
				var DeliveryTime = require('/js/DeliveryTime');
				var delivery_time = new DeliveryTime(_self);
				_scrollableView.addView(delivery_time);
				_scrollableView.scrollToView(delivery_time);
			break;
			
			case 'delivery_locations':
				var DeliveryLocations = require('/js/DeliveryLocations');
				var delivery_locations = new DeliveryLocations(_self);
				_scrollableView.addView(delivery_locations);
				_scrollableView.scrollToView(delivery_locations);
			break;
			
			case 'refer_earn':
				var ReferEarn = require('/js/ReferEarn');
				var refer_earn = new ReferEarn(_self);
				_scrollableView.addView(refer_earn);
				_scrollableView.scrollToView(refer_earn);
			break;
			
			case 'money_on_referral':
				var MOR = require('/js/MOR');
				var mor = new MOR(_self);
				_scrollableView.addView(mor);
				_scrollableView.scrollToView(mor);
			break;
			
			case 'updateCDOB':  //CMN 78
				var upCDOB = require('/js/transfer/updateCitizenshipAndRecDOB');
				var cdob = new upCDOB(_self);
				_scrollableView.addView(cdob);
				_scrollableView.scrollToView(cdob);
			break;
		}
	}
	
	Ti.App.addEventListener('sections', sections);
    
	return _self;
	}catch(e){
		Ti.API.info("Exception handled globally and exception is--->",e);
	}
}; // Main()

module.exports = Main;
