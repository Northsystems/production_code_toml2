exports.KYCModal = function(conf) {
	require('/lib/analytics').GATrackScreen('KYC Form UK');

	var _obj = {
		style : require('/styles/kyc/KYC').KYC,
		winKYC : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,

		kycView : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,

		//Pratiksha
		lblDLIssuePlace : null,
		lblDLUnitedStates : null,
		//--

		stateView : null,
		lblDLIssueBy : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,

		doneDay : null,
		dayView : null,
		lblDay : null,
		txtDay : null,
		borderViewS30 : null,

		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS31 : null,

		tabView : null,
		tabSelView : null,
		lblPassport : null,
		lblDriving : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,

		btnSubmitPassport : null,
		btnSubmitDriving : null,
		btnUploadPassport : null,
		btnSubmit : null,

		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		tab : null,
		image : null,
		image1 : null,
		f2 : null,
		f : null,
		f1 : null,
      // f1:{}, 
		//
		uploadPPLabel : null,
		btnPPChooseFile : null,
		lblChooseFile : null,
		btnAddPP : null,
		uploadBorderView : null,

		tblPassport : null,
		//	tblDriving : null,
		rowPassport : null,
		tableData : [],

		uploadDLLabel : null,
		btnDLChooseFile : null,
		lblChooseFile1 : null,
		btnAddDL : null,
		uploadBorderView1 : null,

		tblDriving : null,
		rowDriving : null,
		tableData1 : [],

		rowCount : null,
		rowCount1 : null,
		passSuccessCheck : false,
		drivingSuccessCheck : false,
        
        imgUploadView:null,
		imgUploadViewP2 : null,
		imgUploadViewP3 : null,
		imgUploadViewP4 : null,
		imgUploadViewP5 : null,
		
        imgUploadView1:null,
		imgUploadViewD2 : null,
		imgUploadViewD3 : null,
		imgUploadViewD4 : null,
		imgUploadViewD5 : null,
		doneMobile:null,
		driveIssuDate:null,
		issueDateDtl:null

	};

	_obj.tab = 'passport';
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);

	_obj.winKYC = Titanium.UI.createWindow(_obj.style.winKYC);

	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYC);

	_obj.globalView = Ti.UI.createView(_obj.style.globalView);

	_obj.mainView = Ti.UI.createView(_obj.style.mainView);

	_obj.headerView = Ti.UI.createView(_obj.style.headerView);

	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);


    _obj.lblHeader.text = 'ID Document'; //added on 09-May-17 by sanjivani
	//Pratiksha
	//_obj.lblHeader.text = 'ID Limit Breached';
	//_obj.lblHeader.text = 'Help us know you better';


	//_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);

	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);

	_obj.headerView.add(_obj.lblHeader);
	//_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// KYC ///////////////////

	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);

	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblPassport = Ti.UI.createLabel(_obj.style.lblPassport);
	_obj.lblPassport.text = 'Passport Details';
	_obj.lblDriving = Ti.UI.createLabel(_obj.style.lblDriving);
	_obj.lblDriving.text = 'Driving Licence Details';

	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
	if (Ti.Platform.osname === 'android') {
		_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
		_obj.scrollableView.height = 950;
		_obj.scrollableView.scrollingEnabled = false;
	} else {
		_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
		_obj.scrollableView.height = Ti.UI.SIZE;
		_obj.scrollableView.scrollingEnabled = false;
	}

	// Passport
	_obj.passportView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote.text = 'Note';

	_obj.lblNoteTxt = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt.text = '\u00B7 If you are entering your passport details, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your passport';
	_obj.lblNoteTxt.top = 5;

	_obj.txtPassportNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtPassportNo.maxLength = 9;
	_obj.txtPassportNo.hintText = 'Passport No*';
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);

	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblPPIssueBy = Ti.UI.createLabel(_obj.style.lblCountry);
	//Pratiksha
	//_obj.lblPPIssueBy.text = 'Passport Issue Place*';
	_obj.lblPPIssueBy.text = 'Passport Issued By*';
	//--
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS33 = Ti.UI.createView(_obj.style.borderView);

	_obj.countryView.addEventListener('click', function() {
		country();
	});

	_obj.expiryView2 = Ti.UI.createView(_obj.style.dobView);
	_obj.lblPPIssueDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblPPIssueDt.text = 'Passport Issuance Date(dd/mm/yyyy)*';
	_obj.imgExpiry2 = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS52 = Ti.UI.createView(_obj.style.borderView);

	_obj.expiryView2.addEventListener('click', function(e) {
		TiGlobals.issuFlag = false;
		require('/utils/DatePicker').DatePicker(_obj.lblPPIssueDt, 'normal1');

	});
 
	
	_obj.expiryView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblPPExpDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblPPExpDt.text = 'Passport Expiry Date(dd/mm/yyyy)*';
	_obj.imgExpiry = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS34 = Ti.UI.createView(_obj.style.borderView);

	_obj.expiryView.addEventListener('click', function(e) {
		TiGlobals.issuFlag = true;
		require('/utils/DatePicker').DatePicker(_obj.lblPPExpDt, 'normal2');

	});

	
		function issueDateDtl(evt) { // CMN 55A
			    var issuDate = evt.data;
			    Ti.API.info("IN ISSUEDATE",issuDate);
			   
			     try{
			    _obj.lblPPIssueDt.value = issuDate.getTime();
			    Ti.API.info("IN ISSUDIF", _obj.lblPPIssueDt.value);
			    }catch(e){}
			    TiGlobals.issuFlag = true;
			}
			
			Ti.App.addEventListener('issueDateDtl',issueDateDtl);
		
		function expDateDtl(evt) { // CMN 55A
			    var expDate = evt.data;
			    Ti.API.info("IN EXPDATE",expDate);
			    try{
			   _obj.lblPPExpDt.value = expDate.getTime();
			    Ti.API.info("IN AGEDIF",_obj.lblPPExpDt.value );
			    }catch(e){}
			    TiGlobals.issuFlag = false;
			}
			
			Ti.App.addEventListener('expDateDtl',expDateDtl);
        
	_obj.txtPPIssuePlace = Ti.UI.createTextField(_obj.style.txtPassportNo);
	//Pratiksha
	//_obj.txtPPIssuePlace.hintText = 'Passport Issued by*';
	_obj.txtPPIssuePlace.hintText = 'Passport Issue Place*';
	//--
	_obj.borderViewS53 = Ti.UI.createView(_obj.style.borderView);
    
	_obj.txtPassportPersonalNo = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	//Pratiksha
	_obj.txtPassportPersonalNo.maxLength = 2;
	//_obj.txtPassportPersonalNo.maxLength = 16;
	//--
	_obj.txtPassportPersonalNo.hintText = 'Passport Personal No.*';
	
	_obj.borderViewS36 = Ti.UI.createView(_obj.style.borderView);
	_obj.txtPassportPersonalNo.keyboardType = Ti.UI.KEYBOARD_DECIMAL_PAD;
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	
    // _obj.txtDayNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
   
	_obj.txtPassportPersonalNo.keyboardToolbar = [_obj.doneMobile];
	_obj.doneMobile.addEventListener('click',function(){
		try{
		_obj.txtPassportPersonalNo.blur();
		}
		catch(e){
			Ti.API.info(e);
		}
	});
  

	_obj.lblMRZ = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblMRZ.text = 'MRZ No.*';
	_obj.lblMRZ.top = 15;
	_obj.mrzView = Ti.UI.createView(_obj.style.mrzView);

	_obj.txtMRZ1 = Ti.UI.createTextField(_obj.style.txtMRZ1);
	_obj.txtMRZ1.maxLength = 8;
	_obj.txtMRZ1.hintText = 'xxxxxxxx';
	_obj.borderMRZ1 = Ti.UI.createView(_obj.style.borderMRZ1);

	_obj.txtMRZ2 = Ti.UI.createTextField(_obj.style.txtMRZ2);
	_obj.borderMRZ2 = Ti.UI.createView(_obj.style.borderMRZ2);
	_obj.txtMRZ2.value = '<';
	_obj.txtMRZ2.editable = false;

	_obj.txtMRZ3 = Ti.UI.createTextField(_obj.style.txtMRZ3);
	_obj.txtMRZ3.maxLength = 19;
	_obj.txtMRZ3.hintText = 'xxxxxxxxxxxxxxxxxxx';
	_obj.borderMRZ3 = Ti.UI.createView(_obj.style.borderMRZ3);

	_obj.txtMRZ4 = Ti.UI.createTextField(_obj.style.txtMRZ4);
	_obj.borderMRZ4 = Ti.UI.createView(_obj.style.borderMRZ4);
	_obj.txtMRZ4.value = '<<<<<<<<<<<<<';
	_obj.txtMRZ4.editable = false;

	_obj.txtMRZ5 = Ti.UI.createTextField(_obj.style.txtMRZ5);
	_obj.borderMRZ5 = Ti.UI.createView(_obj.style.borderMRZ5);
	_obj.txtMRZ5.maxLength = 2;
	_obj.txtMRZ5.hintText = 'xx';

	_obj.imgPassport = Ti.UI.createImageView(_obj.style.imgPassport);

	_obj.imgUploadView = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewP2 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewP3 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewP4 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewP5 = Ti.UI.createView(_obj.style.imgUploadView);

	_obj.imgUploadViewP2.height = 0;
	_obj.imgUploadViewP3.height = 0;
	_obj.imgUploadViewP4.height = 0;
	_obj.imgUploadViewP5.height = 0;

	_obj.imgUploadViewD1 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewD2 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewD3 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewD4 = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadViewD5 = Ti.UI.createView(_obj.style.imgUploadView);

	_obj.imgUploadViewD2.height = 0;
	_obj.imgUploadViewD3.height = 0;
	_obj.imgUploadViewD4.height = 0;
	_obj.imgUploadViewD5.height = 0;

	//_obj.imgUploadView.top = 20;
	//_obj.imgUploadView.visible = false;

	//by sanjivani
	_obj.uploadPPLabel = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.uploadPPLabel.top = 25;
	_obj.uploadPPLabel.text = "Please Upload your Passport documents";

	_obj.btnPPChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
	_obj.btnPPChooseFile.title = 'Choose File';

	_obj.btnAddPP = Ti.UI.createButton(_obj.style.btnAddPP);

	_obj.lblChooseFile = Ti.UI.createLabel(_obj.style.lblChooseFile);

	_obj.uploadBorderView = Ti.UI.createView(_obj.style.borderView);
	_obj.uploadBorderView.top = 60;

	_obj.tblPassport = Ti.UI.createTableView(_obj.style.tableView);

	_obj.rowPassport = Ti.UI.createTableViewRow({
		hasChild : false,
		height : 60
	});

	
	_obj.imgView = Ti.UI.createImageView(_obj.style.imgView);
	_obj.lblDocSize = Ti.UI.createLabel(_obj.style.lblDocSize);
	_obj.btnUpload = Ti.UI.createButton(_obj.style.btnUpload);

	//_obj.btnSubmitPassport = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitPassport = Ti.UI.createButton(_obj.style.suBbutton);
	_obj.btnSubmitPassport.title = 'SUBMIT';

	_obj.btnCancelPassport = Ti.UI.createButton(_obj.style.btnCancel);
	_obj.btnCancelPassport.title = 'CANCEL';

	// Driving
	_obj.drivingView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote1 = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote1.text = 'Note';

	_obj.lblNoteTxt1 = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt1.text = '\u00B7 If you are entering your driving license number, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your driving license';
	_obj.lblNoteTxt1.top = 5;

	//added for new fields in driving licence
	_obj.txtDrivingLNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtDrivingLNo.maxLength = 9;
	_obj.txtDrivingLNo.hintText = 'Driving License No*';
	_obj.borderViewS37 = Ti.UI.createView(_obj.style.borderView);

	//Pratiksha
	// _obj.countryView1 = Ti.UI.createView(_obj.style.countryView);
	// _obj.lblPPIssueBy1 = Ti.UI.createLabel(_obj.style.lblPPIssueBy);
	// _obj.lblPPIssueBy1.text = 'Driving License Issue Place*';
	// _obj.imgCountry1 = Ti.UI.createImageView(_obj.style.imgCountry);
	// _obj.borderViewS38 = Ti.UI.createView(_obj.style.borderView);
	//
	// _obj.countryView1.addEventListener('click', function() {
	// country1();
	// });

	_obj.lblDLIssuePlace = Ti.UI.createLabel(_obj.style.lblDLIssuePlace);
	_obj.lblDLIssuePlace.text = 'Driving License Issue Place*';

	_obj.lblDLCountry = Ti.UI.createLabel(_obj.style.lblDLUnitedStates);
	_obj.lblDLCountry.text = 'United States';
	_obj.lblDLCountry.top = 0;
	_obj.lblDLCountry.height = 18;
	_obj.lblDLCountry.bottom = 0;
	//--
   //issuance
	_obj.expiryView3 = Ti.UI.createView(_obj.style.dobView); 
	_obj.lblDLIssueDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDLIssueDt.text = 'Driving License Issuance Date(dd/mm/yyyy)*';
	_obj.imgExpiry3 = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS55 = Ti.UI.createView(_obj.style.borderView);

	_obj.expiryView3.addEventListener('click', function(e) {
		require('/utils/DatePicker').DatePicker(_obj.lblDLIssueDt, 'normal3');

	});

	_obj.expiryView1 = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDLExpDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDLExpDt.text = 'Driving License Expiry Date(dd/mm/yyyy)*';
	_obj.imgExpiry1 = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS39 = Ti.UI.createView(_obj.style.borderView);

	_obj.expiryView1.addEventListener('click', function(e) {
		require('/utils/DatePicker').DatePicker(_obj.lblDLExpDt, 'normal4');

	});
	
	
	 //For DL validations
            function driveIssueDateDtl(evt) { 
            	try{
			    var driveIssuDate = evt.data;
			    Ti.API.info("IN DRIVEISSUEDATE",driveIssuDate);
			  _obj.lblDLIssueDt.value = driveIssuDate;
			   
			     TiGlobals.exp1 =driveIssuDate;
			     	var issueDateTime = driveIssuDate.getTime();
			     	var totalTime= issueDateTime + 631138520000;
			     	Ti.API.info("IN SPLIT",totalTime);
			     	if(Ti.Platform.osname === 'Android'){
			     	TiGlobals.expiDLDate = new Date(totalTime);
			     	}	
			     	else{
			     	TiGlobals.expiDLDate = new Date(issueDateTime);
			     	}
			       
			     _obj.lblDLExpDt.value = TiGlobals.expiDLDate;
			    Ti.API.info("IN DRIVEXP",_obj.lblDLExpDt.value);
			  
			    }catch(e){}
			   TiGlobals.drivIssueFlag = true;
			}
			
			Ti.App.addEventListener('driveIssueDateDtl',driveIssueDateDtl);      
      
      function driveDateDtl(evt) { // CMN 55A
      	try{
			    var driveDate = evt.data;
			    Ti.API.info("IN DRIVEEXP",driveDate);
			    
			  _obj.lblDLExpDt.value = driveDate;
			    Ti.API.info("IN AGEDIF", _obj.lblDLExpDt.value );
			    }catch(e){}
			    TiGlobals.drivIssueFlag = false;
			}
			
			Ti.App.addEventListener('driveDateDtl',driveDateDtl);
           	//Pratiksha
	// _obj.txtPassportNo3 = Ti.UI.createTextField(_obj.style.txtPassportNo);
	// _obj.txtPassportNo3.hintText = 'Driving License Issued by*';
	// _obj.borderViewS54 = Ti.UI.createView(_obj.style.borderView);
	//

	_obj.stateView = Ti.UI.createView(_obj.style.stateView);
	_obj.stateView.top = 10;
	_obj.lblDLIssueBy = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblDLIssueBy.text = 'Driving License Issued By*';
	_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
	_obj.borderViewS54 = Ti.UI.createView(_obj.style.borderView);

	_obj.stateView.addEventListener('click', function() {
		state();
	});
	//---

	_obj.txtDrivingLPresonalNo = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	_obj.txtDrivingLPresonalNo.maxLength = 16;
	_obj.txtDrivingLPresonalNo.hintText = 'Driving License Personal No.*';
	_obj.borderViewS41 = Ti.UI.createView(_obj.style.borderView);

	_obj.txtDrivingLNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtDrivingLNo.maxLength = 16;
	//Sanjivani code
	_obj.txtDrivingLNo.hintText = 'Driving License No*';
	_obj.borderViewS42 = Ti.UI.createView(_obj.style.borderView);

	_obj.imgUploadView1 = Ti.UI.createView(_obj.style.imgUploadView);
	

	//by sanjivani
	_obj.uploadDLLabel = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.uploadDLLabel.top = 25;
	_obj.uploadDLLabel.text = "Please Upload your Driving License documents";

	_obj.btnDLChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
	_obj.btnDLChooseFile.title = 'Choose File';

	_obj.btnAddDL = Ti.UI.createButton(_obj.style.btnAddPP);

	_obj.lblChooseFile1 = Ti.UI.createLabel(_obj.style.lblChooseFile);

	_obj.uploadBorderView1 = Ti.UI.createView(_obj.style.borderView);
	_obj.uploadBorderView1.top = 60;

	_obj.tblDriving = Ti.UI.createTableView(_obj.style.tableView);

	_obj.rowDriving = Ti.UI.createTableViewRow({
		hasChild : false,
		height : 60
	});

	_obj.imgView1 = Ti.UI.createImageView(_obj.style.imgView);
	_obj.lblDocSize1 = Ti.UI.createLabel(_obj.style.lblDocSize);
	_obj.btnUpload1 = Ti.UI.createButton(_obj.style.btnUpload);

	_obj.btnSubmitDriving = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitDriving.title = 'SUBMIT';

	_obj.btnCancelDriving = Ti.UI.createButton(_obj.style.btnCancel);
	_obj.btnCancelDriving.title = 'CANCEL';

	_obj.passportView.add(_obj.lblNote);
	_obj.passportView.add(_obj.lblNoteTxt);
	_obj.passportView.add(_obj.txtPassportNo);
	_obj.passportView.add(_obj.borderViewS32);

	_obj.expiryView2.add(_obj.lblPPIssueDt);
	_obj.expiryView2.add(_obj.imgExpiry2);
	_obj.passportView.add(_obj.expiryView2);
	_obj.passportView.add(_obj.borderViewS52);

	_obj.expiryView.add(_obj.lblPPExpDt);
	_obj.expiryView.add(_obj.imgExpiry);
	_obj.passportView.add(_obj.expiryView);
	_obj.passportView.add(_obj.borderViewS34);

	//Pratiksha
	_obj.passportView.add(_obj.txtPPIssuePlace);
	_obj.passportView.add(_obj.borderViewS53);

	_obj.countryView.add(_obj.lblPPIssueBy);
	_obj.countryView.add(_obj.imgCountry);
	_obj.passportView.add(_obj.countryView);
	_obj.passportView.add(_obj.borderViewS33);

	// _obj.countryView.add(_obj.lblPPIssueBy);
	// _obj.countryView.add(_obj.imgCountry);
	// _obj.passportView.add(_obj.countryView);
	// _obj.passportView.add(_obj.borderViewS33);
	//
	// _obj.passportView.add(_obj.txtPPIssuePlace);
	// _obj.passportView.add(_obj.borderViewS53);
	//--

	_obj.passportView.add(_obj.txtPassportPersonalNo);
	_obj.passportView.add(_obj.borderViewS36);
	_obj.passportView.add(_obj.lblMRZ);
	_obj.mrzView.add(_obj.txtMRZ1);
	_obj.mrzView.add(_obj.borderMRZ1);
	_obj.mrzView.add(_obj.txtMRZ2);
	_obj.mrzView.add(_obj.borderMRZ2);
	_obj.mrzView.add(_obj.txtMRZ3);
	_obj.mrzView.add(_obj.borderMRZ3);
	_obj.mrzView.add(_obj.txtMRZ4);
	_obj.mrzView.add(_obj.borderMRZ4);
	_obj.mrzView.add(_obj.txtMRZ5);
	_obj.mrzView.add(_obj.borderMRZ5);
	_obj.passportView.add(_obj.mrzView);
	_obj.passportView.add(_obj.imgPassport);

	/*_obj.imgUploadView.add(_obj.btnUploadPassport);
	_obj.imgUploadView.add(_obj.imgView);
	_obj.imgUploadView.add(_obj.lblDocSize);
	_obj.imgUploadView.add(_obj.btnUpload);*/

	//_obj.imgUploadView.add(_obj.uploadPPLabel);
	_obj.passportView.add(_obj.uploadPPLabel);
	_obj.imgUploadView.add(_obj.btnPPChooseFile);
	_obj.imgUploadView.add(_obj.imgView);

	//Pratiksha -- commented for single file upload
	//_obj.imgUploadView.add(_obj.btnAddPP);
	//---

	_obj.imgUploadView.add(_obj.lblChooseFile);
	_obj.imgUploadView.add(_obj.uploadBorderView);
	//_obj.rowPassport.add(_obj.btnPPChooseFile);
	_obj.rowCount = 1;
	//_obj.rowPassport.add(_obj.imgView);
	//_obj.rowPassport.add(_obj.btnAddPP);  //temp commented for single upload build
	//_obj.rowPassport.add(_obj.lblChooseFile);
	//_obj.rowPassport.add(_obj.uploadBorderView);
	//_obj.tableData.push(_obj.rowPassport);
	//_obj.rowPassport.add(_obj.imgUploadView);//temp
	//_obj.tblPassport.appendRow(_obj.rowPassport); //temp

	//_obj.tblPassport.data = _obj.tableData;

	_obj.passportView.add(_obj.imgUploadView);
	_obj.passportView.add(_obj.imgUploadViewP2);
	_obj.passportView.add(_obj.imgUploadViewP3);
	_obj.passportView.add(_obj.imgUploadViewP4);
	_obj.passportView.add(_obj.imgUploadViewP5);
	//_obj.passportView.add(_obj.tblPassport);

	_obj.passportView.add(_obj.btnSubmitPassport);
	_obj.passportView.add(_obj.btnCancelPassport);

	_obj.drivingView.add(_obj.lblNote1);
	_obj.drivingView.add(_obj.lblNoteTxt1);
	_obj.drivingView.add(_obj.txtDrivingLNo);
	_obj.drivingView.add(_obj.borderViewS37);

	_obj.expiryView3.add(_obj.lblDLIssueDt);
	_obj.expiryView3.add(_obj.imgExpiry3);
	_obj.drivingView.add(_obj.expiryView3);
	_obj.drivingView.add(_obj.borderViewS55);

	_obj.expiryView1.add(_obj.lblDLExpDt);
	_obj.expiryView1.add(_obj.imgExpiry1);
	_obj.drivingView.add(_obj.expiryView1);
	_obj.drivingView.add(_obj.borderViewS39);

	//Pratiksha
	// _obj.countryView1.add(_obj.lblPPIssueBy1);
	// _obj.countryView1.add(_obj.imgCountry1);
	// _obj.drivingView.add(_obj.countryView1);
	// _obj.drivingView.add(_obj.borderViewS38);

	_obj.drivingView.add(_obj.lblDLIssuePlace);
	_obj.drivingView.add(_obj.lblDLCountry);

	_obj.stateView.add(_obj.lblDLIssueBy);
	_obj.stateView.add(_obj.imgState);
	_obj.drivingView.add(_obj.stateView);
	_obj.drivingView.add(_obj.borderViewS54);

	// _obj.drivingView.add(_obj.txtPassportNo3);
	// _obj.drivingView.add(_obj.borderViewS54);
	//---

	_obj.drivingView.add(_obj.uploadDLLabel);

	/*
	_obj.rowDriving.add(_obj.btnDLChooseFile);
	_obj.rowCount1 = 1;
	_obj.rowDriving.add(_obj.imgView1);
	//_obj.rowDriving.add(_obj.btnAddDL); //temp commented for single upload build
	_obj.rowDriving.add(_obj.lblChooseFile1);
	_obj.rowDriving.add(_obj.uploadBorderView1);
	_obj.tableData1.push(_obj.rowDriving);
	_obj.tblDriving.data = _obj.tableData1;
	*/
	//_obj.passportView.add(_obj.imgUploadView);
	//_obj.drivingView.add(_obj.tblDriving);

	_obj.imgUploadView1.add(_obj.btnDLChooseFile);
	_obj.imgUploadView1.height = 60;
	_obj.imgUploadView1.add(_obj.imgView1);

	//Pratiksha -- commented for single file upload
	//_obj.imgUploadView1.add(_obj.btnAddDL);
	//
	_obj.imgUploadView1.add(_obj.lblChooseFile1);
	_obj.imgUploadView1.add(_obj.uploadBorderView1);

	_obj.drivingView.add(_obj.imgUploadView1);
	_obj.drivingView.add(_obj.imgUploadViewD2);
	_obj.drivingView.add(_obj.imgUploadViewD3);
	_obj.drivingView.add(_obj.imgUploadViewD4);
	_obj.drivingView.add(_obj.imgUploadViewD5);
	/*_obj.imgUploadView1.add(_obj.btnUploadDriving1);
	 _obj.imgUploadView1.add(_obj.imgView1);
	 _obj.imgUploadView1.add(_obj.lblDocSize1);
	 _obj.imgUploadView1.add(_obj.btnUpload1);
	 _obj.drivingView.add(_obj.imgUploadView1);*/

	_obj.drivingView.add(_obj.btnSubmitDriving);
	_obj.drivingView.add(_obj.btnCancelDriving);

	_obj.scrollableView.views = [_obj.passportView, _obj.drivingView];

	_obj.kycView.add(_obj.tabView);
	_obj.tabView.add(_obj.lblPassport);
	_obj.tabView.add(_obj.lblDriving);
	_obj.tabView.add(_obj.tabSelView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.tabView.add(_obj.tabSelIconView);
	_obj.kycView.add(_obj.scrollableView);

	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYC.add(_obj.globalView);
	_obj.winKYC.open();

	function changeTabs(selected) {
		if (selected === 'passport') {
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblPassport.color = TiFonts.FontStyle('whiteFont');
			_obj.lblPassport.backgroundColor = '#6F6F6F';
			_obj.lblDriving.color = TiFonts.FontStyle('blackFont');
			_obj.lblDriving.backgroundColor = '#E9E9E9';
		} else {
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblDriving.color = TiFonts.FontStyle('whiteFont');
			_obj.lblDriving.backgroundColor = '#6F6F6F';
			_obj.lblPassport.color = TiFonts.FontStyle('blackFont');
			_obj.lblPassport.backgroundColor = '#E9E9E9';
		}
	}


	_obj.tabView.addEventListener('click', function(e) {
		if (e.source.sel === 'passport' && _obj.tab !== 'passport') {
			_obj.scrollableView.movePrevious();
			changeTabs('passport');
			_obj.tab = 'passport';
		}

		if (e.source.sel === 'driving' && _obj.tab !== 'driving') {
			_obj.scrollableView.moveNext();
			changeTabs('driving');
			_obj.tab = 'driving';
		}
	});

	/*function validateAlpaNumeric()
	{
	var amtinv = _obj.txtLicense;
	var validStr = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ";

	for(i=0;i<=_obj.txtLicense.value.length-1;i++)
	{
	if(validStr.indexOf(amtinv.charAt(i))==-1)
	{
	require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in '+msg,[L('btn_ok')]).show();
	return false;
	break;
	}
	}
	};

	validateAlpaNumeric();*/

	//_obj.btnUploadDriving1.addEventListener('click',function(e){
	_obj.btnDLChooseFile.addEventListener('click', function(e) {
		if (TiGlobals.osname === 'android') {
			if (Ti.Media.hasCameraPermissions()) {
				uploadImage1();
			} else {
				Ti.Media.requestCameraPermissions(function(e) {
					if (e.success === true) {
						uploadImage1();
					} else {
						/*alert("Access denied, error: " + e.error);*/
					}
				});
			}
		} else {
			uploadImage1();
		}

	});

	_obj.btnSubmitPassport.addEventListener('click', function(e) {// submit button

		if (_obj.tab === 'passport') {
			
			Ti.API.info("IN SUBMIT:---",_obj.lblPPExpDt.value);  //CMN 55A
		    Ti.API.info("IN SUBMIT1:---",_obj.lblPPIssueDt.value);
	        Ti.API.info("IN SUBMIT2:---",_obj.lblPPExpDt.value - _obj.lblPPIssueDt.value);
			var timeDiff = Math.abs(_obj.lblPPExpDt.value - _obj.lblPPIssueDt.value);
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            
			// Passport No
			if (_obj.txtPassportNo.value.trim() === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a passport number', [L('btn_ok')]).show();
				_obj.txtPassportNo.value = '';
				_obj.txtPassportNo.focus();
				return;
			} else {

			}
			// Passport Issuance Date
			if (_obj.lblPPIssueDt.text === 'Passport Issuance Date(dd/mm/yyyy)*') {
				require('/utils/AlertDialog').showAlert('', 'Please provide Passport Issuance Date', [L('btn_ok')]).show();
				return;
			} else {

			}
			
			
			// Passport Expiry Date
			if (_obj.lblPPExpDt.text === 'Passport Expiry Date(dd/mm/yyyy)*') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a passport expiry date', [L('btn_ok')]).show();
				return;
			} 
	     if(diffDays < 1825){  //cmn 55a
		     require('/utils/AlertDialog').showAlert('','Passport Expiry date & Issuance Date should have minimum of 5 yrs difference.',[L('btn_ok')]).show();
    		_obj.lblPPExpDt.value = '';
    		//_obj.lblPPExpDt.focus();
    		_obj.lblPPExpDt.text = 'Passport Expiry Date(dd/mm/yyyy)*';
    		
    		return;
	     }
			// Passport Issue place
			if (_obj.txtPPIssuePlace.value.trim() === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a passport issue Place', [L('btn_ok')]).show();
				_obj.txtPPIssuePlace.value = '';
				_obj.txtPPIssuePlace.focus();
				return;
			} else if(_obj.txtPPIssuePlace.value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in issue place.',[L('btn_ok')]).show();
	    		_obj.txtPPIssuePlace.focus();
	    		_obj.txtPPIssuePlace.value = '';
				return;
			}else {

			}
			
			// Passport Issued by
			if (_obj.lblPPIssueBy.text === 'Passport Issued By*') {
				require('/utils/AlertDialog').showAlert('', 'Please select a passport country', [L('btn_ok')]).show();
				return;
			} else {

			}
            

			// Passport Personal No
			if (_obj.txtPassportPersonalNo.value === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a passport personal number', [L('btn_ok')]).show();
				_obj.txtPassportPersonalNo.value = '';
				_obj.txtPassportPersonalNo.focus();
				return;
			}
			//Pratiksha
			// else {
			//
			// }

			if (_obj.txtPassportPersonalNo.value.search("[^0-9]") >= 0) {
				require('/utils/AlertDialog').showAlert('', 'Only numbers are allowed in Passport Personal No', [L('btn_ok')]).show();
				_obj.txtPassportPersonalNo.value = '';
				_obj.txtPassportPersonalNo.focus();
				return;
			}
			//--

			// MRZ Validation Starts
			var mrz1 = _obj.txtMRZ1.value;
			var mrz2 = _obj.txtMRZ3.value;
			var mrz3 = _obj.txtMRZ5.value;
			if (mrz1.trim() === '' || mrz2.trim() === '' || mrz3.trim() === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide your valid MRZ Number', [L('btn_ok')]).show();
				return;
			} else if (mrz1.length != 8) {
				require('/utils/AlertDialog').showAlert('', 'The first section of MRZ Number should be of 8 digits', [L('btn_ok')]).show();
				_obj.txtMRZ1.value = '';
				_obj.txtMRZ1.focus();
				return;
			} else if (mrz2.length != 19) {
				require('/utils/AlertDialog').showAlert('', 'The second section of MRZ Number should be of 19 digits', [L('btn_ok')]).show();
				_obj.txtMRZ2.value = '';
				_obj.txtMRZ2.focus();
				return;
			} else if (mrz3.length != 2) {
				require('/utils/AlertDialog').showAlert('', 'The third section of MRZ Number should be of 2 digits', [L('btn_ok')]).show();
				_obj.txtMRZ3.value = '';
				_obj.txtMRZ3.focus();
				return;
			} else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtMRZ1.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Special characters not allowed in MRZ no.',[L('btn_ok')]).show();
    		            _obj.txtMRZ1.value = '';
			    		_obj.txtMRZ1.focus();
						return;
					}else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtMRZ3.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Special characters not allowed in MRZ no.',[L('btn_ok')]).show();
    		            _obj.txtMRZ3.value = '';
			    		_obj.txtMRZ3.focus();
						return;
					}else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtMRZ5.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Special characters not allowed in MRZ no.',[L('btn_ok')]).show();
    		            _obj.txtMRZ5.value = '';
			    		_obj.txtMRZ5.focus();
						return;
					}else {

			}
			
			
		}

		var dateSplit = _obj.lblPPIssueDt.text.split('/');
		var dt = dateSplit[0] + '-' + dateSplit[1] + '-' + dateSplit[2];
        Ti.API.info("DT****",dt);
		var dateSplit1 = _obj.lblPPExpDt.text.split('/');
		var dt1 = dateSplit1[0] + '-' + dateSplit1[1] + '-' + dateSplit1[2];
		var RequestId = Math.floor((Math.random() * 1000000000) + 10000);
       Ti.API.info("DT111****",dt1);
		 //Commented on 29th Nov for unable to upload document -testnew server
		//var targetFile = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
  var targetFile = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');

 
		var filename = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy') + ".jpg";
		var docname = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
		var relativepath = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/";
        if(_obj.f != null){
				
		
		var xmlHttp = Ti.Network.createHTTPClient();
		activityIndicator.showIndicator();
		xmlHttp.onload = function(e) {
			//Ti.API.info("In onload3:--",xmlHttp.responseText);

			activityIndicator.hideIndicator();

   
			var test = JSON.parse(xmlHttp.responseText);
			Ti.API.info("In xmlresponseText:--", test.result);
			Ti.API.info("In xmlresponseText:--", test.path);
			var path = test.path;
			var pathlength = test.path.length;
			var dot = path.lastIndexOf(".");

			var extn = path.substring(dot, pathlength);
			Ti.API.info("In extn:--", extn);
			targetFile = targetFile + extn;

		

			if (test.result === 'true' || test.result === true || test.result === 1) {
				activityIndicator.showIndicator();
				Ti.API.info("Inside if:--");
				var xhr = require('/utils/XHR');
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					//Pratiksha
					post : '{' + '"requestId":"' + RequestId + '",' + 
					'"channelId":"' + TiGlobals.channelId + '",' + 
					'"partnerId":"' + TiGlobals.partnerId + '",' + 
					'"requestName":"CUSTKYCIDDETAILSWITHDOCUPLOAD",' + 
					'"ipAddress":"' + TiGlobals.ipAddress + '",' + 
					'"loginId":"' + Ti.App.Properties.getString('loginId') + '",' + 
					'"sessionId":"' + Ti.App.Properties.getString('sessionId') + '",' + 
					'"ownerId":"' + Ti.App.Properties.getString('ownerId') + '",' + 
					'"IDType":"Passport",' + 
					'"IDNumber":"' + _obj.txtPassportNo.value + '",' + 
					'"IDIssueBy":"' + _obj.lblPPIssueBy.text + '",' + 
					'"IDIssueAt":"' + _obj.txtPPIssuePlace.value + '",' + 
					'"IDIssueDate":"' + dt + '",' + 
					'"IDIssueExpiryDate":"' + dt1 + '",' + 
					'"passportPersonalNo":"' + _obj.txtPassportPersonalNo.value + '",' + 
					'"originatingCountryName":"' + countryName[0] + '",' + 
					'"originatingCountry":"' + countryCode[0] + '",' + 
					'"mrzLine":"' + mrz1 + mrz2 + mrz3 + '",' + 
					'"sourceFlag":"01",'+ 
					'"docPathArray":["' + targetFile + '"]' + 
					'}',
					//post : '{' + '"requestId":"' + RequestId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"requestName":"CUSTKYCIDDETAILSWITHDOCUPLOAD",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"loginId":"' + Ti.App.Properties.getString('loginId') + '",' + '"sessionId":"' + Ti.App.Properties.getString('sessionId') + '",' + '"ownerId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"IDType":"Passport",' + '"IDNumber":"' + _obj.txtPassportNo.value + '",' + '"IDIssueBy":"' + _obj.txtPPIssuePlace.value + '",' + '"IDIssueAt":"' + _obj.lblPPIssueBy.text + '",' + '"IDIssueDate":"' + dt + '",' + '"IDIssueExpiryDate":"' + dt1 + '",' + '"passportPersonalNo":"' + _obj.txtPassportPersonalNo.value + '",' + '"originatingCountryName":"' + countryName[0] + '",' + '"originatingCountry":"' + countryCode[0] + '",' + '"mrzLine":"' + mrz1 + mrz2 + mrz3 + '",' + '"docPathArray":["' + targetFile + '"]' + '}',
					//--
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhrSuccess(evt) {
					activityIndicator.hideIndicator();
					
					Ti.API.info("evt.result.responseCode", evt.result.responseCode);
					if(evt.result.responseCode =='01'){
					    if(TiGlobals.limitBreachSI == true){
                      	       TiGlobals.limitBreachSI = false;
                            if(TiGlobals.osname === 'android')
							{	
							  require('/utils/AlertDialog').toast(evt.result.responseMessage);
							  require('/js/schedule_payment/ScheduleTransactionPreConfirmationModal').ScheduleTransactionPreConfirmationModal(conf);
                              destroy_kyc();
							}
							else
							{
                                 setTimeout(function(){
                                 	        require('/js/schedule_payment/ScheduleTransactionPreConfirmationModal').ScheduleTransactionPreConfirmationModal(conf);
					                        destroy_kyc();
					                      },1600);
								require('/utils/AlertDialog').iOSToast(evt.result.responseMessage);
							}
                         }

                           else{
					       TiGlobals.usKycDtlFlag = true;//added on 9-may-17
					      
							if(TiGlobals.osname === 'android')
							{	
								require('/utils/AlertDialog').toast(evt.result.responseMessage);
                                                               require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
					                 destroy_kyc();
							}
							else
							{
                                           setTimeout(function(){
					                         require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
					                          destroy_kyc();
					                      },1600);
								require('/utils/AlertDialog').iOSToast(evt.result.responseMessage);
							}
					
                       }
				}
				else{
				//else if(evt.result.responseCode =='04'){
						
						require('/utils/AlertDialog').showAlert('',evt.result.responseMessage,[L('btn_ok')]).show();
						_obj.txtPassportNo.value = '';
				       _obj.txtPassportNo.focus();
				       return;
					}
				
					xhr = null;
				}

				function xhrError(evt) {
					Ti.API.info("In xhrError:--");

					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}

			} else {
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('', 'Error uploading document. Please try again later.', [L('btn_ok')]).show();
			}
		};

		xmlHttp.onerror = function(e) {
			activityIndicator.hideIndicator();
		};

		Ti.API.info("In xhrSuccess:--");

		Ti.API.info("In xhrSuccess1:--");

		xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php', true);
		//Ti.API.info("In xhrSuccess2:--");
        
		xmlHttp.send({
			doc : _obj.f.read(),
			source : 'APP',
			name : filename,
			docname : docname,
			uploadpath : relativepath
		});
		}
		else{
			require('/utils/AlertDialog').showAlert('', 'Please select document to upload.', [L('btn_ok')]).show();
			
		}

	});

	//function submitKYC()//btnSubmitDriving
	_obj.btnSubmitDriving.addEventListener('click', function(e) {// submit button

		if (_obj.tab === 'driving') {
			// Driving License No
			if (_obj.txtDrivingLNo.value.trim() === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a driving license number', [L('btn_ok')]).show();
				_obj.txtDrivingLNo.value = '';
				_obj.txtDrivingLNo.focus();
				return;
			} else {

			}

			// driving license Issuance Date
			if (_obj.lblDLIssueDt.text === 'Driving License Issuance Date(dd/mm/yyyy)*') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a driving license issuance date', [L('btn_ok')]).show();
				return;
			} else {

			}
	
			
			// driving license Expiry Date
			if (_obj.lblDLExpDt.text === 'Driving License Expiry Date(dd/mm/yyyy)*') {
				require('/utils/AlertDialog').showAlert('', 'Please provide Driving License expiry date', [L('btn_ok')]).show();
				return;
			} else {

			}
			// Driving License Issue Place

			//Pratiksha
			// if (_obj.lblPPIssueBy1.text === 'Driving License Issue Place*') {
			// require('/utils/AlertDialog').showAlert('', 'Please select a driving license country', [L('btn_ok')]).show();
			// return;
			// } else {
			//
			// }

			// Driving license Issued By
			if (_obj.lblDLIssueBy.text === 'Driving License Issued By*') {
				require('/utils/AlertDialog').showAlert('', 'Please provide Driving License Issued By', [L('btn_ok')]).show();
				return;
			} else {
			}

			// if (_obj.txtPassportNo3.value.trim() === '') {
			// require('/utils/AlertDialog').showAlert('', 'Please provide a driving license issued by', [L('btn_ok')]).show();
			// _obj.txtPassportNo3.value = '';
			// _obj.txtPassportNo3.focus();
			// return;
			// } else {
			//
			// }
			//---

			try {
				//Driving Licence No
				if (_obj.txtLicense.value.trim() === '')//||_obj.txtLicense.value.search("[^a-zA-Z0-9]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('', 'Please provide 16 digit alpha-numeric driving licence number', [L('btn_ok')]).show();
					_obj.txtLicense.value = '';
					_obj.txtLicense.focus();
					return;
				} else if (_obj.txtLicense.value.length > 0) {
					if (require('/lib/toml_validations').checkBeginningSpace(_obj.txtLocality.value) === false) {
						require('/utils/AlertDialog').showAlert('', 'Space not allowed at the beginning Driving Licence No', [L('btn_ok')]).show();
						_obj.txtLicense.value = '';
						_obj.txtLicense.focus();
						//_obj.kycView.scrollTo(0,0);
						return;
					} else {
						if (_obj.txtLicense.value.search("[^a-zA-Z0-9 ]") >= 0) {
							require('/utils/AlertDialog').showAlert('', 'Only alphabets and numbers are allowed in Driving Licence  field', [L('btn_ok')]).show();
							_obj.txtLicense.value = '';
							_obj.txtLicense.focus();
							//_obj.kycView.scrollTo(0,0);
							return;
						}

					}
				} else {

				}
			
			} catch(e) {
			}
		}
		if(_obj.f1!= null){
	
		var dateSplit = _obj.lblDLIssueDt.text.split('/');
		var dt = dateSplit[0] + '-' + dateSplit[1]+ '-' + dateSplit[2];

		var dateSplit1 = _obj.lblDLExpDt.text.split('/');
		var dt1 = dateSplit1[0] + '-' +dateSplit1[1]+ '-' + dateSplit1[2];
	Ti.API.info("_obj.tblPassport.data lenght", +dt1);
	
	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
var firstDate = new Date(dateSplit1[2],dateSplit1[1],dateSplit1[0]);
var secondDate = new Date(dateSplit[2],dateSplit[1],dateSplit[0]);

Ti.API.info("************", firstDate);	
Ti.API.info("************", secondDate);	

// firstDate.setHours(0, 0, 0); 
// secondDate.setHours(0, 0, 0);
var drivediffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
		Ti.API.info("************", drivediffDays);	
	
		//var rowCount = _obj.tblPassport.rowCount;
		Ti.API.info("_obj.tblPassport.data lenght", +_obj.rowCount1);
		var RequestId = Math.floor((Math.random() * 1000000000) + 10000);
		//for single upload
		//var targetFile1 = docUploadPath1 + "/" + TiGlobals.partnerId + "/" + docTypeCode1 + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
           var targetFile1 = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');

 
		var filename1 = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy') + ".jpg";
		var docname1 = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
		var relativepath1 = docUploadPath1 + "/" + TiGlobals.partnerId + "/" + docTypeCode1 + "/";
        
        
			/*Ti.API.info("IN SUBMIT:---",dt);  //CMN 55A
			Ti.API.info("IN SUBMIT:---",_obj.lblDLExpDt.text);
			//var a=_obj.lblDLExpDt.text;
			//var b= _obj.lblDLIssueDt.text;
		    Ti.API.info("IN SUBMIT1:---",dt1);
	        Ti.API.info("IN SUBMIT2:---",_obj.lblDLExpDt.value.getTime() -_obj.lblDLIssueDt.value.getTime());
			var drivetimeDiff = Math.abs(_obj.lblDLExpDt.value.getTime() -_obj.lblDLIssueDt.value.getTime());
            var drivediffDays = Math.ceil(drivetimeDiff / (1000 * 3600 * 24)); 
            Ti.API.info("IN SUBMIT:---",drivediffDays);*/
			
	       if(drivediffDays > 7305){  //cmn 55a
				 Ti.API.info("IN SUBMIT:---");
		     require('/utils/AlertDialog').showAlert('','Driving Expiry date & Issuance Date should have minimum of 20 yrs difference.',[L('btn_ok')]).show();
    		_obj.lblDLExpDt.value = '';
    		//_obj.lblPPExpDt.focus();
    		_obj.lblDLExpDt.text = 'Driving License Expiry Date(dd/mm/yyyy)*';
            return;
	     }
       
        
        
        
        if(_obj.f1!=null){
		var xmlHttp = Ti.Network.createHTTPClient();
		activityIndicator.showIndicator();
		xmlHttp.onload = function(e) {
			
			//Ti.API.info("In onload3:--",xmlHttp.responseText);
			activityIndicator.hideIndicator();
			var test = JSON.parse(xmlHttp.responseText);
			Ti.API.info("In xmlresponseText:--", test.result);
			Ti.API.info("In xmlresponseText:--", test.path);
			var path = test.path;
			var pathlength = test.path.length;
			var dot = path.lastIndexOf(".");

			var extn = path.substring(dot, pathlength);
			Ti.API.info("In extn:--", extn);
			targetFile1 = targetFile1 + extn;

			if (test.result === 'true' || test.result === true || test.result === 1) {
				activityIndicator.showIndicator();
				Ti.API.info("Inside if:--");
				var xhr = require('/utils/XHR');
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					//Pratiksha
					post : '{' + 
					'"requestId":"' + RequestId + '",' + 
					'"channelId":"' + TiGlobals.channelId + '",' + 
					'"partnerId":"' + TiGlobals.partnerId + '",' + 
					'"requestName":"CUSTKYCIDDETAILSWITHDOCUPLOAD",' + 
					'"ipAddress":"' + TiGlobals.ipAddress + '",' + 
					'"loginId":"' + Ti.App.Properties.getString('loginId') + '",' + 
					'"sessionId":"' + Ti.App.Properties.getString('sessionId') + '",' +
					'"ownerId":"' + Ti.App.Properties.getString('ownerId') + '",' + 
					'"IDType":"Driving Licence",' + 
					'"IDNumber":"' + _obj.txtDrivingLNo.value + '",' + 
					'"IDIssueBy":"' + _obj.lblDLIssueBy.text + '",' + 
					'"IDIssueAt":"' + _obj.lblDLCountry.text + '",' + 
					'"IDIssueDate":"' + dt + '",' + 
					'"IDIssueExpiryDate":"' + dt1 + '",' + 
					'"passportPersonalNo":" ",' + 
					'"originatingCountryName":"' + countryName[0] + '",' + 
					'"originatingCountry":"' + countryCode[0] + '",' + 
                                        '"sourceFlag":"01",'+ 
					'"mrzLine":" ",' + 
					'"docPathArray":["' + targetFile1 + '"]' + 
					'}',
					// post : '{' + '"requestId":"' + RequestId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"requestName":"CUSTKYCIDDETAILSWITHDOCUPLOAD",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"loginId":"' + Ti.App.Properties.getString('loginId') + '",' + '"sessionId":"' + Ti.App.Properties.getString('sessionId') + '",' + '"ownerId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"IDType":"Driving Licence",' + '"IDNumber":"' + _obj.txtDrivingLNo.value + '",' + '"IDIssueBy":"' + _obj.txtPassportNo3.value + '",' + '"IDIssueAt":"' + _obj.lblPPIssueBy1.text + '",' + '"IDIssueDate":"' + dt + '",' + '"IDIssueExpiryDate":"' + dt1 + '",' + '"passportPersonalNo":" ",' + '"originatingCountryName":"' + countryName[0] + '",' + '"originatingCountry":"' + countryCode[0] + '",' + '"mrzLine":" ",' + '"docPathArray":["' + targetFile1 + '"]' + '}',
					//--
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhrSuccess(evt) {
					activityIndicator.hideIndicator();
					
					Ti.API.info("evt.result.responseCode", evt.result.responseCode);
					
					if(evt.result.responseCode =='01'){
					     //require('/utils/AlertDialog').showAlert('',evt.result.responseMessage,[L('btn_ok')]).show();	
                      if(TiGlobals.limitBreachSI == true){ //added on 14-July-17 for CMN55-A
                      	TiGlobals.limitBreachSI= false;
                         if(TiGlobals.osname === 'android')
							{	
							  require('/utils/AlertDialog').toast(evt.result.responseMessage);
							  require('/js/schedule_payment/ScheduleTransactionPreConfirmationModal').ScheduleTransactionPreConfirmationModal(conf);
                              destroy_kyc();
							}
							else
							{
                                 setTimeout(function(){
                                 	        require('/js/schedule_payment/ScheduleTransactionPreConfirmationModal').ScheduleTransactionPreConfirmationModal(conf);
					                        destroy_kyc();
					                      },1600);
								require('/utils/AlertDialog').iOSToast(evt.result.responseMessage);
							}
                      }

                       else{
					       TiGlobals.usKycDtlFlag = true;//added on 9-may-17
					      
							if(TiGlobals.osname === 'android')
							{	
								require('/utils/AlertDialog').toast(evt.result.responseMessage);
                                  require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
					                 destroy_kyc();
							}
							else
							{
                                       setTimeout(function(){
					                         require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
					                          destroy_kyc();
					                      },1600);
								require('/utils/AlertDialog').iOSToast(evt.result.responseMessage);
							}
					
                       }
					//---
                                     }
					xhr = null;
				}

				function xhrError(evt) {
					Ti.API.info("In xhrError:--");

					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}

			} else {
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('', 'Error uploading document. Please try again later.', [L('btn_ok')]).show();
			}
		};

		xmlHttp.onerror = function(e) {
			activityIndicator.hideIndicator();
		};

		Ti.API.info("In xhrSuccess:--");

		Ti.API.info("In xhrSuccess1:--");

		xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php', true);
		//Ti.API.info("In xhrSuccess2:--");

		xmlHttp.send({
			doc : _obj.f1.read(),
			source : 'APP',
			name : filename1,
			docname : docname1,
			uploadpath : relativepath1
		});
		}
		}
		else{
			require('/utils/AlertDialog').showAlert('', 'Please select document to upload.', [L('btn_ok')]).show();
			
		}

	});

	//_obj.btnUploadPassport.addEventListener('click',function(e){
	_obj.btnPPChooseFile.addEventListener('click', function(e) {
		if (TiGlobals.osname === 'android') {
			if (Ti.Media.hasCameraPermissions()) {
				uploadImage();
			} else {
				Ti.Media.requestCameraPermissions(function(e) {
					if (e.success === true) {
						uploadImage();
					} else {
						/*alert("Access denied, error: " + e.error);*/
					}
				});
			}
		} else {
			uploadImage();
		}

	});

	/*var docType = [];
	 var docRequestId = [];
	 var docTypeCode = [];
	 var docUploadPath =[];
	 var targetFile = [];*/

	var docType = null;
	var docRequestId = null;
	var docTypeCode = null;
	var docUploadPath = null;
	var targetFile = [];

	/*var filename = [];
	 var docname = [];
	 var relativepath = [];	*/
	var filename = null;
	var docname = null;
	var relativepath = null;

	function addMultipleDoc() {
		Ti.API.info("inside addMultipleDoc(), rowCount", _obj.rowCount);
		_obj.rowCount = _obj.rowCount + 1;
		var i = _obj.rowCount;
		var image = null;
		var f = null;

		if (_obj.rowCount <= 5) {

			Ti.API.info("Start of if---");
			Ti.API.info("_obj.rowCount: ", _obj.rowCount);

			var imgView = Ti.UI.createImageView(_obj.style.imgView);
			var btnPPChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
			btnPPChooseFile.title = 'Choose File';
			var btnMinus = Ti.UI.createButton(_obj.style.btnMinus);
			var lblChooseFile = Ti.UI.createLabel(_obj.style.lblChooseFile);
			var uploadBorderView = Ti.UI.createView(_obj.style.borderView);
			Ti.API.info("added UI");

			switch(_obj.rowCount) {

			case 2:
				_obj.imgUploadViewP2.height = 60;
				Ti.API.info("_obj.imgUploadViewP2.height ", _obj.imgUploadViewP2.height);

				_obj.imgUploadViewP2.add(btnPPChooseFile);
				_obj.imgUploadViewP2.add(lblChooseFile);
				_obj.imgUploadViewP2.add(btnMinus);
				_obj.imgUploadViewP2.add(imgView);
				_obj.imgUploadViewP2.add(uploadBorderView);

				break;

			case 3:
				_obj.imgUploadViewP3.height = 60;
				Ti.API.info("_obj.imgUploadViewP3.height ", _obj.imgUploadViewP3.height);

				_obj.imgUploadViewP3.add(btnPPChooseFile);
				_obj.imgUploadViewP3.add(lblChooseFile);
				_obj.imgUploadViewP3.add(btnMinus);
				_obj.imgUploadViewP3.add(imgView);
				_obj.imgUploadViewP3.add(uploadBorderView);
				break;

			case 4:
				_obj.imgUploadViewP4.height = 60;
				Ti.API.info("_obj.imgUploadViewP4.height ", _obj.imgUploadViewP4.height);

				_obj.imgUploadViewP4.add(btnPPChooseFile);
				_obj.imgUploadViewP4.add(lblChooseFile);
				_obj.imgUploadViewP4.add(btnMinus);
				_obj.imgUploadViewP4.add(imgView);
				_obj.imgUploadViewP4.add(uploadBorderView);
				break;

			case 5:
				_obj.imgUploadViewP5.height = 60;
				Ti.API.info("_obj.imgUploadViewP5.height ", _obj.imgUploadViewP5.height);

				_obj.imgUploadViewP5.add(btnPPChooseFile);
				_obj.imgUploadViewP5.add(lblChooseFile);
				_obj.imgUploadViewP5.add(btnMinus);
				_obj.imgUploadViewP5.add(imgView);
				_obj.imgUploadViewP5.add(uploadBorderView);
				break;

			}

			Ti.API.info("End of if---");
		}

		btnMinus.addEventListener('click', function(e) {

			switch(_obj.rowCount) {

			case 2:
				_obj.imgUploadViewP2.removeAllChildren();
				_obj.imgUploadViewP2.height = 0;
				Ti.API.info("_obj.imgUploadViewP2.height ", _obj.imgUploadViewP2.height);
				break;

			case 3:
				_obj.imgUploadViewP3.removeAllChildren();
				_obj.imgUploadViewP3.height = 0;
				Ti.API.info("_obj.imgUploadViewP3.height ", _obj.imgUploadViewP3.height);
				break;

			case 4:
				_obj.imgUploadViewP4.removeAllChildren();
				_obj.imgUploadViewP4.height = 0;
				Ti.API.info("_obj.imgUploadViewP4.height ", _obj.imgUploadViewP4.height);
				break;

			case 5:
				_obj.imgUploadViewP5.removeAllChildren();
				_obj.imgUploadViewP5.height = 0;
				Ti.API.info("_obj.imgUploadViewP5.height ", _obj.imgUploadViewP5.height);
				break;

			}

			_obj.rowCount = _obj.rowCount - 1;
		});

		btnPPChooseFile.addEventListener('click', function(e) {
			if (TiGlobals.osname === 'android') {
				if (Ti.Media.hasCameraPermissions()) {
					uploadImagePassport();
				} else {
					Ti.Media.requestCameraPermissions(function(e) {
						if (e.success === true) {
							uploadImagePassport();
						} else {
							/*alert("Access denied, error: " + e.error);*/
						}
					});
				}
			} else {
				uploadImagePassport();
			}

			function uploadImagePassport() {
				Ti.API.info("****************___IN UPLOAD IMAGE pASSPORT*****");
				activityIndicator.showIndicator();
				var xhr = require('/utils/XHR_BCM');

				xhr.call({
					url : TiGlobals.appURLBCM,
					get : '',
					post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"' + TiGlobals.bcmConfig + '",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"source":"' + TiGlobals.bcmSource + '",' + '"type":"document",' + '"docType":"Passport",' + '"platform":"' + TiGlobals.osname + '",' + '"corridor":"' + countryName[0] + '"' + '}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhrSuccess(evt) {
					try {
						activityIndicator.hideIndicator();
						if (evt.result.status === "S") {
							require('/utils/Console').info('Result****** ======== ' + evt);
							require('/utils/Console').info('Result ======== ' + evt.result);
							docType = evt.result.response.docType;
							docTypeCode = evt.result.response.docType;
							docUploadPath = evt.result.response.uploadpath;

							/*docType.push(evt.result.response.docType);
							 docTypeCode.push(evt.result.response.docType);
							 docUploadPath.push(evt.result.response.uploadpath);*/

							_obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, docType + '.jpg');
							_obj.imgUpload = 0;

							var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('', [L('takePicture'), L('selectPicture')], -1);
							optionDialogPhoto.show();

							optionDialogPhoto.addEventListener('click', function(evt) {
								if (optionDialogPhoto.options[evt.index] !== L('btn_cancel')) {
									if (evt.index === 0) {
										// Take Picture

										Ti.Media.showCamera({
											showControls : true,
											mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
											autohide : true,
											allowEditing : false,
											autorotate : true,
											success : function(event) {
												Ti.API.info("Media events:", +JSON.stringify(event));

												image = event.media;

												_obj.f.write(image);

												if (_obj.f.size < 5000000) {
													if (TiGlobals.osname === 'android') {
														imgView.image = _obj.f.nativePath;
													} else {
														imgView.image = _obj.f.resolve();
													}

													_obj.lblDocSize.text = '(File Size : ' + bytesToSize(_obj.f.size) + ')';
													imgView.show();
													/*_obj.btnUpload.show();
													_obj.btnUploadPassport.hide();*/
													//btnMinus.show();
													btnPPChooseFile.hide();
													lblChooseFile.hide();

												} else {
													require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();

													//	alert('The file you upload should not exceed the file size of 5 MB.');
													//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
												}
											}
										});
									}

									if (evt.index === 1) {
										// Obtain an image from the gallery

										Titanium.Media.openPhotoGallery({
											success : function(event) {
												Ti.API.info("Media events:", +event);
												Ti.API.info("Media events name:", +event.media);

												if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
													image = event.media;
													_obj.f.write(image);

													Ti.API.info(event.media.name);
													Ti.API.info(event.media.type);
													Ti.API.info(event.media.tmp_name);
													Ti.API.info(event.media.error);
													Ti.API.info(event.media.size);
													alert(f1.name);
													alert(f1.type);
													alert(f1.tmp_name);
													alert(f1.error);
													alert(f1.size);

													Ti.API.info(f1.name);
													Ti.API.info(f1.type);
													Ti.API.info(f1.tmp_name);
													Ti.API.info(f1.error);
													Ti.API.info(f1.size);
													Ti.API.info(f1.read());

													Ti.API.info("Media obj:", +_obj.f);
													if (_obj.f.size < 10000000)//5000000 original
													{
														imgView.image = _obj.f.read();
														_obj.lblDocSize.text = '(File Size : ' + bytesToSize(_obj.f.size) + ')';
														imgView.show();
														/*_obj.btnUpload.show();
														 _obj.btnUploadPassport.hide();*/
														btnMinus.show();
														btnPPChooseFile.hide();
														lblChooseFile.hide();

													} else {
														require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
														//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
													}
												}
											},
											cancel : function() {
											}
										});
									}

									optionDialogPhoto = null;
								}
							});

						} else {
							if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
								require('/lib/session').session();
								destroy_kyc();
							} else {
								require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
							}
						}
						xhr = null;
					} catch(e) {
					}
				}

				function xhrError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}

			}

		});
		/* }else{

		 }*/
		/*btnAddPP.addEventListener('click',function(e){

		 addMultipleDoc();

		 } );*/

	}//end of passport multiple doc function

	//_obj.btnUpload.addEventListener('click',function(e){
	_obj.btnAddPP.addEventListener('click', function(e) {

		try {

			Ti.API.info("in btnAddPP Click");
			addMultipleDoc();
		} catch(e) {
			Ti.API.info(e);
		}
	});

	var docType1 = null;
	var docRequestId1 = null;
	var docTypeCode1 = null;
	var docUploadPath1 = null;
	var targetFile1 = [];

	var filename1 = [];
	var docname1 = [];
	var relativepath1 = [];

	function addMultipleDocDL() {

		Ti.API.info("inside addMultipleDocDL(), rowCount1", _obj.rowCount1);

		_obj.rowCount1 = _obj.rowCount1 + 1;

		// var rowDriving1 = Ti.UI.createTableViewRow({
		// hasChild : false,
		// height : 60
		// });

		// var imgView1 = Ti.UI.createImageView(_obj.style.imgView);
		// var btnDLChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
		// btnDLChooseFile.title = 'Choose File';

		// var btnMinus1 = Ti.UI.createButton(_obj.style.btnMinus);
		// var lblChooseFile1 = Ti.UI.createLabel(_obj.style.lblChooseFile);
		var image1 = null;
		//var uploadBorderView1 = Ti.UI.createView(_obj.style.borderView);
		var f1 = null;
		//_obj.passportView.add(_obj.uploadPPLabel);

		if (_obj.rowCount1 <= 5) {

			// rowDriving1.add(btnDLChooseFile);
			// //_obj.rowPassport.add(_obj.imgView);
			//
			// rowDriving1.add(lblChooseFile1);
			// rowDriving1.add(btnMinus1);
			// rowDriving1.add(imgView1);
			// rowDriving1.add(uploadBorderView1);
			// _obj.tableData1.push(rowDriving1);
			//
			// _obj.tblDriving.data = _obj.tableData1;
			//

			Ti.API.info("Start of if  1---");
			Ti.API.info("_obj.rowCount1: ", _obj.rowCount1);

			var imgView1 = Ti.UI.createImageView(_obj.style.imgView);
			var btnDLChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
			btnDLChooseFile.title = 'Choose File';
			var btnMinus1 = Ti.UI.createButton(_obj.style.btnMinus);
			var lblChooseFile1 = Ti.UI.createLabel(_obj.style.lblChooseFile);
			var uploadBorderView1 = Ti.UI.createView(_obj.style.borderView);
			Ti.API.info("added UI");

			switch(_obj.rowCount1) {

			case 2:
				_obj.imgUploadViewD2.height = 60;
				Ti.API.info("_obj.imgUploadViewD2.height ", _obj.imgUploadViewD2.height);

				_obj.imgUploadViewD2.add(btnDLChooseFile);
				_obj.imgUploadViewD2.add(lblChooseFile1);
				_obj.imgUploadViewD2.add(btnMinus1);
				_obj.imgUploadViewD2.add(imgView1);
				_obj.imgUploadViewD2.add(uploadBorderView1);

				break;

			case 3:
				_obj.imgUploadViewD3.height = 60;
				Ti.API.info("_obj.imgUploadViewD3.height ", _obj.imgUploadViewD3.height);

				_obj.imgUploadViewD3.add(btnDLChooseFile);
				_obj.imgUploadViewD3.add(lblChooseFile1);
				_obj.imgUploadViewD3.add(btnMinus1);
				_obj.imgUploadViewD3.add(imgView1);
				_obj.imgUploadViewD3.add(uploadBorderView1);
				break;

			case 4:
				_obj.imgUploadViewD4.height = 60;
				Ti.API.info("_obj.imgUploadViewD4.height ", _obj.imgUploadViewD4.height);

				_obj.imgUploadViewD4.add(btnDLChooseFile);
				_obj.imgUploadViewD4.add(lblChooseFile1);
				_obj.imgUploadViewD4.add(btnMinus1);
				_obj.imgUploadViewD4.add(imgView1);
				_obj.imgUploadViewD4.add(uploadBorderView1);
				break;

			case 5:
				_obj.imgUploadViewD5.height = 60;
				Ti.API.info("_obj.imgUploadViewD5.height ", _obj.imgUploadViewD5.height);

				_obj.imgUploadViewD5.add(btnDLChooseFile);
				_obj.imgUploadViewD5.add(lblChooseFile1);
				_obj.imgUploadViewD5.add(btnMinus1);
				_obj.imgUploadViewD5.add(imgView1);
				_obj.imgUploadViewD5.add(uploadBorderView1);
				break;

			}

			//_obj.rowPassport.add(_obj.imgView);
			//  _obj.tableData.push(rowPassport);
			// _obj.tblPassport.data = _obj.tableData;

			Ti.API.info("End of if---");
		}

		btnMinus1.addEventListener('click', function(e) {

			switch(_obj.rowCount1) {

			case 2:
				_obj.imgUploadViewD2.removeAllChildren();
				_obj.imgUploadViewD2.height = 0;
				Ti.API.info("_obj.imgUploadViewD2.height ", _obj.imgUploadViewD2.height);
				break;

			case 3:
				_obj.imgUploadViewD3.removeAllChildren();
				_obj.imgUploadViewD3.height = 0;
				Ti.API.info("_obj.imgUploadViewD3.height ", _obj.imgUploadViewD3.height);
				break;

			case 4:
				_obj.imgUploadViewD4.removeAllChildren();
				_obj.imgUploadViewD4.height = 0;
				Ti.API.info("_obj.imgUploadViewD4.height ", _obj.imgUploadViewD4.height);
				break;

			case 5:
				_obj.imgUploadViewD5.removeAllChildren();
				_obj.imgUploadViewD5.height = 0;
				Ti.API.info("_obj.imgUploadViewD5.height ", _obj.imgUploadViewD5.height);
				break;

			}

			_obj.rowCount1 = _obj.rowCount1 - 1;
			// _obj.tableData1.pop(rowDriving1);
			// _obj.tblDriving.data = _obj.tableData1;

		});

		btnDLChooseFile.addEventListener('click', function(e) {
			if (TiGlobals.osname === 'android') {
				if (Ti.Media.hasCameraPermissions()) {
					uploadImageDriving();
				} else {
					Ti.Media.requestCameraPermissions(function(e) {
						if (e.success === true) {
							uploadImageDriving();
						} else {
							/*alert("Access denied, error: " + e.error);*/
						}
					});
				}
			} else {
				uploadImageDriving();
			}

			function uploadImageDriving() {
				activityIndicator.showIndicator();
				var xhr = require('/utils/XHR_BCM');

				xhr.call({
					url : TiGlobals.appURLBCM,
					get : '',
					post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"' + TiGlobals.bcmConfig + '",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"source":"' + TiGlobals.bcmSource + '",' + '"type":"document",' + '"docType":"Driving Licence",' + '"platform":"' + TiGlobals.osname + '",' + '"corridor":"' + countryName[0] + '"' + '}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhrSuccess(evt) {
					try {
						activityIndicator.hideIndicator();
						if (evt.result.status === "S") {
							require('/utils/Console').info('Result****** ======== ' + evt);
							require('/utils/Console').info('Result ======== ' + evt.result);
							docType1 = evt.result.response.docType;
							docTypeCode1 = evt.result.response.docType;
							docUploadPath1 = evt.result.response.uploadpath;

							_obj.f1 = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, docType1 + '.jpg');
							_obj.imgUpload = 0;

							var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('', [L('takePicture'), L('selectPicture')], -1);
							optionDialogPhoto.show();

							optionDialogPhoto.addEventListener('click', function(evt) {
								if (optionDialogPhoto.options[evt.index] !== L('btn_cancel')) {
									if (evt.index === 0) {
										// Take Picture

										Ti.Media.showCamera({
											showControls : true,
											mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
											autohide : true,
											allowEditing : false,
											autorotate : true,
											success : function(event) {
												Ti.API.info("Media events:", +JSON.stringify(event));

												image1 = event.media;

												_obj.f1.write(image1);
                                                 // ff.write(image1);
												//_obj.f1.push(ff);

												if (_obj.f1.size < 5000000) {
													if (TiGlobals.osname === 'android') {
														imgView1.image = _obj.f1.nativePath;
													} else {
														imgView1.image = _obj.f1.resolve();
													}

													_obj.lblDocSize1.text = '(File Size : ' + bytesToSize(_obj.f.size) + ')';
													imgView1.show();
													/*_obj.btnUpload.show();
													_obj.btnUploadPassport.hide();*/
													//btnMinus.show();
													btnDLChooseFile.hide();
													lblChooseFile1.hide();

												} else {

													require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
													//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
												}
											}
										});
									}

									if (evt.index === 1) {
										// Obtain an image from the gallery

										Titanium.Media.openPhotoGallery({
											success : function(event) {
												Ti.API.info("Media events:", +event);
												Ti.API.info("Media events name:", +event.media);

												if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
													image1 = event.media;
													_obj.f1.write(image1);
													if (_obj.f1.size < 10000000)//5000000 original
													{
														imgView1.image = _obj.f1.read();
														_obj.lblDocSize1.text = '(File Size : ' + bytesToSize(_obj.f1.size) + ')';
														imgView1.show();
														/*_obj.btnUpload.show();
														 _obj.btnUploadPassport.hide();*/
														btnMinus1.show();
														btnDLChooseFile.hide();
														lblChooseFile1.hide();

													} else {
														require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
														//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
													}
												}
											},
											cancel : function() {
											}
										});
									}

									optionDialogPhoto = null;
								}
							});

						} else {
							if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
								require('/lib/session').session();
								destroy_kyc();
							} else {
								require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
							}
						}
						xhr = null;
					} catch(e) {
					}
				}

				function xhrError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}

			}

		});

		/*}else{

		 }*/	/*btnAddPP.addEventListener('click',function(e){

		 addMultipleDoc();

		 } );*/

	}//end of passport multiple doc function1


	_obj.btnAddDL.addEventListener('click', function(e) {

		try {
			Ti.API.info("btnAddDL");
			addMultipleDocDL();
		} catch(e) {
			Ti.API.info(e);
		}
	});

	/*_obj.btnUpload1.addEventListener('click',function(e){
	 var targetFile1 = docUploadPath1 + "/" + TiGlobals.partnerId + "/" + docTypeCode1 + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + docRequestId1 + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
	 var filename1 = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + docRequestId1 + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
	 var docname1 = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode1 + "_" + docRequestId1 + "_" + require('/utils/Date').today('-','dmy');
	 var relativepath1 = docUploadPath1 + "/" + TiGlobals.partnerId + "/" + docTypeCode1 + "/";

	 var xmlHttp = Ti.Network.createHTTPClient();
	 activityIndicator.showIndicator();
	 xmlHttp.onload = function(e)
	 {
	 if(xmlHttp.responseText === '1' || xmlHttp.responseText === 1)
	 {
	 var xhr = require('/utils/XHR');
	 xhr.call({
	 url : TiGlobals.appURLTOML,
	 get : '',
	 post : '{' +
	 '"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
	 '"requestName":"KYCVERIFICATIONDOCUPLOAD",'+
	 '"partnerId":"'+TiGlobals.partnerId+'",'+
	 '"channelId":"'+TiGlobals.channelId+'",'+
	 '"ipAddress":"'+TiGlobals.ipAddress+'",'+
	 '"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+
	 '"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
	 '"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
	 '"countryCode":"'+countryCode[0]+'",'+
	 '"docType":"'+docType1+'"'+
	 '"docRequestId":"'+docRequestId1+'"'+
	 '"docPath":"'+targetFile1+'"'+
	 '}',
	 success : xhrSuccess,
	 error : xhrError,
	 contentType : 'application/json',
	 timeout : TiGlobals.timer
	 });

	 function xhrSuccess(evt) {
	 try{
	 activityIndicator.hideIndicator();
	 if(evt.result.responseFlag === "S")
	 {
	 if(TiGlobals.osname === 'android')
	 {
	 require('/utils/AlertDialog').toast('Document uploaded successfully');
	 }
	 else
	 {
	 require('/utils/AlertDialog').iOSToast('Document uploaded successfully');
	 }

	 //myDocuments();
	 }
	 else
	 {
	 if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
	 {
	 require('/lib/session').session();
	 destroy_kyc();
	 }
	 else
	 {
	 require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
	 }
	 }
	 xhr = null;
	 }catch(e){}
	 }

	 function xhrError(evt) {
	 activityIndicator.hideIndicator();
	 require('/utils/Network').Network();
	 xhr = null;
	 }

	 }
	 else
	 {
	 activityIndicator.hideIndicator();
	 require('/utils/AlertDialog').showAlert('','Error uploading document. Please try again later.',[L('btn_ok')]).show();
	 }
	 };

	 xmlHttp.onerror = function(e)
	 {
	 activityIndicator.hideIndicator();
	 };

	 require('/utils/Console').info(JSON.stringify({doc:_obj.f.read(),source:'APP',name:filename1,docname:docname1,uploadPath:relativepath1}));

	 xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php',true);
	 xmlHttp.send({doc:f.read(),source:'APP',name:filename1,docname:docname1,uploadpath:relativepath1});
	 });

	 */
	function bytesToSize(bytes) {
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		if (bytes == 0)
			return '0 Byte';
		var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};

	function uploadImage() {
		Ti.API.info("****************___IN UPLOAD IMAGE*****");
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"' + TiGlobals.bcmConfig + '",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"source":"' + TiGlobals.bcmSource + '",' + '"type":"document",' + '"docType":"Passport",' + '"platform":"' + TiGlobals.osname + '",' + '"corridor":"' + countryName[0] + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(evt) {
			try {
				activityIndicator.hideIndicator();
				if (evt.result.status === "S") {
					require('/utils/Console').info('Result****** ======== ' + evt);
					require('/utils/Console').info('Result ======== ' + evt.result);
					docType = evt.result.response.docType;
					docTypeCode = evt.result.response.docType;
					docUploadPath = evt.result.response.uploadpath;

					_obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, docType + '.jpg');
					Ti.API.info("IN _OBJ.F:", +_obj.f);
					_obj.imgUpload = 0;

					var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('', [L('takePicture'), L('selectPicture')], -1);
					optionDialogPhoto.show();

					optionDialogPhoto.addEventListener('click', function(evt) {
						if (optionDialogPhoto.options[evt.index] !== L('btn_cancel')) {
							if (evt.index === 0) {
								// Take Picture

								Ti.Media.showCamera({
									showControls : true,
									mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
									autohide : true,
									allowEditing : false,
									autorotate : true,

									success : function(event) {
										Ti.API.info("Media events:", +JSON.stringify(event));

										_obj.image = event.media;

										_obj.f.write(_obj.image);
										/*Ti.API.info("event.media.file.name : " + JSON.stringify(event.media.file.name));
										 var imgname = JSON.stringify(event.media.file.name);
										 alert(imgname);*/
										if (_obj.f.size < 5000000) {
											if (TiGlobals.osname === 'android') {
												_obj.imgView.image = _obj.f.nativePath;
											} else {
												_obj.imgView.image = _obj.f.resolve();
											}

											_obj.lblDocSize.text = '(File Size : ' + bytesToSize(_obj.f.size) + ')';
											_obj.imgView.show();
											/*_obj.btnUpload.show();
											_obj.btnUploadPassport.hide();*/
											//_obj.btnAddPP.show();
											_obj.btnPPChooseFile.hide();
											_obj.lblChooseFile.hide();

										} else {
											require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
											//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
										}
									}
								});
							}

							if (evt.index === 1) {
								// Obtain an image from the gallery

								Titanium.Media.openPhotoGallery({
									success : function(event) {
										Ti.API.info("Media events:", +event);
										Ti.API.info("Media events name:", +event.media);

										if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
											_obj.image = event.media;
											_obj.f.write(_obj.image);
											Ti.API.info("Media obj:", +_obj.f);
											/* Ti.API.info("event.media.file.name : " + JSON.stringify(event.media.file.name));
											 var imgname = JSON.stringify(event.media.file.name);
											 alert(imgname);*/
											if (_obj.f.size < 10000000)//5000000 original
											{
												_obj.imgView.image = _obj.f.read();
												_obj.lblDocSize.text = '(File Size : ' + bytesToSize(_obj.f.size) + ')';
												_obj.imgView.show();
												/*_obj.btnUpload.show();
												_obj.btnUploadPassport.hide();*/
												//_obj.btnAddPP.show();
												_obj.btnPPChooseFile.hide();
												_obj.lblChooseFile.hide();

											} else {
												require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
												//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
											}
										}
									},
									cancel : function() {
									}
								});
							}

							optionDialogPhoto = null;
						}
					});

				} else {
					if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
						require('/lib/session').session();
						destroy_kyc();
					} else {
						require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
					}
				}
				xhr = null;
			} catch(e) {
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	function uploadImage1() {
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"' + TiGlobals.bcmConfig + '",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"source":"' + TiGlobals.bcmSource + '",' + '"type":"document",' + '"docType":"Driving Licence",' + '"platform":"' + TiGlobals.osname + '",' + '"corridor":"' + countryName[0] + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(evt) {
			try {
				activityIndicator.hideIndicator();
				if (evt.result.status === "S") {
					require('/utils/Console').info('Result****** ======== ' + evt);
					require('/utils/Console').info('Result ======== ' + evt.result);
					docType1 = evt.result.response.docType;
					docTypeCode1 = evt.result.response.docType;
					docUploadPath1 = evt.result.response.uploadpath;

					/*docType.push(evt.result.response.docType);
					 docTypeCode.push(evt.result.response.docType);
					 docUploadPath.push(evt.result.response.uploadpath);*/

					_obj.f1 = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, docType1 + '.jpg');
					_obj.imgUpload = 0;

					var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('', [L('takePicture'), L('selectPicture')], -1);
					optionDialogPhoto.show();

					optionDialogPhoto.addEventListener('click', function(evt) {
						if (optionDialogPhoto.options[evt.index] !== L('btn_cancel')) {
							if (evt.index === 0) {
								// Take Picture

								Ti.Media.showCamera({
									showControls : true,
									mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
									autohide : true,
									allowEditing : false,
									autorotate : true,
									success : function(event) {
										Ti.API.info("Media events:", +JSON.stringify(event));

										_obj.image1 = event.media;

										_obj.f1.write(_obj.image1);

										if (_obj.f1.size < 5000000) {
											if (TiGlobals.osname === 'android') {
												_obj.imgView1.image = _obj.f1.nativePath;
											} else {
												_obj.imgView1.image = _obj.f1.resolve();
											}

											_obj.lblDocSize1.text = '(File Size : ' + bytesToSize(_obj.f1.size) + ')';
											_obj.imgView1.show();
											/*_obj.btnUpload.show();
											_obj.btnUploadPassport.hide();*/
											//btnMinus.show();
											_obj.btnDLChooseFile.hide();
											_obj.lblChooseFile1.hide();

										} else {
											require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
											//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
										}
									}
								});
							}

							if (evt.index === 1) {
								// Obtain an image from the gallery

								Titanium.Media.openPhotoGallery({
									success : function(event) {
										Ti.API.info("Media events:", +event);
										Ti.API.info("Media events name:", +event.media);

										if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
											_obj.image1 = event.media;
											_obj.f1.write(_obj.image1);
											if (_obj.f1.size < 5000000) {
												_obj.imgView1.image = _obj.f1.read();
												_obj.lblDocSize1.text = '(File Size : ' + bytesToSize(_obj.f1.size) + ')';
												_obj.imgView1.show();
												/*_obj.btnUpload.show();
												_obj.btnUploadPassport.hide();*/
												//_obj.btnMinus1.show();
												_obj.btnDLChooseFile.hide();
												_obj.lblChooseFile1.hide();

											} else {

												require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
												// require('/utils/AlertDialog').showAlert('The file you upload should not exceed the file size of 5 MB.');
												//require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
											}
										}
									},
									cancel : function() {
									}
								});
							}

							optionDialogPhoto = null;
						}
					});

				} else {
					if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
						require('/lib/session').session();
						destroy_kyc();
					} else {
						require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
					}
				}
				xhr = null;
			} catch(e) {
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	/*_obj.imgClose.addEventListener('click',function(e){
	 destroy_kyc();
	 });
	 */

	_obj.winKYC.addEventListener('androidback', function() {
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames : [L('btn_yes'), L('btn_no')],
			message : L('screen_exit')
		});

		alertDialog.show();

		alertDialog.addEventListener('click', function(e) {
			alertDialog.hide();
			if (e.index === 0 || e.index === "0") {
				destroy_kyc();
				alertDialog = null;
			}
		});
	});

	function country() {
		activityIndicator.showIndicator();
		var countryOptions = [];

		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"GETCOUNTRYRELATEDDETAILS",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"country":"' + countryName[0] + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if (e.result.responseFlag === "S") {
				var splitArr = e.result.citizenship.split('~');

				for (var i = 0; i < splitArr.length; i++) {
					countryOptions.push(splitArr[i]);
				}

				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country', countryOptions, -1);
				optionDialogCountry.show();

				optionDialogCountry.addEventListener('click', function(evt) {
					if (optionDialogCountry.options[evt.index] !== L('btn_cancel')) {
						_obj.lblPPIssueBy.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					} else {
						optionDialogCountry = null;
						_obj.lblPPIssueBy.text = 'Passport Issued By*';
					}
				});
			} else {
				if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
					require('/lib/session').session();
					destroy_kyc();
				} else {
					require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	//Pratiksha
	// function country1() {
	// activityIndicator.showIndicator();
	// var countryOptions = [];
	//
	// var xhr = require('/utils/XHR');
	//
	// xhr.call({
	// url : TiGlobals.appURLTOML,
	// get : '',
	// post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"GETCOUNTRYRELATEDDETAILS",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"country":"' + countryName[0] + '"' + '}',
	// success : xhrSuccess,
	// error : xhrError,
	// contentType : 'application/json',
	// timeout : TiGlobals.timer
	// });
	//
	// function xhrSuccess(e) {
	// activityIndicator.hideIndicator();
	// if (e.result.responseFlag === "S") {
	// var splitArr = e.result.citizenship.split('~');
	//
	// for (var i = 0; i < splitArr.length; i++) {
	// countryOptions.push(splitArr[i]);
	// }
	//
	// var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country', countryOptions, -1);
	// optionDialogCountry.show();
	//
	// optionDialogCountry.addEventListener('click', function(evt) {
	// if (optionDialogCountry.options[evt.index] !== L('btn_cancel')) {
	// _obj.lblPPIssueBy1.text = optionDialogCountry.options[evt.index];
	// optionDialogCountry = null;
	// } else {
	// optionDialogCountry = null;
	// _obj.lblPPIssueBy1.text = 'Driving License Issue Place*';
	// }
	// });
	// } else {
	// if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
	// require('/lib/session').session();
	// destroy_kyc();
	// } else {
	// require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
	// }
	// }
	// xhr = null;
	// }
	//
	// function xhrError(e) {
	// activityIndicator.hideIndicator();
	// require('/utils/Network').Network();
	// xhr = null;
	// }
	//
	// }

	function state() {
		activityIndicator.showIndicator();
		var stateOptions = [];

		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"GETCOUNTRYRELATEDDETAILS",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"country":"' + countryName[0] + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if (e.result.responseFlag === "S") {
				var splitArr = e.result.countryRelatedStates.split('~');

				for (var i = 0; i < splitArr.length; i++) {
					stateOptions.push(splitArr[i]);
				}

				var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State', stateOptions, -1);
				optionDialogState.show();

				optionDialogState.addEventListener('click', function(evt) {
					if (optionDialogState.options[evt.index] !== L('btn_cancel')) {
						_obj.lblDLIssueBy.text = optionDialogState.options[evt.index];
						optionDialogState = null;
					} else {
						optionDialogState = null;
						_obj.lblDLIssueBy.text = 'Driving License Issued By*';
					}
				});
			} else {
				if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
					require('/lib/session').session();
					destroy_kyc();
				} else {
					require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}

	}//---


	_obj.btnCancelPassport.addEventListener('click', function(e) {

		destroy_kyc();
		//require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
	});
	_obj.btnCancelDriving.addEventListener('click', function(e) {
		//require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
		destroy_kyc();

	});
	function destroy_kyc() {
		try {
			if (_obj.globalView === null) {
				return;
			}

			require('/utils/Console').info('############## Remove US KYC start ##############');

			_obj.winKYC.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYC, _obj.globalView);

			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyc', destroy_kyc);
			require('/utils/Console').info('############## Remove US KYC end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_kyc', destroy_kyc);
};
// KYCModal()
