exports.KYCCallingModal = function()
{
	require('/lib/analytics').GATrackScreen('KYC Calling');
	
	var _obj = {
		style : require('/styles/kyc/KYCCalling').KYCCalling,
		winKYCCalling : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		webView : null,
		currCode : null,
		
		contactDate : '',
		timeFrom : '',
		timeTo : '',
		timeFrom1 : '',
		timeTo1 : '',
		
		timeOptions : []
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var destCountryName = Ti.App.Properties.getString('destinationCountryCurName');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winKYCCalling = Ti.UI.createWindow(_obj.style.winKYCCalling);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYCCalling);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Know your customer calling details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);
	
	_obj.lblKYCTxt1 = Ti.UI.createLabel(_obj.style.lblKYCTxt1);
	_obj.lblKYCTxt1.text = 'Please select a date, time and number for us to call you. Our compliance team will get in touch with you at the time provided by you. Your transaction may be on hold in the interim period till the KYC verification is completed.';
	
	_obj.lblKYCTxt2 = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblKYCTxt2.top = 10;
	_obj.lblKYCTxt2.text = 'Note';
	
	_obj.lblKYCTxt3 = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblKYCTxt3.text = '\u00B7 The KYC calling selected time will be as per Indian Standard Time (IST)';
	
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = 'Available Date*';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.timeView1 = Ti.UI.createView(_obj.style.DDView);
	_obj.lblTime1 = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblTime1.text = 'Available Time*';
	_obj.imgTime1 = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.timeView2 = Ti.UI.createView(_obj.style.DDView);
	_obj.lblTime2 = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblTime2.text = 'Available Time*';
	_obj.imgTime2 = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView3 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryTimeView1 = Ti.UI.createView(_obj.style.countryTimeView);
	_obj.countryTimeMainView1 = Ti.UI.createView(_obj.style.countryTimeMainView);
	_obj.lblCTime11 = Ti.UI.createLabel(_obj.style.lblCTime1);
	_obj.lblCTime12 = Ti.UI.createLabel(_obj.style.lblCTime2);
	 
	 _obj.borderView4 = Ti.UI.createView(_obj.style.borderFullView);
	 
	_obj.countryTimeView2 = Ti.UI.createView(_obj.style.countryTimeView);
	_obj.countryTimeView2.top = 0;
	_obj.countryTimeMainView2 = Ti.UI.createView(_obj.style.countryTimeMainView);
	_obj.lblCTime21 = Ti.UI.createLabel(_obj.style.lblCTime1);
	_obj.lblCTime22 = Ti.UI.createLabel(_obj.style.lblCTime2);
	
	_obj.btnContinue = Ti.UI.createButton(_obj.style.btnContinue);
	_obj.btnContinue.title = 'CONTINUE';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.kycView.add(_obj.lblKYCTxt1);
	_obj.kycView.add(_obj.lblKYCTxt2);
	_obj.kycView.add(_obj.lblKYCTxt3);
	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.kycView.add(_obj.dobView);
	_obj.kycView.add(_obj.borderView1);
	_obj.timeView1.add(_obj.lblTime1);
	_obj.timeView1.add(_obj.imgTime1);
	_obj.kycView.add(_obj.timeView1);
	_obj.kycView.add(_obj.borderView2);
	_obj.timeView2.add(_obj.lblTime2);
	_obj.timeView2.add(_obj.imgTime2);
	_obj.kycView.add(_obj.timeView2);
	_obj.kycView.add(_obj.borderView3);
	_obj.countryTimeMainView1.add(_obj.lblCTime11);
	_obj.countryTimeMainView1.add(_obj.lblCTime12);
	_obj.countryTimeView1.add(_obj.countryTimeMainView1);
	_obj.kycView.add(_obj.countryTimeView1);
	_obj.kycView.add(_obj.borderView4);
	_obj.countryTimeMainView2.add(_obj.lblCTime21);
	_obj.countryTimeMainView2.add(_obj.lblCTime22);
	_obj.countryTimeView2.add(_obj.countryTimeMainView2);
	_obj.kycView.add(_obj.countryTimeView2);
	_obj.kycView.add(_obj.btnContinue);
	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYCCalling.add(_obj.globalView);
	_obj.winKYCCalling.open();
	
	function getAvailableTime(type)
	{
		var optionDialogTime = require('/utils/OptionDialog').showOptionDialog('Available Time',_obj.timeOptions,-1);
		optionDialogTime.show();
		
		setTimeout(function(){
			activityIndicator.hideIndicator();
		},200);
		
		optionDialogTime.addEventListener('click',function(evt){
			if(optionDialogTime.options[evt.index] !== L('btn_cancel'))
			{
				if(type === 't1')
				{
					_obj.lblTime1.text = optionDialogTime.options[evt.index];
				}
				else
				{
					_obj.lblTime2.text = optionDialogTime.options[evt.index];
				}
				
				optionDialogTime = null;
				_obj.timeOptions.pop(); // Pops out cancel
			}
			else
			{
				_obj.timeOptions.pop(); // Pops out cancel
				optionDialogTime = null;
				if(type === 't1')
				{
					_obj.lblTime1.text = 'Available Time*';
				}
				else
				{
					_obj.lblTime2.text = 'Available Time*';
				}
			}
		});
	}
	
	_obj.timeView1.addEventListener('click',function(e){
		getAvailableTime('t1');
	});
	
	_obj.timeView2.addEventListener('click',function(e){
		getAvailableTime('t2');
	});
	
	_obj.dobView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblDOB,'t15');
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_kyccalling();
	});
	
	_obj.btnContinue.addEventListener('click',function(e){
		//","destCountryDateTime":"Fri | 12-05-2017  |  11:19 AM","
		var sourceDTSplit = _obj.lblCTime12.text.split('|');
		Ti.API.info("IN CH:--",sourceDTSplit);
		
		var sourceTimeSplit = sourceDTSplit[2].trim().split(' ');
				Ti.API.info("IN CH:--",sourceTimeSplit);

		var sourceHourSplit = sourceTimeSplit[2].split(':');
				Ti.API.info("IN CH:--",sourceHourSplit);

		var ch = parseInt(sourceHourSplit[0]);
				Ti.API.info("IN CH:--",ch);

		
		var sourceDateSplit = sourceDTSplit[1].trim().split('-'); //12-05-2017
		Ti.API.info("IN CH1:--",sourceDateSplit);
		
	    var sourceDate = sourceDateSplit[0]+'-'+require('/utils/Date').monthShort(sourceDateSplit[1])+'-'+sourceDateSplit[2];
		Ti.API.info("IN CH2:--",sourceDate);
		
		var sourceTimeSplit = Date().split(' ');  //for date comparision
	   Ti.API.info("IN sourceTimeSplit:--",sourceTimeSplit); //Fri,May,12,2017,14:43:48,GMT+0530,(IST)
       
       var today = sourceTimeSplit[2]+'-'+sourceTimeSplit[1]+'-'+sourceTimeSplit[3];
		Ti.API.info("IN today:--",today); //12-May-2017

		
		var dateSplit = _obj.lblDOB.text.split('/');
	    Ti.API.info("IN CH2:--",dateSplit);

		var dt = dateSplit[0]+'-'+require('/utils/Date').monthShort(dateSplit[1])+'-'+dateSplit[2];
		 Ti.API.info("IN CH2:--",dt);

		
		var time1Split = _obj.lblTime1.text.split('-');
	   Ti.API.info("IN CH2:--",time1Split);

		var time2Split = _obj.lblTime2.text.split('-');
		Ti.API.info("IN CH2:--",time2Split);

		
		require('/utils/Console').info('parseInt(time1Split[0]) <= (ch+4)' + parseInt(time1Split[0]) +' ===== '+ parseInt(time1Split[1])+' ===== '+ parseInt(time2Split[0]) +' ===== '+ parseInt(time2Split[1]) +' ===== ' + (ch+4) + '=====' +(parseInt(time1Split[0]) <= (ch+4)));
			
		require('/utils/Console').info('((parseInt(time1Split[0]) <= (ch+4)) && (sourceDate === dt))' + parseInt(time1Split[0]) +' ===== '+ (ch+4) + '=====' + today + '---' +dt + '=========' + (today === dt));
		
		if(_obj.lblDOB.text === 'Available Date*')
		{
			require('/utils/AlertDialog').showAlert('','Please choose a valid Calling Date',[L('btn_ok')]).show();
			return;	
		}
		else if(_obj.lblTime1.text === 'Available Time*')
		{
			require('/utils/AlertDialog').showAlert('','Please choose a Calling Time',[L('btn_ok')]).show();
			_obj.lblTime1.text = 'Available Time*';
			return;
		}
		else if(_obj.lblTime2.text === 'Available Time*')
		{
			require('/utils/AlertDialog').showAlert('','Please choose an Alternate Calling Time',[L('btn_ok')]).show();
			_obj.lblTime2.text = 'Available Time*';
			return;
		}	
		 else if((parseInt(time1Split[0]) <= (ch+4)) && (today === dt)){
		//else if((parseInt(time2Split[0]) <= parseInt(time1Split[0])+4)){  // && (sourceDate === dt)){
			require('/utils/AlertDialog').showAlert('','Please choose a Calling Time 4 hrs from the current time',[L('btn_ok')]).show();
			_obj.lblTime1.text = 'Available Time*';
			return;
		}
		 else if((parseInt(time1Split[0]) < ch ) && (today === dt)){
		//else if((parseInt(time2Split[0]) <= parseInt(time1Split[0])+4)){  // && (sourceDate === dt)){
			require('/utils/AlertDialog').showAlert('','Please choose a Calling Time 4 hrs from the current time',[L('btn_ok')]).show();
			_obj.lblTime1.text = 'Available Time*';
			return;
		}
	    else if((parseInt(time2Split[0]) <= ch ) && (today === dt)){
		//else if((parseInt(time2Split[0]) <= parseInt(time1Split[0])+4)){  // && (sourceDate === dt)){
			require('/utils/AlertDialog').showAlert('','Please choose a Calling Time 4 hrs from the current time',[L('btn_ok')]).show();
			_obj.lblTime2.text = 'Available Time*';
			return;
		}
	  
	  else if(parseInt(time2Split[0]) <= parseInt(time1Split[0])){
			require('/utils/AlertDialog').showAlert('','Please choose an Alternate Calling Time',[L('btn_ok')]).show();
			_obj.lblTime2.text = 'Available Time*';
			return;
		}
		
		else
		{
			// Pass these to preconf and changefirstTxnFlag in Transfer Money
			Ti.API.info("time1Split[0]:----",+parseInt(time1Split[0]));
			Ti.API.info("time1Split[1]:----",+parseInt(time1Split[1]));
			Ti.API.info("time2Split[0]:----",+parseInt(time2Split[0]));
			Ti.API.info("time1Split[1]:----",+parseInt(time2Split[1]));
			
		/*	_obj.contactDate = dt;
			_obj.timeFrom = time1Split[0];
		     _obj.timeTo = time1Split[1];
		     _obj.timeFrom1 = time2Split[0];
		     _obj.timeTo1 = time2Split[1];
		     Ti.API.info("time1Split[0]:----",+_obj.timeTo1);*/
		   // var data = _obj.contactDate +'||'+ _obj.timeFrom +':00||'+ _obj.timeTo +':00||'+ _obj.timeFrom1 +':00||'+ _obj.timeTo1 +':00';
			var data = dt +'||'+ time1Split[0] +':00||'+ time1Split[1] +':00||'+ time2Split[0] +':00||'+ time2Split[1] +':00';
			Ti.API.info("Data 1",+parseInt(data));
			
			Ti.App.fireEvent('changefirstTxnFlag',{data:data});
			
			time1Split = null;
			time2Split = null;
			dateSplit = null;
			dt = null;
			data = null;
			
			destroy_kyccalling();
		}
		 // destroy_kyccalling();
	});
	
	_obj.winKYCCalling.addEventListener('androidback', function(){
		destroy_kyccalling();
	});
	
	function getKYCDetails()
	{
		// For US check occupation
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETKYCCALLINGTIMERANGEDTLS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"destinationCountry":"'+destSplit[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			require('/utils/Console').info('Result ======== ' + e.result);
			try{
			_obj.transactionInfoResult = e.result;
		   }catch(e){}
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.timeRanges.length;i++)
				{
					_obj.timeOptions.push(e.result.timeRanges[i]);
				}
				
				_obj.lblCTime11.text = 'Time in ' + countryName[0] + ' ('+ e.result.orgCountryTimezone +')';
				_obj.lblCTime12.text = e.result.orgCountryDateTime;
				
				_obj.lblCTime21.text = 'Time in ' + destCountryName + ' ('+ e.result.destCountryTimezone +')';
				_obj.lblCTime22.text = e.result.destCountryDateTime;
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyccalling();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getKYCDetails();
	
	function destroy_kyccalling()
	{
		try{
			
			require('/utils/Console').info('############## Remove kyc calling start ##############');
			
			_obj.winKYCCalling.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYCCalling);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyccalling',destroy_kyccalling);
			require('/utils/Console').info('############## Remove kyc calling end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_kyccalling', destroy_kyccalling);
}; // KYCCallingModal()
