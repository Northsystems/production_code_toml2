exports.KYCModal = function()
{
	require('/lib/analytics').GATrackScreen('KYC Form Australia');
	
	var _obj = {
		style : require('/styles/kyc/aus/KYC').KYC,
		winKYC : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		kycView : null,
		//txtAptNo : null,
		//borderViewS21 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreetName : null,
		borderViewSNAme : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		streetTypeView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		
		doneDay : null,
		dayView : null,
		lblDay : null,
		txtDay : null,
		borderViewS30 : null,
		
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS31 : null,
		
		tabView : null,
		tabSelView : null,
		lblPassport : null,
		lblMedicare : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		btnSubmitPassport : null,
		//btnSubmitDriving : null,
		btnCancelPassport : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		streetType : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		tab : null, 
		term : null,
		term1 : null,
		
		f : null,
		imgUpload : null,
		image : null,
		data_to_send : null,
		
		//CMN73
		issuanceView:null,
        lblPPIssueDt:null,
        imgIssuance:null,
        borderIssuView:null,
        
        expiryView:null,
        lblPPExpDt:null,
        imgExpiry:null,
        borderExpView:null,
        
        doneMobilePPNo:null,
        txtPassportPersonalNo:null,
        borderPPNoView:null,

        lblMRZ:null,
        mrzView:null,
        txtMRZ1:null,
        borderMRZ1:null,
        txtMRZ2:null,
        borderMRZ2:null,
        txtMRZ3:null,
        borderMRZ3:null,
        txtMRZ4:null,
        borderMRZ4:null,
        txtMRZ5:null,
        borderMRZ5:null,
      
        imgPassport:null,
        
        imgUploadView:null,
        uploadPPLabel:null,
        btnPPChooseFile:null,
        imgView:null,
        lblChooseFile:null,
        uploadBorderView:null,
    
         mrz1:null,
         mrz2:null,
         mrz3:null,
         
         privacyFlag:null  //Added on 1-Dec-17
        
	};
	
	_obj.tab = 'passport';
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winKYC = Titanium.UI.createWindow(_obj.style.winKYC);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYC);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	/////////////////// KYC ///////////////////
	
	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);
	
	_obj.txtStreet1 = Ti.UI.createTextField(_obj.style.txtStreet1);
	_obj.txtStreet1.hintText = "Street No.*";
	_obj.txtStreet1.maxLength = 25;
	_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtStreetName = Ti.UI.createTextField(_obj.style.txtStreet1);
	_obj.txtStreetName.hintText = "Street Name*";
	_obj.txtStreetName.maxLength = 25;
	_obj.borderViewSName = Ti.UI.createView(_obj.style.borderView);
	
	_obj.streetTypeView = Ti.UI.createView(_obj.style.cityView);
	_obj.lblStreetType = Ti.UI.createLabel(_obj.style.txtCity);
	_obj.lblStreetType.top = 0;
	_obj.lblStreetType.left = 0;
	_obj.lblStreetType.text = 'Select Street Type*';
	_obj.imgStreetType = Ti.UI.createImageView(_obj.style.imgCity);
	_obj.borderViewSST = Ti.UI.createView(_obj.style.borderView);
	
	_obj.streetTypeView.addEventListener('click',function(){
		activityIndicator.showIndicator();
		var streetOptions = [];
		var streetCodeOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"streetType",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].streetType.length;i++)
				{
					streetOptions.push(e.result.response[0].streetType[i].street);
					streetCodeOptions.push(e.result.response[0].streetType[i].code);
				}
				
				var optionDialogStreetType = require('/utils/OptionDialog').showOptionDialog('Street Type',streetOptions,-1);
				optionDialogStreetType.show();
				
				optionDialogStreetType.addEventListener('click',function(evt){
					if(optionDialogStreetType.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblStreetType.text = optionDialogStreetType.options[evt.index];
						_obj.streetType = streetCodeOptions[evt.index]; 
						optionDialogStreetType = null;
					}
					else
					{
						optionDialogStreetType = null;
						_obj.lblStreetType.text = 'Select Street Type*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
	_obj.txtCity.maxLength = 25;
	_obj.txtCity.hintText = 'City*';
	_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
	
    _obj.stateView = Ti.UI.createView(_obj.style.stateView);
	_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
	_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
	_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
			
	_obj.lblState.text = 'Select State*';	
			
	_obj.stateView.addEventListener('click',function(){
				activityIndicator.showIndicator();
				var stateOptions = [];
				
				var xhr = require('/utils/XHR');
		
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"country":"'+countryName[0]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrSuccess(e) {
					try{
					activityIndicator.hideIndicator();
					if(e.result.responseFlag === "S")
					{
						var splitArr = e.result.countryRelatedStates.split('~');
				
						for(var i=0;i<splitArr.length;i++)
						{
							stateOptions.push(splitArr[i]);
						}
						
						var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State',stateOptions,-1);
						optionDialogState.show();
						
						optionDialogState.addEventListener('click',function(evt){
							if(optionDialogState.options[evt.index] !== L('btn_cancel'))
							{
								_obj.lblState.text = optionDialogState.options[evt.index];
								optionDialogState = null;
							}
							else
							{
								optionDialogState = null;
								_obj.lblState.text = 'Select State*';	
							
							}
						});	
					}
					else
					{
						if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
					}catch(e){}
				}
		
				function xhrError(e) {
					activityIndicator.hideIndicator();
				    require('/utils/Network').Network();
					xhr = null;
				}
			});

	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Zip Code*';
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
	
	////// Pin code hintText ///////
   _obj.txtZip.hintText = 'Zip Code (4 digits)*';
   _obj.txtZip.maxLength = 4;
   _obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
   _obj.txtZip.keyboardToolbar = [_obj.doneZip];
  
	
	_obj.borderViewS29 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDay.blur();
	});
	
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDay = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtDay.hintText = 'Day Time Number*';
	_obj.txtDay.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDay.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS30 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobile.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number*';
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	
	// Set HintText for mobile and day nos
	_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
	_obj.txtMobile.maxLength = 9;
    _obj.txtDay.hintText = 'Day Time Number (9 digits)*';
	_obj.txtDay.maxLength = 9;
			

	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.tabSelView.width = '100%';
	_obj.lblPassport = Ti.UI.createLabel(_obj.style.lblPassport);
	_obj.lblPassport.width = '100%';
	_obj.lblPassport.text = 'Passport Details';
	_obj.lblMedicare = Ti.UI.createLabel(_obj.style.lblMedicare);
	_obj.lblMedicare.text = 'Medicare Number';
	
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.tabSelIconView.width = '100%';
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
		
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	// Passport 
	_obj.passportView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote.text = 'Note';
	
	_obj.lblNoteTxt = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt.text = '\u00B7 If you are entering your passport details, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your passport';
	_obj.lblNoteTxt.top = 5; 
	
	_obj.txtPassportNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtPassportNo.maxLength = 9;
	//_obj.txtPassportNo.hintText = 'Passport No*';
	_obj.txtPassportNo.hintText = '*Passport No';
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	//_obj.lblCountry.text = 'Passport Country*';   //Commenting on 04-July-17 for CMN67
	_obj.lblCountry.text = '*Passport Issuing Country';//'Passport Issuing Country*';
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS33 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView.addEventListener('click',function(){
		country();
	});
	
	_obj.txtPassportIssuePlace = Ti.UI.createTextField(_obj.style.txtPassportNo);
	//_obj.txtPassportIssuePlace.hintText = 'Passport Issue Place*';//Commenting on 04-July-17 for CMN67
	_obj.txtPassportIssuePlace.hintText = '*Place of issue'; //'Place of issue*';
	_obj.borderViewSPIP = Ti.UI.createView(_obj.style.borderView);
	
	_obj.COBView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCOB = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCOB.text = '*Country of Birth'; //'Country of Birth*';
	_obj.imgCOB = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS34 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.COBView.addEventListener('click',function(){
		nationality();
	});
	
	_obj.txtSurname = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtSurname.hintText = '*Surname at Citizenship';//'Surname at Citizenship*';
	_obj.txtSurname.maxLength = 25;
	_obj.borderViewS35 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtSurnameBirth = Ti.UI.createTextField(_obj.style.txtPassportNo);
	//_obj.txtSurnameBirth.hintText = 'Surname at Birth*';//Commenting on 04-July-17 for CMN67
	_obj.txtSurnameBirth.hintText = '*Surname at time of birth';//'Surname at time of birth*';
	_obj.txtSurnameBirth.maxLength = 25;
	_obj.borderViewS36 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtPOB = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	//_obj.txtPOB.hintText = 'Passport Place of Birth*';//Commenting on 04-July-17 for CMN67
	_obj.txtPOB.hintText = '*Place of birth as per passport';//'Place of birth as per passport*' ;
	_obj.txtPOB.maxLength = 25;
	_obj.borderViewS37 = Ti.UI.createView(_obj.style.borderView);
	
	//CMN73
	

	//Document Issuance date
	_obj.issuanceView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblPPIssueDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblPPIssueDt.text = '*Document Issuance date';
	_obj.imgIssuance = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderIssuView = Ti.UI.createView(_obj.style.borderView);

    _obj.issuanceView.addEventListener('click', function(e) {
		TiGlobals.issuFlagAus = false;
		require('/utils/DatePicker').DatePicker(_obj.lblPPIssueDt, 'normal1');

	});
	
	//Document Expiry date
	
	_obj.expiryView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblPPExpDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblPPExpDt.text = '*Document Expiry date';
	_obj.imgExpiry = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderExpView = Ti.UI.createView(_obj.style.borderView);

	_obj.expiryView.addEventListener('click', function(e) {
		TiGlobals.issuFlagAus = true;
		require('/utils/DatePicker').DatePicker(_obj.lblPPExpDt, 'normal2');

	});
	
		function issueDateDtlAus(evt) { // CMN 73
			    var issuDate = evt.data;
			    Ti.API.info("IN ISSUEDATE",issuDate);
			   
			     try{
			    _obj.lblPPIssueDt.value = issuDate.getTime();
			    Ti.API.info("IN ISSUDIF", _obj.lblPPIssueDt.value);
			    }catch(e){}
			    TiGlobals.issuFlagAus = true;
			}
			
			Ti.App.addEventListener('issueDateDtlAus',issueDateDtlAus);
		
		function expDateDtlAus(evt) { // CMN 73
			    var expDate = evt.data;
			    Ti.API.info("IN EXPDATE",expDate);
			    try{
			   _obj.lblPPExpDt.value = expDate.getTime();
			    Ti.API.info("IN AGEDIF",_obj.lblPPExpDt.value );
			    }catch(e){}
			    TiGlobals.issuFlagAus = false;
			}
			
			Ti.App.addEventListener('expDateDtlAus',expDateDtlAus);
			
	_obj.txtPassportPersonalNo = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	_obj.txtPassportPersonalNo.maxLength = 2;
	_obj.txtPassportPersonalNo.hintText = '*Personal No';
	_obj.borderPPNoView = Ti.UI.createView(_obj.style.borderView);
	_obj.txtPassportPersonalNo.keyboardType = Ti.UI.KEYBOARD_DECIMAL_PAD;
	
	_obj.doneMobilePPNo = Ti.UI.createButton(_obj.style.done);
	
    // _obj.txtDayNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
   
	_obj.txtPassportPersonalNo.keyboardToolbar = [_obj.doneMobilePPNo];
	_obj.doneMobilePPNo.addEventListener('click',function(){
		try{
		_obj.txtPassportPersonalNo.blur();
		}
		catch(e){
			Ti.API.info(e);
		}
	});
  

	_obj.lblMRZ = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblMRZ.text = '*MRZ No';
	_obj.lblMRZ.top = 15;
	_obj.mrzView = Ti.UI.createView(_obj.style.mrzView);

	_obj.txtMRZ1 = Ti.UI.createTextField(_obj.style.txtMRZ1);
	_obj.txtMRZ1.maxLength = 8;
	_obj.txtMRZ1.hintText = 'xxxxxxxx';
	_obj.borderMRZ1 = Ti.UI.createView(_obj.style.borderMRZ1);

	_obj.txtMRZ2 = Ti.UI.createTextField(_obj.style.txtMRZ2);
	_obj.borderMRZ2 = Ti.UI.createView(_obj.style.borderMRZ2);
	_obj.txtMRZ2.value = '<';
	_obj.txtMRZ2.editable = false;

	_obj.txtMRZ3 = Ti.UI.createTextField(_obj.style.txtMRZ3);
	_obj.txtMRZ3.maxLength = 19;
	_obj.txtMRZ3.hintText = 'xxxxxxxxxxxxxxxxxxx';
	_obj.borderMRZ3 = Ti.UI.createView(_obj.style.borderMRZ3);

	_obj.txtMRZ4 = Ti.UI.createTextField(_obj.style.txtMRZ4);
	_obj.borderMRZ4 = Ti.UI.createView(_obj.style.borderMRZ4);
	_obj.txtMRZ4.value = '<<<<<<<<<<<<<';
	_obj.txtMRZ4.editable = false;

	_obj.txtMRZ5 = Ti.UI.createTextField(_obj.style.txtMRZ5);
	_obj.borderMRZ5 = Ti.UI.createView(_obj.style.borderMRZ5);
	_obj.txtMRZ5.maxLength = 2;
	_obj.txtMRZ5.hintText = 'xx';

	_obj.imgPassport = Ti.UI.createImageView(_obj.style.imgPassport);

	
	_obj.termsAgreeView = Ti.UI.createView(_obj.style.termsView);
	_obj.termsAgreeView.backgroundColor = '#e9e9e9';
	_obj.lblAgree = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblAgree.text = 'I agree to the following';
	_obj.lblAgreeTxt = Ti.UI.createLabel(_obj.style.lblTermsTxt);
	_obj.lblAgreeTxt.text = 'We may disclose your personal information to an organisation providing verification of your identity, including on-line verification of your identity where we are required to do so by law, such as under the Anti-Money Laundering and Counter Terrorism Financing Act 2006. We may verify your identity using information held by a Credit Reporting Body (CRB). To do this we may disclose personal information such as your name, date of birth and address to the CRB to obtain an assessment of whether that personal information matches information held by the CRB. The CRB may give us a report on that assessment and to do so may use personal information about you and other individuals in their files. Alternative means of verifying your identity are available on request. If we are unable to verify your identity using information held by a CRB we will provide you with a notice to this effect and give you the opportunity to contact the CRB to update your information held by them';
	_obj.imgAgree = Ti.UI.createImageView(_obj.style.imgTerms);
	
	_obj.termsDonotAgreeView = Ti.UI.createView(_obj.style.termsView);
	_obj.termsDonotAgreeView.backgroundColor = '#fff';
	_obj.termsDonotAgreeView.top = 0;
	_obj.lblDonotAgree = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblDonotAgree.text = 'I do not agree';
	_obj.lblDonotAgreeTxt = Ti.UI.createLabel(_obj.style.lblTermsTxt);  //CMN 74
	_obj.lblDonotAgreeTxt.text = 'To get my identity verified through the stated means.';
	//_obj.lblDonotAgreeTxt.text = 'To get my identity verified through the stated means and would be willing to share any identity documents that you may want such as Passport copy, Driving licence, etc.';
	_obj.imgDonotAgree = Ti.UI.createImageView(_obj.style.imgTerms);
	
	_obj.termsAgreeView.addEventListener('click',function(){
		if(_obj.imgAgree.image === '/images/checkbox_unsel.png')
		{
			_obj.term = 1;
			_obj.imgAgree.image = '/images/checkbox_sel.png';
			_obj.imgDonotAgree.image = '/images/checkbox_unsel.png';
			_obj.privacyFlag = 'Y';  //Added on 1-Dec-17
		}
		else
		{
			_obj.term = 0;
			_obj.imgAgree.image = '/images/checkbox_unsel.png';
		}
		
		//_obj.imgUploadView.height = 0;
		//_obj.imgUploadView.visible = false;
	});
	
	var docType = null;
	var docRequestId = null;
	var docTypeCode = null;
	var docUploadPath = null;
	
	_obj.termsDonotAgreeView.addEventListener('click',function(){ 
		if(_obj.imgDonotAgree.image === '/images/checkbox_unsel.png')
		{
			_obj.term = 0;
			_obj.imgDonotAgree.image = '/images/checkbox_sel.png';
			_obj.imgAgree.image = '/images/checkbox_unsel.png';
			
			_obj.privacyFlag = 'N';  //Added on 1-Dec-17
			 
		}
		else
		{
			_obj.term = 0;
			_obj.imgDonotAgree.image = '/images/checkbox_unsel.png';
			
			
			//_obj.imgUploadView.height = 0;   //CMN73
			//_obj.imgUploadView.visible = false;  
		}
	});
	
	_obj.btnSubmitPassport = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitPassport.title = 'SUBMIT';
	
	_obj.btnCancelPassport = Ti.UI.createButton(_obj.style.btnCancel);
	_obj.btnCancelPassport.title = 'CANCEL';
	
	_obj.imgUploadView = Ti.UI.createView(_obj.style.imgUploadView);
	//_obj.imgUploadView.height = 0;
	//_obj.imgUploadView.visible = false;
	
	_obj.uploadPPLabel = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.uploadPPLabel.top = 25;
	//_obj.uploadPPLabel.text = "Please Upload your Passport documents";
      _obj.uploadPPLabel.text ="*Upload a document";
	_obj.btnPPChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
	_obj.btnPPChooseFile.title = 'Choose File';

	_obj.lblChooseFile = Ti.UI.createLabel(_obj.style.lblChooseFile);

	_obj.uploadBorderView = Ti.UI.createView(_obj.style.borderView);
	_obj.uploadBorderView.top = 60;
	
	_obj.imgView = Ti.UI.createImageView(_obj.style.imgView);
	_obj.lblDocSize = Ti.UI.createLabel(_obj.style.lblDocSize);
	
	//////_obj.btnSubmitDriving = Ti.UI.createButton(_obj.style.btnSubmit);
	/////_obj.btnSubmitDriving.title = 'SUBMIT';
	
	/*_obj.passportView.add(_obj.lblNote);
	_obj.passportView.add(_obj.lblNoteTxt);*/  //Commenting on 04-July-17 for CMN67
	_obj.passportView.add(_obj.txtPassportNo);
	_obj.passportView.add(_obj.borderViewS32);
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.passportView.add(_obj.countryView);
	_obj.passportView.add(_obj.borderViewS33);
	
	_obj.passportView.add(_obj.txtPassportIssuePlace);
	_obj.passportView.add(_obj.borderViewSPIP);
	
	_obj.COBView.add(_obj.lblCOB);
	_obj.COBView.add(_obj.imgCOB);
	_obj.passportView.add(_obj.COBView);
	_obj.passportView.add(_obj.borderViewS34);
	
	_obj.passportView.add(_obj.txtSurname);
	_obj.passportView.add(_obj.borderViewS35);
	
	_obj.passportView.add(_obj.txtSurnameBirth);
	_obj.passportView.add(_obj.borderViewS36);
	
	_obj.passportView.add(_obj.txtPOB);
	_obj.passportView.add(_obj.borderViewS37);
	
	//CMN 73
	//added on 10-10-17
    _obj.issuanceView.add(_obj.lblPPIssueDt);
    _obj.issuanceView.add(_obj.imgIssuance);
    _obj.passportView.add(_obj.issuanceView);
    _obj.passportView.add(_obj.borderIssuView);
  
    _obj.expiryView.add(_obj.lblPPExpDt);
    _obj.expiryView.add(_obj.imgExpiry);
    _obj.passportView.add(_obj.expiryView);
    _obj.passportView.add(_obj.borderExpView);
    
    _obj.passportView.add(_obj.txtPassportPersonalNo);
	_obj.passportView.add(_obj.borderPPNoView);
	
	_obj.passportView.add(_obj.lblMRZ);
	_obj.mrzView.add(_obj.txtMRZ1);
	_obj.mrzView.add(_obj.borderMRZ1);
	_obj.mrzView.add(_obj.txtMRZ2);
	_obj.mrzView.add(_obj.borderMRZ2);
	_obj.mrzView.add(_obj.txtMRZ3);
	_obj.mrzView.add(_obj.borderMRZ3);
	_obj.mrzView.add(_obj.txtMRZ4);
	_obj.mrzView.add(_obj.borderMRZ4);
	_obj.mrzView.add(_obj.txtMRZ5);
	_obj.mrzView.add(_obj.borderMRZ5);
	_obj.passportView.add(_obj.mrzView);
	
	_obj.passportView.add(_obj.imgPassport);

     //Added on 11-Oct-17
    _obj.passportView.add(_obj.uploadPPLabel);
    
   
	_obj.imgUploadView.add(_obj.btnPPChooseFile);
	_obj.imgUploadView.add(_obj.imgView);
    _obj.imgUploadView.add(_obj.lblChooseFile);
	_obj.imgUploadView.add(_obj.uploadBorderView);
	
	_obj.passportView.add(_obj.imgUploadView);
	
	_obj.btnPPChooseFile.addEventListener('click', function(e) {
		if (TiGlobals.osname === 'android') {
			if (Ti.Media.hasCameraPermissions()) {
				uploadImage();
			} else {
				Ti.Media.requestCameraPermissions(function(e) {
					if (e.success === true) {
						uploadImage();
					} else {
						/*alert("Access denied, error: " + e.error);*/
					}
				});
			}
		} else {
			uploadImage();
		}

	});
	
    //
    _obj.termsAgreeView.add(_obj.lblAgree);
	_obj.termsAgreeView.add(_obj.lblAgreeTxt);
	_obj.termsAgreeView.add(_obj.imgAgree);
	_obj.passportView.add(_obj.termsAgreeView);
	
	_obj.termsDonotAgreeView.add(_obj.lblDonotAgree);
	_obj.termsDonotAgreeView.add(_obj.lblDonotAgreeTxt);
	_obj.termsDonotAgreeView.add(_obj.imgDonotAgree);
	_obj.passportView.add(_obj.termsDonotAgreeView);
	
	/*//_obj.imgUploadView.add(_obj.btnPassport);  //CMN 74
	_obj.imgUploadView.add(_obj.imgView);
	_obj.imgUploadView.add(_obj.lblDocSize);
	_obj.imgUploadView.add(_obj.btnUpload);
	_obj.passportView.add(_obj.imgUploadView);*/
	
	_obj.passportView.add(_obj.btnSubmitPassport);
	//_obj.passportView.add(_obj.btnCancelPassport);
	
	// Medicare
	
	/*_obj.medicareView.add(_obj.txtMedicareNo);
	_obj.medicareView.add(_obj.borderViewS40);
	_obj.medicareView.add(_obj.txtMedicareRefNo);
	_obj.medicareView.add(_obj.borderViewS41);
	
	_obj.termsAgreeView1.add(_obj.lblAgree1);
	_obj.termsAgreeView1.add(_obj.lblAgreeTxt1);
	_obj.termsAgreeView1.add(_obj.imgAgree1);
	_obj.medicareView.add(_obj.termsAgreeView1);
	
	_obj.termsDonotAgreeView1.add(_obj.lblDonotAgree1);
	_obj.termsDonotAgreeView1.add(_obj.lblDonotAgreeTxt1);
	_obj.termsDonotAgreeView1.add(_obj.imgDonotAgree1);
	_obj.medicareView.add(_obj.termsDonotAgreeView1);
	
	_obj.medicareView.add(_obj.btnSubmitDriving);*/
	
	//_obj.scrollableView.views = [_obj.passportView,_obj.medicareView];
	//_obj.scrollableView.views = [_obj.passportView];
	
	_obj.kycView.add(_obj.txtStreet1);
	_obj.kycView.add(_obj.borderViewS23);
	
	_obj.kycView.add(_obj.txtStreetName);
	_obj.kycView.add(_obj.borderViewSName);
	
	_obj.streetTypeView.add(_obj.lblStreetType);
	_obj.streetTypeView.add(_obj.imgStreetType);
	_obj.kycView.add(_obj.streetTypeView);
	_obj.kycView.add(_obj.borderViewSST);
	
	_obj.kycView.add(_obj.txtCity);
	_obj.kycView.add(_obj.borderViewS27);
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			_obj.stateView.add(_obj.lblState);
			_obj.stateView.add(_obj.imgState);
			_obj.kycView.add(_obj.stateView);
			_obj.kycView.add(_obj.borderViewS28);
        }
        else
        {
			_obj.kycView.add(_obj.lblState);
			_obj.kycView.add(_obj.borderViewS28);
        }
	}
	
	_obj.kycView.add(_obj.txtZip);
	_obj.kycView.add(_obj.borderViewS29);
	
	_obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDay);
	_obj.kycView.add(_obj.dayView);
	_obj.kycView.add(_obj.borderViewS30);
	
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.kycView.add(_obj.mobileView);
	_obj.kycView.add(_obj.borderViewS31);
	
	_obj.kycView.add(_obj.tabView);
	_obj.tabView.add(_obj.lblPassport);
	//_obj.tabView.add(_obj.lblMedicare);
	_obj.tabView.add(_obj.tabSelView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.tabView.add(_obj.tabSelIconView);
	_obj.kycView.add(_obj.passportView);
	
	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYC.add(_obj.globalView);
	_obj.winKYC.open();
	
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
	
	function uploadImage()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"document",'+
				'"docType":"Passport",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(evt) {
			try{
			activityIndicator.hideIndicator();
			if(evt.result.status === "S")
			{
				require('/utils/Console').info('Result ======== ' + evt.result);
				docType = evt.result.response.docType;
				docTypeCode = evt.result.response.docType;
				docUploadPath = evt.result.response.uploadpath;
				try{
		       _obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,docType+'.jpg');
				_obj.imgUpload = 0;
				}		catch(e){}
				var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('',[L('takePicture'), L('selectPicture')],-1);
				optionDialogPhoto.show();
				
				optionDialogPhoto.addEventListener('click',function(evt){
					if(optionDialogPhoto.options[evt.index] !== L('btn_cancel'))
					{
						if(evt.index === 0)
						{
							// Take Picture
							
							Ti.Media.showCamera({
						        showControls:true,
						        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
						        autohide:true,
						        allowEditing:false,
						        autorotate:true,
						        success:function(event) {
						        	
									_obj.image = event.media;
						        	_obj.f.write(_obj.image);
						        	
						        	if(_obj.f.size < 5000000)
						        	{
						        		if(TiGlobals.osname === 'android')
						        		{
						        			_obj.imgView.image = _obj.f.nativePath;	
						        		}
						        		else
						        		{
						        			_obj.imgView.image = _obj.f.resolve();
						        		}
							        	
							        	_obj.lblDocSize.text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
							            _obj.imgView.show();
										_obj.btnPPChooseFile.hide();
									    _obj.lblChooseFile.hide();
							        }
							        else
							        {
							        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
							        }	
						        }
							});
						}
						
						if(evt.index === 1)
						{
							// Obtain an image from the gallery
							
					        Titanium.Media.openPhotoGallery({
					            success:function(event)
					            { 
					            	if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO)
					                {
					                	_obj.image = event.media;				            
									    _obj.f.write(_obj.image);
									    
									    if(_obj.f.size < 5000000)
							        	{
								        	_obj.imgView.image = _obj.f.read();
								        	_obj.lblDocSize.text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
								        	_obj.imgView.show();
										    _obj.btnPPChooseFile.hide();
									        _obj.lblChooseFile.hide();
								        }
								        else
								        {
								        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
								        }
					                }      
					            },
					            cancel:function()
					            {
					            }
					        });
						}
						
						optionDialogPhoto = null;
					}
				});
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}
	
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}

	_obj.btnSubmitPassport.addEventListener('click',function(e){
		//submitKYC();
			// StreetNo
		
		_obj.street1 = _obj.txtStreet1.value;
		if(_obj.txtStreet1.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street No',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street No',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		/*else if(_obj.txtStreet1.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only numbers are allowed in Street No',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}*/
		//Added on 28June17 by sanjivani
		else if(_obj.txtStreet1.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
			require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Street No',[L('btn_ok')]).show();
			_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
					}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreet1.value.charAt(i)+' entered for '+_obj.txtStreet1.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		// Street Name
		_obj.street2 = _obj.txtStreetName.value;
		if(_obj.txtStreetName.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street Name',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreetName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street Name',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtStreetName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Street Name',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreetName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreetName.value.charAt(i)+' entered for '+_obj.txtStreetName.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		// Street Type
		if(_obj.lblStreetType.text === 'Select Street Type*')
		{
			require('/utils/AlertDialog').showAlert('','Please select a street type',[L('btn_ok')]).show();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		// City
		_obj.city = _obj.txtCity.value;
			
		if(_obj.txtCity.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtCity.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtCity.value.search("[^a-zA-Z ]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
		}	
		
		//State
	   
				_obj.state = _obj.lblState.text;
				
			   if(_obj.lblState.text === 'Select State*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your state',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select State*';
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
			
		
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your '+_obj.txtZip.hintText,[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of '+_obj.txtZip.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		
		if(_obj.txtZip.value.length > 0)
		{
	 		var len = _obj.txtZip.value.length;
			var validchars = '';	
			validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
			var currChar, prevChar, nextChar;
			var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var validNum = "0123456789";
			var validAlphFlag = false;
			var validNumFlag = false;
	
			for(i = 0; i <= len - 1; i++)
			{
				currChar = _obj.txtZip.value.charAt(i);
				prevChar = _obj.txtZip.value.charAt(i-1);
				nextChar = _obj.txtZip.value.charAt(i+1);
	
				if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
				{
					validAlphFlag = true;
				}
	
				if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
				{
					validNumFlag = true;
				}
							
				if(validchars.indexOf(currChar) == -1)
				{
					if(countryName[0] != 'Canada')
					{
						require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
				}
	
				if(currChar == ' ' && prevChar == ' ' || prevChar == ' ' && currChar == ' ' && nextChar == ' ' || prevChar == ' ' && currChar == ' ' && i == len-1 || currChar == ' ' && i == len-1)
				{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
				
					break;
				}
			}
						
			if((countryName[0] != 'United Arab Emirates') && (countryName[0] != 'UAE') && (countryName[0] != 'Hong Kong')) //Country check for HK & UAE
			{		
				if((countryName[0] == 'Singapore') || (countryName[0] == 'Australia') || (countryName[0] == 'United States') || (countryName[0] == 'Portugal'))
				{
					if(countryName[0] != 'Portugal')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
	
					if((countryName[0] == 'Singapore') && (len != 6))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 6 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'Australia') && (len != 4))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'United States') && (len != 5))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'Portugal'))
					{			
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else if( len < 3 || len > 25)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 25 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
				}
				else
				{
					if(countryName[0] == 'Canada')
					{
						var pcode = _obj.txtZip.value;
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else if(pcode.indexOf(" ") != 3)
						{
							require('/utils/AlertDialog').showAlert('','There must be a space after 3rd character in '+_obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'United Kingdom')
					{			
						if( len < 5 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 5 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else
					{
						if( len < 3 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
	
					if((countryName[0] == 'France') || (countryName[0] == 'Germany') || (countryName[0] == 'Italy') || (countryName[0] == 'Euroland') || (countryName[0] == 'New Zealand'))
					{
						var pncode = _obj.txtZip.value;
						if(pncode.split(" ").length > 2)
						{
							require('/utils/AlertDialog').showAlert('','Only one single space is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
							
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Belgium')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
	
						if(len != 4)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Spain')
					{
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'United States')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						if(len != 5)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Japan')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] != 'Spain')
					{ 
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
				}
			} 
		 }
		 
		////////////////////// Mobile No ////////////////////
		if(_obj.txtMobile.value === '' || _obj.txtMobile.value === 'Mobile Number*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(countryName[0] === 'United Kingdom')
		{ 						
			if(_obj.txtMobile.value.charAt(0) === 0 && _obj.txtMobile.value != '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Mobile Number without 0 in the beginning',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
		|| countryName[0] === 'Finland' || countryName[0] === 'France' 
		|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
		|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
		|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
		 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal' || countryName[0] === 'Belgium' || countryName[0] === 'Netherlands')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Germany')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 11))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Cyprus')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 11) && countryName[0] === 'New Zealand')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Spain')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 10 || _obj.txtMobile.value.length > 11) && countryName[0] === 'Austria')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 7 || _obj.txtMobile.value.length > 9) && ((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')))
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else
		{
			
		}
		
		////////////////////// Daytime No ////////////////////
		//if UAE than it is not cumpusory
		if(countryName[0] != 'United Arab Emirates' && countryName[0] != 'UAE') 
		{		
			if(_obj.txtDay.value === '' || _obj.txtDay.value === 'Day Time Number*')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDay.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDay.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if((_obj.txtDay.value.length < 9 || _obj.txtDay.value.length > 10) && countryName[0] === 'Spain')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(_obj.txtDay.value.length != 0 && _obj.txtDay.value.length < 7 && countryName[0] === 'New Zealand')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if((_obj.txtDay.value.length < 3 || _obj.txtDay.value.length > 9) && countryName[0] === 'Germany')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(countryName[0] === 'United Kingdom')
			{
				if(_obj.txtDay.value.charAt(0) === 0 && _obj.txtDay.value != '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Day Time Number without 0 in the beginning',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
				else if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
			|| countryName[0] === 'Finland' || countryName[0] === 'France' 
			|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
			|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
			|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
			 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 9))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'Belgium' || countryName[0] === 'Netherlands' || countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 8))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
		}
		if(((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')) && _obj.txtDay.value.length > 0)
		{
			if(_obj.txtDay.value.length < 7 || _obj.txtDay.value.length > 9)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		
		if(_obj.txtMobile.value === _obj.txtDay.value)
		{
			require('/utils/AlertDialog').showAlert('','Day Time No. and Mobile No. cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDay.value = '';
    		_obj.txtDay.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		
		// If Passport
		
		if(_obj.tab === 'passport')
		{
			// Passport No
			if(_obj.txtPassportNo.value.trim() === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport number',[L('btn_ok')]).show();
	    		_obj.txtPassportNo.value = '';
	    		_obj.txtPassportNo.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;		
			}
			else
			{
				
			}
			
			// PassportCountry
			//if(_obj.lblCountry.text === 'Passport Country*')
			if(_obj.lblCountry.text === 'Passport Issuing Country*') //Updated on 04-July-17 for CMN67
			{
				require('/utils/AlertDialog').showAlert('','Please select a passport issuing country',[L('btn_ok')]).show();
				_obj.kycView.scrollTo(0,250);
	    		return;		
			}
			else
			{
				
			}
			
			//Place of issue
			
			if(_obj.txtPassportIssuePlace.value === "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport place of issue',[L('btn_ok')]).show();
	    		_obj.txtPassportIssuePlace.value = '';
	    		_obj.txtPassportIssuePlace.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassportIssuePlace.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of passport place of issue',[L('btn_ok')]).show();
	    		_obj.txtPassportIssuePlace.value = '';
	    		_obj.txtPassportIssuePlace.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;
			}
			else if(_obj.txtPassportIssuePlace.value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in passport place of issue field',[L('btn_ok')]).show();
	    		_obj.txtPassportIssuePlace.value = '';
	    		_obj.txtPassportIssuePlace.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;
			}
			else
			{
				
			}
				
			// Country of Birth
			//Updated on 04-July-17 for CMN67
			if(_obj.lblCOB.text === 'Country of Birth*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a country of birth',[L('btn_ok')]).show();
	    		return;		
			}
			else
			{
				
			}
			
			// Surname at Citizenship
			if(_obj.txtSurname.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a surname at citizenship',[L('btn_ok')]).show();
	    		_obj.txtSurname.value = '';
	    		_obj.txtSurname.focus();
	    		return;		
			}
			else
			{
				
			}
			
			// Surname at time of birth
			if(_obj.txtSurnameBirth.value === '')
			{    //Updated on 04-July-17 for CMN67
				//require('/utils/AlertDialog').showAlert('','Please provide a surname at birth',[L('btn_ok')]).show();
				require('/utils/AlertDialog').showAlert('','Please provide a Surname at time of birth',[L('btn_ok')]).show();
				_obj.txtSurnameBirth.value = '';
	    		_obj.txtSurnameBirth.focus();
	    		_obj.kycView.scrollTo(0,250);
	    		return;		
			}
			else
			{
				
			}
			
			// POB
			if(_obj.txtPOB.value === '')
			{
				//Updated on 04-July-17 for CMN67
				//require('/utils/AlertDialog').showAlert('','Please provide a passport place of birth',[L('btn_ok')]).show();
				require('/utils/AlertDialog').showAlert('','Please provide a passport place of birth as per passport',[L('btn_ok')]).show();
	    		_obj.txtPOB.value = '';
	    		_obj.txtPOB.focus();
	    		_obj.kycView.scrollTo(0,250);
	    		return;		
			}
			//CMN 73 added on 13OCt 17
			
			Ti.API.info("IN SUBMIT:---",_obj.lblPPExpDt.value);  
		    Ti.API.info("IN SUBMIT1:---",_obj.lblPPIssueDt.value);
	        Ti.API.info("IN SUBMIT2:---",_obj.lblPPExpDt.value - _obj.lblPPIssueDt.value);
			var timeDiff = Math.abs(_obj.lblPPExpDt.value - _obj.lblPPIssueDt.value);
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            
            /*_obj.txtPassportPersonalNo

'*Personal No'

_obj.lblPPIssueDt.text = '*Document Issuance date';

_obj.lblPPExpDt.text = '*Document Expiry date';

	_obj.txtPassportPersonalNo.hintText = '*Personal No';

             */
            
		 
			// Passport Issuance Date
			if (_obj.lblPPIssueDt.text === '*Document Issuance date') {
				require('/utils/AlertDialog').showAlert('', 'Please provide Passport Issuance Date', [L('btn_ok')]).show();
				return;
			} else {

			}
			
			
			// Passport Expiry Date
			if (_obj.lblPPExpDt.text === '*Document Expiry date') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a passport expiry date', [L('btn_ok')]).show();
				return;
			} 
			
			 
	     if(diffDays < 1825){  //cmn 55a
		     require('/utils/AlertDialog').showAlert('','Passport Expiry date & Issuance Date should have minimum of 5 yrs difference.',[L('btn_ok')]).show();
    		_obj.lblPPExpDt.value = '';
    		//_obj.lblPPExpDt.focus();
    		_obj.lblPPExpDt.text = '*Document Expiry date*';
    		
    		return;
	     }
	     
	     	// Passport Personal No
			if (_obj.txtPassportPersonalNo.value === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide a passport personal number', [L('btn_ok')]).show();
				_obj.txtPassportPersonalNo.value = '';
				_obj.txtPassportPersonalNo.focus();
				return;
			}
	     
	     // MRZ Validation Starts
			_obj.mrz1 = _obj.txtMRZ1.value;
			_obj.mrz2 = _obj.txtMRZ3.value;
			_obj.mrz3 = _obj.txtMRZ5.value;
			if (_obj.mrz1.trim() === '' || _obj.mrz2.trim() === '' || _obj.mrz3.trim() === '') {
				require('/utils/AlertDialog').showAlert('', 'Please provide your valid MRZ Number', [L('btn_ok')]).show();
				return;
			} else if (_obj.mrz1.length != 8) {
				require('/utils/AlertDialog').showAlert('', 'The first section of MRZ Number should be of 8 digits', [L('btn_ok')]).show();
				_obj.txtMRZ1.value = '';
				_obj.txtMRZ1.focus();
				return;
			} else if (_obj.mrz2.length != 19) {
				require('/utils/AlertDialog').showAlert('', 'The second section of MRZ Number should be of 19 digits', [L('btn_ok')]).show();
				_obj.txtMRZ2.value = '';
				_obj.txtMRZ2.focus();
				return;
			} else if (_obj.mrz3.length != 2) {
				require('/utils/AlertDialog').showAlert('', 'The third section of MRZ Number should be of 2 digits', [L('btn_ok')]).show();
				_obj.txtMRZ3.value = '';
				_obj.txtMRZ3.focus();
				return;
			} else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtMRZ1.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Special characters not allowed in MRZ no.',[L('btn_ok')]).show();
    		            _obj.txtMRZ1.value = '';
			    		_obj.txtMRZ1.focus();
						return;
					}else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtMRZ3.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Special characters not allowed in MRZ no.',[L('btn_ok')]).show();
    		            _obj.txtMRZ3.value = '';
			    		_obj.txtMRZ3.focus();
						return;
					}else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtMRZ5.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Special characters not allowed in MRZ no.',[L('btn_ok')]).show();
    		            _obj.txtMRZ5.value = '';
			    		_obj.txtMRZ5.focus();
						return;
					}else {

			}
			
			
		
			
			if(_obj.term === null)
			{
				require('/utils/AlertDialog').showAlert('','Please select any one of the option',[L('btn_ok')]).show();
	    		return;
			}
			else
			{
				
			}
		}
		///end of submit kyc
		var dateSplit = _obj.lblPPIssueDt.text.split('/');
		var dt = dateSplit[0] + '-' + dateSplit[1] + '-' + dateSplit[2];
        Ti.API.info("DT Issue****",dt);
        
        /*var dateSplit = _obj.lblExpiry.text.split('/');
		var dt = dateSplit[0]+'-'+require('/utils/Date').monthShort(dateSplit[1])+'-'+dateSplit[2];
		*/
		var dateSplit1 = _obj.lblPPExpDt.text.split('/');
		var dt1 = dateSplit1[0] + '-' + dateSplit1[1] + '-' + dateSplit1[2];
		var RequestId  = Math.floor((Math.random() * 1000000000) + 10000);
       Ti.API.info("DT111 Exp****",dt1);
    //Commented on 29th Nov for unable to upload document -testnew server
		//var targetFile = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
         var targetFile = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');

 
		var filename = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy') + ".jpg";
		var docname = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
		var relativepath = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/";
		if(_obj.f != null){
		var xmlHttp = Ti.Network.createHTTPClient();
      	activityIndicator.showIndicator();
  		xmlHttp.onload = function(e)
		{
			activityIndicator.hideIndicator();
   
			var test = JSON.parse(xmlHttp.responseText);
			Ti.API.info("In xmlresponseText:--", test.result);
			Ti.API.info("In xmlresponseText:--", test.path);
			var path = test.path;
			var pathlength = test.path.length;
			var dot = path.lastIndexOf(".");

			var extn = path.substring(dot, pathlength);
			Ti.API.info("In extn:--", extn);
			targetFile = targetFile + extn;

		

			if (test.result === 'true' || test.result === true || test.result === 1) {
				activityIndicator.showIndicator();
				Ti.API.info("Inside if:--");
			
			
			var xhr = require('/utils/XHR');
		
		    xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
			'"requestId":"'+ RequestId +'",'+
            '"channelId":"'+TiGlobals.channelId+'",'+
            '"partnerId":"'+TiGlobals.partnerId+'",'+
		    '"requestName":"CUSTKYCIDDETAILSWITHDOCUPLOAD",'+
			'"ipAddress":"'+TiGlobals.ipAddress+'",'+
			'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
			'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
			'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
			'"originatingCountryName":"' + countryName[0] + '",' + 
            '"IDType":"Passport",' + 
			'"IDNumber":"' + _obj.txtPassportNo.value + '",' + 
			'"IDIssueBy":"'+_obj.lblCountry.text+'",'+ //"F",'+//"' + _obj.lblPPIssueBy.text + '",' +   //Not available country list for issue by hense assigning 'F' temporarily
			'"IDIssueAt":"'+_obj.txtPassportIssuePlace.value+'",'+ 
			'"IDIssueDate":"' + dt + '",' + 
			'"IDIssueExpiryDate":"' + dt1 + '",' + 
			'"passportPersonalNo":"' + _obj.txtPassportPersonalNo.value + '",' + 
			'"originatingCountry":"' + countryCode[0] + '",' + 
			'"mrzLine":"' + _obj.mrz1 + _obj.mrz2 + _obj.mrz3 + '",' + 
			'"sourceFlag":"02",'+ 
			'"operatingmodeId":"INTL",'+
			'"roleId":"SEND",'+
			'"flatNo":"'+_obj.street1+'",'+
			'"buildingNo":"",'+
			'"street1":"'+_obj.street2+'",'+
			'"street2":"'+_obj.streetType+'",'+
			'"locality":"",'+
			'"subLocality":"",'+
			'"vendorId":"F",'+
			'"zipCode":"'+_obj.txtZip.value+'",'+
			'"city":"'+_obj.city+'",'+
			'"state":"'+_obj.state+'",'+
			'"ssn":"",'+
			'"passNationality":"",'+
	        //'"passportCountry":"'+_obj.lblCountry.text+'",'+  
	        '"passportCountry":"",'+              //updated on 10 Nov 17   
			'"passportBirthPlace":"'+_obj.txtPOB.value+'",'+
			'"medicareNo":"",'+
			'"medicareRefNo":"",'+
            '"surnameAtCitizenShip":"'+_obj.txtSurname.value+'",'+
			'"surnameAtBirth":"'+_obj.txtSurnameBirth.value+'",'+
			'"countryOfBirth":"'+_obj.lblCOB.text+'",'+
            '"numOfYearsResidingInCountry":"",'+
            '"currentEmployerName":"",'+
            '"designation":"",'+
            '"annualIncome":"",'+
            '"countryCode":"'+Ti.App.Properties.getString('sourceCountryISDN')+'",'+
			'"dayTimeNumber":"'+_obj.lblDay.text+_obj.txtDay.value+'",'+  			   
            '"mobileNo":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+
			'"privacyPolicyFlag":"'+_obj.privacyFlag+'",'+ //'"privacyPolicyFlag":"Y",'+  
			'"docPathArray":["' + targetFile + '"]' + 
			
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseCode === "01")  
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.responseMessage);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.responseMessage);
				}
				
				try{
					Ti.App.fireEvent('changekycFlag');
				}catch(e){}
				
				destroy_kyc();
			}else if(e.result.responseCode === "00"){  //Added on 27th Nov during CMN 84a
				
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.responseMessage);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.responseMessage);
				}
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					
				}
			}
	       xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		
		}else {
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('', 'Error uploading document. Please try again later.', [L('btn_ok')]).show();
			}
			/*if(xmlHttp.responseText === '1' || xmlHttp.responseText === 1)
			{
				var xhr = require('/utils/XHR');
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"KYCVERIFICATIONDOCUPLOAD",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
						'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
						'"countryCode":"'+countryCode[0]+'",'+
						'"docType":"'+docType+'",'+  // added , on 11th aug
						'"docRequestId":"'+docRequestId+'",'+// added , on 11th aug
						'"docPath":"'+targetFile+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
					
				function xhrSuccess(evt) {
					try{
					activityIndicator.hideIndicator();
					if(evt.result.responseFlag === "S")
					{
						if(TiGlobals.osname === 'android')
						{	
							require('/utils/AlertDialog').toast('Document uploaded successfully');
						}
						else
						{
							require('/utils/AlertDialog').iOSToast('Document uploaded successfully');
						}
						
						myDocuments();
					}
					else
					{
						if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
					}catch(e){}
				}
		
				function xhrError(evt) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
				
			}
			else
			{
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('','Error uploading document. Please try again later.',[L('btn_ok')]).show();	
			}*/
		};
		
		xmlHttp.onerror = function(e)
		{
			activityIndicator.hideIndicator();
		};
		
		require('/utils/Console').info(JSON.stringify({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadPath:relativepath}));
		
		xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php',true);
		try{
		xmlHttp.send({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadpath:relativepath});
		}
		catch(e){}
		}
		else{
			require('/utils/AlertDialog').showAlert('', 'Please select document to upload.', [L('btn_ok')]).show();
			
		}
	});
	
	function changeTabs(selected)
	{
		if(selected === 'passport')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblPassport.color = TiFonts.FontStyle('whiteFont');
			_obj.lblPassport.backgroundColor = '#6F6F6F';
			_obj.lblMedicare.color = TiFonts.FontStyle('blackFont');
			_obj.lblMedicare.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblMedicare.color = TiFonts.FontStyle('whiteFont');
			_obj.lblMedicare.backgroundColor = '#6F6F6F';
			_obj.lblPassport.color = TiFonts.FontStyle('blackFont');
			_obj.lblPassport.backgroundColor = '#E9E9E9';
		}
	}
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'passport' && _obj.tab !== 'passport')
		{
			_obj.scrollableView.movePrevious();
			changeTabs('passport');
			_obj.tab = 'passport';
		}
		
		if(e.source.sel === 'medicare' && _obj.tab !== 'medicare')
		{
			_obj.scrollableView.moveNext();
			changeTabs('medicare');
			_obj.tab = 'medicare';
		}
	});
	
	/*function submitKYC()
	{
		// Apartment/House No.
		/*_obj.flat = _obj.txtAptNo.value; 
		if(_obj.txtAptNo.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your ' + _obj.txtAptNo.hintText,[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtAptNo.hintText + ' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('',_obj.txtAptNo.hintText + ' cannot contain  |  or  \'  or  ` or  \" or #',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtAptNo.value.charAt(i)+' entered for '+_obj.txtAptNo.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}*/
		
	
		
			
		// xhr call
		
		/*activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CUSTKYCVERIFICATIONREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"operatingmodeId":"INTL",'+
				'"roleId":"SEND",'+
				'"flatNo":"'+_obj.street1+'",'+
				'"buildingNo":"",'+
				'"street1":"'+_obj.street2+'",'+
				'"street2":"'+_obj.streetType+'",'+
				'"locality":"",'+
				'"subLocality":"",'+
				'"vendorId":"F",'+
				'"zipCode":"'+_obj.txtZip.value+'",'+
				'"city":"'+_obj.city+'",'+
				'"state":"'+_obj.state+'",'+
				'"country":"'+countryName[0]+'",'+
				'"ssn":"",'+
				'"mrzLine":"",'+
				'"drivingLicenseNo":"",'+
				'"passportNo":"'+_obj.txtPassportNo.value+'",'+
				'"passportIssuePlace":"'+_obj.txtPassportIssuePlace.value+'",'+
				'"passNationality":"'+_obj.lblCOB.text+'",'+
				'"passportPersonalNo":"",'+
				'"passportCountry":"'+_obj.lblCountry.text+'",'+
				'"passportBirthPlace":"'+_obj.txtPOB.value+'",'+
				'"medicareNo":"",'+
				'"medicareRefNo":"",'+
				'"surnameAtCitizenShip":"'+_obj.txtSurname.value+'",'+
				'"surnameAtBirth":"'+_obj.txtSurnameBirth.value+'",'+
				'"countryOfBirth":"'+_obj.lblCOB.text+'",'+
				'"passExpiryDate":"",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"dayTimeNumber":"'+_obj.lblDay.text+_obj.txtDay.value+'",'+  // Changed on 17th aug for not getting country code in API req
				'"mobileNo":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+
			    '"privacyPolicyFlag":"Y"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
		

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")  //by Sanjivani on 24/11/16 for bug.1175
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				try{
					Ti.App.fireEvent('changekycFlag');
				}catch(e){}
				
				destroy_kyc();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
    _obj.btnSubmitDriving.addEventListener('click',function(e){
		submitKYC();
	});
	
	_obj.btnSubmitPassport.addEventListener('click',function(e){
		submitKYC();
	});*/
   
   _obj.btnCancelPassport.addEventListener('click', function(e) {
       destroy_kyc();
	});
	_obj.imgClose.addEventListener('click',function(e){
		destroy_kyc();
	});
	
	_obj.winKYC.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_kyc();
				alertDialog = null;
			}
		});
	});
	
	function country()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCountry.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCountry.text = 'Passport Issuing Country*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function nationality()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Nationality',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCOB.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCOB.text = 'Passport Nationality*';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function destroy_kyc()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove AUS KYC start ##############');
			
			_obj.winKYC.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYC,_obj.globalView);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyc',destroy_kyc);
			require('/utils/Console').info('############## Remove AUS KYC end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_kyc', destroy_kyc);
}; // KYCModal()
