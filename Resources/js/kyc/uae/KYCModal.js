exports.KYCModal = function()
{
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	
	require('/lib/analytics').GATrackScreen('KYC Form ' + countryName[0]);
	
	var _obj = {
		style : require('/styles/kyc/others/KYC').KYC,
		winKYC : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		kycView : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		
		doneDay : null,
		dayView : null,
		lblDay : null,
		txtDay : null,
		borderViewS30 : null,
		
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS31 : null,
		
		tabView : null,
		tabSelView : null,
		lblPassport : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		btnSubmitPassport : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		tab : null, 
		
		//CMN73
		issuanceView:null,
        lblPPIssueDt:null,
        imgIssuance:null,
        borderIssuView:null,
    
        countryViewIssueBy:null,
        lblPPIssueBy:null,
        lblExpiry:null,
        imgCountryIssueBy:null,
        borderIssueByView:null,
        
        f : null,
		imgUpload : null,
		image : null,
		data_to_send : null,
		lblDocSize:null,
		imgPassport:null,
        
        imgUploadView:null,
        uploadPPLabel:null,
        btnPPChooseFile:null,
        imgView:null,
        lblChooseFile:null,
        uploadBorderView:null,
        
        mrz1:null,
        mrz2:null,
        mrz3:null
	};
	
	var docType = null;
	var docRequestId = null;
	var docTypeCode = null;
	var docUploadPath = null;
	
	_obj.tab = 'passport';
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winKYC = Titanium.UI.createWindow(_obj.style.winKYC);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYC);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	/////////////////// KYC ///////////////////
	
	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);
	
	_obj.txtAptNo = Ti.UI.createTextField(_obj.style.txtAptNo);
	
		_obj.txtAptNo.hintText = "Flat/House No.*";
	
	_obj.txtAptNo.maxLength = 25;
	_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
	
	

		_obj.txtBldgNo = Ti.UI.createTextField(_obj.style.txtBldgNo);
		_obj.txtBldgNo.hintText = 'Building No./Name';
		_obj.txtBldgNo.maxLength = 25;
		_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtStreet1 = Ti.UI.createTextField(_obj.style.txtStreet1);
	
	
		_obj.txtStreet1.hintText = "Street*";
		_obj.txtStreet1.maxLength = 25;
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
 	
	

		_obj.cityView = Ti.UI.createView(_obj.style.cityView);
		_obj.txtCity = Ti.UI.createLabel(_obj.style.txtCity);
		_obj.txtCity.top = 0;
		_obj.txtCity.left = 0;
		_obj.txtCity.text = 'Select City*';
		_obj.imgCity = Ti.UI.createImageView(_obj.style.imgCity);
		
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.cityView.addEventListener('click',function(){
			activityIndicator.showIndicator();
			var cityOptions = [];
			
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"'+TiGlobals.bcmConfig+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"source":"'+TiGlobals.bcmSource+'",'+
					'"type":"citylisting",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"corridor":"'+countryName[0]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				try{
				activityIndicator.hideIndicator();
				if(e.result.status === "S")
				{
					for(var i=0;i<e.result.response[0].citylisting.UAE.length;i++)
					{
						cityOptions.push(e.result.response[0].citylisting.UAE[i]);
					}
					
					var optionDialogCity = require('/utils/OptionDialog').showOptionDialog('City',cityOptions,-1);
					optionDialogCity.show();
					
					optionDialogCity.addEventListener('click',function(evt){
						if(optionDialogCity.options[evt.index] !== L('btn_cancel'))
						{
							_obj.txtCity.text = optionDialogCity.options[evt.index];
							optionDialogCity = null;
						}
						else
						{
							optionDialogCity = null;
							_obj.txtCity.text = 'Select City*';
						}
					});	
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_kyc();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
				}catch(e){}
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
	
            _obj.stateView = Ti.UI.createView(_obj.style.stateView);
			_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
			_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
			
			
				_obj.lblState.text = 'Select Emirates*';	
			
			
			_obj.stateView.addEventListener('click',function(){
				activityIndicator.showIndicator();
				var stateOptions = [];
				
				var xhr = require('/utils/XHR');
		
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"country":"'+countryName[0]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrSuccess(e) {
					try{
					activityIndicator.hideIndicator();
					if(e.result.responseFlag === "S")
					{
						var splitArr = e.result.countryRelatedStates.split('~');
				
						for(var i=0;i<splitArr.length;i++)
						{
							stateOptions.push(splitArr[i]);
						}
						
						var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State',stateOptions,-1);
						optionDialogState.show();
						
						optionDialogState.addEventListener('click',function(evt){
							if(optionDialogState.options[evt.index] !== L('btn_cancel'))
							{
								_obj.lblState.text = optionDialogState.options[evt.index];
								optionDialogState = null;
							}
							else
							{
								optionDialogState = null;
								_obj.lblState.text = 'Select Emirates*';	
							}
						});	
					}
					else
					{
						if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
					}catch(e){}
				}
		
				function xhrError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
			});

	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Zip Code*';
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
	
	////// Pin code hintText ///////
	
            _obj.txtZip.hintText = 'P. O. Box (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
		
	_obj.borderViewS29 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDay.blur();
	});
	
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDay = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtDay.hintText = 'Day Time Number*';
	_obj.txtDay.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDay.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS30 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobile.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number*';
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	
	// Set HintText for mobile and day nos
	
    _obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
	_obj.txtMobile.maxLength = 9;

	_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
	_obj.txtDay.maxLength = 9;
			
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblPassport = Ti.UI.createLabel(_obj.style.lblPassport);
	_obj.lblPassport.text = 'Passport Details';
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
		
	// Passport 
	_obj.passportView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote.text = 'Note';
	
	_obj.lblNoteTxt = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt.text = '\u00B7 If you are entering your passport details, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your passport';
	_obj.lblNoteTxt.top = 5; 
	
	_obj.txtPassportNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtPassportNo.maxLength = 9;
	_obj.txtPassportNo.hintText = '*Passport No.';
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCountry.text = '*Passport Issue Place';
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS33 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView.addEventListener('click',function(){
		country();
	});
	
	_obj.expiryView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblExpiry = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblExpiry.text = '*Passport Expiry Date';
	_obj.imgExpiry = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS34 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.expiryView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblExpiry,'normal2');
	});
	
	_obj.nationalityView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblNationality = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblNationality.text = '*Passport Nationality';
	_obj.imgNationality = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS35 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.nationalityView.addEventListener('click',function(){
		nationality();
	});
	
	//CMN73
	//Document Issuance Date
	//Issued By
	//Upload Document
	
	//Document Issuance date
	_obj.issuanceView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblPPIssueDt = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblPPIssueDt.text = '*Document Issuance date';
	_obj.imgIssuance = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderIssuView = Ti.UI.createView(_obj.style.borderView);

    _obj.issuanceView.addEventListener('click', function(e) {
		TiGlobals.issuFlagUAE = false;
		require('/utils/DatePicker').DatePicker(_obj.lblPPIssueDt, 'normal1');

	});
	//Issued By
	_obj.countryViewIssueBy = Ti.UI.createView(_obj.style.countryView);
	_obj.lblPPIssueBy = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblPPIssueBy.text = '*Issued By';
	
	_obj.imgCountryIssueBy = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderIssueByView = Ti.UI.createView(_obj.style.borderView);

	_obj.countryViewIssueBy.addEventListener('click', function() {
		countryIssueBy();
	});
   
   _obj.imgUploadView = Ti.UI.createView(_obj.style.imgUploadView); 
   
    //Upload Document
    _obj.uploadPPLabel = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.uploadPPLabel.top = 25;
	_obj.uploadPPLabel.text = "*Upload a document"; //"Please Upload your Passport documents";

	_obj.btnPPChooseFile = Ti.UI.createButton(_obj.style.btnChoose);
	_obj.btnPPChooseFile.title = 'Choose File';

	_obj.lblChooseFile = Ti.UI.createLabel(_obj.style.lblChooseFile);

	_obj.uploadBorderView = Ti.UI.createView(_obj.style.borderView);
	_obj.uploadBorderView.top = 60;
	
	_obj.imgView = Ti.UI.createImageView(_obj.style.imgView);
	_obj.lblDocSize = Ti.UI.createLabel(_obj.style.lblDocSize);
	
	function issueDateDtlUAE(evt) { // CMN 73
			    var issuDate = evt.data;
			    Ti.API.info("IN ISSUEDATE",issuDate);
			   
			     try{
			    _obj.lblPPIssueDt.value = issuDate.getTime();
			    Ti.API.info("IN ISSUDIF", _obj.lblPPIssueDt.value);
			    }catch(e){}
			    TiGlobals.issuFlagUAE = true;
			}
			
			Ti.App.addEventListener('issueDateDtlUAE',issueDateDtlUAE);
		
		function expDateDtlUAE(evt) { // CMN 73
			    var expDate = evt.data;
			    Ti.API.info("IN EXPDATE",expDate);
			    try{
			   _obj.lblExpiry.value = expDate.getTime();
			    Ti.API.info("IN AGEDIF",_obj.lblExpiry.value );
			    }catch(e){}
			    TiGlobals.issuFlagUAE = false;
			}
			
			Ti.App.addEventListener('expDateDtlUAE',expDateDtlUAE);
		
 //end of cmn73
	
	_obj.txtPassportPersonalNo = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	_obj.txtPassportPersonalNo.maxLength = 16;
	_obj.txtPassportPersonalNo.hintText = '*Passport Personal No.';
	_obj.borderViewS36 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblMRZ = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblMRZ.text = 'MRZ No.*';
	_obj.lblMRZ.top = 15;
	_obj.mrzView = Ti.UI.createView(_obj.style.mrzView);
	
	_obj.txtMRZ1 = Ti.UI.createTextField(_obj.style.txtMRZ1);
	_obj.txtMRZ1.maxLength = 8;
	_obj.txtMRZ1.hintText = 'xxxxxxxx';
	_obj.borderMRZ1 = Ti.UI.createView(_obj.style.borderMRZ1);
	
	_obj.txtMRZ2 = Ti.UI.createTextField(_obj.style.txtMRZ2);
	_obj.borderMRZ2 = Ti.UI.createView(_obj.style.borderMRZ2);
	_obj.txtMRZ2.value = '<';
	_obj.txtMRZ2.editable = false;
	
	_obj.txtMRZ3 = Ti.UI.createTextField(_obj.style.txtMRZ3);
	_obj.txtMRZ3.maxLength = 19;
	_obj.txtMRZ3.hintText = 'xxxxxxxxxxxxxxxxxxx';
	_obj.borderMRZ3 = Ti.UI.createView(_obj.style.borderMRZ3);
	
	_obj.txtMRZ4 = Ti.UI.createTextField(_obj.style.txtMRZ4);
	_obj.borderMRZ4 = Ti.UI.createView(_obj.style.borderMRZ4);
	_obj.txtMRZ4.value = '<<<<<<<<<<<<<';
	_obj.txtMRZ4.editable = false;
	
	_obj.txtMRZ5 = Ti.UI.createTextField(_obj.style.txtMRZ5);
	_obj.borderMRZ5 = Ti.UI.createView(_obj.style.borderMRZ5);
	_obj.txtMRZ5.maxLength = 2;
	_obj.txtMRZ5.hintText = 'xx';
	
	_obj.imgPassport = Ti.UI.createImageView(_obj.style.imgPassport);
	
	_obj.btnSubmitPassport = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitPassport.title = 'SUBMIT';
	
	_obj.passportView.add(_obj.lblNote);
	_obj.passportView.add(_obj.lblNoteTxt);
	_obj.passportView.add(_obj.txtPassportNo);
	_obj.passportView.add(_obj.borderViewS32);
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.passportView.add(_obj.countryView);
	_obj.passportView.add(_obj.borderViewS33);
	_obj.expiryView.add(_obj.lblExpiry);
	_obj.expiryView.add(_obj.imgExpiry);
	_obj.passportView.add(_obj.expiryView);
	_obj.passportView.add(_obj.borderViewS34);
	_obj.nationalityView.add(_obj.lblNationality);
	_obj.nationalityView.add(_obj.imgNationality);
	_obj.passportView.add(_obj.nationalityView);
	_obj.passportView.add(_obj.borderViewS35);
	
	//CMN73
	   
    _obj.issuanceView.add(_obj.lblPPIssueDt);
    _obj.issuanceView.add(_obj.imgIssuance);
    _obj.passportView.add(_obj.issuanceView);
	_obj.passportView.add(_obj.borderIssuView);
    
    _obj.countryViewIssueBy.add(_obj.lblPPIssueBy);
    _obj.countryViewIssueBy.add(_obj.imgCountryIssueBy);
    _obj.passportView.add(_obj.countryViewIssueBy);
	_obj.passportView.add(_obj.borderIssueByView);
    //
	_obj.passportView.add(_obj.txtPassportPersonalNo);
	_obj.passportView.add(_obj.borderViewS36);
	_obj.passportView.add(_obj.lblMRZ);
	_obj.mrzView.add(_obj.txtMRZ1);
	_obj.mrzView.add(_obj.borderMRZ1);
	_obj.mrzView.add(_obj.txtMRZ2);
	_obj.mrzView.add(_obj.borderMRZ2);
	_obj.mrzView.add(_obj.txtMRZ3);
	_obj.mrzView.add(_obj.borderMRZ3);
	_obj.mrzView.add(_obj.txtMRZ4);
	_obj.mrzView.add(_obj.borderMRZ4);
	_obj.mrzView.add(_obj.txtMRZ5);
	_obj.mrzView.add(_obj.borderMRZ5);
	_obj.passportView.add(_obj.mrzView);
	_obj.passportView.add(_obj.imgPassport);
	
	//CMN73
	_obj.passportView.add(_obj.uploadPPLabel);
    
   
	_obj.imgUploadView.add(_obj.btnPPChooseFile);
	_obj.imgUploadView.add(_obj.imgView);
    _obj.imgUploadView.add(_obj.lblChooseFile);
	//_obj.imgUploadView.add(_obj.uploadBorderView);
	
	_obj.passportView.add(_obj.imgUploadView);
	
	_obj.btnPPChooseFile.addEventListener('click', function(e) {
		if (TiGlobals.osname === 'android') {
			if (Ti.Media.hasCameraPermissions()) {
				uploadImage();
			} else {
				Ti.Media.requestCameraPermissions(function(e) {
					if (e.success === true) {
						uploadImage();
					} else {
						/*alert("Access denied, error: " + e.error);*/
					}
				});
			}
		} else {
			uploadImage();
		}

	});
	_obj.passportView.add(_obj.btnSubmitPassport);
	
	_obj.kycView.add(_obj.txtAptNo);
	_obj.kycView.add(_obj.borderViewS21);
	

		_obj.kycView.add(_obj.txtBldgNo);
		_obj.kycView.add(_obj.borderViewS22);
	
	_obj.kycView.add(_obj.txtStreet1);
	_obj.kycView.add(_obj.borderViewS23);
	
	
	
		_obj.cityView.add(_obj.txtCity);
		_obj.cityView.add(_obj.imgCity);
		_obj.kycView.add(_obj.cityView);
		_obj.kycView.add(_obj.borderViewS27);

            _obj.stateView.add(_obj.lblState);
			_obj.stateView.add(_obj.imgState);
			_obj.kycView.add(_obj.stateView);
			_obj.kycView.add(_obj.borderViewS28);
       
	
	_obj.kycView.add(_obj.txtZip);
	_obj.kycView.add(_obj.borderViewS29);
	
	_obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDay);
	_obj.kycView.add(_obj.dayView);
	_obj.kycView.add(_obj.borderViewS30);
	
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.kycView.add(_obj.mobileView);
	_obj.kycView.add(_obj.borderViewS31);
	
	_obj.kycView.add(_obj.tabView);
	_obj.tabView.add(_obj.lblPassport);
	_obj.tabView.add(_obj.tabSelView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.tabView.add(_obj.tabSelIconView);
	_obj.kycView.add(_obj.passportView);
	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYC.add(_obj.globalView);
	_obj.winKYC.open();
	
		function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
	
	function uploadImage()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"document",'+
				'"docType":"Passport",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(evt) {
			try{
			activityIndicator.hideIndicator();
			if(evt.result.status === "S")
			{
				require('/utils/Console').info('Result ======== ' + evt.result);
				docType = evt.result.response.docType;
				docTypeCode = evt.result.response.docType;
				docUploadPath = evt.result.response.uploadpath;
				try{
		       _obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,docType+'.jpg');
				_obj.imgUpload = 0;
				}		catch(e){}
				var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('',[L('takePicture'), L('selectPicture')],-1);
				optionDialogPhoto.show();
				
				optionDialogPhoto.addEventListener('click',function(evt){
					if(optionDialogPhoto.options[evt.index] !== L('btn_cancel'))
					{
						if(evt.index === 0)
						{
							// Take Picture
							
							Ti.Media.showCamera({
						        showControls:true,
						        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
						        autohide:true,
						        allowEditing:false,
						        autorotate:true,
						        success:function(event) {
						        	
									_obj.image = event.media;
						        	_obj.f.write(_obj.image);
						        	
						        	if(_obj.f.size < 5000000)
						        	{
						        		if(TiGlobals.osname === 'android')
						        		{
						        			_obj.imgView.image = _obj.f.nativePath;	
						        		}
						        		else
						        		{
						        			_obj.imgView.image = _obj.f.resolve();
						        		}
							        	
							        	_obj.lblDocSize.text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
							            _obj.imgView.show();
										_obj.btnPPChooseFile.hide();
									    _obj.lblChooseFile.hide();
							        }
							        else
							        {
							        	require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
                                    }	
						        }
							});
						}
						
						if(evt.index === 1)
						{
							// Obtain an image from the gallery
							
					        Titanium.Media.openPhotoGallery({
					            success:function(event)
					            { 
					            	if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO)
					                {
					                	_obj.image = event.media;				            
									    _obj.f.write(_obj.image);
									    
									    if(_obj.f.size < 5000000)
							        	{
								        	_obj.imgView.image = _obj.f.read();
								        	_obj.lblDocSize.text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
								        	_obj.imgView.show();
										    _obj.btnPPChooseFile.hide();
									        _obj.lblChooseFile.hide();
								        }
								        else
								        {
								       require('/utils/AlertDialog').showAlert('', 'The file you upload should not exceed the file size of 5 MB.', [L('btn_ok')]).show();
                                      }
					                }      
					            },
					            cancel:function()
					            {
					            }
					        });
						}
						
						optionDialogPhoto = null;
					}
				});
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}
	
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	//function submitKYC()
	//{
		_obj.btnSubmitPassport.addEventListener('click',function(e){
			
			//CMN 73
			if (_obj.tab === 'passport') {
		    Ti.API.info("IN SUBMIT:---",_obj.lblExpiry.value);  
		    Ti.API.info("IN SUBMIT1:---",_obj.lblPPIssueDt.value);
	        Ti.API.info("IN SUBMIT2:---",_obj.lblExpiry.value - _obj.lblPPIssueDt.value);
			var timeDiff = Math.abs(_obj.lblExpiry.value - _obj.lblPPIssueDt.value);
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		
		// Apartment/House No.
		_obj.flat = _obj.txtAptNo.value; 
		if(_obj.txtAptNo.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your ' + _obj.txtAptNo.hintText,[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtAptNo.hintText + ' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('',_obj.txtAptNo.hintText + ' cannot contain  |  or  \'  or  ` or  \" or #',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtAptNo.value.charAt(i)+' entered for '+_obj.txtAptNo.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		// Building No.
	
			_obj.bldgNo = _obj.txtBldgNo.value;
			if(_obj.txtBldgNo.value.length > 0)
			{
				if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtBldgNo.hintText + ' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtBldgNo.value.charAt(i)+' entered for '+_obj.txtBldgNo.hintText+' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
			}
			else
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid ' + _obj.txtBldgNo.hintText,[L('btn_ok')]).show();
	    		_obj.txtBldgNo.value = '';
	    		_obj.txtBldgNo.focus();
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
		
		
		//Street1
		
		_obj.street1 = _obj.txtStreet1.value;
		if(_obj.txtStreet1.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Street Name cannot contain | or \' or ` or \" or #',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtStreet1.value.search("[^a-zA-Z ]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Street Name field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreet1.value.charAt(i)+' entered for '+_obj.txtStreet1.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		
		
		// City
			_obj.city = _obj.txtCity.text;
			if(_obj.txtCity.text === 'Select City*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your city',[L('btn_ok')]).show();
	    		_obj.txtCity.text = 'Select City*';
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
			//State
	
				_obj.state = _obj.lblState.text;
				if(_obj.lblState.text === 'Select Emirates*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your Emirate',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select Emirates*';
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
			
	
		
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your '+_obj.txtZip.hintText,[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of '+_obj.txtZip.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		
		if(_obj.txtZip.value.length > 0)
		{
	 		var len = _obj.txtZip.value.length;
			var validchars = '';	
			validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
			var currChar, prevChar, nextChar;
			var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var validNum = "0123456789";
			var validAlphFlag = false;
			var validNumFlag = false;
	
			for(i = 0; i <= len - 1; i++)
			{
				currChar = _obj.txtZip.value.charAt(i);
				prevChar = _obj.txtZip.value.charAt(i-1);
				nextChar = _obj.txtZip.value.charAt(i+1);
	
				if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
				{
					validAlphFlag = true;
				}
	
				if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
				{
					validNumFlag = true;
				}
							
				if(validchars.indexOf(currChar) == -1)
				{
					require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
				}
	
				if(currChar == ' ' && prevChar == ' ' || prevChar == ' ' && currChar == ' ' && nextChar == ' ' || prevChar == ' ' && currChar == ' ' && i == len-1 || currChar == ' ' && i == len-1)
				{
					if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
				
					break;
				}
			}
						
	
		 }
		 
		////////////////////// Mobile No ////////////////////
		if(_obj.txtMobile.value === '' || _obj.txtMobile.value === 'Mobile Number*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		

		else if((_obj.txtMobile.value.length < 7 || _obj.txtMobile.value.length > 9))
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else
		{
			
		}
		
		////////////////////// Daytime No ////////////////////
		//if UAE than it is not cumpusory
	
		if( _obj.txtDay.value.length > 0)
		{
			if(_obj.txtDay.value.length < 7 || _obj.txtDay.value.length > 9)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		
		if(_obj.txtMobile.value === _obj.txtDay.value)
		{
			require('/utils/AlertDialog').showAlert('','Day Time No. and Mobile No. cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDay.value = '';
    		_obj.txtDay.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		
		// Passport No
		if(_obj.txtPassportNo.value.trim() === '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide a passport number',[L('btn_ok')]).show();
    		_obj.txtPassportNo.value = '';
    		_obj.txtPassportNo.focus();
			return;		
		}
		else
		{
			
		}
		
		// Passport Issue Place
		if(_obj.lblCountry.text === '*Passport Issue Place')
		{
			require('/utils/AlertDialog').showAlert('','Please select a passport country',[L('btn_ok')]).show();
    		return;		
		}
		else
		{
			
		}
			
		// Passport Expiry Date
		if(_obj.lblExpiry.text === '*Passport Expiry Date')
		{
			require('/utils/AlertDialog').showAlert('','Please provide a passport expiry date',[L('btn_ok')]).show();
    		return;		
		}
		else
		{
			
		}
		
		// Passport Nationality
		if(_obj.lblNationality.text === '*Passport Nationality')
		{
			require('/utils/AlertDialog').showAlert('','Please select a passport nationality',[L('btn_ok')]).show();
    		return;		
		}
		
		
		// Passport Issuance Date
			if (_obj.lblPPIssueDt.text === '*Document Issuance date') {
				require('/utils/AlertDialog').showAlert('', 'Please provide Passport Issuance Date', [L('btn_ok')]).show();
				return;
			} else {

			}
			
			
   if(diffDays < 1825){  //cmn 55a
		     require('/utils/AlertDialog').showAlert('','Passport Expiry date & Issuance Date should have minimum of 5 yrs difference.',[L('btn_ok')]).show();
    		_obj.lblExpiry.value = '';
    		_obj.lblExpiry.text = '*Passport Expiry Date';
    		return;
	     }
	     
	     // Passport Issued by
			if (_obj.lblPPIssueBy.text === '*Issued By') {
				require('/utils/AlertDialog').showAlert('', 'Please select a passport country', [L('btn_ok')]).show();
				return;
			} 
	    //end of /cmn 73 validation
		
		// Passport Personal No
		if(_obj.txtPassportPersonalNo.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide a passport personal number',[L('btn_ok')]).show();
    		_obj.txtPassportPersonalNo.value = '';
    		_obj.txtPassportPersonalNo.focus();
    		return;		
		}
		else
		{
			
		}
		
		// MRZ Validation Starts
		_obj.mrz1 = _obj.txtMRZ1.value;
		_obj.mrz2 = _obj.txtMRZ3.value;
		_obj.mrz3 = _obj.txtMRZ5.value;
		if(_obj.mrz1.trim() === '' || _obj.mrz2.trim() === '' || _obj.mrz3.trim() === '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid MRZ Number',[L('btn_ok')]).show();
    		return;
		}
		else if(_obj.mrz1.length != 8)
		{
			require('/utils/AlertDialog').showAlert('','The first section of MRZ Number should be of 8 digits',[L('btn_ok')]).show();
    		_obj.txtMRZ1.value = '';
    		_obj.txtMRZ1.focus();
    		return;				
		}
		else if(_obj.mrz2.length != 19)
		{
			require('/utils/AlertDialog').showAlert('','The second section of MRZ Number should be of 19 digits',[L('btn_ok')]).show();
    		_obj.txtMRZ2.value = '';
    		_obj.txtMRZ2.focus();
    		return;
		}
		else if(_obj.mrz3.length != 2)
		{
			require('/utils/AlertDialog').showAlert('','The third section of MRZ Number should be of 2 digits',[L('btn_ok')]).show();
    		_obj.txtMRZ3.value = '';
    		_obj.txtMRZ3.focus();
    		return;
		}
		else
		{
			
		}
		}
		var dateSplit = _obj.lblPPIssueDt.text.split('/');
		var dt = dateSplit[0] + '-' + dateSplit[1] + '-' + dateSplit[2];
        Ti.API.info("DT****",dt);
        
        /*var dateSplit = _obj.lblExpiry.text.split('/');
		var dt = dateSplit[0]+'-'+require('/utils/Date').monthShort(dateSplit[1])+'-'+dateSplit[2];
		*/
		var dateSplit1 = _obj.lblExpiry.text.split('/');
		var dt1 = dateSplit1[0] + '-' + dateSplit1[1] + '-' + dateSplit1[2];
		var RequestId  = Math.floor((Math.random() * 1000000000) + 10000);
       Ti.API.info("DT111****",dt1);
        //Commented on 29th Nov for unable to upload document -testnew server
		//var targetFile = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
         var targetFile = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');

 
		var filename = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy') + ".jpg";
		var docname = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + RequestId + "_" + require('/utils/Date').today('-', 'dmy');
		var relativepath = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/";
		if(_obj.f != null){
		var xmlHttp = Ti.Network.createHTTPClient();
      	activityIndicator.showIndicator();
  		xmlHttp.onload = function(e)
		{
			activityIndicator.hideIndicator();
   
			var test = JSON.parse(xmlHttp.responseText);
			Ti.API.info("In xmlresponseText:--", test.result);
			Ti.API.info("In xmlresponseText:--", test.path);
			var path = test.path;
			var pathlength = test.path.length;
			var dot = path.lastIndexOf(".");

			var extn = path.substring(dot, pathlength);
			Ti.API.info("In extn:--", extn);
			targetFile = targetFile + extn;

		

			if (test.result === 'true' || test.result === true || test.result === 1) {
				activityIndicator.showIndicator();
				Ti.API.info("Inside if:--");
			var xhr = require('/utils/XHR');
			xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ RequestId +'",'+ 
                '"channelId":"'+TiGlobals.channelId+'",'+ 
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"requestName":"CUSTKYCIDDETAILSWITHDOCUPLOAD",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
                '"originatingCountry":"' + countryCode[0] + '",' + 
 	            '"IDType":"Passport",' + 
			    '"IDNumber":"' + _obj.txtPassportNo.value + '",' + 
			    '"IDIssueBy":"' + _obj.lblPPIssueBy.text + '",' + 
			    '"IDIssueAt":"'+_obj.lblCountry.text+'",'+ 
                '"IDIssueDate":"' + dt + '",' + 
			    '"IDIssueExpiryDate":"' + dt1 + '",' + 
                '"passportPersonalNo":"'+_obj.txtPassportPersonalNo.value+'",'+
                '"originatingCountryName":"' + countryName[0] + '",' + 
                '"mrzLine":"'+_obj.mrz1+_obj.mrz2+_obj.mrz3+'",'+
                '"sourceFlag":"02",'+ 
                '"operatingmodeId":"INTL",'+
				'"roleId":"SEND",'+
                '"flatNo":"'+_obj.flat+'",'+
				'"buildingNo":"'+_obj.bldgNo+'",'+
				'"street1":"'+_obj.street1+'",'+
				'"street2":"",'+
				'"locality":"",'+
				'"subLocality":"",'+
				'"vendorId":"F",'+
				'"zipCode":"'+_obj.txtZip.value+'",'+
				'"city":"'+_obj.city+'",'+
				'"state":"'+_obj.state+'",'+
				'"ssn":"",'+
				'"passNationality":"'+_obj.lblNationality.text+'",'+
                '"passportCountry":"",'+
                '"passportBirthPlace":"",'+
                '"medicareNo":"",'+
				'"medicareRefNo":"",'+
                '"surnameAtCitizenShip":"",'+
				'"surnameAtBirth":"",'+
				'"countryOfBirth":"",'+
                '"numOfYearsResidingInCountry":"",'+
                '"currentEmployerName":"",'+
                '"designation":"",'+
                '"annualIncome":"",'+
                '"countryCode":"'+Ti.App.Properties.getString('sourceCountryISDN')+'",'+
                '"passportNo":"'+_obj.txtPassportNo.value+'",'+
				'"dayTimeNumber":"'+_obj.lblDay.text+_obj.txtDay.value+'",'+
				'"mobileNo":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+
				'"privacyPolicyFlag":"Y",'+
				'"docPathArray":["' + targetFile + '"]' + 
			
				'}',
				
			
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

         function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseCode === "01")  
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.responseMessage);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.responseMessage);
				}
				
				try{
					Ti.App.fireEvent('changekycFlag');
				}catch(e){}
				
				destroy_kyc();
			}else if(e.result.responseCode === "00"){  //Added on 27th Nov during CMN 84a
				
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.responseMessage);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.responseMessage);
				}
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		
		}else {
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('', 'Error uploading document. Please try again later.', [L('btn_ok')]).show();
			}
			/*if(xmlHttp.responseText === '1' || xmlHttp.responseText === 1)
			{
				var xhr = require('/utils/XHR');
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"KYCVERIFICATIONDOCUPLOAD",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
						'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
						'"countryCode":"'+countryCode[0]+'",'+
						'"docType":"'+docType+'",'+  // added , on 11th aug
						'"docRequestId":"'+docRequestId+'",'+// added , on 11th aug
						'"docPath":"'+targetFile+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
					
				function xhrSuccess(evt) {
					try{
					activityIndicator.hideIndicator();
					if(evt.result.responseFlag === "S")
					{
						if(TiGlobals.osname === 'android')
						{	
							require('/utils/AlertDialog').toast('Document uploaded successfully');
						}
						else
						{
							require('/utils/AlertDialog').iOSToast('Document uploaded successfully');
						}
						
						myDocuments();
					}
					else
					{
						if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
					}catch(e){}
				}
		
				function xhrError(evt) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
				
			}
			else
			{
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('','Error uploading document. Please try again later.',[L('btn_ok')]).show();	
			}*/
		};
		
		xmlHttp.onerror = function(e)
		{
			activityIndicator.hideIndicator();
		};
		
		require('/utils/Console').info(JSON.stringify({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadPath:relativepath}));
		
		xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php',true);
		try{
		xmlHttp.send({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadpath:relativepath});
		}catch(e){}
		
		}
		else{
			require('/utils/AlertDialog').showAlert('', 'Please select document to upload.', [L('btn_ok')]).show();
			
		}
	});
		//var dateSplit = _obj.lblExpiry.text.split('/');
		//var dt = dateSplit[0]+'-'+require('/utils/Date').monthShort(dateSplit[1])+'-'+dateSplit[2];
		
		// xhr call
		
		/*activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CUSTKYCVERIFICATIONREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"operatingmodeId":"INTL",'+
				'"roleId":"SEND",'+
				'"flatNo":"'+_obj.flat+'",'+
				'"buildingNo":"'+_obj.bldgNo+'",'+
				'"street1":"'+_obj.street1+'",'+
				'"street2":"'+_obj.street2+'",'+
				'"locality":"'+_obj.locality+'",'+
				'"subLocality":"'+_obj.sub_locality+'",'+
				'"vendorId":"F",'+
				'"zipCode":"'+_obj.txtZip.value+'",'+
				'"city":"'+_obj.city+'",'+
				'"state":"'+_obj.state+'",'+
				'"country":"'+countryName[0]+'",'+
				'"ssn":"",'+
				'"mrzLine":"'+mrz1+mrz2+mrz3+'",'+
				'"drivingLicenseNo":"",'+
				'"passportNo":"'+_obj.txtPassportNo.value+'",'+
				'"passportIssuePlace":"'+_obj.lblCountry.text+'",'+
				'"passNationality":"'+_obj.lblNationality.text+'",'+
				'"passportPersonalNo":"'+_obj.txtPassportPersonalNo.value+'",'+
				'"passportCountry":"",'+
				'"passportBirthPlace":"",'+
				'"medicareNo":"",'+
				'"medicareRefNo":"",'+
				'"surnameAtCitizenShip":"",'+
				'"surnameAtBirth":"",'+
				'"countryOfBirth":"",'+
				'"passExpiryDate":"'+dt+'",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"dayTimeNumber":"'+_obj.lblDay.text+_obj.txtDay.value+'",'+
				'"mobileNo":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+
				'"privacyPolicyFlag":"Y"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
		

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				try{
					Ti.App.fireEvent('changekycFlag');
				}catch(e){}
				
				destroy_kyc();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}*/
	//}
	
	/*_obj.btnSubmitPassport.addEventListener('click',function(e){
		submitKYC();
	});*/
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_kyc();
	});
	
	_obj.winKYC.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_kyc();
				alertDialog = null;
			}
		});
	});
	
	function country()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCountry.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCountry.text = 'Passport Issue Place*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function countryIssueBy() {
		activityIndicator.showIndicator();
		var countryOptions = [];

		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"GETCOUNTRYRELATEDDETAILS",' + '"partnerId":"' + TiGlobals.partnerId + '",' + '"channelId":"' + TiGlobals.channelId + '",' + '"ipAddress":"' + TiGlobals.ipAddress + '",' + '"country":"' + countryName[0] + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if (e.result.responseFlag === "S") {
				var splitArr = e.result.citizenship.split('~');

				for (var i = 0; i < splitArr.length; i++) {
					countryOptions.push(splitArr[i]);
				}

				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country', countryOptions, -1);
				optionDialogCountry.show();

				optionDialogCountry.addEventListener('click', function(evt) {
					if (optionDialogCountry.options[evt.index] !== L('btn_cancel')) {
						_obj.lblPPIssueBy.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					} else {
						optionDialogCountry = null;
						_obj.lblPPIssueBy.text = '*Issued By';
					}
				});
			} else {
				if (e.result.message === L('invalid_session') || e.result.message === 'Invalid Session') {
					require('/lib/session').session();
					destroy_kyc();
				} else {
					require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}

	}
	
	function nationality()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Nationality',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblNationality.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblNationality.text = 'Passport Nationality*';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function destroy_kyc()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winKYC.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYC,_obj.globalView);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyc',destroy_kyc);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_kyc', destroy_kyc);
}; // KYCModal()
