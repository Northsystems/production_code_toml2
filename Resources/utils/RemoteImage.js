// TiRemoteImage - A Titanium component to support remote image loading with full image caching.
//
// For full usage examples and documentation:
// https://github.com/ulizama/TiRemoteImage
//
// Author: Uriel Lizama <uriel@baboonmedia.com>
// http://ulizama.com/

var _ = require('/utils/Underscore'), Component = require('/utils/Component'), FileLoader = require('/utils/FileLoader');

//Call garbage collection on initialization
FileLoader.gc;

function RemoteImage(params) {

	params = _.extend({
		autoload: true,
		ondone: function(e){ },
		onerror: function(e){ },
		hires: false,

		//Image defaults
		backgroundColor: 'transparent',
		//defaultImage: 'graphics/default_image.png',

		//Indicator styling
		indicatorStyle: TiGlobals.osname === 'android' ? Ti.UI.ActivityIndicatorStyle.PLAIN : Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,

	}, params);
	
	if(params.width)
	{
		var width = params.width;
	}
	else
	{
		var width = null;
	}
	
	if(params.right)
	{
		var right = params.right;
	}
	else
	{
		var right = null;	
	}

	//Component wrapper
	var myImage = new Component( Ti.UI.createView({
		top:params.top,
		left:params.left,
		right:right,
		width:width,
		height:params.height,
		backgroundColor:params.backgroundColor
	}));
	
	var imgView = _.defaults({top:0, left:0, right:right, width:width, height:params.height, opacity:0, hires:true}, params);

	//Actual image holder
	myImage._image = Ti.UI.createImageView(imgView);
	myImage.add(myImage._image);

	//Activity indicator
	myImage._activityIndicator = Ti.UI.createActivityIndicator({
	  style:params.indicatorStyle,
	  height:Ti.UI.SIZE,
	  width:Ti.UI.SIZE
	});
	myImage.add( myImage._activityIndicator );

	//Private Methods

	myImage._display = function( file ){
		
		if( _.isObject(file) ){
			myImage._image.image = file.getFile();
			myImage._image.opacity = 1;
			myImage._activityIndicator.hide();

			//If we have an ondone callback, call it
			if( _.isFunction( params.ondone ) ){
				params.ondone( file.toString() );
			}

		}
	};

	//Public methods
	
	myImage.load = function(){

		if( !_.has(params,'url') || !_.isString(params.url) || params.url == '' ){
			return;
		}

		myImage._activityIndicator.show();

		var url = getImageURL( params.url, params.hires );

		FileLoader.download( url )
	    .then(myImage._display)
	    .fail()
	    .done();
	};

	//Public image method to change the image location
	myImage.image = function( url ){
		myImage._image.image = params.defaultImage;
		params.url = url;
		myImage.load();
	};

	myImage.gc = function(){
		FileLoader.gc;
	};

	myImage.wipeCache = function(){
		FileLoader.gc(true);
	};

	if( params.autoload ){
		//If we are to autoload the image, call the load function
		myImage.load();
	}

	return myImage;
};


function getImageURL( url, hires ){

	//If the image is not set to be hires, then it parses the image url
	//to include @2x suffix to load the hires versions, otherwise returns
	//the url as is
	if( hires ){
		return url;
	}

	var image = url;
  var basename = image.replace(/\\/g,'/').replace( /.*\//, '' );
  var segment = basename.split('.');

  // replace with hires filename
  return image.replace(basename, segment[0]+'@2x.'+segment[1]);

}

module.exports = RemoteImage;