exports.fonts = {
	
	// Font Style
	
	//Arial
	lblBold38 : {"fontSize":TiGlobals.osname === 'ipad' ? 36 : 38, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal38 : {"fontSize":TiGlobals.osname === 'ipad' ? 36 : 38, "font-weight":'normal',"fontFamily":'Arial-Regular'},
	lblBold28 : {"fontSize":TiGlobals.osname === 'ipad' ? 26 : 28, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal28 : {"fontSize":TiGlobals.osname === 'ipad' ? 26 : 28,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold27 : {"fontSize":TiGlobals.osname === 'ipad' ? 25 : 27, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal27 : {"fontSize":TiGlobals.osname === 'ipad' ? 25 : 27,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold26 : {"fontSize":TiGlobals.osname === 'ipad' ? 24 : 26, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal26 : {"fontSize":TiGlobals.osname === 'ipad' ? 24 : 26,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold25 : {"fontSize":TiGlobals.osname === 'ipad' ? 23 : 25, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal25 : {"fontSize":TiGlobals.osname === 'ipad' ? 23 : 25,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold24 : {"fontSize":TiGlobals.osname === 'ipad' ? 22 : 24, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal24 : {"fontSize":TiGlobals.osname === 'ipad' ? 22 : 24,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold23 : {"fontSize":TiGlobals.osname === 'ipad' ? 21 : 23, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal23 : {"fontSize":TiGlobals.osname === 'ipad' ? 21 : 23,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold22 : {"fontSize":TiGlobals.osname === 'ipad' ? 20 : 22, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal22 : {"fontSize":TiGlobals.osname === 'ipad' ? 20 : 22,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold21 : {"fontSize":TiGlobals.osname === 'ipad' ? 19 : 21, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal21 : {"fontSize":TiGlobals.osname === 'ipad' ? 19 : 21,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold20 : {"fontSize":TiGlobals.osname === 'ipad' ? 18 : 20, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal20 : {"fontSize":TiGlobals.osname === 'ipad' ? 18 : 20,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold18 : {"fontSize":TiGlobals.osname === 'ipad' ? 16 : 18, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal18 : {"fontSize":TiGlobals.osname === 'ipad' ? 16 : 18,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold16 : {"fontSize":TiGlobals.osname === 'ipad' ? 14 : 16, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal16 : {"fontSize":TiGlobals.osname === 'ipad' ? 14 : 16,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold14 : {"fontSize":TiGlobals.osname === 'ipad' ? 12 : 14, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal14 : {"fontSize":TiGlobals.osname === 'ipad' ? 12 : 14,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold12 : {"fontSize":TiGlobals.osname === 'ipad' ? 10 : 12, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal12 : {"fontSize":TiGlobals.osname === 'ipad' ? 10 : 12,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold11 : {"fontSize":TiGlobals.osname === 'ipad' ? 9 : 11, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal11 : {"fontSize":TiGlobals.osname === 'ipad' ? 9 : 11,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	lblBold10 : {"fontSize":TiGlobals.osname === 'ipad' ? 8 : 10, "font-weight":'bold', "fontFamily":'Arial-Regular'},
	lblNormal10 : {"fontSize":TiGlobals.osname === 'ipad' ? 8 : 10,"font-weight":'normal', "fontFamily":'Arial-Regular'},
	
	// Swiss 721
	lblSwissBold38 : {"fontSize":TiGlobals.osname === 'ipad' ? 36 : 38, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal38 : {"fontSize":TiGlobals.osname === 'ipad' ? 36 : 38,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold28 : {"fontSize":TiGlobals.osname === 'ipad' ? 26 : 28, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal28 : {"fontSize":TiGlobals.osname === 'ipad' ? 26 : 28,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold27 : {"fontSize":TiGlobals.osname === 'ipad' ? 25 : 27, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal27 : {"fontSize":TiGlobals.osname === 'ipad' ? 25 : 27,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold26 : {"fontSize":TiGlobals.osname === 'ipad' ? 24 : 26, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal26 : {"fontSize":TiGlobals.osname === 'ipad' ? 24 : 26,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold25 : {"fontSize":TiGlobals.osname === 'ipad' ? 23 : 25, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal25 : {"fontSize":TiGlobals.osname === 'ipad' ? 23 : 25,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold24 : {"fontSize":TiGlobals.osname === 'ipad' ? 22 : 24, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal24 : {"fontSize":TiGlobals.osname === 'ipad' ? 22 : 24,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold23 : {"fontSize":TiGlobals.osname === 'ipad' ? 21 : 23, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal23 : {"fontSize":TiGlobals.osname === 'ipad' ? 21 : 23,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold22 : {"fontSize":TiGlobals.osname === 'ipad' ? 20 : 22, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal22 : {"fontSize":TiGlobals.osname === 'ipad' ? 20 : 22,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold21 : {"fontSize":TiGlobals.osname === 'ipad' ? 19 : 21, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal21 : {"fontSize":TiGlobals.osname === 'ipad' ? 19 : 21,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold20 : {"fontSize":TiGlobals.osname === 'ipad' ? 18 : 20, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal20 : {"fontSize":TiGlobals.osname === 'ipad' ? 18 : 20,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold18 : {"fontSize":TiGlobals.osname === 'ipad' ? 16 : 18, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal18 : {"fontSize":TiGlobals.osname === 'ipad' ? 16 : 18,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold16 : {"fontSize":TiGlobals.osname === 'ipad' ? 14 : 16, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal16 : {"fontSize":TiGlobals.osname === 'ipad' ? 14 : 16,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold14 : {"fontSize":TiGlobals.osname === 'ipad' ? 12 : 14, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal14 : {"fontSize":TiGlobals.osname === 'ipad' ? 12 : 14,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold12 : {"fontSize":TiGlobals.osname === 'ipad' ? 10 : 12, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal12 : {"fontSize":TiGlobals.osname === 'ipad' ? 10 : 12,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold11 : {"fontSize":TiGlobals.osname === 'ipad' ? 9 : 11, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal11 : {"fontSize":TiGlobals.osname === 'ipad' ? 9 : 11,"font-weight":'normal', "fontFamily":'Swiss 721'},
	lblSwissBold10 : {"fontSize":TiGlobals.osname === 'ipad' ? 8 : 10, "font-weight":'bold', "fontFamily":'Swiss 721'},
	lblSwissNormal10 : {"fontSize":TiGlobals.osname === 'ipad' ? 8 : 10,"font-weight":'normal', "fontFamily":'Swiss 721'},
	
	
	// Font Color
	
	whiteFont : '#FFFFFF',
	blackFont : '#000000',
	greyFont : '#969696',
	footerFont : '#bebebe',
	redFont : '#ed1b24',
	pullToRefresh : '#8899A6'
};


exports.FontStyle = function(style)
{
    return this.fonts[style];
};
