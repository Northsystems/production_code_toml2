exports.subString = function(str,start,end,append){
	
	if(str.length >= end)
	{
		return str.substr(start,end) + append;	
	}
	else
	{
		return str;
	}
};

exports.ucFirst = function(str){
	
	if(str.length > 0)
	{
		return str.slice(0,1).toUpperCase() + str.slice(1);	
	}
	else
	{
		return str;
	}
};

// Trim Functions

exports.trim = function(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+|\\s+$', 'g') : new RegExp('^'+chr+'+|'+chr+'+$', 'g');
  return str.replace(rgxtrim, '');
};

exports.rtrim = function(str, chr){ 
  var rgxtrim = (!chr) ? new RegExp('\\s+$') : new RegExp(chr+'+$');
  return str.replace(rgxtrim, '');
};

exports.ltrim = function(str, chr){
  var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
  return str.replace(rgxtrim, '');
};

exports.arrayToString = function(arr,separator){
	var newStr = '';	
						
	for(var s=0; s<arr.length; s++)
	{
		newStr = newStr + arr[s] + separator;
	}
	
	return newStr;
};
