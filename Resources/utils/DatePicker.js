exports.DatePicker = function(obj, condition) {
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//var origCC = origSplit[0]+'-'+origSplit[1];
	
	var today = require('/utils/Date').today('/', 'dmy').split('/');

	if (condition === 'dob18') {
		var _minDate = new Date();
		_minDate.setFullYear(1920);
		_minDate.setMonth(0);
		_minDate.setDate(01);

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear() - 18);
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate());
		
		var value = _maxDate;
	} 
	else if (condition === 'normal') {
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate());

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear() + 10);
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate() + 14);
		
		var value = _maxDate;
	}
	
	else if (condition === 'normal1') {  // PP issuance date
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear() - 10);
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate()+30);

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate()-1);
		
		var value = _maxDate;
	}
	else if (condition === 'normal2') { // PP expiry date
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate()+30);

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear()+10);
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate()-1);
		
		var value = _minDate;
	}
	else if (condition === 'normal3') {  //DL issuance date
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear()-19);  //Done on 1st sept 
		_minDate.setMonth(_minDate.getMonth()-11);
		_minDate.setDate(_minDate.getDate());//Done on 1st sept 

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear());//Done on 1st sept 
        _maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate()-1);//Done on 1st sept
		
		var value = _maxDate;
	}
	else if (condition === 'normal4') {  //DL expiry date
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate() +30);

		//var _maxDate = new Date();
		try{
		//Added on 24th Oct 17
		if(origSplit[0] == 'UK'){
	    var _maxDate =TiGlobals.expiDLDateUK;
		}else{
		var _maxDate =TiGlobals.expiDLDate;
		}
		//var _maxDate =TiGlobals.exp1;
		_maxDate.setFullYear(_maxDate.getFullYear()+20);
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate());
		}catch(e){Ti.API.info(e);}
		var value = _minDate;
		
	}
	else {
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate());

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate()+15);
		
		var value = _minDate;
	}

	if (TiGlobals.osname === 'android') {
		Ti.UI.Android.hideSoftKeyboard();

		var _picker = Ti.UI.createPicker({
			top : 10,
			left : 10,
			right : 10,
			type : Ti.UI.PICKER_TYPE_DATE,
			minDate : _minDate,
			maxDate : _maxDate,
			backgroundColor : '#fff',
			value : value 
		});

		_picker.showDatePickerDialog({
			value : value,
			callback : function(e) {
				if (e.cancel) {

				} else {
					var _pickerdateSplit = [];
					_pickerdateSplit = e.value.toLocaleString().split(' ');
					Ti.API.info(e.value);
				
					 //US events
			        if(TiGlobals.issuFlag == false){   //passport
						Ti.App.fireEvent('issueDateDtl',{data:e.value});
					}else{
						Ti.App.fireEvent('expDateDtl',{data:e.value});
					}
					
					if(TiGlobals.drivIssueFlag == false){//driving
						Ti.App.fireEvent('driveIssueDateDtl',{data:e.value});
					}else{
						Ti.App.fireEvent('driveDateDtl',{data:e.value});
					}	
					
				 //AUS events
				 
				   if(TiGlobals.issuFlagAus == false){   
						Ti.App.fireEvent('issueDateDtlAus',{data:e.value});
					}else{
						Ti.App.fireEvent('expDateDtlAus',{data:e.value});
					}
					
				 //UK events
				   if(TiGlobals.issuFlagUK == false){   
						Ti.App.fireEvent('issueDateDtlUK',{data:e.value});
					}else{
						Ti.App.fireEvent('expDateDtlUK',{data:e.value});
					}
			        if(TiGlobals.drivIssueFlagUK == false){//driving
						Ti.App.fireEvent('driveIssueUK',{data:e.value});
					}else{
						Ti.App.fireEvent('driveDateDtlUK',{data:e.value});
					}	
					
				 //Others
					 if(TiGlobals.issuFlagOthers == false){  
						Ti.App.fireEvent('issueDateDtlOthers',{data:e.value});
					 }else{
						Ti.App.fireEvent('expDateDtlOthers',{data:e.value});
					 }			
					
				 //UAE
					 if(TiGlobals.issuFlagUAE == false){  
						Ti.App.fireEvent('issueDateDtlUAE',{data:e.value});
					 }else{
						Ti.App.fireEvent('expDateDtlUAE',{data:e.value});
					 }

	                              if(TiGlobals.dobCheckF == true){
				       TiGlobals.recDOB = e.value;	
				       TiGlobals.dobFlag = true;
				       Ti.API.info("TiGlobals.recDOB",TiGlobals.recDOB);
				     }		
					Ti.App.Properties.setObject('dt', e.value);
					var day = _pickerdateSplit[2];
					var month = require('/utils/Date').monthNo(_pickerdateSplit[1]);
					var year = _pickerdateSplit[3];

					obj.text = day + '/' + month + '/' + year;
					Ti.App.fireEvent('calculateAge',{data:e.value});
					
					
					
				}
			}
		});
	} else {
		
	
		var _winPicker = Ti.UI.createWindow({
			backgroundColor : 'transparent'
		});

		var _viewPickerModal = Ti.UI.createScrollView({
			top : '0dp',
			contentHeight : Ti.UI.SIZE,
			backgroundColor : 'transparent',
			disableBounce : true,
			scrollType : 'vertical'
		});

		var _codeViewPicker = Ti.UI.createView({
			backgroundColor : '#6f6f6f',
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			borderRadius : 5,
			layout : 'vertical'
		});

		var _picker = Ti.UI.createPicker({
			top : 10,
			width : (TiGlobals.platformWidth - 20),
			type : Ti.UI.PICKER_TYPE_DATE,
			backgroundColor : '#fff',
			minDate : _minDate,
			maxDate : _maxDate,
			value : new Date(parseInt(today[2]), parseInt(today[1] - 1), parseInt(today[0]))
		});

		var _btnPickerView = Titanium.UI.createView({
			top : 10,
			height : 35,
			left : 10,
			right : 10,
			bottom : 10
		});

		var _btnPickerCancel = Titanium.UI.createButton({
			title : L('btn_cancel'),
			left : '0dp',
			height : 35,
			width : '48%',
			textAlign : 'center',
			font : TiFonts.FontStyle('lblBold14'),
			color : TiFonts.FontStyle('whiteFont'),
			borderRadius : 3,
			backgroundColor : '#202020'
		});

		var _btnPickerModal = Ti.UI.createButton({
			title : L('btn_proceed'),
			right : '0dp',
			height : 35,
			width : '48%',
			textAlign : 'center',
			font : TiFonts.FontStyle('lblBold14'),
			color : TiFonts.FontStyle('whiteFont'),
			borderRadius : 3,
			backgroundColor : '#f4343b'
		});

		_codeViewPicker.add(_picker);
		_btnPickerView.add(_btnPickerCancel);
		_btnPickerView.add(_btnPickerModal);
		_codeViewPicker.add(_btnPickerView);
		_viewPickerModal.add(_codeViewPicker);
		_winPicker.add(_viewPickerModal);
		_winPicker.open();

		_btnPickerCancel.addEventListener('click', function(e) {
			//borderViewS14.backgroundColor=  '#ff0000'; //red	

			_winPicker.close();
			_winPicker.remove(_viewPickerModal);
			_codeViewPicker.remove(_picker);
			_btnPickerView.remove(_btnPickerCancel);
			_btnPickerView.remove(_btnPickerModal);
			_codeViewPicker.remove(_btnPickerView);
			_viewPickerModal.remove(_codeViewPicker);

			_winPicker = null;
			_viewPickerModal = null;
			_picker = null;
			_btnPickerCancel = null;
			_btnPickerModal = null;
			_btnPickerView = null;
			_codeViewPicker = null;
			_pickerdate = null;
		});

		var _pickerdate = '';

		_picker.addEventListener('change', function(e) {
			_pickerdate = e.value.toLocaleString();
		});

					
		_btnPickerModal.addEventListener('click', function(e) {
			        //US events
			        if(TiGlobals.issuFlag == false){    //Passport
			        	
						Ti.App.fireEvent('issueDateDtl',{data:_picker.value});
					}else{
						Ti.App.fireEvent('expDateDtl',{data:_picker.value});
					}
			       
			       try{
			        if(TiGlobals.drivIssueFlag == false){//driving
			         Ti.App.fireEvent('driveIssueDateDtl',{data:_picker.value});
					}else{
						Ti.App.fireEvent('driveDateDtl',{data:_picker.value});
						_picker.setFullYear(TiGlobals.expiDLDate.getFullYear()+20);
					}
					}catch(e){Ti.API.info(e);}
					
				 //AUS events
				 
				   if(TiGlobals.issuFlagAus == false){  
				    	
						Ti.App.fireEvent('issueDateDtlAus',{data:_picker.value});
					}else{
						Ti.App.fireEvent('expDateDtlAus',{data:_picker.value});
					}
					
				 //UK events
				   if(TiGlobals.issuFlagUK == false){ 
				   	 
						Ti.App.fireEvent('issueDateDtlUK',{data:_picker.value});
					}else{
						Ti.App.fireEvent('expDateDtlUK',{data:_picker.value});
					}
					try{
			      
			        if(TiGlobals.drivIssueFlagUK == false){//driving
			     
						Ti.App.fireEvent('driveIssueUK',{data:_picker.value});

					}else{
						
						Ti.App.fireEvent('driveDateDtlUK',{data:_picker.value});
						_picker.setFullYear(TiGlobals.expiDLDateUK.getFullYear()+20);
					}
					}catch(e){}	
				  //Others  //Added on 24-Oct-17
					 if(TiGlobals.issuFlagOthers == false){  
					
						Ti.App.fireEvent('issueDateDtlOthers',{data:_picker.value});
					 }else{
						Ti.App.fireEvent('expDateDtlOthers',{data:_picker.value});
					 }			
					
				 //UAE
					 if(TiGlobals.issuFlagUAE == false){ 
				 
						Ti.App.fireEvent('issueDateDtlUAE',{data:_picker.value});
					 }else{
						Ti.App.fireEvent('expDateDtlUAE',{data:_picker.value});
					 }	
					
				
										
					
			if (_pickerdate !== '') {
				 //US events
			        if(TiGlobals.issuFlag == false){   //added by sanjivani on 11-July-17 for CMN55a.1.
			        	
						Ti.App.fireEvent('issueDateDtl',{data:_picker.value});
					}else{
						Ti.App.fireEvent('expDateDtl',{data:_picker.value});
					}
					try{
			        if(TiGlobals.drivIssueFlag == false){//driving
			         Ti.App.fireEvent('driveIssueDateDtl',{data:_picker.value});
					}else{
						Ti.App.fireEvent('driveDateDtl',{data:_picker.value});
						_picker.setFullYear(TiGlobals.expiDLDate.getFullYear()+20);
					}	
					}catch(e){}
					//CMN 73
				 //AUS events
				 
				   if(TiGlobals.issuFlagAus == false){   
				   	
						Ti.App.fireEvent('issueDateDtlAus',{data:_picker.value});
					}else{
						Ti.App.fireEvent('expDateDtlAus',{data:_picker.value});
					}
					
				 //UK events
				   if(TiGlobals.issuFlagUK == false){   
				   	_picker.setFullYear(_picker.getFullYear() - 10);
		                 
						Ti.App.fireEvent('issueDateDtlUK',{data:_picker.value});
					}else{
						Ti.App.fireEvent('expDateDtlUK',{data:_picker.value});
					}
					try{
			        if(TiGlobals.drivIssueFlagUK == false){//driving
			       
						Ti.App.fireEvent('driveIssueUK',{data:_picker.value});
					}else{
						Ti.App.fireEvent('driveDateDtlUK',{data:_picker.value});
						_picker.setFullYear(TiGlobals.expiDLDateUK.getFullYear()+20);
					}	
					}catch(e){}
					 //Others  //Added on 24-Oct-17
					 if(TiGlobals.issuFlagOthers == false){  
					 
						Ti.App.fireEvent('issueDateDtlOthers',{data:_picker.value});
					 }else{
						Ti.App.fireEvent('expDateDtlOthers',{data:_picker.value});
					 }			
					
				 //UAE
					 if(TiGlobals.issuFlagUAE == false){ 
					 	 
						Ti.App.fireEvent('issueDateDtlUAE',{data:_picker.value});
					 }else{
						Ti.App.fireEvent('expDateDtlUAE',{data:_picker.value});
					 }	
					
					
					Ti.API.info("_picker.value",_picker.value);
					
				_pickerdate = _picker.value;
				
				if(TiGlobals.dobCheckF == true){
				TiGlobals.recDOB = _picker.value;	
				TiGlobals.dobFlag = true;
				Ti.API.info("TiGlobals.recDOB",TiGlobals.recDOB);
				}
				var day = _pickerdate.getDate();
				day = day.toString();
                
				if (day.length < 2) {
					day = '0' + day;
				}
   
				var month = _pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if (month.length < 2) {
					month = '0' + month;
				}

				var year = _pickerdate.getFullYear();
			
				obj.text = day + '/' + month + '/' + year;
              
				_winPicker.close();
				_winPicker.remove(_viewPickerModal);
				_codeViewPicker.remove(_picker);
				_btnPickerView.remove(_btnPickerCancel);
				_btnPickerView.remove(_btnPickerModal);
				_codeViewPicker.remove(_btnPickerView);
				_viewPickerModal.remove(_codeViewPicker);

				_winPicker = null;
				_viewPickerModal = null;
				_picker = null;
				_btnPickerCancel = null;
				_btnPickerModal = null;
				_btnPickerView = null;
				_codeViewPicker = null;
				_pickerdate = null;
			} else {
				
				if (condition === 'dob18') {
					_pickerdate = _picker.maxDate;
				} else {
					_pickerdate = _picker.minDate;
				}
				
				var day = _pickerdate.getDate();
				day = day.toString();

				if (day.length < 2) {
					day = '0' + day;
				}

				var month = _pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if (month.length < 2) {
					month = '0' + month;
				}

				var year = _pickerdate.getFullYear();
				obj.text = day + '/' + month + '/' + year;
				
				_winPicker.close();
				_winPicker.remove(_viewPickerModal);
				_codeViewPicker.remove(_picker);
				_btnPickerView.remove(_btnPickerCancel);
				_btnPickerView.remove(_btnPickerModal);
				_codeViewPicker.remove(_btnPickerView);
				_viewPickerModal.remove(_codeViewPicker);

				_winPicker = null;
				_viewPickerModal = null;
				_picker = null;
				_btnPickerCancel = null;
				_btnPickerModal = null;
				_btnPickerView = null;
				_codeViewPicker = null;
				_pickerdate = null;
			}
		});

		_picker.selectionIndicator = true;
	}
}; 

//For New Registration flow- with color logic
exports.DatePicker1 = function(obj, condition,borderViewS14) {
	var today = require('/utils/Date').today('/', 'dmy').split('/');

	if (condition === 'dob18') {
		var _minDate = new Date();
		_minDate.setFullYear(1920);
		_minDate.setMonth(0);
		_minDate.setDate(01);

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear() - 18);
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate());
		
		var value = _maxDate;
	} 
	else if (condition === 'normal') {
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate());

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear() + 10);
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate() + 14);
		
		var value = _maxDate;
	}
	else {
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate());

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate()+15);
		
		var value = _minDate;
	}

	if (TiGlobals.osname === 'android') {
		Ti.UI.Android.hideSoftKeyboard();

		var _picker = Ti.UI.createPicker({
			top : 10,
			left : 10,
			right : 10,
			type : Ti.UI.PICKER_TYPE_DATE,
			minDate : _minDate,
			maxDate : _maxDate,
			backgroundColor : '#fff',
			value : value 
		});

		_picker.showDatePickerDialog({
			value : value,
			callback : function(e) {
				if (e.cancel) {

				} else {
					var _pickerdateSplit = [];
					_pickerdateSplit = e.value.toLocaleString().split(' ');
					Ti.API.info(e.value);
					Ti.App.Properties.setObject('dt', e.value);
					var day = _pickerdateSplit[2];
					var month = require('/utils/Date').monthNo(_pickerdateSplit[1]);
					var year = _pickerdateSplit[3];

					obj.text = day + '/' + month + '/' + year;
					Ti.App.fireEvent('calculateAge',{data:e.value});
				}
			}
		});
	} else {
		var _winPicker = Ti.UI.createWindow({
			backgroundColor : 'transparent'
		});

		var _viewPickerModal = Ti.UI.createScrollView({
			top : '0dp',
			contentHeight : Ti.UI.SIZE,
			backgroundColor : 'transparent',
			disableBounce : true,
			scrollType : 'vertical'
		});

		var _codeViewPicker = Ti.UI.createView({
			backgroundColor : '#6f6f6f',
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			borderRadius : 5,
			layout : 'vertical'
		});

		var _picker = Ti.UI.createPicker({
			top : 10,
			width : (TiGlobals.platformWidth - 20),
			type : Ti.UI.PICKER_TYPE_DATE,
			backgroundColor : '#fff',
			minDate : _minDate,
			maxDate : _maxDate,
			value : new Date(parseInt(today[2]), parseInt(today[1] - 1), parseInt(today[0]))
		});

		var _btnPickerView = Titanium.UI.createView({
			top : 10,
			height : 35,
			left : 10,
			right : 10,
			bottom : 10
		});

		var _btnPickerCancel = Titanium.UI.createButton({
			title : L('btn_cancel'),
			left : '0dp',
			height : 35,
			width : '48%',
			textAlign : 'center',
			font : TiFonts.FontStyle('lblBold14'),
			color : TiFonts.FontStyle('whiteFont'),
			borderRadius : 3,
			backgroundColor : '#202020'
		});

		var _btnPickerModal = Ti.UI.createButton({
			title : L('btn_proceed'),
			right : '0dp',
			height : 35,
			width : '48%',
			textAlign : 'center',
			font : TiFonts.FontStyle('lblBold14'),
			color : TiFonts.FontStyle('whiteFont'),
			borderRadius : 3,
			backgroundColor : '#f4343b'
		});

		_codeViewPicker.add(_picker);
		_btnPickerView.add(_btnPickerCancel);
		_btnPickerView.add(_btnPickerModal);
		_codeViewPicker.add(_btnPickerView);
		_viewPickerModal.add(_codeViewPicker);
		_winPicker.add(_viewPickerModal);
		_winPicker.open();

		_btnPickerCancel.addEventListener('click', function(e) {
			borderViewS14.backgroundColor=  '#ff0000'; //red	

			_winPicker.close();
			_winPicker.remove(_viewPickerModal);
			_codeViewPicker.remove(_picker);
			_btnPickerView.remove(_btnPickerCancel);
			_btnPickerView.remove(_btnPickerModal);
			_codeViewPicker.remove(_btnPickerView);
			_viewPickerModal.remove(_codeViewPicker);

			_winPicker = null;
			_viewPickerModal = null;
			_picker = null;
			_btnPickerCancel = null;
			_btnPickerModal = null;
			_btnPickerView = null;
			_codeViewPicker = null;
			_pickerdate = null;
		});

		var _pickerdate = '';

		_picker.addEventListener('change', function(e) {
			_pickerdate = e.value.toLocaleString();
		});

		_btnPickerModal.addEventListener('click', function(e) {
			borderViewS14.backgroundColor= '#228b22';//dark lime green;

			if (_pickerdate !== '') {
				_pickerdate = _picker.value;
				var day = _pickerdate.getDate();
				day = day.toString();

				if (day.length < 2) {
					day = '0' + day;
				}

				var month = _pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if (month.length < 2) {
					month = '0' + month;
				}

				var year = _pickerdate.getFullYear();
				obj.text = day + '/' + month + '/' + year;

				_winPicker.close();
				_winPicker.remove(_viewPickerModal);
				_codeViewPicker.remove(_picker);
				_btnPickerView.remove(_btnPickerCancel);
				_btnPickerView.remove(_btnPickerModal);
				_codeViewPicker.remove(_btnPickerView);
				_viewPickerModal.remove(_codeViewPicker);

				_winPicker = null;
				_viewPickerModal = null;
				_picker = null;
				_btnPickerCancel = null;
				_btnPickerModal = null;
				_btnPickerView = null;
				_codeViewPicker = null;
				_pickerdate = null;
			} else {
				
				if (condition === 'dob18') {
					_pickerdate = _picker.maxDate;
				} else {
					_pickerdate = _picker.minDate;
				}
				
				var day = _pickerdate.getDate();
				day = day.toString();

				if (day.length < 2) {
					day = '0' + day;
				}

				var month = _pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if (month.length < 2) {
					month = '0' + month;
				}

				var year = _pickerdate.getFullYear();
				obj.text = day + '/' + month + '/' + year;
				
				_winPicker.close();
				_winPicker.remove(_viewPickerModal);
				_codeViewPicker.remove(_picker);
				_btnPickerView.remove(_btnPickerCancel);
				_btnPickerView.remove(_btnPickerModal);
				_codeViewPicker.remove(_btnPickerView);
				_viewPickerModal.remove(_codeViewPicker);

				_winPicker = null;
				_viewPickerModal = null;
				_picker = null;
				_btnPickerCancel = null;
				_btnPickerModal = null;
				_btnPickerView = null;
				_codeViewPicker = null;
				_pickerdate = null;
			}
		});

		_picker.selectionIndicator = true;
	}
};
