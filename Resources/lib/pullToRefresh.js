///////////// PULL TO REFRESH /////////////
			
exports.pullToRefresh = function (tableView,callbackRefreshFunction)
{
	// Platform Specific
	
	if(TiGlobals.osname === 'android')
	{
		var tableHeader = Ti.UI.createView({
			left:'0dp',
			right:'0dp',
			height:screen.fit(40),
		    backgroundColor:'#1d1d1d'
		});
		
		var tableHeaderMainView = Ti.UI.createView({
			width: Ti.UI.SIZE,
		    height:screen.fit(39),
		});
		 
		var lblRefresh = Ti.UI.createLabel({
		    text:'Refreshing content...',
		    left:screen.fit(30),
		    wordWrap:false,
		    textAlign:'center',
		    color:TiFonts.FontStyle('whiteFont'),
		    font:TiFonts.FontStyle('lblNormal12'),
		    visible:false
		});
		
		var lblTap = Ti.UI.createLabel({
		    text:'Tap to Refresh',
		    width: Ti.UI.SIZE,
		    wordWrap:false,
		    textAlign:'center',
		    color:TiFonts.FontStyle('whiteFont'),
		    font:TiFonts.FontStyle('lblNormal12')
		});
		
		var actInd = Ti.UI.createActivityIndicator({
		    left:'0dp', 
		    style:Ti.UI.ActivityIndicatorStyle.DARK
		});
		
		tableHeaderMainView.add(lblRefresh);
		tableHeaderMainView.add(lblTap);
		tableHeaderMainView.add(actInd);
		tableHeader.add(tableHeaderMainView);
		
		function resetTable(table){
			
			lblTap.show();
			lblRefresh.hide();
		    actInd.hide();
		    row = null;
		}
		
		var row = null;
		
		tableView.addEventListener('scroll',function(e){
			
			//if((e.firstVisibleItem + e.visibleItemCount) === (_page * _limit))
			
			require('/utils/Console').info(e.visibleItemCount +' ========== '+ e.totalItemCount);
			
			if(e.visibleItemCount <= e.totalItemCount)
			{
				if(row === null)
	            {
	            	row = Ti.UI.createTableViewRow({
                		height:Ti.UI.SIZE,
                		pull:1
                	});
                	
                	lblTap.show();
					lblRefresh.hide();
				    actInd.hide();
                	row.add(tableHeader);
					
		            tableView.appendRow(row);
		            
		            row.addEventListener('click',function(evt){
						actInd.show();
		    			lblTap.hide();
						lblRefresh.show();
		    			
		    			setTimeout(function(){
		    				callbackRefreshFunction(tableView,resetTable(tableView));	
		    			},2000);
		            });
		       }
		    }
		});	
	}
	else
	{
		
		var tableHeader = Ti.UI.createView({
			left:'0dp',
			right:'0dp',
			height: '60dp',
		    backgroundColor:'#F5F8FA'
		});
		
		var tableHeaderMainView = Ti.UI.createView({
			bottom: screen.fit(11),
		    width: Ti.UI.SIZE,
		    height: Ti.UI.SIZE
		});
		 
		var border = Ti.UI.createView({
		    bottom:'0dp',
		    height:screen.fit(1),
		    width:Ti.UI.FILL,
		    backgroundColor: "#E1E8ED",
		});
		
		var imageArrow = Ti.UI.createImageView({
		    image:'/images/pullToRefresh/clean.png',
		    left:screen.fit(8),
		    width:screen.fit(12),
		    height:screen.fit(30),
		});
		
		var labelStatus = Ti.UI.createLabel({
		    text:'Pull down to refresh...',
		    left:screen.fit(30),
		    wordWrap:false,
		    textAlign:'center',
		    color:TiFonts.FontStyle('pullToRefresh'),
		    font:TiFonts.FontStyle('lblBold14')
		});
		
		var actInd = Ti.UI.createActivityIndicator({
		    left:'0dp', 
		    style:Ti.UI.ActivityIndicatorStyle.DARK
		});
		
		tableHeaderMainView.add(imageArrow);
		tableHeaderMainView.add(labelStatus);
		tableHeaderMainView.add(actInd);
		tableHeader.add(tableHeaderMainView);
		tableHeader.add(border);
		
		tableView.headerPullView = tableHeader;
		
		var pulling = false;
		var reloading = false;
		var offset = 0;
		 
		tableView.addEventListener('scroll',function(e){
		    offset = e.contentOffset.y;
		    if (pulling && !reloading && offset > -80 && offset < 0){
		        pulling = false;
		        var unrotate = Ti.UI.create2DMatrix();
		        imageArrow.animate({transform:unrotate, duration:180});
		        labelStatus.text = 'Pull down to refresh...';
		    } else if (!pulling && !reloading && offset < -80){
		        pulling = true;
		        var rotate = Ti.UI.create2DMatrix().rotate(180);
		        imageArrow.animate({transform:rotate, duration:180});
		        labelStatus.text = 'Release to refresh...';
		    }
		});
		 
		function resetPullHeader(table){
		    reloading = false;
		    actInd.hide();
		    imageArrow.transform=Ti.UI.create2DMatrix();
		    imageArrow.show();
		    labelStatus.text = 'Pull down to refresh...';
		    table.setContentInsets({top:0}, {animated:true});
		}
		 
		tableView.addEventListener('dragEnd',function(e){
		    if (pulling && !reloading && offset < -80){
		    	pulling = false;
		        reloading = true;
		        imageArrow.hide();
		        actInd.show();
		        e.source.setContentInsets({top:80}, {animated:true});
		        setTimeout(function(){
		        	tableView.separatorColor = '#000000';
		            callbackRefreshFunction(e.source,resetPullHeader(e.source));
		        }, 2000);
		    }
		});
	}
};