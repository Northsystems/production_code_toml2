/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:15,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:40,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.KYC = {
	winKYC : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	
	kycView : _.defaults({}, stepView),
	
	txtAptNo : _.defaults({top:15,right:20}, textField),
	txtBldgNo : _.defaults({top:15,right:20}, textField),
	txtStreet1 : _.defaults({top:15,right:20}, textField),
	txtStreet2 : _.defaults({top:15,right:20}, textField),
	txtLocality : _.defaults({top:15,right:20}, textField),
	txtSubLocality : _.defaults({top:15,right:20}, textField),
	
	cityView : _.defaults({}, optionsView),
	txtCity : _.defaults({top:15,right:20}, textField),
	imgCity : _.defaults({right:0}, imgArrow),
	
	stateView : _.defaults({}, optionsView),
	lblState : _.defaults({top:0,left:0,right:20}, label),
	imgState : _.defaults({right:0}, imgArrow),
	
	dobView : _.defaults({top:15,left:20,right:20}, label),
	lblDOB : _.defaults({top:0,left:0,right:55}, label),
	imgDOB : {top:0,right:0,height:30,width:30,image:'/images/calendar.png'},
	
	txtZip : _.defaults({top:15,right:20}, textField),
	btnSubmit : _.defaults({}, button),
	btnCancel : _.defaults({top:10, backgroundColor:'#000'}, button),
	
	mobileView : {top:15,left:20,right:20,height:35,layout:'horizontal',horizontalWrap:false},
	lblMobile : _.defaults({height:Ti.UI.SIZE,left:0,width:Ti.UI.SIZE}, label),
	txtMobile : _.defaults({top:TiGlobals.osname === 'android' ? 3 : 0,left:2,width:Ti.UI.FILL}, textField),
	
	tabView : {
		top:20,
		height:47,
		left:0,
		right:0
	},
	tabSelView : {
		top:0,
		left:0,
		height:2,
		width:'100%',
		zIndex:10,
		backgroundColor:'#FF243A',
	},
	lblPassport : {
		top:0,
		left:0,
		height:40,
		width:'100%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('whiteFont'),
		backgroundColor:'#6F6F6F',
		sel:'passport'
	},
	tabSelIconView : {
		top:40,
		left:0,
		height:7,
		width:'100%',
		zIndex:10
	},
	imgTabSel : {
		image:'/images/tab_sel.png',
		top:0,
		height:7,
		width:15
	},
	scrollableView : {
		left:0,
		right:0,
		top:10,
		height:Ti.UI.SIZE,
		disableBounce:true,
		showPagingControl:false,
		backgroundColor:'#FFF'
	},
	stepView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
	},
	lblKYCNote : {
		top:0,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont'),
	},
	txtPassportNo : _.defaults({top:15,right:20}, textField),
	countryView : _.defaults({top:20}, optionsView),
	lblCountry : _.defaults({top:0,left:0,right:20}, textField),
	imgCountry : _.defaults({right:0}, imgArrow),
	txtPassportPersonalNo : _.defaults({top:15,right:20}, textField),
	
	mrzView : {top:0,left:20,right:20,height:45},
	txtMRZ1 : _.defaults({top:14,left:0,width:'25%',paddingLeft:0,paddingRight:0}, textField),
	borderMRZ1 : {top:44,left:0,width:'25%',height:1,backgroundColor:'#000'},
	
	txtMRZ2 : _.defaults({top:14,left:'27%',right:null,width:'5%',paddingLeft:0,paddingRight:0}, textField),
	borderMRZ2 : {top:44,left:'27%',right:null,width:'5%',height:1,backgroundColor:'#000'},
	
	txtMRZ3 : _.defaults({top:14,left:'34%',right:null,width:'35%',paddingLeft:0,paddingRight:0}, textField),
	borderMRZ3 : {top:44,left:'34%',width:'35%',height:1,backgroundColor:'#000'},
	
	txtMRZ4 : _.defaults({top:14,left:'71%',right:null,width:'15%',paddingLeft:0,paddingRight:0}, textField),
	borderMRZ4 : {top:44,left:'71%',width:'15%',height:1,backgroundColor:'#000'},
	
	txtMRZ5 : _.defaults({top:14,left:'88%',right:null,width:'12%',paddingLeft:0,paddingRight:0}, textField),
	borderMRZ5 : {top:44,left:'88%',width:'12%',height:1,backgroundColor:'#000'},
	
	/*imgPassport : {
		top:15,
		image:'/images/passport.jpg',
	    width:302,
	    height:230
	},*/
	
	//CMN 73
	imgPassport : {
		top:15,
		image:'/images/passportKYC.jpg',
	    width:302,
	    height:200
	},
	
	btnChoose:{
		top:15,
		height:35,
		width:120,
		left : 20,
		bottom:10,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold14'),
	    color:TiFonts.FontStyle('blackFont'),
	    borderRadius:5,
	   // backgroundColor:'transparent',
	    borderColor:'black'
	},
	
	lblChooseFile : {
		text : 'No File Chosen',
		top:30,
		left:150,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	
	imgView : {
		left : 35,
		height : 25,
		width:45,
		visible:false,
		type:'gallery'
	},
	imgUploadView : {
		//top :15,
		left : 0,
		right:0,
		height : 62,
		backgroundColor : '#fff',
		//borderColor:'#ffcf8f',
		type:'gallery'
	},
	
	lblDocSize : {
		top:10,
		left : 70,
		right : 100,
		height : Ti.UI.SIZE,
		width:Ti.UI.SIZE,
        textAlign: 'left',
		font : TiFonts.FontStyle('lblNormal12'),
		color : TiFonts.FontStyle('greyFont'),
		type:'gallery'
	},
		
 // EO CMN 73
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};
