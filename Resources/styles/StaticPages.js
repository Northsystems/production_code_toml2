/*global exports,require*/
var _ = require('/utils/Underscore');

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.StaticPages = {
	winModal : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF'
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgBack : {
		left:0,
		image:'/images/back.png',
	    width:35,
	    height:40
	},
	mainView : {
		top:40,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		layout:'vertical'
	},
	webView : {
		top:0,
		left:0,
		right:0,
		bottom:0,
		enableZoomControls:false,
		scalesPageToFit:true,
		disableBounce:true
	
	},
	btnStarted : _.defaults({}, button),
};