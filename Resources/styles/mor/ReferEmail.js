/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:20,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.ReferEmail = {
	winReferEmail : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	mainView : {
		top:40,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	
	lblSectionHead : {
		top:0,
		left:0,
		right:0,
		height:52,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('redFont'),
		backgroundImage:'/images/bg_subtitle.jpg'
	},
	sectionHeaderBorder : {
		top:0,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	headView : {top:0,height:Ti.UI.SIZE,left:0,right:0,backgroundColor:'#f0f0f0'},
	lblHeadCount : _.defaults({left:20,width:21,height:21,color:TiFonts.FontStyle('whiteFont'),textAlign:'center',backgroundImage:'/images/refer_count.png'}, label),
	lblHeadTxt : _.defaults({left:60,right:20,top:20,bottom:20,height:Ti.UI.SIZE,}, label),
	
	txt : _.defaults({top:15,right:20}, textField),
	txtEmail: _.defaults({top:15,height:80,right:20}, textField), 
	
	termsView : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:true
	},
	imgTerms : {
		top:0,
		left:0,
		image:'/images/checkbox_unsel.png',
	    width:15,
	    height:15
	},
	lblTerms : {
		left:5,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	lblTerms1 : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('redFont'),
	},
	
	btnSubmit : _.defaults({top:20}, button),
	
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};