/*global exports,require*/
var _ = require('/utils/Underscore');

var label = {
	height:40,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont')
};

var optionsView = {
	height:40,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

exports.Location = {
	winLocation : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundImage:'/images/default.png',
		zIndex:1000
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0
	},
	tableView : {
		top:0,
		left:0,
		bottom:0,
		right:0,
		scrollable:true,
		showVerticalScrollIndicator:true,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent',
		footerTitle:TiGlobals.osname === 'android' ? null : '' // Fix for empty rows in iOS
	},
	
	// Page
	
	locationTxtView : {
		bottom:84,
		left:0,
		right:0,
		height:110,
		backgroundColor:'#66e0e0e0'
	},
	locationMainView : {
		left:0,
		right:0,
		height:Ti.UI.SIZE,
		layout:'vertical'
	},
	lblTxt1 : {
		top : 10,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		textAlign : 'center',
		font : TiFonts.FontStyle('lblNormal18'),
		color : TiFonts.FontStyle('blackFont')
	},
	locationHorizontalView : {
		top : 5,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:false
	}, 
	lblTxt2 : {
		top : 0,
		left:0,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		textAlign : 'left',
		font : TiFonts.FontStyle('lblNormal24'),
		color : TiFonts.FontStyle('redFont')
	},
	lblTxt3 : {
		top : 0,
		left:0,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		textAlign : 'left',
		font : TiFonts.FontStyle('lblNormal24'),
		color : TiFonts.FontStyle('blackFont')
	},
	locationBottomView : {
		bottom : 0,
		left:0,
		right:0,
		height:84,
		backgroundColor:'#ffffff',
		layout:'vertical'
	},
	countryView : _.defaults({top:18}, optionsView),
	lblCountry : _.defaults({top:0,left:0,right:20}, label),
	imgCountry : _.defaults({right:0}, imgArrow),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#cdcdcd'
	},
};