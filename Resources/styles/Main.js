exports.Main = {
	self : {
		left:0,
		top:0,
		right:0,
	    navBarHidden:true,
		fullscreen:false,
	    backgroundColor:'#FFF'
	},
	headerView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		left:0,
		right:0,
		height:59,
		zIndex:10
	},
	lblLogo : {
		backgroundImage:'/images/logo.png',
		top:0,
	    left:0,
	    width:190,
	    height:59,
	    bubbleParent:false
	},
	exchangeView : {
		backgroundColor:'#4f4f4f',
	    right:0,
	    left:190,
	    height:59
	},
	exchangeMainView : {
		backgroundColor:'#4f4f4f',
	    right:32,
	    height:Ti.UI.SIZE,
	    width:Ti.UI.SIZE
	},
	lblExchangeTxt : {
		top:0,
		width:Ti.UI.SIZE,
	    height:13,
	    textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	scrollableView : {
		left:0,
		right:0,
		top:59,
		
		bottom:59,
		disableBounce:true,
		//scrollingEnabled:false, //for android till 02-may-17
                scrollingEnabled:true,
		showPagingControl:false,
		backgroundColor:'transparent'
	},
	exchangeRateView : {
		top:18,
		backgroundColor:'#4f4f4f',
	    height:Ti.UI.SIZE,
	    width:Ti.UI.SIZE,
	    layout:'horizontal',
	    horizontalWrap:false
	},
	imgCurrency : {
		image:'/images/rs.png',
		left:0,
	    height:16,
	    width:10
	},
	lblCurrency : {
		left:5,
		width:Ti.UI.SIZE,
	    height:Ti.UI.SIZE,
	    textAlign:'center',
		font:TiFonts.FontStyle('lblBold18'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	imgExchangeArrow : {
		image:'/images/arrow.png',
		right:0,
	    height:59,
	    width:28
	},
	headerBorder : {
		top:58,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eeeeee'
	},
	footerView : {
		bottom:0,
		left:0,
		right:0,
		height:59,
		zIndex:105,
		backgroundColor:'#202020'
	},
	registerView : {
		top:0,
		left:0,
		width:140,
		height:59
	},
	lblWelcome : {
		top:14,
		left:20,
		width:Ti.UI.SIZE,
	    height:11,
	    textAlign:'left',
		font:TiFonts.FontStyle('lblSwissNormal11'),
		color:TiFonts.FontStyle('greyFont'),
	},
	lblName : {
		top:29,
		left:20,
		width:Ti.UI.SIZE,
	    height:16,
	    textAlign:'left',
		font:TiFonts.FontStyle('lblSwissNormal14'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	imgRegister : {
		image:'/images/register.png',
		height:20,
	    width:86
	},
	imgMenu : {
		image:'/images/menu.png',
		height:35,
	    width:40
	},
	loginView : {
		top:0,
		right:0,
		width:140,
		height:59
	},
	customerView : {
		top:14,
		left:10,
		width:Ti.UI.SIZE,
	    height:12,
	    layout:'horizontal',
	    horizontalWrap:false
	},
	lblCustomer : {
		top:0,
		left:0,
		width:Ti.UI.SIZE,
	    height:11,
	    textAlign:'left',
		font:TiFonts.FontStyle('lblNormal10'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	lblCustomerId : {
		top:0,
		left:2,
		width:Ti.UI.SIZE,
	    height:11,
	    textAlign:'left',
		font:TiFonts.FontStyle('lblNormal10'),
		color:TiFonts.FontStyle('redFont'),
	},
	lblLastLogin : {
		top:32,
		left:10,
		width:Ti.UI.SIZE,
	    height:14,
	    textAlign:'left',
		font:TiFonts.FontStyle('lblNormal10'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	imgLogin : {
		image:'/images/login.png',
		height:20,
	    width:65
	},
};
