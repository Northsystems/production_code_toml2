/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	//borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	//top:20,
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont')
};

var optionsView = {
	top:15,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:0,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.Register = {
	winRegister : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	scrollableView : {
		//top:140,
		top:50,
		left:0,
		right:0,
		bottom:0,
		disableBounce:true,
		scrollingEnabled:false,
		showPagingControl:false,
		backgroundColor:'transparent'
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	progressView : {
		top:40,
		height:100,
		left:0,
		right:0,
		backgroundColor:'#202020'
	},
	progressMainView : {
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical'
	},
	lblPercent : {
		top:0,
		width:Ti.UI.SIZE,
		height:Ti.UI.SIZE,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissNormal20'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	lblComplete : {
		top:5,
		width:Ti.UI.SIZE,
		height:Ti.UI.SIZE,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal11'),
		color:'#6f6f6f',
	},
	imgProgress : {
		top:10,
		width:288,
		height:18,
	},
	progressBar : {
		top:50,
		height:50,
		left:20,
		right:20,
		message : 'Profile Completion 0%',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('blackFont'),
		min:0,
		max:100,
		value:0
	},
	
	//social view added on 8 feb by sanjivani

	lblOR : {
		top:5,
		height:35,
		//width:Ti.UI.SIZE,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont')
	},
	lblOR1 : {
		top:5,
	    height:35, 
	   //width:Ti.UI.SIZE,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont')
	},

	socialView : {
		top:15,
		height:35,
		width:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:false
	},
	socialMainView : {
		height:130,
		left:0,
		right:0,
		layout:'vertical'
	},
	/*progressView : {
		top:40,
		height:100,
		left:0,
		right:0,
		backgroundColor:'#202020'
	},
		
		 * property same like progress main view
		 socialView : {
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical'
	},*/
	imgFB : {
		top:0,
		height:35,
		width:35,
		left:0,
		image:'/images/fb.png',
		type:'fb',
	},
	imgGP : {
		top:0,
		height:35,
		width:35,
		left:30,
		image:'/images/gp.png',
		type:'gp',
	},
	imgLI : {
		top:0,
		height:35,
		width:35,
		left:30,
		image:'/images/li.png',
		type:'li',
	},
	
	stepView1 : _.defaults({}, stepView),
	stepView2 : _.defaults({}, stepView),
	stepView3 : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	stepView4 : _.defaults({}, stepView),
	
	//////////////////// Step 1 ///////////////////////
	
	txtFirstName : _.defaults({top:15,right:20}, textField),
	txtLastName : _.defaults({top:15,right:20}, textField),
	txtEmail : _.defaults({top:15,right:20}, textField),
	mobileView : {top:15,left:20,right:20,height:35,layout:'horizontal',horizontalWrap:false},
	lblMobile : _.defaults({top:TiGlobals.osname === 'android' ? -2 : 0,left:0,width:Ti.UI.SIZE}, label),
	txtMobile : _.defaults({top:TiGlobals.osname === 'android' ? 3 : 0,left:2,width:Ti.UI.FILL}, textField),
	dobView : _.defaults({top:15,left:20,right:20}, label),
	lblDOB : _.defaults({top:0,left:0,right:55}, label),
	imgDOB : {top:0,right:0,height:30,width:30,image:'/images/calendar.png'},
	txtDayNo : _.defaults({top:15,right:20}, textField),
	genderView : {
		top:15,
		height:30,
		left:20,
		right:20
	},
	lblGender : {
		left:0,
		width:Ti.UI.SIZE,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont'),
	},
	lblMale : {
		right:45,
		height:27,
		width:27,
		backgroundImage:'/images/male_unsel.png'
	},
	lblFemale : {
		right:0,
		height:27,
		width:27,
		backgroundImage:'/images/female_unsel.png'
	},
	citizenshipView : _.defaults({}, optionsView),
	lblCitizenship : _.defaults({top:0,left:0,right:20}, textField),
	imgCitizenship : _.defaults({right:0}, imgArrow),
	btnStep1 : _.defaults({}, button),
	
	//////////////////// Step 2 ///////////////////////
	
	txtAptNo : _.defaults({top:15,right:20}, textField),
	txtBldgNo : _.defaults({top:15,right:20}, textField),
	txtStreet1 : _.defaults({top:15,right:20}, textField),
	txtStreet2 : _.defaults({top:15,right:20}, textField),
	txtLocality : _.defaults({top:15,right:20}, textField),
	txtSubLocality : _.defaults({top:15,right:20}, textField),
	
	cityView : _.defaults({}, optionsView),
	txtCity : _.defaults({top:15,right:20}, textField),
	imgCity : _.defaults({right:0}, imgArrow),
	
	stateView : _.defaults({}, optionsView),
	lblState : _.defaults({top:0,left:0,right:20}, label),
	imgState : _.defaults({right:0}, imgArrow),
	
	txtZip : _.defaults({top:15,right:20}, textField),
	btnStep2 : _.defaults({}, button),
	
	occupationView : _.defaults({}, optionsView),
	lblOccupation : _.defaults({top:0,left:0,right:20}, label),
	imgOccupation : _.defaults({right:0}, imgArrow),
	
	//////////////////// Step 3 ///////////////////////
	
	lblPasswordSetup : {
		top:15,
		left:20,
		right:20,
		height:16,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	
	lblUsername : {
		top:41,
		left:20,
		right:20,
		height:20,
		textAlign:'left',
		font:TiFonts.FontStyle('lblBold16'),
		color:TiFonts.FontStyle('blackFont'),
	},
	//txtPassword : _.defaults({top:76,right:20,passwordMask:true}, textField),
	txtPassword : _.defaults({top:15,right:20,passwordMask:true}, textField),
	borderViewS31 : {
		//top:107,
		top:47,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	//txtConfirmPassword : _.defaults({top:121,right:20,passwordMask:true}, textField),
	txtConfirmPassword : _.defaults({top:15,right:20,passwordMask:true}, textField),
	borderViewS32 : {
		//top:152,
		top:47,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	lblPasswordValidate : {
		//top:166,
		top:15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont')
	},
	btnStep3 : _.defaults({top:207}, button),
	
	//////////////////// Step 4 ///////////////////////
	
	lblHow : {
		top:15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	
	HDYView : _.defaults({}, optionsView),
	lblHDY : _.defaults({top:0,left:0,right:20}, label),
	imgHDY : _.defaults({right:0}, imgArrow),
	txtHDY : _.defaults({top:15,right:20}, textField),
	
	assuranceView : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE
	},
	imgAssurance : {
		top:0,
		left:0,
		image:'/images/checkbox_unsel.png',
	    width:20,
	    height:20
	},
	lblAssurance : {
		top:0,
		left:30,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	termsView : {
		top:15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:true
	},
	lblTerms : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	lblAgreement : {
		left:0,
		height:15,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('redFont'),
	},
	lblAnd : {
		left:0,
		height:15,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	lblPrivacy : {
		left:0,
		height:15,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('redFont'),
	},
	
	btnRegister : _.defaults({}, button),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	},
//lblEmailMsg added by Sanjivani on 08-June-2017
  lblEmailMsg :{
	top:0,
	left:20,
	color:'RED',
	height:0,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal12')
	//color:TiFonts.FontStyle('blackFont')
	},
	
	//added on 04-July-17 for CMN67
	lblNote :{
	//left:20,
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('blackFont')
}
};
