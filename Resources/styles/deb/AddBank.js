/*global exports,require*/
var _ = require('/utils/Underscore');

var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:5,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:40,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

var imgRadio = {
	image:'/images/radio_unsel.png',
    width:40,
    height:40
};

exports.AddBank = {
	winAddBank : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	scrollableView : {
		top:0,
		left:0,
		right:0,
		bottom:0,
		disableBounce:true,
		scrollingEnabled:false,
		showPagingControl:false,
		backgroundColor:'transparent'
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	lblSectionHead : {
		top:40,
		left:0,
		right:0,
		height:52,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('redFont'),
		backgroundImage:'/images/bg_subtitle.jpg'
	},
	sectionHeaderBorder : {
		top:92,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	
	addBankView : _.defaults({top:93}, stepView),
	lblBankTransaction : {
		top:10,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	
	txtFirstName : _.defaults({top:15,right:20}, textField),
	txtLastName : _.defaults({top:15,right:20}, textField),
	txtNickName : _.defaults({top:15,right:20}, textField),
	txtBankName : _.defaults({top:15,right:20}, textField),
	lblAccountType : _.defaults({top:15,left:20,right:20,height:Ti.UI.SIZE,textAlign:'left',font:TiFonts.FontStyle('lblNormal12')}, label),
	lblAccount : _.defaults({top:5,left:20,height:Ti.UI.SIZE,font:TiFonts.FontStyle('lblNormal14'),}, label),
	txtBLZCode : _.defaults({top:15,right:20}, textField),
	txtAccountNo : _.defaults({top:10,right:20}, textField),
	
	ddView : _.defaults({top:15,height:35}, optionsView),
	lblDD : _.defaults({left:0,right:20}, textField),
	imgDD : _.defaults({right:0}, imgArrow),
	
	termsView : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:true
	},
	imgTerms : {
		top:0,
		left:0,
		image:'/images/checkbox_unsel.png',
	    width:15,
	    height:15
	},
	lblTerms : {
		left:5,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	lblTerms1 : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('redFont'),
	},
	
	btnSubmit : _.defaults({top:20}, button),
	btnCancel : _.defaults({top:10, backgroundColor:'#000'}, button),
	
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};