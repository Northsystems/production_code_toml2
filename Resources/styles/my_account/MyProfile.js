/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:5,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:0,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.MyProfile = {
	winMyProfile : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	imgBack : {
		left:0,
		image:'/images/back.png',
	    width:35,
	    height:40
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	scrollableView : {
		top:80,
		left:0,
		right:0,
		bottom:0,
		disableBounce:true,
		scrollingEnabled:true,
		showPagingControl:false,
		backgroundColor:'transparent'
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	tabView : {
		top:40,
		height:40,
		left:0,
		right:0
	},
	tabSelView : {
		top:0,
		left:0,
		height:2,
		width:'50%',
		zIndex:10,
		backgroundColor:'#FF243A',
	},
	lblMyProfile : {
		left:0,
		height:40,
		width:'50%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold14'),
		color:TiFonts.FontStyle('whiteFont'),
		backgroundColor:'#6F6F6F',
		sel:'profile'
	},
	lblMyDocuments : {
		right:0,
		height:40,
		width:'50%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold14'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'#E9E9E9',
		sel:'documents'
	},
	tabSelIconView : {
		top:80,
		left:0,
		height:7,
		width:'50%',
		zIndex:10
	},
	imgTabSel : {
		image:'/images/tab_sel.png',
		top:0,
		height:7,
		width:15
	},
	
	stepView1 : _.defaults({}, stepView),
	stepView2 : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	
	//////////////////// My Profile ///////////////////////
	
	txtFirstName : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtLastName : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtEmail : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtAddress : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtCity : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtState : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtZip : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	txtCountry : _.defaults({top:15,right:20,color:TiFonts.FontStyle('greyFont')}, textField),
	
	mobileView : {top:15,left:20,right:20,height:35,layout:'horizontal',horizontalWrap:false},
	lblMobile : _.defaults({height:Ti.UI.SIZE,left:0,width:Ti.UI.SIZE}, label),
	imgAlternateNo : _.defaults({left:2}, imgArrow),
	txtMobile : _.defaults({top:TiGlobals.osname === 'android' ? 3 : 0,left:2,width:Ti.UI.FILL}, textField),
	
	dobView : _.defaults({top:15,left:20,right:20}, label),
	lblDOB : _.defaults({top:0,left:0,right:55}, label),
	imgDOB : {top:0,right:0,height:30,width:30,image:'/images/calendar.png'},
	
	txtDayNo : _.defaults({top:15,right:20}, textField),
	lblChangePassword : _.defaults({top:15,height:Ti.UI.SIZE,width:Ti.UI.SIZE}, label),
	borderViewChangePassword : {top:5,height:1,width:Ti.UI.SIZE,backgroundColor:'#000'},
	btnSave : _.defaults({}, button),
	
	//////////////////// My Documents ///////////////////////
	
	tableView : {
		top:0,
		left:0,
		height:Ti.UI.SIZE,
		right:0,
		scrollable:true,
		showVerticalScrollIndicator:true,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent',
		footerTitle:TiGlobals.osname === 'android' ? null : '' // Fix for empty rows in iOS
	},
	
	btnSubmit : _.defaults({}, button),
	btnBrowse : _.defaults({}, button),
	
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};
