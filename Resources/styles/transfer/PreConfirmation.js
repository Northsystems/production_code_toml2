/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:20,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.PreConfirmation = {
	winPreConfirmation : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	mainView : {
		top:40,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	
	lblAddressHeader : _.defaults({top:15,height:Ti.UI.SIZE,left:20,right:20,color:'#ed1b24'}, label),
	lblAddress : _.defaults({top:5,height:Ti.UI.SIZE,left:20,right:20,font:TiFonts.FontStyle('lblNormal12')}, label),
	
	lblDateHeader : _.defaults({top:15,height:Ti.UI.SIZE,left:20,right:20,color:'#ed1b24'}, label),
	lblDate : _.defaults({top:5,height:Ti.UI.SIZE,left:20,right:20,font:TiFonts.FontStyle('lblNormal12')}, label),
	
	paymentView : _.defaults({top:0,height:40,left:0,right:0,backgroundColor:'#303030'}, optionsView),//Done on 25th sept for query raised
	lblPaymentHeader : _.defaults({height:Ti.UI.SIZE,left:20,width:Ti.UI.SIZE,color:TiFonts.FontStyle('whiteFont')}, label),
	
	actionView : _.defaults({top:0,height:40,right:20,left:null,width:Ti.UI.SIZE,layout:'horizontal',horizontalWrap:false}, optionsView),
	lblAction : _.defaults({top:0,height:40,width:Ti.UI.SIZE,left:5,font:TiFonts.FontStyle('lblNormal12'),color:TiFonts.FontStyle('greyFont')}, label),
	
	tableView : {
		top:0,
		left:0,
		right:0,
		height:Ti.UI.SIZE,
		scrollable:false,
		showVerticalScrollIndicator:false,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent'
	},
	
	convertedView : _.defaults({top:0,height:40,left:0,right:0,backgroundColor:'#6f6f6f'}, optionsView),
	lblAmountConvertedHeader : _.defaults({height:Ti.UI.SIZE,left:20,width:Ti.UI.SIZE,color:TiFonts.FontStyle('greyFont')}, label),
	lblAmountConverted : _.defaults({height:Ti.UI.SIZE,right:20,width:Ti.UI.SIZE,color:TiFonts.FontStyle('whiteFont')}, label),
	
	summaryView : _.defaults({top:0,height:Ti.UI.SIZE,left:0,right:0,backgroundColor:'#fff',layout:'vertical'}, optionsView),
	lblSummary : _.defaults({top:10,height:Ti.UI.SIZE,width:Ti.UI.SIZE}, label),
	lblSummaryReceipt : _.defaults({top:5,bottom:10,height:Ti.UI.SIZE,width:Ti.UI.SIZE,font:TiFonts.FontStyle('lblNormal10'),color:TiFonts.FontStyle('greyFont')}, label),
	
	indicatorView : _.defaults({top:0,height:7,left:0,right:0,backgroundColor:'#303030'}, optionsView),
	imgIndicator : {top:0,height:7,width:15,image:'/images/tab_white_sel.png'},
	
	infoTextView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		backgroundColor:'#e9e9e9'
	},
	imgInfo: {
		top:10,
		height:30,
		width:30,
		image:'/images/info_icon.png'
	},
	lblInfo : {
		top:0,
		left:20,
		right:20,
		bottom:20,
		height:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont')
	},
	//added on 30 June 17
	lblInfo1 : {
		top:50,
		left:20,
		right:20,
		bottom:20,
		height:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont')
	},
	
	lblInfo2 : {
		top:0,
		left:05,
		right:20,
		bottom:20,
		height:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold14'),
		color:TiFonts.FontStyle('blackFont')
	},
	//
	termsView : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:true
	},
	imgTerms : {
		top:0,
		left:0,
		image:'/images/checkbox_unsel.png',
	    width:15,
	    height:15
	},
	lblTerms : {
		left:5,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	lblTerms1 : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('redFont'),
	},
	
	btnSubmit : _.defaults({}, button),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};
