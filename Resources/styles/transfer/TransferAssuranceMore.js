/*global exports,require*/
var _ = require('/utils/Underscore');

exports.TransferAssuranceMore = {
	winTransferAssuranceMore : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF'
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	lblSectionHead : {
		top:40,
		left:0,
		right:0,
		height:52,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold14'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundImage:'/images/bg_subtitle.jpg'
	},
	sectionHeaderBorder : {
		top:92,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	webView : {
		top:93,
		left:10,
		right:10,
		bottom:0,
		enableZoomControls:false,
		scalesPageToFit:true,
		disableBounce:true
	}
};