/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont')
};

var optionsView = {
	top:15,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:0,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};


	

exports.LimitEnhancement = {
	ButtonView : {
		top:15,
		height:40,
		bottom:5,    //added for submit & cancel button
		width:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:false
	},
	 SubBtn : {
	top:0,
	//left:20,
	width:110,
	height:40,
	
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
},
 CancelBtn : {
	top:0,
	left:20,
	width:110,
	height:40,
	left:20,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
},

	
	winLimitEnhancement : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	scrollableView : {
		//top:133,
		top:55,
		left:0,
		right:0,
		bottom:0,
		disableBounce:true,
		scrollingEnabled:false,
		showPagingControl:false,
		backgroundColor:'transparent'
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	
	lblHeader1 : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('redFont'),
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	tabView : {
		top:40,
		height:40,
		left:0,
		right:0,
		backgroundColor:'#d1d1d1',
		layout:'horizontal',
		horizontalWrap:false
	},
	tabSelView : {
		top:40,
		left:0,
		height:2,
		width:'25%',
		zIndex:10,
		backgroundColor:'#FF243A',
		zIndex:2
	},
	lblStep : {
		left:0,
		height:38,
		width:'25%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('whiteFont'),
		backgroundColor:'#6F6F6F'
	},
	tabSelIconView : {
		top:79,
		left:0,
		height:7,
		width:'25%',
		zIndex:10
	},
	imgTabSel : {
		image:'/images/tab_sel.png',
		top:0,
		height:7,
		width:15,
		zIndex:10
	},
	lblSectionHead : {
		top:80,
		left:0,
		right:0,
		height:52,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('redFont'),
		backgroundImage:'/images/bg_subtitle.jpg'
	},
	sectionHeaderBorder : {
		top:132,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	stepView1 : _.defaults({}, stepView),
	stepView2 : _.defaults({}, stepView),
	stepView3 : _.defaults({}, stepView),
	stepView4 : _.defaults({}, stepView),
	
	//////////////// Instructions View ///////////////
	
	instructionView : _.defaults({top:40}, stepView),
	
	lblInstructions : _.defaults({top:15,height:Ti.UI.SIZE,left:20,right:20,color:'#ed1b24'}, label),
	lblNote : _.defaults({top:20,left:20,height:Ti.UI.SIZE,width:Ti.UI.SIZE,font:TiFonts.FontStyle('lblBold12')}, label),
	lblPt : _.defaults({top:10,left:20,height:Ti.UI.SIZE,right:20,font:TiFonts.FontStyle('lblNormal12')}, label),
	
	noteView : {top:20,height:Ti.UI.SIZE,left:0,right:0,layout:'vertical',backgroundColor:'#f0f0f0'},
	
	lblStateHead : _.defaults({top:15,left:20,right:20,height:Ti.UI.SIZE,font:TiFonts.FontStyle('lblBold14')}, textField),
	lblState : _.defaults({top:5,left:20,right:20,height:Ti.UI.SIZE}, label),
	txtField : _.defaults({top:15,right:20}, textField),
	maritalView : _.defaults({}, optionsView),
	lblMarital : _.defaults({top:0,left:0,right:20}, textField),
	imgMarital : _.defaults({right:0}, imgArrow),
	
	phoneView : {top:0,left:20,right:20,height:35,layout:'horizontal',horizontalWrap:false},
	txtCC : {
		top:TiGlobals.osname === 'android' ? 3 : 0,
		left:0,
		width:80,
		height:35,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'transparent',
		borderColor:'transparent',
		borderWidth:1,
		paddingLeft:0,
		paddingRight:0,
		autoComplete:false,
		autocorrect:false,
		autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
		
	},
	txtAC : {
		top:TiGlobals.osname === 'android' ? 3 : 0,
		left:5,
		width:60,
		height:35,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'transparent',
		borderColor:'transparent',
		borderWidth:1,
		paddingLeft:0,
		paddingRight:0,
		autoComplete:false,
		autocorrect:false,
		autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
		
	},
	txtNo : {
		top:TiGlobals.osname === 'android' ? 3 : 0,
		left:5,
		width:Ti.UI.FILL,
		height:35,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'transparent',
		borderColor:'transparent',
		borderWidth:1,
		paddingLeft:0,
		paddingRight:0,
		autoComplete:false,
		autocorrect:false,
		autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
	},
	
	phoneBorderView : {top:0,left:20,right:20,height:1,layout:'horizontal',horizontalWrap:false},
	borderCC : {
		top:0,
		left:0,
		width:80,
		height:1,
		backgroundColor:'#000'
	},
	borderAC : {
		top:0,
		left:5,
		width:60,
		height:1,
		backgroundColor:'#000'
	},
	borderNo : {
		top:0,
		left:5,
		width:Ti.UI.FILL,
		height:1,
		backgroundColor:'#000'
	},
	
	ssnTxtView : {top:0,left:20,right:20,height:36},
	txtSSN1 : _.defaults({top:0,height:35,left:0,width:'25%',paddingLeft:0,paddingRight:0}, textField),
	borderSSN1 : {top:35,left:0,width:'25%',height:1,backgroundColor:'#000'},
	
	txtSSN2 : _.defaults({top:0,height:35,left:null,right:null,width:'25%',paddingLeft:0,paddingRight:0}, textField),
	borderSSN2 : {top:35,left:null,right:null,width:'25%',height:1,backgroundColor:'#000'},
	
	txtSSN3 : _.defaults({top:0,height:35,left:null,right:0,width:'25%',paddingLeft:0,paddingRight:0}, textField),
	borderSSN3 : {top:35,right:0,width:'25%',height:1,backgroundColor:'#000'},
	
	ddView : _.defaults({}, optionsView),
	lblDD : _.defaults({top:0,left:0,right:20}, textField),
	imgDD : _.defaults({right:0}, imgArrow),
	
	
	lblKYCTxt1 : {
		top: 15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	lblKYCNote : {
		top: 5,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('blackFont'),
	},
	dobView : _.defaults({top:20,left:20,right:20}, label),
	lblDOB : _.defaults({top:0,left:0,right:55}, label),
	imgDOB : {top:0,right:0,height:30,width:30,image:'/images/calendar.png'},
	
	ddTimeView : _.defaults({top:15,left:20,right:20,height:35}, label),
	ddTimeView1 : _.defaults({right:null,left:0,width:'40%',top:0}, optionsView),
	ddTimeView2 : _.defaults({left:null,right:0,width:'40%',top:0}, optionsView),
	lblTimeOR : _.defaults({height:35}, label),
	
	ddBorderTimeView : _.defaults({top:0,left:20,right:20,height:1}, label),
	borderTimeView1 : {
		top:0,
		left:0,
		width:'40%',
		height:1,
		backgroundColor:'#000'
	},
	borderTimeView2 : {
		top:0,
		right:0,
		width:'40%',
		height:1,
		backgroundColor:'#000'
	},
	
	noteView : {
		top:10,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		backgroundColor:'#e9e9e9',
		layout:'vertical'
	},
	lblNoteTxt : {
		top:10,
		left:20,
		right:20,
		bottom:20,
		height:Ti.UI.SIZE,
    	font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont')
	},
	//btnStep1 : _.defaults({}, button1), 
	btnStep : _.defaults({}, button),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};
