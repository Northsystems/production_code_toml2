var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:15,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	//top:40,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	//top:15,
	left:20,
	right:20,
	height:40,
	//bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};
exports.updateCDOB = {
	winCDOB : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:20,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		top:25,
		height:Ti.UI.SIZE,
    	width:Ti.UI.SIZE,
    	left:20,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('blackFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:15,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#a3a3a3'//'#eee'
	},
	
    CDOBView : _.defaults({}, stepView),
    
    lblNote : _.defaults({top:0,left:20,right:20}, label),

	
	dobView : _.defaults({top:15,left:20,right:20}, label),
	imgDOB : {top:0,right:0,height:30,width:30,image:'/images/calendar.png'},
	
	
	//btnSubmit : _.defaults({}, button),
	btnCancel : _.defaults({top:20, 
		backgroundColor:'#000'}, button),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},

	suBbutton :{
	top:20,
	left:20,
	right:20,
	height:40,
	//bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
},
//added by sanjivani on 09-may-17
done : {
		title:'Done',
		height:35,
		width:50
},

countryView : _.defaults({top:10}, optionsView),
lblCountry : _.defaults({top:0,left:0,right:20}, textField),
imgCountry : _.defaults({right:0}, imgArrow),
	
};
